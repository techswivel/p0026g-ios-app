//
//  NotificationExtension.swift
//  Base iOS Project
//
//  Created by Mac on 13/01/2021.
//  Copyright © 2021 TechSwivel. All rights reserved.
//

import Foundation

extension Notification.Name{
    static let errorObserver = Notification.Name("ErrorObserver")
    static let completeObserver = Notification.Name("CompleteObserver")
    //Logout Observer
    static let logoutObserver = Notification.Name("logoutObserver")
    static let errorLogoutObserver = Notification.Name("errorLogoutObserver")
    ///SignUp Observers
    static let signUpNotificationObserver = Notification.Name("signUpNotificationObserver")
    static let socialSignUpObserver = Notification.Name("socialSignUpObserver")
    ///SignIn Observers
    static let signInObserver = Notification.Name("signInObserver")
    static let socialSignInObserver = Notification.Name("socialSignInObserver")
   ///Forget Password Observer
    static let forgetPasswordObserver = Notification.Name("forgetPasswordObserver")
    static let resetPasswordObserver = Notification.Name("resetPasswordObserver")
}
