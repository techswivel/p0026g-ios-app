//
//  Extentions.swift
//  Base iOS Project
//
//  Created by Mac on 13/01/2021.
//  Copyright © 2021 TechSwivel. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

// MARK: - UI Segment Control Extension for Title and Color configuration
extension UISegmentedControl {

    func setTitleColor(_ color: UIColor, state: UIControl.State = .normal) {
        var attributes = self.titleTextAttributes(for: state) ?? [:]
        attributes[.foregroundColor] = color
        self.setTitleTextAttributes(attributes, for: state)
    }
    
    func setTitleFont(_ font: UIFont, state: UIControl.State = .normal) {
        var attributes = self.titleTextAttributes(for: state) ?? [:]
        attributes[.font] = font
        self.setTitleTextAttributes(attributes, for: state)
    }

}
//MARK: - UiText Input Selected Range for textView selected text Extension
extension UITextInput {
    var selectedRange: NSRange? {
        guard let range = selectedTextRange else { return nil }
        let location = offset(from: beginningOfDocument, to: range.start)
        let length = offset(from: range.start, to: range.end)
        return NSRange(location: location, length: length)
    }
    
   
}
//Extension to strip out html tags from attributed string and covert it into string
extension String {

    func removeHtmlTags() -> String? {
        do {
            guard let data = self.data(using: .unicode) else {
                return nil
            }
            let attributed = try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
            return attributed.string
        } catch {
            return nil
        }
    }
    
     mutating func removePunctuationIfInStart()->String{
        let trimmedstr = self.trimmingCharacters(in: .whitespacesAndNewlines)

        if let firstChar = trimmedstr.first {
            if [","].contains(firstChar) {
                
                let newStr = String(trimmedstr.trimmingCharacters(in: .punctuationCharacters) )
               self = newStr
                
            }
        }
        self =  self.trimmingCharacters(in: .whitespacesAndNewlines)
         return self
    }
}
//MARK: - String Protocol Range Extension
extension StringProtocol {
   
    func ranges(of targetString: Self, options: String.CompareOptions = [], locale: Locale? = nil) -> [Range<String.Index>] {

                let result: [Range<String.Index>] = self.indices.compactMap { startIndex in
                    let targetStringEndIndex = index(startIndex, offsetBy: targetString.count, limitedBy: endIndex) ?? endIndex
                    return range(of: targetString, options: options, range: startIndex..<targetStringEndIndex, locale: locale)
                }
                return result
            }
}

extension UIStoryboard {
    static let someName = UIStoryboard(name: "someName", bundle: nil)
}

//MARK: - UITextField Text Padding
extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
    
    func setIcon(_ image: UIImage) {
       let iconView = UIImageView(frame:
                      CGRect(x: 15, y: 5, width: 22, height: 20))
       iconView.image = image
       let iconContainerView: UIView = UIView(frame:
                      CGRect(x: 10, y: 0, width: 50, height: 30))
       iconContainerView.addSubview(iconView)
       leftView = iconContainerView
       leftViewMode = .always
    }
    


        
    
}

//MARK: - UiColor RGB
extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
}



//MARK: - Validation
extension String {
    
    func isValidEmail() -> Bool {
        // here, `try!` will always succeed because the pattern is valid
        let regex = try! NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
        
    }
    
    func isValidPassword() -> Bool {
        ///Atleast One Alphabet, One lower and One Number
        let regularExpression = "^(?=.*[a-z])(?=.*[!@_*#$%^&+=])(?=.*[0-9])(?=.*[A-Z]).{4,}$"
          let passwordValidation = NSPredicate.init(format: "SELF MATCHES %@", regularExpression)

          return passwordValidation.evaluate(with: self)
      }
    
    func isValidName() -> Bool {
        
        let range = NSRange(location: 0, length: self.utf8.count)
        let regex = try! NSRegularExpression(pattern: ".*[^A-Za-z ].*")
        if regex.firstMatch(in: self, options: [], range: range) != nil {
            return false
        }
        else {
            return true
        }
        
    }
    
    func isValidCard() -> Bool {
        
        let range = NSRange(location: 0, length: self.utf16.count)
        let regex = try! NSRegularExpression(pattern: "^[0-9]{14,20}$")
        if regex.firstMatch(in: self, options: [], range: range) != nil {
            return true
        }
        else {
            return false
        }
        
    }
    
    var isBackspace: Bool {
        let char = self.cString(using: String.Encoding.utf8)!
        return strcmp(char, "\\b") == -92
    }
    
    
    var isPhoneNumber: Bool {
        do {
            let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
            let matches = detector.matches(in: self, options: [], range: NSMakeRange(0, self.count))
            if let res = matches.first {
                return res.resultType == .phoneNumber && res.range.location == 0 && res.range.length == self.count
            } else {
                return false
            }
        } catch {
            return false
        }
    }
    
}



//MARK: - DeviceName Extension
public extension UIDevice {

    static let modelName: String = {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }

        func mapToDevice(identifier: String) -> String { // swiftlint:disable:this cyclomatic_complexity
            #if os(iOS)
            switch identifier {
            case "iPod5,1":                                 return "iPod touch (5th generation)"
            case "iPod7,1":                                 return "iPod touch (6th generation)"
            case "iPod9,1":                                 return "iPod touch (7th generation)"
            case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
            case "iPhone4,1":                               return "iPhone 4s"
            case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
            case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
            case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
            case "iPhone7,2":                               return "iPhone 6"
            case "iPhone7,1":                               return "iPhone 6 Plus"
            case "iPhone8,1":                               return "iPhone 6s"
            case "iPhone8,2":                               return "iPhone 6s Plus"
            case "iPhone8,4":                               return "iPhone SE"
            case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
            case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
            case "iPhone10,1", "iPhone10,4":                return "iPhone 8"
            case "iPhone10,2", "iPhone10,5":                return "iPhone 8 Plus"
            case "iPhone10,3", "iPhone10,6":                return "iPhone X"
            case "iPhone11,2":                              return "iPhone XS"
            case "iPhone11,4", "iPhone11,6":                return "iPhone XS Max"
            case "iPhone11,8":                              return "iPhone XR"
            case "iPhone12,1":                              return "iPhone 11"
            case "iPhone12,3":                              return "iPhone 11 Pro"
            case "iPhone12,5":                              return "iPhone 11 Pro Max"
            case "iPhone12,8":                              return "iPhone SE (2nd generation)"
            case "iPhone13,1":                              return "iPhone 12 mini"
            case "iPhone13,2":                              return "iPhone 12"
            case "iPhone13,3":                              return "iPhone 12 Pro"
            case "iPhone13,4":                              return "iPhone 12 Pro Max"
            case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
            case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad (3rd generation)"
            case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad (4th generation)"
            case "iPad6,11", "iPad6,12":                    return "iPad (5th generation)"
            case "iPad7,5", "iPad7,6":                      return "iPad (6th generation)"
            case "iPad7,11", "iPad7,12":                    return "iPad (7th generation)"
            case "iPad11,6", "iPad11,7":                    return "iPad (8th generation)"
            case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
            case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
            case "iPad11,3", "iPad11,4":                    return "iPad Air (3rd generation)"
            case "iPad13,1", "iPad13,2":                    return "iPad Air (4th generation)"
            case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad mini"
            case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad mini 2"
            case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad mini 3"
            case "iPad5,1", "iPad5,2":                      return "iPad mini 4"
            case "iPad11,1", "iPad11,2":                    return "iPad mini (5th generation)"
            case "iPad6,3", "iPad6,4":                      return "iPad Pro (9.7-inch)"
            case "iPad7,3", "iPad7,4":                      return "iPad Pro (10.5-inch)"
            case "iPad8,1", "iPad8,2", "iPad8,3", "iPad8,4":return "iPad Pro (11-inch) (1st generation)"
            case "iPad8,9", "iPad8,10":                     return "iPad Pro (11-inch) (2nd generation)"
            case "iPad6,7", "iPad6,8":                      return "iPad Pro (12.9-inch) (1st generation)"
            case "iPad7,1", "iPad7,2":                      return "iPad Pro (12.9-inch) (2nd generation)"
            case "iPad8,5", "iPad8,6", "iPad8,7", "iPad8,8":return "iPad Pro (12.9-inch) (3rd generation)"
            case "iPad8,11", "iPad8,12":                    return "iPad Pro (12.9-inch) (4th generation)"
            case "AppleTV5,3":                              return "Apple TV"
            case "AppleTV6,2":                              return "Apple TV 4K"
            case "AudioAccessory1,1":                       return "HomePod"
            case "AudioAccessory5,1":                       return "HomePod mini"
            case "i386", "x86_64":                          return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "iOS"))"
            default:                                        return identifier
            }
            #elseif os(tvOS)
            switch identifier {
            case "AppleTV5,3": return "Apple TV 4"
            case "AppleTV6,2": return "Apple TV 4K"
            case "i386", "x86_64": return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "tvOS"))"
            default: return identifier
            }
            #endif
        }

        return mapToDevice(identifier: identifier)
    }()
}

extension UIView {

    func addPrimaryBorder(){
        self.layer.cornerRadius = 4
        self.layer.borderWidth = 0.5
        self.layer.borderColor = ColorConstrants.PRIMARY_COLOR.cgColor
    }
    func roundCorner(radius:Float) {
        self.layer.cornerRadius = CGFloat(radius)
            
    }
   
}

extension UIButton {
    func addBtnCornerRadius(){
        self.layer.cornerRadius = 10
    }
}


extension UIViewController {
    
    func showAlert(message: String?, completion: (() -> Void)? = nil ) {
            let alert = UIAlertController(title: message, message: nil, preferredStyle: .alert)
            let okAction = UIAlertAction(title: StringConstant.AlertOkAction, style: .default) { _ in
                if let complete = completion {
                    complete()
                }
            }
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        }
    
    func showYesNoAlert (title: String?, message: String, completion:@escaping () -> Void) {
        let customAlert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let yesAction = UIAlertAction(title: StringConstant.AlertButtonYes, style: UIAlertAction.Style.default) { _ in
            completion()
        }
        let noAction = UIAlertAction(title: StringConstant.AlertButtonNo, style: UIAlertAction.Style.default) { _ in
            // Do Nothing
        }
        customAlert.view.tintColor = #colorLiteral(red: 0.09803921569, green: 0.6352941176, blue: 1, alpha: 1)
        
        customAlert.addAction(noAction)
        customAlert.addAction(yesAction)
        self.present(customAlert, animated: true, completion: nil)
    }
}

extension UIView {
    
    func setTransitionOnView(isHidden: Bool = false, transitionDuration: CFTimeInterval = 0.3, withDuration: CFTimeInterval = 0.3, options: UIView.AnimationOptions = .curveEaseIn, startProgress: Float = 0.0, type: CATransitionType = .push, subType: CATransitionSubtype = .fromTop, timingFunction: CAMediaTimingFunctionName = .linear, layerColor: CGColor = UIColor.clear.cgColor) {
        
        let transition: CATransition = CATransition()
        UIView.animate(withDuration: withDuration, delay: 0.0, options: options, animations: {
            self.isHidden = isHidden
            transition.duration = transitionDuration
            transition.timingFunction = CAMediaTimingFunction(name: timingFunction)
            transition.type = type
            transition.subtype = subType
            transition.startProgress = startProgress
            self.layer.backgroundColor = layerColor
            
            self.layer.add(transition, forKey: kCATransition)
        }) // { (_) in }
    }
}
private let imageOptions : SDWebImageOptions = SDWebImageOptions(rawValue: SDWebImageOptions.retryFailed.rawValue | SDWebImageOptions.progressiveLoad.rawValue | SDWebImageOptions.continueInBackground.rawValue |
       SDWebImageOptions.allowInvalidSSLCertificates.rawValue)
        
extension UIImageView {
  
    func blurImageViewWithTransparentBlackColor(){
       
        
        let darkBlur = UIBlurEffect(style: .dark)
        let blurView = UIVisualEffectView(effect: darkBlur)
            blurView.frame = self.bounds
        blurView.alpha = 1
        blurView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        self.addSubview(blurView)
       
       
        
        
    }
    
    func circleImageView() {
        let radius = self.frame.width / 2
              self.layer.cornerRadius = radius
              self.layer.masksToBounds = true
    }
    func roundCornerImageView() {
              self.layer.cornerRadius = 5
            
    }
    
    func loadImage(imageUrl:String,placeHolderName: String,activiyIndicator: UIActivityIndicatorView)  {
        activiyIndicator.isHidden = false
        activiyIndicator.startAnimating()
        self.sd_setImage(with: URL(string : imageUrl), placeholderImage: UIImage(named: placeHolderName), options: imageOptions) { (image, error, type, url) in
            if error != nil {
                self.image = UIImage(named: placeHolderName)
                activiyIndicator.isHidden = true
                activiyIndicator.stopAnimating()
                return
            }
            activiyIndicator.isHidden = true
            activiyIndicator.stopAnimating()
        }
    }
}
extension String {
    func strikeThrough() -> NSAttributedString {
        let attributeString =  NSMutableAttributedString(string: self)
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: NSUnderlineStyle.single.rawValue, range: NSMakeRange(0,attributeString.length))
        return attributeString
    }
}



/*
 This is reference link from where they are copied, in future we can debug from refernce link :
 https://medium.com/@balzsvincze/5-swift-extensions-to-make-your-life-easier-1accb384cbac
 */

//MARK: - Padding UI Button
extension CGSize {
    func addingPadding(width: CGFloat, height: CGFloat) -> CGSize {
        CGSize(width: self.width + width, height: self.height + height)
    }
}

/*
 // Usage:
 class PaddedButton: UIButton {
     override var intrinsicContentSize: CGSize {
         super.intrinsicContentSize.addingPadding(width: 60, height: 20)
     }
 }

*/


//MARK: - Highlight Parts of String
extension NSAttributedString {
    func highlighting(_ substring: String, using color: UIColor) -> NSAttributedString {
        let attributedString = NSMutableAttributedString(attributedString: self)
        attributedString.addAttribute(.foregroundColor, value: color, range: (self.string as NSString).range(of: substring))
        return attributedString
    }
}
/*
// Usage:
label.attributedText = NSAttributedString(string: "Budapest")
label.attributedText = label.attributedText?.highlighting("Bud", using: .blue)
*/
// MARK: - UILabelExtension
extension UILabel {
    func calculateMaxLines() -> Int {
        let maxSize = CGSize(width: frame.size.width, height: CGFloat(Float.infinity))
        let charSize = font.lineHeight
        let text = (self.text ?? "") as NSString
        let textSize = text.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        let linesRoundedUp = Int(ceil(textSize.height/charSize))
        return linesRoundedUp
    }
}

//MARK: - Finding superviews with a given type
extension UIView {
    func superview<T>(of type: T.Type) -> T? {
        return superview as? T ?? superview.flatMap { $0.superview(of: type) }
    }
    func addGradientBackground(){
        let gradient = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.masksToBounds = true
        gradient.colors = [UIColor.black.cgColor,UIColor.black.cgColor, ColorConstrants.BACKGROUND_GRADIENT_COLOR.cgColor,ColorConstrants.BACKGROUND_GRADIENT_COLOR.cgColor]
        
        
        self.layer.insertSublayer(gradient, at: 0)
    }
    
}
/*
/ Example usage:
func textFieldDidBeginEditing(_ textField: UITextField) {
    // Get the cell containing the textfield.
    if let cell = textField.superview(of: TextFieldTableViewCell.self) {
        cell.toggle(isHighlighted: true)
    }
}
*/
//MARK: - Embedding a View Controller inside another View Controller, a.k.a. container views

extension UIViewController {
    func embed(_ vc: UIViewController, in _containerView: UIView? = nil) {
        let containerView: UIView = _containerView ?? view // Use the whole view if no container view is provided.
        containerView.addSubview(vc.view)
        NSLayoutConstraint.activate([
            vc.view.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 0),
            vc.view.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: 0),
            vc.view.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 0),
            vc.view.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: 0)
        ])
        vc.view.translatesAutoresizingMaskIntoConstraints = false
        addChild(vc)
        vc.didMove(toParent: self)
    }
}


//MARK: - UI Switch Extension
extension UISwitch {
// Set size and Width of UiSwitch button
    func set(width: CGFloat, height: CGFloat) {

        let standardHeight: CGFloat = 31
        let standardWidth: CGFloat = 51

        let heightRatio = height / standardHeight
        let widthRatio = width / standardWidth

        transform = CGAffineTransform(scaleX: widthRatio, y: heightRatio)
    }
}
