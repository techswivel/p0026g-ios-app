//
//  Validation.swift
//  Base iOS Project
//
//  Created by Hammad Hassan on 09/02/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import Foundation

//MARK: - These Enum are used For Validation

//MARK: - Enum for failure and success results
enum Alert {
    case success
    case failure
    case error
}
//MARK: - Enum for success or failure of validation with alert message
enum Valid {
    case success
    case failure(Alert, AlertMessages)
}

//MARK: - Enum For Validation Type
enum ValidationType {
    case email
    case stringWithFirstLetterCaps
    case phoneNo
    case alphabeticString
    case password
}
enum RegEx: String {
    case email = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}" // Email
    case password = "^.{6,15}$" // Password length 6-15
    case alphabeticStringWithSpace = "^[a-zA-Z ]*$" // e.g. hello sandeep
    case alphabeticStringFirstLetterCaps = "^[A-Z]+[a-zA-Z]*$" // SandsHell
    case phoneNo = "[0-9]{10,14}" // PhoneNo 10-14 Digits        //Change RegEx according to your Requirement
}

//MARK: - Validation Alert Message Enum
enum AlertMessages {
    case inValidEmail
     case invalidFirstLetterCaps
     case inValidPhone
    case invalidAlphabeticString
     case inValidPSW
     case emptyPhone
     case emptyEmail
     case emptyFirstLetterCaps
     case emptyAlphabeticString
     case emptyPSW
    
    var value : String{
        switch self {
        
        case .inValidEmail:
            return StringConstant.inValidEmail
        case .invalidFirstLetterCaps:
            return StringConstant.invalidFirstLetterCaps
        case .inValidPhone:
            return StringConstant.invalidPhone
        case .invalidAlphabeticString:
            return StringConstant.invalidAlphabeticString
        case .inValidPSW:
            return StringConstant.inValidPSW
        case .emptyPhone:
            return StringConstant.emptyPhone
        case .emptyEmail:
            return StringConstant.emptyEmail
        case .emptyFirstLetterCaps:
            return StringConstant.emptyFirstLetterCaps
        case .emptyAlphabeticString:
            return StringConstant.emptyAlphabeticString
        case .emptyPSW:
            return StringConstant.emptyPSW
        }
    }
}

//MARK: - Validatin Class
class Validation: NSObject {
    
    public static let shared = Validation()
    
    func validate(values: (type: ValidationType, inputValue: String)...) -> Valid {
        for valueToBeChecked in values {
            switch valueToBeChecked.type {
            case .email:
                if let tempValue = isValidString((valueToBeChecked.inputValue, .email, .emptyEmail, .inValidEmail)) {
                    return tempValue
                }
            case .stringWithFirstLetterCaps:
                if let tempValue = isValidString((valueToBeChecked.inputValue, .alphabeticStringFirstLetterCaps, .emptyFirstLetterCaps, .invalidFirstLetterCaps)) {
                    return tempValue
                }
            case .phoneNo:
                if let tempValue = isValidString((valueToBeChecked.inputValue, .phoneNo, .emptyPhone, .inValidPhone)) {
                    return tempValue
                }
            case .alphabeticString:
                if let tempValue = isValidString((valueToBeChecked.inputValue, .alphabeticStringWithSpace, .emptyAlphabeticString, .invalidAlphabeticString)) {
                    return tempValue
                }
            case .password:
                if let tempValue = isValidString((valueToBeChecked.inputValue, .password, .emptyPSW, .inValidPSW)) {
                    return tempValue
                }
            }
        }
        return .success
    }
    
    func isValidString(_ input: (text: String, regex: RegEx, emptyAlert: AlertMessages, invalidAlert: AlertMessages)) -> Valid? {
        if input.text.isEmpty {
            return .failure(.error, input.emptyAlert)
        } else if isValidRegEx(input.text, input.regex) != true {
            return .failure(.error, input.invalidAlert)
        }
        return nil
    }
    
    func isValidRegEx(_ testStr: String, _ regex: RegEx) -> Bool {
        let stringTest = NSPredicate(format:"SELF MATCHES %@", regex.rawValue)
        let result = stringTest.evaluate(with: testStr)
        return result
    }
}
