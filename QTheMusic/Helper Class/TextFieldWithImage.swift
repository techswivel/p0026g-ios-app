//
//  TextFieldWithImage.swift
//  Base iOS Project
//
//  Created by Hammad Hassan on 09/02/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import UIKit

@IBDesignable
class TextFieldWithImage: UITextField {

    @IBInspectable var cornerRadius : CGFloat = 0 {
        didSet{
            layer.cornerRadius = cornerRadius
            layer.borderWidth = 0
            layer.borderColor = UIColor.darkGray.cgColor
         clipsToBounds = true
        }
    }
    
    @IBInspectable var leftPadding : Int = 0 {
        didSet{
            updateView()
        }
    }
    
    @IBInspectable var leftImage : UIImage?{
        didSet{
            updateView()
        }
    }
    
    
    func updateView(){
        if let image = leftImage{
            leftViewMode = .always
            
            var width = leftPadding + 15
            
            if borderStyle == UITextField.BorderStyle.line || borderStyle == UITextField.BorderStyle.none{
                width = width + 5
            }
            let imageView = UIImageView(frame: CGRect(x: leftPadding, y: 0, width: 15, height: 15))
            imageView.image = image
            imageView.tintColor = tintColor
            
            
            let view = UIView(frame: CGRect(x: 0, y: 0, width: width, height: 15))
            view.addSubview(imageView)
            
            leftView = view
            
            
            attributedPlaceholder = NSAttributedString(string: placeholder != nil ? placeholder! : "" , attributes: [NSAttributedString.Key.foregroundColor : tintColor!])
            
        }else{
            leftViewMode = .never
        }
    }
}
