//
//  LandScapeManager.swift
//  Base iOS Project
//
//  Created by Hammad Hassan on 09/02/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import Foundation

class LandscapeManager {
    static let shared = LandscapeManager()
    
    var isFirstLaunch: Bool {
        get {
            !UserDefaultUtils.getBool(key: #function)
            
        }
        set {
            UserDefaultUtils.setBool(value: newValue, key: #function)
           
        }
    }
    
    var isFirstFetch : Bool {
        get{
            !UserDefaultUtils.getBool(key: #function)
        }
        set{
            UserDefaultUtils.setBool(value: newValue, key: #function)
        }
    }
    
}
