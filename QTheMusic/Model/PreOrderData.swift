//
//  PreOrderData.swift
//  QtheMusic
//
//  Created by Assad Khan on 12/05/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import Foundation
// MARK: - PreOrderDatum
class PreOrderData: Codable {
    var preOrderID, releaseDate: Int?
    var preOrderTitle, preOrderCoverImageURL: String?
    var preOrderedCount, totalPreOrders: Int?
    var price: Double?
    var preOrderType: String?
    var isPurchesed: Bool?
    var totalnumberOfSongs: Int?
    var sku: String?
    enum CodingKeys: String, CodingKey {
        case preOrderID = "preOrderId"
        case releaseDate, preOrderTitle
        case preOrderCoverImageURL = "preOrderCoverImageUrl"
        case sku = "sku"
        case preOrderedCount, totalPreOrders, price, preOrderType, isPurchesed, totalnumberOfSongs
    }

    init(preOrderID: Int?, releaseDate: Int?, preOrderTitle: String?, preOrderCoverImageURL: String?, preOrderedCount: Int?, totalPreOrders: Int?, price: Double?, preOrderType: String?, isPurchesed: Bool?, totalnumberOfSongs: Int?,sku: String?) {
        self.preOrderID = preOrderID
        self.releaseDate = releaseDate
        self.preOrderTitle = preOrderTitle
        self.preOrderCoverImageURL = preOrderCoverImageURL
        self.preOrderedCount = preOrderedCount
        self.totalPreOrders = totalPreOrders
        self.price = price
        self.preOrderType = preOrderType
        self.isPurchesed = isPurchesed
        self.totalnumberOfSongs = totalnumberOfSongs
        self.sku = sku
    }
    func toString()->String {
        return "preOrderID \(String(describing: self.preOrderID?.description)) releaseDate \(String(describing: self.releaseDate?.description)) preOrderTitle \(String(describing: self.preOrderTitle?.description)) preOrderCoverImageURL \(String(describing: self.preOrderCoverImageURL?.description)) preOrderedCount \(String(describing: self.preOrderedCount?.description)) totalPreOrders \(String(describing: self.totalPreOrders?.description)) price \(String(describing: self.price?.description)) preOrderType \(String(describing: self.preOrderType?.description)) isPurchesed \(String(describing: self.isPurchesed?.description)) totalnumberOfSongs \(String(describing: self.totalnumberOfSongs?.description))"
    }
}
