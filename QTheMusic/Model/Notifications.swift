//
//  Notification.swift
//  QtheMusic
//
//  Created by Assad Khan on 14/03/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import Foundation
// MARK: - Notification
class Notifications: Codable {
    let isNotificationEnabled, isArtistUpdateEnabled: Bool?
    
    enum CodingKeys: String, CodingKey {
        case isNotificationEnabled = "isNotificationEnabled"
        case isArtistUpdateEnabled
    }
    
    init(isNotificationEnabled: Bool?, isArtistUpdateEnabled: Bool?) {
            self.isNotificationEnabled = isNotificationEnabled
            self.isArtistUpdateEnabled = isArtistUpdateEnabled
        }
    
    func toString() -> String {
        return " isNotificationEnabled \(String(describing: self.isNotificationEnabled?.description)) isArtistUpdateEnabled \(String(describing: self.isArtistUpdateEnabled?.description)) "
    }
}
