//
//  ResponseMain.swift
//  Base iOS Project
//
//  Created by Hammad Hassan on 09/02/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import Foundation


// MARK: - Welcome
struct ResponseMain: Codable {
    let response: Response?
   

   
    enum CodingKeys: String, CodingKey {
        case response = "response"
        
    }
    init(response: Response?) {
            self.response = response
        
        }
    
    func toString()->String{
        return "response \(String(describing: response?.toString().description))"
        }
    
}
