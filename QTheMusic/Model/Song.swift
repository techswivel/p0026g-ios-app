//
//  Song.swift
//  QtheMusic
//
//  Created by Assad Khan on 04/04/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import Foundation
import RealmSwift

// MARK: - Song
class Song: Object, Codable {
    @Persisted var songID:Int?
    @Persisted var albumID:Int?
    @Persisted var isAddedToPlayList:Bool?
    @Persisted var playListID:Int?
    @Persisted var artistID:Int?
    @Persisted var categoryID:Int?
    @Persisted var type: String?
    @Persisted var isFavourit:Bool?
    @Persisted var songTitle:String?
    @Persisted var coverImageURL:String?
    @Persisted var songDuration:String?
    @Persisted var songStatus:String?
    @Persisted var songAudioURL:String?
    @Persisted var songVideoURL:String?
    @Persisted var artist:String?
    @Persisted var lyrics:String?
    @Persisted var albumName:String?
    @Persisted var albumTitle: String?
    @Persisted var albumArtist:String?
    @Persisted var albumCoverImageURL:String?
    @Persisted var albumStatus: String?
    @Persisted var numberOfSongs:Int?
    @Persisted var artistName:String?
    @Persisted var artistCoverImageURL:String?
    @Persisted var timeStamp:Double?
    @Persisted var localPath:String?
    @Persisted var isListeningHistory:Bool?
    @Persisted var isSync = false
    var typeOfTransection: String?
    var requestType: String?
    var playedFrom: String?
    var currentSongId:Int?
    var sku: String?
    var query:String?
    var languageId:Int?
    var isRecommendedForYou :Bool?
    var purchaseType: String?
    var purchaseToken: String?
    var itemType: String?
    var planId: Int?
    var preOrderSongId: Int?
    var preOrderAlbumId: Int?
    var paidAmount: Double?
    var price: Double?
    override class func primaryKey() -> String? {
        return "songID"
    }
    
    enum CodingKeys: String, CodingKey {
        case songID = "songId"
        case albumID = "albumId"
        case isAddedToPlayList
        case playListID = "playListId"
        case artistID = "artistId"
        case categoryID = "categoryId"
        case type
        case isFavourit = "isFavourite"
        case songTitle
        case coverImageURL = "coverImageUrl"
        case songDuration, songStatus
        case songAudioURL = "songAudioUrl"
        case songVideoURL = "songVideoUrl"
        case artist, lyrics, albumName
        case typeOfTransection
        case sku = "sku"
        case purchaseType = "purchaseType"
        case purchaseToken = "purchaseToken"
        case itemType = "itemType"
        case planId = "planId"
        case preOrderSongId = "preOrderSongId"
        case preOrderAlbumId = "preOrderAlbumId"
        case paidAmount = "paidAmount"
        case price = "price"
    }

    convenience init(songID: Int, albumID: Int, isAddedToPlayList: Bool, playListID: Int, artistID: Int, categoryID: Int, type: String?, isFavourit: Bool, songTitle: String?, coverImageURL: String?, songDuration: String, songStatus: String?, songAudioURL: String?, songVideoURL: String?, artist: String?, lyrics: String?, albumName: String?, albumTitle: String?, albumArtist: String?, albumCoverImageURL: String?, albumStatus: String?, numberOfSongs: Int, artistName: String?, artistCoverImageURL: String?,query:String?,languageId:Int?,timeStamp:Double,isRecommendedForYou:Bool?,isListeningHistory:Bool?,requestType:String?, playedFrom: String?, currentSongId:Int?, isSync:Bool,typeOfTransection:String?,sku:String?,purchaseType:String,purchaseToken:String,itemType:String,planId:Int,preOrderSongId:Int,preOrderAlbumId:Int,paidAmount:Double,price:Double) {

        self.init()
        self.songID = songID
        self.albumID = albumID
        self.isAddedToPlayList = isAddedToPlayList
        self.playListID = playListID
        self.artistID = artistID
        self.categoryID = categoryID
        self.type = type ?? ""
        self.isFavourit = isFavourit
        self.songTitle = songTitle
        self.coverImageURL = coverImageURL
        self.songDuration = songDuration
        self.songStatus = songStatus
        self.songAudioURL = songAudioURL
        self.songVideoURL = songVideoURL
        self.artist = artist
        self.lyrics = lyrics
        self.albumName = albumName
        self.albumTitle = albumTitle
        self.albumArtist = albumArtist
        self.albumCoverImageURL = albumCoverImageURL
        self.albumStatus = albumStatus
        self.numberOfSongs = numberOfSongs
        self.artistName = artistName
        self.artistCoverImageURL = artistCoverImageURL
        self.query = query
        self.languageId  = languageId
        self.timeStamp = timeStamp
        self.isRecommendedForYou = isRecommendedForYou
        self.isListeningHistory = isListeningHistory
        self.requestType = requestType
        self.currentSongId = currentSongId
        self.playedFrom = playedFrom
        self.isSync = isSync
        self.typeOfTransection = typeOfTransection
        self.sku = sku
        self.purchaseType = purchaseType
        self.purchaseToken = purchaseToken
        self.itemType = itemType
        self.planId = planId
        self.preOrderSongId = preOrderSongId
        self.preOrderAlbumId = preOrderAlbumId
        self.paidAmount = paidAmount
        self.price = price
    }
    
    convenience init(songID: Int, albumID: Int, isAddedToPlayList: Bool, playListID: Int, artistID: Int, categoryID: Int, type: String?, isFavourit: Bool, songTitle: String?, coverImageURL: String?, songDuration: String, songStatus: String?, songAudioURL: String?, songVideoURL: String?, artist: String?, lyrics: String?, albumName: String?, albumTitle: String?, albumArtist: String?, albumCoverImageURL: String?, albumStatus: String?, numberOfSongs: Int, artistName: String?, artistCoverImageURL: String?,query:String?,languageId:Int?,timeStamp:Double,isRecommendedForYou:Bool?,isListeningHistory:Bool?,localPath:String?,requestType:String?,playedFrom: String?, currentSongId:Int?,isSync:Bool,typeOfTransection:String?,sku:String?,purchaseType:String,purchaseToken:String,itemType:String,planId:Int,preOrderSongId:Int,preOrderAlbumId:Int,paidAmount:Double,price:Double) {
        self.init()
        self.songID = songID
        self.albumID = albumID
        self.isAddedToPlayList = isAddedToPlayList
        self.playListID = playListID
        self.artistID = artistID
        self.categoryID = categoryID
        self.type = type ?? ""
        self.isFavourit = isFavourit
        self.songTitle = songTitle
        self.coverImageURL = coverImageURL
        self.songDuration = songDuration
        self.songStatus = songStatus
        self.songAudioURL = songAudioURL
        self.songVideoURL = songVideoURL
        self.artist = artist
        self.lyrics = lyrics
        self.albumName = albumName
        self.albumTitle = albumTitle
        self.albumArtist = albumArtist
        self.albumCoverImageURL = albumCoverImageURL
        self.albumStatus = albumStatus
        self.numberOfSongs = numberOfSongs
        self.artistName = artistName
        self.artistCoverImageURL = artistCoverImageURL
        self.query = query
        self.languageId  = languageId
        self.timeStamp = timeStamp
        self.isRecommendedForYou = isRecommendedForYou
        self.isListeningHistory = isListeningHistory
        self.localPath = localPath
        self.requestType = requestType
        self.currentSongId = currentSongId
        self.playedFrom = playedFrom
        self.isSync = isSync
        self.typeOfTransection = typeOfTransection
        self.sku = sku
        self.purchaseType = purchaseType
        self.purchaseToken = purchaseToken
        self.itemType = itemType
        self.planId = planId
        self.preOrderSongId = preOrderSongId
        self.preOrderAlbumId = preOrderAlbumId
        self.paidAmount = paidAmount
        self.price = price
    }
    
    func toString() -> String {
        return " songID \(String(describing: self.songID?.description)) albumID \(String(describing: self.albumID?.description)) isAddedToPlayList \(String(describing: self.isAddedToPlayList?.description)) playListID \(String(describing: self.playListID?.description)) artistID \(String(describing: self.artistID?.description)) categoryID \(String(describing: self.categoryID?.description)) type \(String(describing: self.type?.description)) isFavourit \(String(describing: self.isFavourit?.description)) songTitle \(String(describing: self.songTitle?.description)) coverImageURL \(String(describing: self.coverImageURL?.description)) songDuration \(String(describing: self.songDuration?.description)) songStatus \(String(describing: self.songStatus?.description)) songAudioURL \(String(describing: self.songAudioURL?.description)) songVideoURL \(String(describing: self.songVideoURL?.description)) artist \(String(describing: self.artist?.description)) lyrics \(String(describing: self.lyrics?.description)) albumName \(String(describing: self.albumName?.description)) albumTitle \(String(describing: self.albumTitle?.description)) albumArtist \(String(describing: self.albumArtist?.description)) albumCoverImageURL \(String(describing: self.albumCoverImageURL?.description)) albumStatus \(String(describing: self.albumStatus?.description)) numberOfSongs \(String(describing: self.numberOfSongs?.description)) artistName \(String(describing: self.artistName?.description)) artistCoverImageURL \(String(describing: self.artistCoverImageURL?.description))  typeOfTransection \(String(describing: self.typeOfTransection?.description))"
    }
}


