//
//  SubscribeToPlan.swift
//  QtheMusic
//
//  Created by zeeshan on 28/04/2023.
//  Copyright © 2023 TechSwivel. All rights reserved.
//

import Foundation

// MARK: - Welcome
struct MainResponse: Codable {
    let response: ResponseData?

    
   
    
}
// MARK: - Response
struct ResponseData: Codable {
    let status: Bool?
    let data: ResponseDataClass?
    let message:String?

    init(status: Bool?, data: ResponseDataClass?, message: String?) {
        self.status = status
        self.data = data
        self.message = message
        
    }
    
}

// MARK: - DataClass
struct ResponseDataClass: Codable {
    let purchasedSong: SongData?
    let purchasedSubscription: PurchasedSubscription?
    let purchasedAlbum: PurchasedAlbum?
    let purchasedPreOrder: PurchasedPreOrder?
    let meta: MetaData?

    init(purchasedSong: SongData?, purchasedSubscription: PurchasedSubscription?, purchasedAlbum: PurchasedAlbum?, purchasedPreOrder: PurchasedPreOrder?, meta: MetaData?) {
        self.purchasedSong = purchasedSong
        self.purchasedSubscription = purchasedSubscription
        self.purchasedAlbum = purchasedAlbum
        self.purchasedPreOrder = purchasedPreOrder
        self.meta = meta
    }
}

// MARK: - Meta
struct MetaData: Codable {
    let total, count, perPage, currentPage: Int?
    let totalPages: Int?

    init(total: Int?, count: Int?, perPage: Int?, currentPage: Int?, totalPages: Int?) {
        self.total = total
        self.count = count
        self.perPage = perPage
        self.currentPage = currentPage
        self.totalPages = totalPages
    }
    /**
    func toString()->String{
        return "response \(String(describing: total?.description))"
        }
     **/
}

// MARK: - PurchasedAlbum
struct PurchasedAlbum: Codable {
    let albumTitle, albumCoverImageURL: String?
    let totalSongs: Int?
    let timeOfPurchased: String?
    let price: Double?
    let songs: [SongData]?

    init(albumTitle: String?, albumCoverImageURL: String?, totalSongs: Int?, price: Double?, timeOfPurchased: String?, songs: [SongData]?) {
        self.albumTitle = albumTitle
        self.albumCoverImageURL = albumCoverImageURL
        self.totalSongs = totalSongs
        self.price = price
        self.timeOfPurchased = timeOfPurchased
        self.songs = songs
    }
}

// MARK: - Song
struct SongData: Codable {
    let songID, albumID: Int?
    let isAddedToPlayList: Bool?
    let playListID, artistID, categoryID: Int?
    let isFavourite: Bool?
    let songTitle, coverImageURL: String?
    let songDuration: String?
    let songStatus, songAudioURL, songVideoURL, artist: String?
    let lyrics, albumName: String?

    enum CodingKeys: String, CodingKey {
        case songID = "songId"
        case albumID = "albumId"
        case isAddedToPlayList
        case playListID = "playListId"
        case artistID = "artistId"
        case categoryID = "categoryId"
        case isFavourite, songTitle
        case coverImageURL = "coverImageUrl"
        case songDuration, songStatus
        case songAudioURL = "songAudioUrl"
        case songVideoURL = "songVideoUrl"
        case artist, lyrics, albumName
    }

    init(songID: Int?, albumID: Int?, isAddedToPlayList: Bool?, playListID: Int?, artistID: Int?, categoryID: Int?, isFavourite: Bool?, songTitle: String?, coverImageURL: String?, songDuration: String?, songStatus: String?, songAudioURL: String?, songVideoURL: String?, artist: String?, lyrics: String?, albumName: String?) {
        self.songID = songID
        self.albumID = albumID
        self.isAddedToPlayList = isAddedToPlayList
        self.playListID = playListID
        self.artistID = artistID
        self.categoryID = categoryID
        self.isFavourite = isFavourite
        self.songTitle = songTitle
        self.coverImageURL = coverImageURL
        self.songDuration = songDuration
        self.songStatus = songStatus
        self.songAudioURL = songAudioURL
        self.songVideoURL = songVideoURL
        self.artist = artist
        self.lyrics = lyrics
        self.albumName = albumName
    }
}

// MARK: - PurchasedPreOrder
struct PurchasedPreOrder: Codable {
    let preOrderID, releaseDate: Int?
    let preOrderTitle, preOrderCoverImageURL: String?
    let preOrderedCount, totalPreOrders: Int?
    let price: Double?
    let sku, preOrderType: String?
    let isPurchesed: Bool?

    enum CodingKeys: String, CodingKey {
        case preOrderID = "preOrderId"
        case releaseDate, preOrderTitle
        case preOrderCoverImageURL = "preOrderCoverImageUrl"
        case preOrderedCount, totalPreOrders, price, sku, preOrderType, isPurchesed
    }

    init(preOrderID: Int?, releaseDate: Int?, preOrderTitle: String?, preOrderCoverImageURL: String?, preOrderedCount: Int?, totalPreOrders: Int?, price: Double?, sku: String?, preOrderType: String?, isPurchesed: Bool?) {
        self.preOrderID = preOrderID
        self.releaseDate = releaseDate
        self.preOrderTitle = preOrderTitle
        self.preOrderCoverImageURL = preOrderCoverImageURL
        self.preOrderedCount = preOrderedCount
        self.totalPreOrders = totalPreOrders
        self.price = price
        self.sku = sku
        self.preOrderType = preOrderType
        self.isPurchesed = isPurchesed
    }
}

// MARK: - PurchasedSubscription
struct PurchasedSubscription: Codable {
    let planID: Int?
    let planTitle, planTopic: String?
    let planPrice: Double?
    let planDuration: String?

    enum CodingKeys: String, CodingKey {
        case planID = "planId"
        case planTitle, planTopic, planPrice, planDuration
    }

    init(planID: Int?, planTitle: String?, planTopic: String?, planPrice: Double?, planDuration: String?) {
        self.planID = planID
        self.planTitle = planTitle
        self.planTopic = planTopic
        self.planPrice = planPrice
        self.planDuration = planDuration
    }
    /**
     This method will convert PurchasedSubscription to Subscription object
     witk sku nill because this response did not have one
     */
    func toSubscription() -> Subscription {
        return Subscription(planID: self.planID, planTitle: self.planTitle, planPrice: self.planPrice, planDuration: self.planDuration, planTopic: self.planTopic, sku: nil)
    }
}
