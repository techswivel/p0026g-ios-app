//
//  AppStoreResponse.swift
//  QtheMusic
//
//  Created by Mac on 10/05/2023.
//  Copyright © 2023 TechSwivel. All rights reserved.
//

import Foundation

// MARK: - AppStoreResponse
struct AppStoreResponse: Codable {
    var status: Int?
    var environment: String?
    var receipt: Receipt?
    var latestReceiptInfo: [LatestReceiptInfo]?
    var latestReceipt: String?
    var pendingRenewalInfo: [PendingRenewalInfo]?

    enum CodingKeys: String, CodingKey {
        case status, environment, receipt
        case latestReceiptInfo = "latest_receipt_info"
        case latestReceipt = "latest_receipt"
        case pendingRenewalInfo = "pending_renewal_info"
    }
}

// MARK: - LatestReceiptInfo
struct LatestReceiptInfo: Codable {
    var quantity, productID, transactionID, originalTransactionID: String?
    var purchaseDate, purchaseDateMS, purchaseDatePst, originalPurchaseDate: String?
    var originalPurchaseDateMS, originalPurchaseDatePst, expiresDate, expiresDateMS: String?
    var expiresDatePst, webOrderLineItemID, isTrialPeriod, isInIntroOfferPeriod: String?
    var subscriptionGroupIdentifier: String?

    enum CodingKeys: String, CodingKey {
        case quantity
        case productID = "product_id"
        case transactionID = "transaction_id"
        case originalTransactionID = "original_transaction_id"
        case purchaseDate = "purchase_date"
        case purchaseDateMS = "purchase_date_ms"
        case purchaseDatePst = "purchase_date_pst"
        case originalPurchaseDate = "original_purchase_date"
        case originalPurchaseDateMS = "original_purchase_date_ms"
        case originalPurchaseDatePst = "original_purchase_date_pst"
        case expiresDate = "expires_date"
        case expiresDateMS = "expires_date_ms"
        case expiresDatePst = "expires_date_pst"
        case webOrderLineItemID = "web_order_line_item_id"
        case isTrialPeriod = "is_trial_period"
        case isInIntroOfferPeriod = "is_in_intro_offer_period"
        case subscriptionGroupIdentifier = "subscription_group_identifier"
    }
}

// MARK: - PendingRenewalInfo
struct PendingRenewalInfo: Codable {
    var expirationIntent, autoRenewProductID, originalTransactionID, isInBillingRetryPeriod: String?
    var productID, autoRenewStatus: String?

    enum CodingKeys: String, CodingKey {
        case expirationIntent = "expiration_intent"
        case autoRenewProductID = "auto_renew_product_id"
        case originalTransactionID = "original_transaction_id"
        case isInBillingRetryPeriod = "is_in_billing_retry_period"
        case productID = "product_id"
        case autoRenewStatus = "auto_renew_status"
    }
}

// MARK: - Receipt
struct Receipt: Codable {
    var receiptType: String?
    var adamID, appItemID: Int?
    var bundleID, applicationVersion: String?
    var downloadID, versionExternalIdentifier: Int?
    var receiptCreationDate, receiptCreationDateMS, receiptCreationDatePst, requestDate: String?
    var requestDateMS, requestDatePst, originalPurchaseDate, originalPurchaseDateMS: String?
    var originalPurchaseDatePst, originalApplicationVersion: String?
    var inApp: [LatestReceiptInfo]?

    enum CodingKeys: String, CodingKey {
        case receiptType = "receipt_type"
        case adamID = "adam_id"
        case appItemID = "app_item_id"
        case bundleID = "bundle_id"
        case applicationVersion = "application_version"
        case downloadID = "download_id"
        case versionExternalIdentifier = "version_external_identifier"
        case receiptCreationDate = "receipt_creation_date"
        case receiptCreationDateMS = "receipt_creation_date_ms"
        case receiptCreationDatePst = "receipt_creation_date_pst"
        case requestDate = "request_date"
        case requestDateMS = "request_date_ms"
        case requestDatePst = "request_date_pst"
        case originalPurchaseDate = "original_purchase_date"
        case originalPurchaseDateMS = "original_purchase_date_ms"
        case originalPurchaseDatePst = "original_purchase_date_pst"
        case originalApplicationVersion = "original_application_version"
        case inApp = "in_app"
    }
}

