//
//  Address.swift
//  QtheMusic
//
//  Created by Assad Khan on 14/03/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import Foundation
// MARK: - Address
class Address: Codable {
    var completeAddress, city, state, country: String?
    var zipCode: String?
    
    enum CodingKeys: String, CodingKey {
        case completeAddress = "completeAddress"
        case city,state,country,zipCode
    }
    
    
    init(completeAddress: String?, city: String?, state: String?, country: String?, zipCode: String?) {
            self.completeAddress = completeAddress
            self.city = city
            self.state = state
            self.country = country
            self.zipCode = zipCode
        }

    func toString() -> String {
        return "completeAddress \(String(describing: self.completeAddress?.description)) city \(String(describing: self.city?.description))  state \(String(describing: self.state?.description))  country \(String(describing: self.country?.description)) zipCode \(String(describing: self.zipCode?.description)) "
    }
}
