//
//  Language.swift
//  QtheMusic
//
//  Created by Assad Khan on 06/04/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import Foundation



// MARK: - Language
class Language: Codable {
    var languageID: Int?
    var languageTitle: String?
    var isSelected:Bool?
    
    enum CodingKeys: String, CodingKey {
        case languageID = "languageId"
        case languageTitle
    }

    init(languageID: Int?, languageTitle: String?,isSelected:Bool?) {
        self.languageID = languageID
        self.languageTitle = languageTitle
        self.isSelected =  isSelected
    }
}
