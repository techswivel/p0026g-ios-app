//
//  Response.swift
//  Base iOS Project
//
//  Created by Hammad Hassan on 09/02/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import Foundation
class Response: Codable {
    let status: Bool?
    let data: MyDataClass?
    let message:String?
    let progress: Double?
    let meta: Meta?
    
    init(status: Bool?, data: MyDataClass?,message:String?,meta:Meta?) {
        self.status = status
        self.data = data
        self.message = message
        self.progress = nil
        self.meta  = meta
      }
    init(status: Bool?, data: MyDataClass?,message:String?) {
        self.status = status
        self.data = data
        self.message = message
        self.progress = nil
        self.meta = nil
      }
    
    init(status: Bool?, data: MyDataClass?,message:String?,progress: Double?,meta:Meta?) {
        self.status = status
        self.data = data
        self.message = message
        self.progress = progress
        self.meta = meta
    }
    init(status: Bool?, data: MyDataClass?,message:String?,progress: Double?) {
        self.status = status
        self.data = data
        self.message = message
        self.progress = progress
        self.meta = nil
    }
    
    func toString()->String{
        return "status \(String(describing: status?.description)) data \(String(describing: data?.toString().description)) message \(String(describing: message?.description)) progress \(progress) meta : \(self.meta)"
        }
}
