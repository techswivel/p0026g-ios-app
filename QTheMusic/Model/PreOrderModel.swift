//
//  PreOrderModel.swift
//  QtheMusic
//
//  Created by zeeshan on 24/05/2023.
//  Copyright © 2023 TechSwivel. All rights reserved.
//

import Foundation
struct PreOrderModel: Codable {
    let songId: Int
    let albumId: Int?
    let isAddedToPlayList: Bool
    let playListId: Int?
    let artistId: Int
    let categoryId: Int
    let type: String
    let isFavourite: Bool
    let songTitle: String
    let sku: String?
    let coverImageUrl: String
    let songDuration: String
    let songStatus: String
    let price: Double?
    let songAudioUrl: String
    let songVideoUrl: String?
    let artist: String
    let lyrics: String?
    let albumName: String?
    let audiofileName: String
    let videofileName: String?
}
