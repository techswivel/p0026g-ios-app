//
//  Category.swift
//  QtheMusic
//
//  Created by Assad Khan on 29/03/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//
import Realm
import RealmSwift
import Foundation

class Category: Object, Codable {
    @objc dynamic var categoryID = 0
    @objc dynamic var categoryTitle, categoryImageURL: String?
     var isSelected: Bool?
    
    @objc dynamic var totalSongs = 0
    @objc dynamic var totalAlbums = 0
    
    enum CodingKeys: String, CodingKey {
        case categoryID = "categoryId"
        case categoryTitle
        case categoryImageURL = "categoryImageUrl"
        case totalSongs,totalAlbums
        case isSelected
    }
    
    override class func primaryKey() -> String? {
        return "categoryID"
    }
    
    convenience init(categoryID: Int, categoryTitle: String?, categoryImageURL: String?,isSelected:Bool?) {
        self.init()
        self.categoryID = categoryID
        self.categoryTitle = categoryTitle
        self.categoryImageURL = categoryImageURL
        self.isSelected = isSelected
    }
    
    convenience init(categoryID: Int, categoryTitle: String?, categoryImageURL: String?,totalSongs:Int,totalAlbums:Int) {
        self.init()
        self.categoryID = categoryID
        self.categoryTitle = categoryTitle
        self.categoryImageURL = categoryImageURL
        self.totalSongs = totalSongs
        self.totalAlbums = totalAlbums
    }
    
    
    func toString()->String{
        return " categoryID \(String(describing: self.categoryID.description)) categoryTitle  \(String(describing:categoryTitle?.description)) categoryImageURL \(String(describing:categoryImageURL?.description)) totalSongs \(String(describing:totalSongs.description)) totalAlbums \(String(describing:totalAlbums.description)) "
    }
}
