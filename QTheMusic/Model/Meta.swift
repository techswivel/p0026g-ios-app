//
//  Meta.swift
//  Base iOS Project
//
//  Created by Hammad Hassan on 09/02/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import Foundation


class Meta: Codable {
    let total, count, perPage, currentPage: Int?
    let totalPages: Int?

    init(total: Int?, count: Int?, perPage: Int?, currentPage: Int?, totalPages: Int?) {
        self.total = total
        self.count = count
        self.perPage = perPage
        self.currentPage = currentPage
        self.totalPages = totalPages
    }
    
    func toString()->String{
        return "response \(String(describing: total?.description))"
        }
}
