//
//  Authorization.swift
//  Base iOS Project
//
//  Created by Hammad Hassan on 09/02/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import Foundation
import UIKit
class Authorization: Codable {
    var name, email, avatar, jwt: String?
    var dOb: Int?
    var phoneNumber, gender,zipCode: String?
    var isInterestsSet: Bool?
    var addressModObj: Address?
    var subscription: Subscription?
    var notification: Notifications?
    var accessToken,deviceIdentifier,deviceName,fcmToken,socialSite,password,loginType: String?
    var otpType: String?
    var otp:Int?
    var city,state,country:String?
    var isEnableNotification,isArtistUpdateEnable:String?
    var address:String?
    var profileImage:UIImage?
    var firebaseCustomToken:String?
    var socialId:String?
    var completeAddress: String?
    
    enum CodingKeys: String, CodingKey {
        case name,avatar,jwt
        case email
        case dOb
        case phoneNumber, gender
        case isInterestsSet
        case addressModObj = "address"
        case subscription
        case notification
        case city
        case state
        case country
        case zipCode
        case socialId
        case isEnableNotification,isArtistUpdateEnable
        case completeAddress
    }
    
    init(name: String?, email: String?, avatar: String?, jwt: String?, dOb: Int?, phoneNumber: String?, gender: String?, isInterestsSet: Bool?, addressModObj: Address?, subscription: Subscription?, notifications: Notifications?,accessToken:String?,deviceIdentifier:String?,deviceName:String?,fcmToken:String?,socialSite:String?,password:String?,loginType:String?,otpType:String?,otp:Int?,city:String?,state:String?,country:String?,zipCode:String?,isEnableNotification:String?,isArtistUpdateEnable:String?,address:String?,profileImage:UIImage?,firebaseCustomToken:String?,socialId:String?,completeAddress:String) {
        self.name = name
        self.email = email
        self.avatar = avatar
        self.jwt = jwt
        self.dOb = dOb
        self.phoneNumber = phoneNumber
        self.gender = gender
        self.isInterestsSet = isInterestsSet
        self.addressModObj = addressModObj
        self.subscription = subscription
        self.notification = notifications
        self.accessToken = accessToken
        self.deviceIdentifier = deviceIdentifier
        self.deviceName = deviceName
        self.fcmToken = fcmToken
        self.socialSite = socialSite
        self.password = password
        self.loginType = loginType
        self.otpType = otpType
        self.otp = otp
        self.city = city
        self.state = state
        self.country = country
        self.zipCode = zipCode
        self.isEnableNotification = isEnableNotification
        self.isArtistUpdateEnable = isArtistUpdateEnable
        self.address = address
        self.profileImage = profileImage
        self.firebaseCustomToken = firebaseCustomToken
        self.socialId = socialId
        self.completeAddress = completeAddress
    }
    
    func toString() -> String {
        return "name :\(String(describing: self.name?.description)) email: \(String(describing: self.email?.description))  avatar :\(String(describing: self.avatar?.description))  jwt :\(String(describing: self.jwt?.description)) dOb: \(String(describing: self.dOb?.description)) phoneNumber: \(String(describing: self.phoneNumber?.description)) isInterestsSet: \(String(describing: self.isInterestsSet?.description)) gender: \(String(describing: self.gender?.description)) addressModObj: \(String(describing: self.addressModObj?.toString())) address:\(self.address?.description) city:\(self.city?.description)state:\(self.state?.description)country:\(self.country?.description) zipCode:\(self.zipCode?.description) subscription: \(String(describing: self.subscription?.toString())) notification :\(String(describing: self.notification?.toString())) isEnableNotification:\(String(describing: self.isEnableNotification)) isArtistUpdateEnable: \(String(describing: self.isArtistUpdateEnable)) completeAddress: \(String(describing: self.completeAddress))"
    }
}
