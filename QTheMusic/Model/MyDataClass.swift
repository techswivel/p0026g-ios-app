//
//  MyDataClass.swift
//  Base iOS Project
//
//  Created by Hammad Hassan on 09/02/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import Foundation



import Foundation
import Combine
class MyDataClass: Codable {
    
    
    let meta: Meta?
    let auth:Authorization?
    var category: [Category]?
    var totalSongs: Int?
    var songs: [Song]?
    var searchResults: [Song]?
    var albums: [Album]?
    var artist: [Artist]?
    var Languages: [Language]?
    var albumStatus:String?
    var artistDetails: ArtistDetails?
    var preOrderData: [PreOrderData]?
    var playlists: [Playlist]?
    var recentlyPlayed: RecentPlayed?
    var listeningHistory: ListeningHistory?
    var playlist: Playlist?
    var buyinghistory: [BuyingHistory]?
    var plans: [Subscription]?
    init(auth:Authorization?, meta: Meta?,category:[Category]?) {
        
        self.auth = auth
        self.meta = meta
        self.category = category
    }
    
    init(totalSongs: Int?, songs: [Song]?, albums: [Album]?, artist: [Artist]?,languages: [Language]?, searchResults: [Song]?, recentlyPlayed: RecentPlayed?, listeningHistory: ListeningHistory?) {
        self.totalSongs = totalSongs
        self.songs = songs
        self.searchResults = searchResults
        self.albums = albums
        self.artist = artist
        self.auth = nil
        self.meta = nil
        self.category = nil
        self.Languages = languages
        self.recentlyPlayed = recentlyPlayed
        self.listeningHistory = listeningHistory
    }
    init(totalSongs:Int?,albumStatus: String?, songs: [Song]?) {
        self.albumStatus = albumStatus
        self.songs = songs
        self.totalSongs = totalSongs
        self.albums = nil
        self.artist = nil
        self.auth = nil
        self.meta = nil
        self.category = nil
        self.Languages = nil
    }
    init(artistDetails:ArtistDetails?){
        self.artistDetails = artistDetails
        self.albumStatus = nil
        self.songs =  nil
        self.totalSongs =  nil
        self.albums = nil
        self.artist = nil
        self.auth = nil
        self.meta = nil
        self.category = nil
        self.Languages = nil
    }
    
    init(preOrderData:[PreOrderData]?){
        self.preOrderData = preOrderData
        self.artistDetails = nil
        self.albumStatus = nil
        self.songs =  nil
        self.totalSongs =  nil
        self.albums = nil
        self.artist = nil
        self.auth = nil
        self.meta = nil
        self.category = nil
        self.Languages = nil
    }
    
    init (playlists : [Playlist]?){
        self.playlists = playlists
        self.preOrderData = nil
        self.artistDetails = nil
        self.albumStatus = nil
        self.songs =  nil
        self.totalSongs = nil
        self.albums = nil
        self.artist = nil
        self.auth = nil
        self.meta = nil
        self.category = nil
        self.Languages = nil
    }
    
    init (playlist: Playlist?){
        self.playlist = playlist
        self.albumStatus = nil
        self.songs =  nil
        self.totalSongs = nil
        self.albums = nil
        self.auth = nil
        self.meta = nil
        self.category = nil
        self.Languages = nil
    }
    
    init (artist : [Artist]?){
        self.artist = artist
        self.albumStatus = nil
        self.songs =  nil
        self.totalSongs = nil
        self.albums = nil
        self.auth = nil
        self.meta = nil
        self.category = nil
        self.Languages = nil
        
    }
    
    init (albums: [Album]?){
        self.albums = albums
        self.albumStatus = nil
        self.songs =  nil
        self.totalSongs =  nil
        self.auth = nil
        self.meta = nil
        self.category = nil
        self.Languages = nil
        
    }
    
    init (buyinghistory: [BuyingHistory]?){
        self.buyinghistory = buyinghistory
        self.albumStatus = nil
        self.songs =  nil
        self.totalSongs = nil
        self.albums = nil
        self.auth = nil
        self.meta = nil
        self.category = nil
        self.Languages = nil
        
    }
    init (plans: [Subscription]?)
    {
        self.plans = plans
        self.albumStatus = nil
        self.songs =  nil
        self.totalSongs = nil
        self.albums = nil
        self.auth = nil
        self.meta = nil
        self.category = nil
        self.Languages = nil
        
    }
    func toString()->String{
        return " auth \(String(describing: auth?.toString())) meta \(String(describing: meta?.toString().description)) Category \(String(describing: category?.description)) totalSongs \(String(describing: totalSongs?.description)) songs \(String(describing: songs?.description)) albums \(String(describing: albums?.description)) songs \(String(describing: songs?.description)) artist \(String(describing: artist?.description)) artistDetails \(String(describing: artistDetails?.toString()))"
        
    }
}
