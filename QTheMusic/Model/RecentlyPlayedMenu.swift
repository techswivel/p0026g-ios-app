//
//  RecentlyPlayedMenu.swift
//  QtheMusic
//
//  Created by Assad Khan on 13/04/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import Foundation

class RecentlyPlayedMenu {
    var isSelected:Bool?
    var menuItem:String?
    
    
    init(isSelected:Bool?,menuItem:String?){
        self.isSelected = isSelected
        self.menuItem = menuItem
    }
}
