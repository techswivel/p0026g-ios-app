//
//  Artist.swift
//  QtheMusic
//
//  Created by Assad Khan on 04/04/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import Foundation
import RealmSwift

// MARK: - Artist
class Artist: Object, Codable {
    @objc dynamic var artistID = 0
    @objc dynamic var artistName, artistCoverImageURL: String?
    @objc dynamic var artistImg : String?
    @objc dynamic var numberOfSong : String?
    @objc dynamic var timeStamp = 0
    @objc dynamic var isSync = false

    override class func primaryKey() -> String? {
        return "artistID"
    }
    
    enum CodingKeys: String, CodingKey {
        case artistID = "artistId"
        case artistName
        case artistCoverImageURL = "artistCoverImageUrl"
    }


    convenience init(artistID: Int, artistName: String?, artistCoverImageURL: String?, timeStamp:Int, isSync: Bool) {

        self.init()
        self.artistID = artistID
        self.artistName = artistName
        self.artistCoverImageURL = artistCoverImageURL
        self.timeStamp = timeStamp
        self.isSync = isSync
    }
    
    func toString() -> String {
        return " artistID \(String(describing: self.artistID.description)) artistName \(String(describing: self.artistName?.description)) artistCoverImageURL \(String(describing: self.artistCoverImageURL?.description)) "
    }
}
