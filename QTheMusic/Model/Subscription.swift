//
//  Subscription.swift
//  QtheMusic
//
//  Created by Assad Khan on 14/03/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import Foundation
// MARK: - Subscription
class Subscription: Codable {
    let planID: Int?
    let planTitle: String?
    let planPrice: Double?
    let planDuration: String?
    let planTopic: String?
    let sku: String?
    enum CodingKeys: String, CodingKey {
        case planID = "planId"
        case planTitle, planPrice, planDuration, planTopic, sku
    }
    
    init(planID: Int?, planTitle: String?, planPrice: Double?, planDuration: String?, planTopic: String?, sku: String?) {
            self.planID = planID
            self.planTitle = planTitle
            self.planPrice = planPrice
            self.planDuration = planDuration
            self.planTopic = planTopic
            self.sku = sku
        }
    

func toString() -> String {
    return " planID \(String(describing: self.planID?.description)) planTitle \(String(describing: self.planTitle?.description))  planPrice \(String(describing: self.planPrice?.description))  planDuration \(String(describing: self.planDuration?.description))"
}

}
