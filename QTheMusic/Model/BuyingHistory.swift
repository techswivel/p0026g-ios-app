//
//  BuyingHistory.swift
//  QtheMusic
//
//  Created by Muhammad Tanzeel Ur Rehman on 16/01/2023.
//  Copyright © 2023 TechSwivel. All rights reserved.
//

import Foundation
import RealmSwift

// MARK: BuyingHistory
class BuyingHistory: Object, Codable {
    
    
    @Persisted var itemType: String?
    @Persisted var songTitle : String?
    @Persisted var coverImageUrl : String?
    @Persisted var songDuration = 0
    @Persisted var price = 0
    @Persisted var timeOfPurchased : String?
    @Persisted var typeOfTransection : String?
    @Persisted var cardPrefix = 0
    @Persisted var albumTitle: String?
    @Persisted var albumItemType: String?
    @Persisted var albumCoverImageUrl : String?
    @Persisted var albumPrice = 0
    @Persisted var albumTimeOfPurchased : String?
    @Persisted var albumTypeOfTransection : String?
    @Persisted var numberOfSongs = 0
    @Persisted var albumCardPrefix = 0
    @Persisted var albumSongTitle: String?
    @Persisted var albumSongCoverImageUrl : String?
    @Persisted var albumSongDuration = 0
    @Persisted var albumSongPrice = 0
    
    
    enum CodingKeys : String, CodingKey {
        case itemType, songTitle, coverImageUrl
        case typeOfTransection
        case albumTitle
        case albumItemType, albumCoverImageUrl
        case albumTypeOfTransection
        case albumSongTitle, albumSongCoverImageUrl
        case timeOfPurchased
        case albumTimeOfPurchased
        
    }
    
    convenience init( itemType:String?, songTitle: String?, coverImageUrl: String?, songDuration: Int, price: Int, timeOfPurchased: String?, typeOfTransection:String?, cardPrefix:Int, albumTitle:String?, albumItemType:String?, albumCoverImageUrl:String?, albumPrice:Int, albumTimeOfPurchased: String?, albumTypeOfTransection: String?, numberOfSongs:Int, albumCardPrefix: Int, albumSongTitle: String? , albumSongDuration: Int , albumSongPrice: Int, albumSongCoverImageUrl: String? )
    {
        self.init()
        self.itemType = itemType
        self.songTitle = songTitle
        self.coverImageUrl = coverImageUrl
        self.songDuration = songDuration
        self.price = price
        self.timeOfPurchased = timeOfPurchased
        self.typeOfTransection = typeOfTransection
        self.cardPrefix = cardPrefix
        self.albumTitle = albumTitle
        self.albumItemType = albumItemType
        self.albumCoverImageUrl = albumCoverImageUrl
        self.albumPrice = albumPrice
        self.albumTimeOfPurchased = albumTimeOfPurchased
        self.albumTypeOfTransection = albumTypeOfTransection
        self.numberOfSongs = numberOfSongs
        self.albumCardPrefix = albumCardPrefix
        self.albumSongTitle = albumSongTitle
        self.albumSongCoverImageUrl = albumSongCoverImageUrl
        self.albumSongDuration = albumSongDuration
        self.albumSongPrice = albumSongPrice
        
    }
    
    
    func toString() -> String {
        
        return " itemType \(String(describing: self.itemType?.description)) songTitle \(String(describing: self.songTitle?.description)) coverImageUrl \(String(describing: self.coverImageUrl?.description)) songDuration \(String(describing: self.songDuration.description)) price \(String(describing: self.price.description)) timeOfPurchased \(String(describing: self.timeOfPurchased?.description)) typeOfTransation \(String(describing: self.typeOfTransection?.description)) cardPrefix \(String(describing: self.cardPrefix.description))  albumTitle \(String(describing: self.albumTitle?.description)) albumItemType \(String(describing: self.albumItemType?.description)) albumCoverImageUrl \(String(describing: self.albumCoverImageUrl?.description)) albumPrice \(String(describing: self.albumPrice.description)) albumTimeOfPurchased \(String(describing: self.albumTimeOfPurchased?.description)) albumTypeOfTransection \(String(describing: self.albumTypeOfTransection?.description)) numberOfSongs \(String(describing: self.numberOfSongs.description)) albumcardPrefix \(String(describing: self.albumCardPrefix.description)) "
    }
    
}
