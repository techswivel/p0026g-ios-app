//
//  RecentPlayed.swift
//  QtheMusic
//
//  Created by Muhammad Usama on 26/01/2023.
//  Copyright © 2023 TechSwivel. All rights reserved.
//

import Foundation

class RecentPlayed: Codable {
    
    var songs: [Song]?
    var albums: [Album]?
    var artists: [Artist]?
    
     init(songs: [Song]?, albums: [Album]?, artists: [Artist]?) {
        self.songs = songs
        self.albums = albums
        self.artists = artists
    }
}
