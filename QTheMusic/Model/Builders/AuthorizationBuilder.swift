//
//  AuthorizationBuilder.swift
//  QtheMusic
//
//  Created by Assad Khan on 14/03/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import Foundation
import UIKit

class AuthorizationBuilder{
    private var name, email, avatar, jwt: String?
    private var dOb: Int?
    private var phoneNumber, gender,zipCode: String?
    private var isInterestsSet: Bool?
    private var addressModObj: Address?
    private var subscription: Subscription?
    private var notification: Notifications?
    private var accessToken,deviceIdentifier,deviceName,fcmToken,socialSite,password,loginType: String?
    private var otpType:String?
    private var otp :Int?
    
    private var city,state,country:String?
    private var isEnableNotification,isArtistUpdateEnable:String?
    private var address:String?
    private var firebaseCustomToken:String?
    private var profileImage:UIImage?
    private var  socialId : String?
    private var completeAddress: String?
    public func setname (_ name:String?){
        self.name = name
    }
    public func setemail (_ email:String?){
        self.email  = email
    }
    public func setavatar (_ avatar:String?){
        self.avatar = avatar
    }
    public func setjwt (_ jwt:String?){
        self.jwt = jwt
    }
    public func setcompleteAddress (_ completeAddress:String?){
        self.completeAddress = completeAddress
    }
    public func setdOb (_ dOb:Int?){
        self.dOb = dOb
    }
    public func setphoneNumber (_ phoneNumber:String?){
        self.phoneNumber = phoneNumber
    }
    public func setgender (_ gender:String?){
        self.gender = gender
    }
    public func setisInterestsSet (_ isInterestsSet:Bool?){
        self.isInterestsSet = isInterestsSet
    }
    public func setaddressModObj (_ address:Address?){
        self.addressModObj = address
    }
    public func setsubscription (_ subscription:Subscription?){
        self.subscription = subscription
    }
    public func setnotification (_ notification:Notifications?){
        self.notification = notification
    }
    public func setAccessToken(_ accessToken:String?){
        self.accessToken = accessToken
    }
    public func setDeviceIdentifier(_ deviceIdentifier:String?){
        self.deviceIdentifier = deviceIdentifier
    }
    public func setDeviceName(_ deviceName:String?){
        self.deviceName = deviceName
    }
    public func setFcmToken(_ fcmToken:String?){
        self.fcmToken = fcmToken
    }
    public func setSocialSite(_ socialSite:String?){
        self.socialSite = socialSite
    }
    public func setPassword(_ password:String?){
        self.password = password
    }
    public func setloginType(_ loginType:String?){
        self.loginType = loginType
    }
    public func setOtpType(_ otpType:String?){
        self.otpType = otpType
    }
    public func setOtp(_ otp:Int?){
        self.otp = otp
    }
    
    public func setcity (_  city:String?){
        self.city = city
    }
    public func setstate (_ state :String?){
        self.state = state
    }
    public func setcountry (_ country :String?){
        self.country = country
    }
    public func setzipCode (_ zipCode :String?){
        self.zipCode = zipCode
    }
    public func setisEnableNotification(_ isEnableNotification :String?){
        self.isEnableNotification = isEnableNotification
    }
    public func setisArtistUpdateEnable (_ isArtistUpdateEnable :String?){
        self.isArtistUpdateEnable = isArtistUpdateEnable
    }
    
    public func setAddress(_ address:String?){
        self.address = address
    }
    
    public func setProfileImage(_ image:UIImage?){
        self.profileImage = image
    }
    public func setFirebaseCustomToken(_ firebaseCustomToken:String?){
        self.firebaseCustomToken = firebaseCustomToken
    }
    
    public func setsocialId(_ socialId:String?)
    {
        self.socialId = socialId
    }
    
    public func build()->Authorization
    {
        
        return Authorization(name: self.name, email: self.email, avatar: self.avatar, jwt: self.jwt, dOb: self.dOb, phoneNumber: self.phoneNumber, gender: self.gender, isInterestsSet: self.isInterestsSet, addressModObj: self.addressModObj, subscription: self.subscription, notifications: self.notification,accessToken:self.accessToken, deviceIdentifier: self.deviceIdentifier, deviceName: self.deviceName, fcmToken: self.fcmToken, socialSite: self.socialSite, password: self.password, loginType: self.loginType,otpType: self.otpType,otp: self.otp, city: self.city, state: self.state, country: self.country, zipCode: self.zipCode, isEnableNotification: self.isEnableNotification, isArtistUpdateEnable: self.isArtistUpdateEnable,address: self.address,profileImage: self.profileImage,firebaseCustomToken: self.firebaseCustomToken, socialId: self.socialId, completeAddress: self.completeAddress ?? "")
    }
    
}
