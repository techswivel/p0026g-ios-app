//
//  SongAndArtistBuilder.swift
//  QtheMusic
//
//  Created by Assad Khan on 06/04/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import Foundation


class SongAndArtistBuilder{
    
  private  var query :String?
  private  var languageId :Int?
   private var songID :Int?
    private var type:String?
    private var playListId :Int?
    private var albumId:Int?
    private var albumTitle:String?
    private var albumCoverImageURL :String?
    private var albumStatus:String?
    private var numberOfSongs:Int?
    private var timeStamp:Double?
    private var isListeningHistory:Bool?
    private var isRecommendedForYou:Bool?
    private var categoryId:Int?
    private var requestType:String?
    private var isFavourit:Bool?
    private var playedFrom:String?
    private var currentSongId:Int?
    private var typeOfTransection : String?
    private var sku : String?
    private var purchaseType: String?
    private var purchaseToken: String?
    private var itemType: String?
    private var planId: Int?
    private var preOrderSongId: Int?
    private var preOrderAlbumId: Int?
    private var paidAmount: Double?
    private var price: Double?
   
    public func setQuery(_ query:String?){
        self.query = query
    }
    public func setLanguageId(_ languageId :Int?)
    {
        self.languageId = languageId
    }
    public func settype(_ type :String?)
    {
        self.type = type
    }
    public func setplayListId(_ playListId :Int?)
    {
        self.playListId = playListId
    }
    public func setalbumId(_ albumId :Int?)
    {
        self.albumId = albumId
    }
    public func setalbumTitle(_ albumTitle:String?){
        self.albumTitle = albumTitle
    }
    public func setalbumCoverImageURL(_ albumCoverImageURL:String?){
        self.albumCoverImageURL = albumCoverImageURL
    }
    public func setalbumStatus(_ albumStatus:String?){
        self.albumStatus = albumStatus
    }
    
    public func setNumberOfSongs(_ numberOfSongs:Int?)
    {
        self.numberOfSongs  = numberOfSongs
    }
    
    public func setTimeStamp(_ timeStamp : Double?){
        self.timeStamp = timeStamp
    }
    
    public func setIsRecommendedForYou (_ isRecommendedForYou:Bool?){
        self.isRecommendedForYou = isRecommendedForYou
        
    }
    public func setIsListeningHistory (_ isListeningHistory:Bool? ){
        self.isListeningHistory = isListeningHistory
        
    }
    
    public func setCategoryId(_ CategoryId:Int?){
        self.categoryId = CategoryId
    }
    
    public func setSongId(_ songId : Int?){
        self.songID  = songId
    }
    
    public func setPlayListId(_ PlayListId : Int?){
        self.playListId  = PlayListId
    }
    
    public func setRequestType(_ requestType : String?){
        self.requestType  = requestType
    }
    
    public func setIsFavourite(_ isFavourite:Bool? ){
        self.isFavourit = isFavourite
        
    }
    
    public func setPlayedForm(_ playedFrom : String?){
        self.playedFrom  = playedFrom
    }
    
    public func setCurrentSongId(_ currentSongId : Int?){
        self.currentSongId  = currentSongId
    }
    public func setTypeOfTransection(_ typeOfTransection : String?){
        self.typeOfTransection  = typeOfTransection
    }
    public func setSku(_ sku : String?){
        self.sku  = sku
    }
    public func setpurchaseType(_ purchaseType : String?){
        self.purchaseType  = purchaseType
    }
    public func setpurchaseToken(_ purchaseToken : String?){
        self.purchaseToken  = purchaseToken
    }
    public func setItemType(_ itemType : String?){
        self.itemType  = itemType
    }
    public func setPlanId(_ planId : Int?){
        self.planId  = planId
    }
    public func setPreOrderSongId(_ preOrderSongId : Int?){
        self.preOrderSongId  = preOrderSongId
    }
    public func setPreOrderAlbumId(_ preOrderAlbumId : Int?){
        self.preOrderAlbumId  = preOrderAlbumId
    }
    public func setPaidAmount(_ paidAmount : Double?){
        self.paidAmount  = paidAmount
    }
    public func setPrice(_ price : Double?){
        self.price  = price
    }
    public func build()->Song
    {
        
        return Song(songID: self.songID ?? -1, albumID: self.albumId ?? -1, isAddedToPlayList: false, playListID: self.playListId ?? -1, artistID: -1, categoryID: self.categoryId ?? -1, type: self.type, isFavourit: self.isFavourit ?? false, songTitle: nil, coverImageURL: nil, songDuration: "-1", songStatus: nil, songAudioURL: nil, songVideoURL: nil, artist: nil, lyrics: nil, albumName: nil, albumTitle: self.albumTitle, albumArtist: nil, albumCoverImageURL: self.albumCoverImageURL, albumStatus: self.albumStatus, numberOfSongs: self.numberOfSongs ?? -1, artistName: nil, artistCoverImageURL: nil, query: self.query , languageId: self.languageId, timeStamp:  self.timeStamp ?? Date().timeIntervalSince1970,isRecommendedForYou: self.isRecommendedForYou, isListeningHistory: self.isListeningHistory, requestType: self.requestType, playedFrom: self.playedFrom, currentSongId: self.currentSongId, isSync: false,typeOfTransection: self.typeOfTransection, sku:self.sku,purchaseType:self.purchaseType ?? "",purchaseToken:self.purchaseToken ?? "" ,itemType:self.itemType ?? "",planId:self.planId ?? 0,preOrderSongId:self.preOrderSongId ?? 0,preOrderAlbumId:self.preOrderAlbumId ?? 0,paidAmount:self.paidAmount ?? 0.0,price: self.price ?? 0.0)
    }
}
