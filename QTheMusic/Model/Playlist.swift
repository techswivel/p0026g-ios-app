//
//  Playlist.swift
//  QtheMusic
//
//  Created by Assad Khan on 06/09/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import Foundation

// MARK: - Playlist
class Playlist: Codable {
    var playListID: Int?
    var playListTitle: String?
    var totalSongs: Int?
    var isSelected:Bool = false

    enum CodingKeys: String, CodingKey {
        case playListID = "playListId"
        case playListTitle, totalSongs
    }

    init(playListID: Int, playListTitle: String, totalSongs: Int) {
        self.playListID = playListID
        self.playListTitle = playListTitle
        self.totalSongs = totalSongs
        
    }
    
    init(playListID: Int, playListTitle: String, totalSongs: Int,isSelected:Bool) {
        self.playListID = playListID
        self.playListTitle = playListTitle
        self.totalSongs = totalSongs
        self.isSelected = isSelected
    }
    
    
    
func toString()->String{
    return "playListID \(String(describing: playListID)) playListTitle \(String(describing: self.playListTitle?.description)) totalSongs  \(String(describing: self.totalSongs?.description))"}
}
