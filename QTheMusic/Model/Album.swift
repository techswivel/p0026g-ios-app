//
//  Album.swift
//  QtheMusic
//
//  Created by Assad Khan on 04/04/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import Foundation
import RealmSwift

// MARK: - Album
class Album: Object, Codable {
    @objc dynamic var albumID = 0
    @objc dynamic var albumTitle, albumCoverImageURL, albumStatus: String?
    @objc dynamic var numberOfAlbum : String?
    @objc dynamic var numberOfSongs = 0
    @objc dynamic var timeStamp  = 0.0
    @objc dynamic var isSync = false
    @objc dynamic var sku : String?
    var price : Double?
    
    override class func primaryKey() -> String? {
        return "albumID"
    }
    
    enum CodingKeys: String, CodingKey {
        case albumID = "albumId"
        case albumTitle
        case albumCoverImageURL = "albumCoverImageUrl"
        case albumStatus, numberOfSongs
        case sku = "sku"
        case price = "price"
    }

    convenience init(albumID: Int, albumTitle: String?, albumCoverImageURL: String?, albumStatus: String?, numberOfSongs: Int,timeStamp:Double, isSync: Bool,sku: String?,price: Double?) {

        self.init()
        self.albumID = albumID
        self.albumTitle = albumTitle
        self.albumCoverImageURL = albumCoverImageURL
        self.albumStatus = albumStatus
        self.numberOfSongs = numberOfSongs
        
        self.timeStamp = timeStamp
        self.isSync = isSync
        self.sku = sku
        self.price = price
    }
    
    func toString() -> String {
        return " albumID \(String(describing: self.albumID.description)) albumTitle \(String(describing: self.albumTitle?.description)) albumCoverImageURL \(String(describing: self.albumCoverImageURL?.description)) albumStatus \(String(describing: self.albumStatus?.description)) numberOfSongs \(String(describing: self.numberOfSongs.description)) "
    }
}
