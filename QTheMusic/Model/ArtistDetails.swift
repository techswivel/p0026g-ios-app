//
//  ArtistDetails.swift
//  QtheMusic
//
//  Created by Assad Khan on 11/05/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import Foundation


// MARK: - ArtistDetails
class ArtistDetails: Codable {
    var totalSongs, totalAlbums: Int?
    var artistName, artistCoverImageURL, aboutArtist: String?
    var isPreOrderAvailble, isPrimumArtist: Bool?
    var songs: [Song]?
    var albums: [Album]?
    var isUserFollowed: Bool?

    enum CodingKeys: String, CodingKey {
        case totalSongs, totalAlbums, artistName
        case artistCoverImageURL = "artistCoverImageUrl"
        case isUserFollowed
        case aboutArtist, isPreOrderAvailble, isPrimumArtist, songs, albums
    }

    init(totalSongs: Int?, totalAlbums: Int?, artistName: String?, artistCoverImageURL: String?, aboutArtist: String?, isPreOrderAvailble: Bool?, isPrimumArtist: Bool?, songs: [Song]?, albums: [Album]?, isUserFollowed: Bool?) {
        self.totalSongs = totalSongs
        self.totalAlbums = totalAlbums
        self.artistName = artistName
        self.artistCoverImageURL = artistCoverImageURL
        self.aboutArtist = aboutArtist
        self.isPreOrderAvailble = isPreOrderAvailble
        self.isPrimumArtist = isPrimumArtist
        self.songs = songs
        self.albums = albums
        self.isUserFollowed = isUserFollowed
    }
    
    func toString() -> String {
        return " totalSongs \(String(describing: self.totalSongs?.description)) totalAlbums \(String(describing: self.totalAlbums?.description)) artistName \(String(describing: self.artistName?.description)) artistCoverImageURL \(String(describing: self.artistCoverImageURL?.description)) aboutArtist \(String(describing: self.aboutArtist?.description)) isPreOrderAvailble \(String(describing: self.isPreOrderAvailble?.description)) isPrimumArtist \(String(describing: self.isPrimumArtist?.description)) songs \(String(describing: self.songs?.description)) albums \(String(describing: self.albums?.description))  "
    }
}
