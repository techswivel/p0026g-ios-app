//
//  InAppPurchaseHandler.swift
//  StormChance
//
//  Created by Mac on 22/03/2021.
//  Copyright © 2021 Yasir Iqbal. All rights reserved.
//

import Foundation
import StoreKit

// MARK: InAppPurchaseMessages
enum InAppPurchaseMessages: String {
    case purchased = "You payment has been successfully processed."
    case failed = "Failed to process the payment."
    case deferred = "Your payment has been Suspended."
    case purchasing = "Your payment is being process please check letter to update."
    case restored = "Your Purchase has been restore"
}

// MARK: PurchaseProduct
enum PurchaseProduct: String, CaseIterable {
    case Song
    case Subscription
    init()
    {
        self = .Song
        self = .Subscription
    }
}

class InAppPurchaseHandler: NSObject {
    static let shared = InAppPurchaseHandler()
    fileprivate var productIds = PurchaseProduct.allCases.map { $0.rawValue }
    fileprivate var productID = ""
    fileprivate var productsRequest = SKProductsRequest()
    fileprivate var productToPurchase: SKProduct?
    fileprivate var fetchProductcompletion: (([SKProduct]?,String?) -> Void)?
    fileprivate var purchaseProductcompletion: ((String, SKProduct?, SKPaymentTransaction?)->Void)?
    
    private func canMakePurchases() -> Bool {
        SKPaymentQueue.canMakePayments()
    }
    
    private func restorePurchases() {
        SKPaymentQueue.default().restoreCompletedTransactions()
    }
}

// MARK: Public methods
extension InAppPurchaseHandler {
    func purchase(product: SKProduct, completion: @escaping ((String, SKProduct?, SKPaymentTransaction?)->Void)) {
        purchaseProductcompletion = completion
        productToPurchase = product
        if canMakePurchases() {
            let payment = SKPayment(product: product)
            SKPaymentQueue.default().add(self)
            SKPaymentQueue.default().add(payment)
            productID = product.productIdentifier
        } else {
            completion("In app purchases are disabled", nil, nil)
        }
    }
    
    func fetchAvailableProducts(completion: @escaping (([SKProduct]?,String?) -> Void)){
        fetchProductcompletion = completion
        
        if productIds.isEmpty {
            fatalError("Product Ids are not found")
        } else {
            productsRequest = SKProductsRequest(productIdentifiers: Set(productIds))
            productsRequest.delegate = self
            productsRequest.start()
        }
    }
}

// MARK: SKProductsRequestDelegate
extension InAppPurchaseHandler: SKProductsRequestDelegate, SKPaymentTransactionObserver {
    
    func productsRequest (_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        print("response count = \(response.products.count)")
        print("error is \(response)")
        for item in response.products {
            print("Response id =\(item.productIdentifier)")
        }
        
        if response.products.count > 0, let completion = fetchProductcompletion {
            completion(response.products, nil)
        } else if let responseData = fetchProductcompletion{
            responseData(nil,"invalid invalidProductIdentifiers names as \(response.invalidProductIdentifiers.joined(separator: " "))")
        } else {
            print("Invalid Product Identifiers: \(response.invalidProductIdentifiers)")
        }
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions {
            switch transaction.transactionState {
            case .failed:
                print("Product purchase failed")
                
                if let completion = purchaseProductcompletion {
                    completion(InAppPurchaseMessages.failed.rawValue, nil, nil)
                }
                SKPaymentQueue.default().finishTransaction(transaction)
                break
            case .purchased:
                print("Product purchase done")
            
                if let completion = purchaseProductcompletion {
                    completion(InAppPurchaseMessages.purchased.rawValue, productToPurchase, transaction)
                }
                
                SKPaymentQueue.default().finishTransaction(transaction)
                break
            case .restored:
                print("Product purchase restored")
               // SKPaymentQueue.default().finishTransaction(transaction)
                
                if let completion = purchaseProductcompletion {
                    completion(InAppPurchaseMessages.restored.rawValue, productToPurchase, transaction)
                }
                break
            case .purchasing:
                print("Product is in purchasing")
               // SKPaymentQueue.default().finishTransaction(transaction)
                
                if let completion = purchaseProductcompletion {
                    completion(InAppPurchaseMessages.purchasing.rawValue, productToPurchase, transaction)
                }
                break
            case .deferred:
                print("Product purchase deferred")
                //SKPaymentQueue.default().finishTransaction(transaction)
                
                if let completion = purchaseProductcompletion {
                    completion(InAppPurchaseMessages.deferred.rawValue, productToPurchase, transaction)
                }
                break
            default:
                if let completion = purchaseProductcompletion {
                    completion(transaction.error?.localizedDescription ?? "Something went wrong", nil, nil)
                }
                break
            }
        }
    }
    
    func request(_ request: SKRequest, didFailWithError error: Error) {
        if let responseData = fetchProductcompletion {
            responseData(nil,error.localizedDescription)
        }
    }
}
