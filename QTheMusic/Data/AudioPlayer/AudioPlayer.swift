//
//  RecordingPlayer.swift
//  CPB
//
//  Created by macbook on 07/07/2021.
//  Copyright © 2021 TechSwivel. All rights reserved.
//

import Foundation
import AVFoundation
import MediaPlayer
import UIKit
import FileProvider


protocol AudioPlayerPlayProtocol{
    func setDelegate(delegate:AudioPlayerPlayDelegate?)
}
protocol AudioPlayerPlayDelegate {
    func playerIsPlaying()
    func playerCatchedException()
    func isUpdatingSeekBarView(currentTime:TimeInterval)
    func stopUpdatingSeekBarView()
    func applicationWillEnterBackgroundObs()
    func applicationWillBecomeActiveObs()
    func onStateChange(isPlaying:Bool,imageName:String)
    func onNextOrPreviousSong(song:Song)
}

class AudioPlayer{
    
    
    internal var displayLink : CADisplayLink?
    internal var aVPlayer: AVAudioPlayer?
    internal var downloadTask:URLSessionDownloadTask?
    internal var nowPlayingInfo:[String : Any]?
    internal var playingFileName :String?
    internal var PlayingFileImage :UIImage?
    internal var playingAlbumName:String?
    private var delegate : AudioPlayerPlayDelegate?
    internal var songsArray:[Song]?
    internal var indexPath:Int?
    private var mSongAndArtistViewModel = SongAndArtistViewModel()
    internal var isPlayingFrom:String?
    private let commandCenter = MPRemoteCommandCenter.shared()
    
    
    
    static var shared: AudioPlayer = {
        let instance = AudioPlayer()
        return instance
    }()
    
    init(){
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(applicationWillEnterBackground(notification:)),
                                               name: UIApplication.didEnterBackgroundNotification,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector:#selector(applicationWillBecomeActive(notification:)),
                                               name: UIApplication.didBecomeActiveNotification,
                                               object: nil)
        
        
        UIApplication.shared.beginReceivingRemoteControlEvents()
        
        self.setPlaybacktrue()
        self.setupNotificationView()
        self.setUpMediaPlayerNotificationView()
   
        self.bindingServerResponse()
        self.bindingServerErrorResponse()
        self.bindingServerCompletionResponse()
        
        
    }
    @objc func applicationWillBecomeActive(notification: Notification) {
        delegate?.applicationWillBecomeActiveObs()
    }
    @objc func applicationWillEnterBackground(notification: Notification) {
        delegate?.applicationWillEnterBackgroundObs()
    }
    func removeObserver(){
        NotificationCenter.default.removeObserver(self)
        
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    @objc func update() {
        
        
        let currentTime = self.aVPlayer?.currentTime ?? 0
        
        
        delegate?.isUpdatingSeekBarView(currentTime: currentTime)
        
        if UserDefaultUtils.getBool(key: StringConstant.resumeplayeratCurrentPositionKey) == false{
            UserDefaultUtils.setInteger(value: Int(currentTime), key: StringConstant.audioCurrentStatusBarStatusKey)
            UserDefaultUtils.setInteger(value: Int(aVPlayer?.duration ?? 0), key: StringConstant.audioFullTimeKey)
        
    }
        if self.aVPlayer?.isPlaying ?? false{
            //Let it play
            
        
        }else{
            
            
            let currentTimes = Double(aVPlayer?.currentTime ?? 0.0)
            
            if currentTimes == 0.0{
                print("zero current time")
                self.setPlaybackFalse()
                
                UserDefaultUtils.setInteger(value: Int(currentTimes), key: StringConstant.audioCurrentStatusBarStatusKey)
                
                UserDefaultUtils.setInteger(value: Int(aVPlayer?.duration ?? 0), key: StringConstant.audioFullTimeKey)
                
                self.aVPlayer?.stop()
             
                delegate?.stopUpdatingSeekBarView()
                
                
                if self.displayLink != nil{
                    self.displayLink?.invalidate()
                }
                
                self.setPlaybackFalse()
                
                nowPlayingInfo?[MPNowPlayingInfoPropertyPlaybackRate] = 0
                nowPlayingInfo?[MPMediaItemPropertyPlaybackDuration] = 0
                nowPlayingInfo?[MPNowPlayingInfoPropertyElapsedPlaybackTime] = 0
                nowPlayingInfo?[MPMediaItemPropertyTitle] = StringConstant.stopped
                
                MPNowPlayingInfoCenter.default().nowPlayingInfo = nowPlayingInfo
            }
            
           
           
           
            
        }
        
        
    }
    
    func downloadFileFromURL(url:NSURL,_ isNextSongDisable:Bool){
        
        self.downloadTask?.cancel()
        self.stopPlaying()
        
        
        self.downloadTask = URLSession.shared.downloadTask(with: url as URL, completionHandler: { [weak self](URL, response, error) -> Void in
            
            
            if error != nil && error?.localizedDescription != "cancelled"{
                print(error)
                DispatchQueue.main.async {
                    self?.delegate?.playerCatchedException()
                    return
                }
            }
            if let url = URL{
                
                self?.play(url: url as NSURL, isNextSongDisable )
                
            }
            
        })
        self.downloadTask?.resume()
    }
    func play(url:NSURL,_ isNextSongDisable:Bool) {
        
        
        
        
        do {
            
            self.aVPlayer = try AVAudioPlayer(contentsOf: url as URL)
            self.aVPlayer?.prepareToPlay()
            self.aVPlayer?.play()
            DispatchQueue.main.async {
                UIApplication.shared.beginReceivingRemoteControlEvents()
                let session = AVAudioSession.sharedInstance()
                do{
                    //session for background playing
                    try session.setCategory(AVAudioSession.Category.playback)
                }
                catch{
                    print(error.localizedDescription,"ERROR")
                }
                
                
                self.setPlaybacktrue()
                
                
                self.setupNotificationView()
                
                self.delegate?.playerIsPlaying()
                if isNextSongDisable{
                    self.diableNextPreviousBtn()

                }else{

                    self.enableNextPreviousBtn()

                }
                
                if self.displayLink != nil{
                    self.displayLink?.invalidate()
                    
                }
                
                self.displayLink = CADisplayLink(target: self,
                                                 selector: #selector(self.update))
                self.displayLink?.add(to: .current, forMode: .common)
                
            }
        } catch let error as NSError {
            
            DispatchQueue.main.async {
                self.setPlaybackFalse()
                self.delegate?.playerCatchedException()
                print("AVAudioPlayer init failed")
                print(error.localizedDescription)
                
                
            }
            
        }
        
    }
    func setPlaybackFalse()
    {
        do {
            try AVAudioSession.sharedInstance().setActive(false)
        }catch let error as NSError{
            print(error.localizedDescription,"error in setting active")
        }
    }
    func setPlaybacktrue()
    {
        do {
            try AVAudioSession.sharedInstance().setActive(true)
        }catch let error as NSError{
            print(error.localizedDescription,"error in setting active")
        }
    }
    
    func stopPlaying(){
        
        
        if displayLink != nil{
            displayLink?.invalidate()
            
        }
        
        if   aVPlayer?.isPlaying ?? false{
            
            aVPlayer?.stop()
            
            
            
            
        }
        if aVPlayer != nil{
            aVPlayer = nil
        }
        self.setPlaybackFalse()
        UIApplication.shared.endReceivingRemoteControlEvents()
        
        MPNowPlayingInfoCenter.default().nowPlayingInfo = [:]
        
        
        
    }
    
    func pausePlaying(){
        self.setPlaybackFalse()
        if displayLink != nil{
            displayLink?.invalidate()
            
        }
        if   aVPlayer?.isPlaying ?? false{
            aVPlayer?.pause()
            
            delegate?.onStateChange(isPlaying: false, imageName: StringConstant.playImage)
            
        }
    }
    
    func instentiateCDisplayLink(){
        displayLink = CADisplayLink(target: self,selector: #selector(AudioPlayer.shared.update))
        displayLink?.add(to: .current, forMode: .common)
    }
    
    
    func diableNextPreviousBtn()
    {
       
        commandCenter.skipBackwardCommand.isEnabled = true
        commandCenter.skipForwardCommand.isEnabled = true
        commandCenter.nextTrackCommand.isEnabled = false
        commandCenter.previousTrackCommand.isEnabled = false
    }
    func enableNextPreviousBtn()
    {
       
        
        
       
        
        commandCenter.skipBackwardCommand.isEnabled = true
        commandCenter.skipForwardCommand.isEnabled = true
        
        commandCenter.nextTrackCommand.isEnabled = true
        commandCenter.previousTrackCommand.isEnabled = true
    }
    func setUpMediaPlayerNotificationView(){
        UIApplication.shared.beginReceivingRemoteControlEvents()
        
        
       
        
        commandCenter.skipBackwardCommand.isEnabled = false
        commandCenter.skipForwardCommand.isEnabled = false
        commandCenter.nextTrackCommand.isEnabled = true
        commandCenter.previousTrackCommand.isEnabled = true
        
        commandCenter.playCommand.addTarget { [unowned self] event in
            if aVPlayer != nil{
                print("command center play")
                self.aVPlayer?.play()
                var title = ""
                if UserDefaultUtils.isValueNil(key: StringConstant.titleOfAudioKey) != nil{
                    title = UserDefaultUtils.getString(key: StringConstant.titleOfAudioKey)
                }else{
                    title = playingFileName ?? ""
                }
                
                self.syncMediaCenterRemoteAudioDetails(title :title,Rate:1)
                
                
                delegate?.onStateChange(isPlaying: true, imageName: StringConstant.pauseImage)
                
                if displayLink != nil{
                    displayLink?.invalidate()
                    
                }
                self.setPlaybacktrue()
                self.instentiateCDisplayLink()
                print("Success in mediaCenter")
                return .success
            }
            return .commandFailed
        }
        
        commandCenter.pauseCommand.addTarget { [unowned self] event in
            if aVPlayer != nil{
                print("command center paused")
                self.aVPlayer?.pause()
                self.syncMediaCenterRemoteAudioDetails(title :StringConstant.paused,Rate:0)
                
                setPlaybackFalse()
                print("Paused media centre")
                
                delegate?.onStateChange(isPlaying: false, imageName: StringConstant.playImage)
                return .success
            }
            return .commandFailed
        }
        
        commandCenter.nextTrackCommand.addTarget { [unowned self] event in
            if aVPlayer != nil{
                self.playNextSong(playNext: false)
                return .success
            }
            return .commandFailed
        }
        commandCenter.previousTrackCommand.addTarget { [unowned self] event in
            if aVPlayer != nil{
                
                self.playPreviousSong()
                
                return .success
            }
            return .commandFailed
        }
    }
    func setupNotificationView(){
        self.nowPlayingInfo = [String : Any]()
        if UserDefaultUtils.isValueNil(key: StringConstant.titleOfAudioKey) != nil{
            let title = UserDefaultUtils.getString(key: StringConstant.titleOfAudioKey)
            nowPlayingInfo?[MPMediaItemPropertyTitle] = title
            
        }
        else{
            nowPlayingInfo?[MPMediaItemPropertyTitle] = playingFileName
        }
        
        nowPlayingInfo?[MPMediaItemPropertyAlbumTitle] = playingAlbumName
        nowPlayingInfo?[MPMediaItemPropertyPlaybackDuration] = aVPlayer?.duration
        nowPlayingInfo?[MPNowPlayingInfoPropertyElapsedPlaybackTime] = aVPlayer?.currentTime
        
        nowPlayingInfo?[MPNowPlayingInfoPropertyPlaybackRate] = 1
        let coverPhoto = PlayingFileImage
        
        self.nowPlayingInfo?[MPMediaItemPropertyArtwork] =  MPMediaItemArtwork(boundsSize: coverPhoto?.size ?? CGSize(), requestHandler: { (size) -> UIImage in
            
            guard let image = coverPhoto else  {return UIImage()}
            
            return image
            
        })
       
        MPNowPlayingInfoCenter.default().nowPlayingInfo = nowPlayingInfo
        
    }
    func syncSeekBarTimeInRemoteControlCenter(){
        if self.aVPlayer != nil {
            self.nowPlayingInfo?[MPMediaItemPropertyPlaybackDuration] =  AudioPlayer.shared.aVPlayer?.duration
            self.nowPlayingInfo?[MPNowPlayingInfoPropertyElapsedPlaybackTime] =  aVPlayer?.currentTime
            MPNowPlayingInfoCenter.default().nowPlayingInfo = AudioPlayer.shared.nowPlayingInfo
        }
    }
    
    func syncMediaCenterRemoteAudioDetails(title :String,Rate:Int){
        nowPlayingInfo?[MPMediaItemPropertyTitle] = title
        nowPlayingInfo?[MPNowPlayingInfoPropertyPlaybackRate] = Rate
        nowPlayingInfo?[MPMediaItemPropertyPlaybackDuration] = aVPlayer?.duration
        nowPlayingInfo?[MPNowPlayingInfoPropertyElapsedPlaybackTime] = aVPlayer?.currentTime
        MPNowPlayingInfoCenter.default().nowPlayingInfo = nowPlayingInfo
    }
    
    
    fileprivate func getMoreSongs(_ currentSongArrayCount: Int, _ index: Int) {
        let countLessThenThree =  (currentSongArrayCount - 3)
        
        if index >= countLessThenThree{
            //Call api in delegate
            self.hitApiforNextSongs()
        }
    }
    
    func playNextSong(playNext:Bool){
        
        guard let currentIndex = self.indexPath else{return}
        guard let currentSongArrayCount = self.songsArray?.count else{return}
        
        if let songsArray = songsArray {
            for (index, element) in songsArray.enumerated() {
                       if index > currentIndex {
                           if songsArray[index].songStatus != SongStatus.PREMIUM.rawValue{
                               
                               self.indexPath = index
                               
                               
                               self.getMoreSongs(currentSongArrayCount, index)
                               
                               
                               if let url = NSURL(string: self.songsArray?[index].songAudioURL ?? "" ),let imageUrl = URL(string:self.songsArray?[index].coverImageURL ?? ""){
                                   
                                   if playNext{
                                       self.downloadFileFromURL(url: url, false)
                                   }else{
                                   //if playing then play other wise save onky
                                   if aVPlayer?.isPlaying ?? false{
                                   self.downloadFileFromURL(url: url, false)
                                   }else{
                                       AudioPlayer.shared.stopPlaying()
                                       
                                      
                                           UserDefaultUtils.setInteger(value: 0, key: StringConstant.audioCurrentStatusBarStatusKey)
                                           
                                   }
                               }
                                   
                                   self.delegate?.onNextOrPreviousSong(song: self.songsArray?[index] ?? Song())
                                   self.playingFileName = self.songsArray?[index].songTitle
                                   self.playingAlbumName = self.songsArray?[index].albumTitle
                                   self.setUpMediaPlayerNotificationView()
                                   
                                   self.getData(from: imageUrl ) { data, response, error in
                                           guard let data = data, error == nil else { return }
                                           DispatchQueue.main.async() { [weak self] in
                                               self?.PlayingFileImage = UIImage(data: data)
                                           }
                                   }
                                   
                                   
                                   
                               }
                               
                               break;
                           }
                       }
                   }
        }
    }
    
    
    func playPreviousSong(){
        guard let currentIndex = self.indexPath else{return}
        if let songsArray = songsArray {
            if currentIndex > 0{
            for index in (0...currentIndex-1).reversed() {
                
                       
                           if songsArray[index].songStatus != SongStatus.PREMIUM.rawValue{
                               
                               self.indexPath = index
                               
                               if let url = NSURL(string: self.songsArray?[index].songAudioURL ?? "" ), let imageUrl = URL(string:self.songsArray?[index].coverImageURL ?? "") {
                                  
                                   //if playing then play other wise save onky
                                   if aVPlayer?.isPlaying ?? false{
                                   self.downloadFileFromURL(url: url, false)
                                   }else{
                                       AudioPlayer.shared.stopPlaying()
                                      
                                           UserDefaultUtils.setInteger(value: 0, key: StringConstant.audioCurrentStatusBarStatusKey)
                                   }
                                   
                                   self.delegate?.onNextOrPreviousSong(song: self.songsArray?[index] ?? Song())
                                   self.playingFileName = self.songsArray?[index].songTitle
                                   self.playingAlbumName = self.songsArray?[index].albumTitle
                                   self.setUpMediaPlayerNotificationView()
                                   
                                   self.getData(from: imageUrl ) { data, response, error in
                                           guard let data = data, error == nil else { return }
                                           DispatchQueue.main.async() { [weak self] in
                                               self?.PlayingFileImage = UIImage(data: data)
                                           }
                                   }
                               }
                               break;
                           }
                    }
            }
         }
    }
    
    internal func playerResumeSetting() {
        UserDefaultUtils.setBool(value: false, key: StringConstant.isPlayingKey)
        UserDefaultUtils.setBool(value: false, key: StringConstant.ResumingMiniPlayerKey)
    }
    
    func hitApiforNextSongs(){
        ///Taking last sentence from isPlayingFrom
        let stringSeperator = self.isPlayingFrom?.components(separatedBy: " ")
        let playedFromType = stringSeperator?.last
        
        let builder = SongAndArtistBuilder()
        
        if playedFromType ==  SongType.ALBUM.rawValue
        {
            builder.setPlayedForm(SongType.ALBUM.rawValue)
            builder.setalbumId(self.songsArray?[indexPath ?? 0].albumID ?? 0)
            builder.setCurrentSongId(self.songsArray?[indexPath ?? 0].songID ?? 0)
            self.mSongAndArtistViewModel.playNextSong(playedFrom: builder.build())
        }
        else if playedFromType == SongType.SEARCHED.rawValue
        {
            builder.setPlayedForm(SongType.SEARCHED.rawValue)
            builder.setCurrentSongId(self.songsArray?[indexPath ?? 0].songID ?? 0)
            self.mSongAndArtistViewModel.playNextSong(playedFrom: builder.build())
        }
        else if playedFromType == SongType.CATEGORY.rawValue
        {
            builder.setPlayedForm(SongType.CATEGORY.rawValue)
            builder.setCategoryId(self.songsArray?[indexPath ?? 0].categoryID ?? 0)
            builder.setCurrentSongId(self.songsArray?[indexPath ?? 0].songID ?? 0)
            self.mSongAndArtistViewModel.playNextSong(playedFrom: builder.build())
            
        }
        else if playedFromType == SongType.FAVOURITE.rawValue
        {
            builder.setPlayedForm(SongType.FAVOURITE.rawValue)
            builder.setCurrentSongId(self.songsArray?[indexPath ?? 0].songID ?? 0)
            self.mSongAndArtistViewModel.playNextSong(playedFrom: builder.build())
            
        }
        else if playedFromType == SongType.PURCHASED_SONG.rawValue {
            builder.setPlayedForm(SongType.PURCHASED_SONG.rawValue)
            builder.setCurrentSongId(self.songsArray?[indexPath ?? 0].songID ?? 0)
            self.mSongAndArtistViewModel.playNextSong(playedFrom: builder.build())
        }
        else if playedFromType == SongType.PURCHASED_ALBUM.rawValue
        {
            builder.setPlayedForm(SongType.PURCHASED_ALBUM.rawValue)
            builder.setalbumId(self.songsArray?[indexPath ?? 0].albumID ?? 0)
            builder.setCurrentSongId(self.songsArray?[indexPath ?? 0].songID ?? 0)
            self.mSongAndArtistViewModel.playNextSong(playedFrom: builder.build())
        }
        else if playedFromType == SongType.DOWNLOADED.rawValue
        {
            builder.setPlayedForm(SongType.DOWNLOADED.rawValue)
            builder.setCurrentSongId(self.songsArray?[indexPath ?? 0].songID ?? 0)
            self.mSongAndArtistViewModel.playNextSong(playedFrom: builder.build())
        }
        else if playedFromType == SongType.PLAYLIST.rawValue
        {
            builder.setPlayedForm(SongType.PLAYLIST.rawValue)
            builder.setCategoryId(self.songsArray?[indexPath ?? 0].playListID ?? 0)
            builder.setCurrentSongId(self.songsArray?[indexPath ?? 0].songID ?? 0)
            self.mSongAndArtistViewModel.playNextSong(playedFrom: builder.build())
        }
    }
    
    private func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
}

extension AudioPlayer : AudioPlayerPlayProtocol{
    func setDelegate(delegate: AudioPlayerPlayDelegate?) {
        if let Delegate = delegate{
            self.delegate  = Delegate
        }
    }
    
    
}
extension AudioPlayer{
    
    
    
     
     private func bindingServerResponse() {
         self.mSongAndArtistViewModel.serverResponseForPlayNextSong.bind {
             if ($0 != nil) {
                 
                 if $0?.response?.status ?? false {
                     
                     if let newSongArray = $0?.response?.data?.songs{
                     for item in newSongArray{
                         
                     self.songsArray?.append(item)
                     }
                     }
                 }
                 else if $0?.response?.status == false {
                    
                    
                     
                 }
             }
         }
     }
     
     //MARK: - Binding Error Response
     private func bindingServerErrorResponse() {
         self.mSongAndArtistViewModel.errorResponseForPlayNextSong.bind {
             if ($0 != nil) {
                
             }
             
            
         }
         
         
     }
     
     //MARK: - Binding Complete Response
     private func bindingServerCompletionResponse(){
         self.mSongAndArtistViewModel.completion.bind {
             if ($0 != nil) {
                 
                 
             }
         }
         
     }
    
}
