//
//  RealmManager.swift
//  CPB
//
//  Created by Mac on 14/01/2021.
//  Copyright © 2021 TechSwivel. All rights reserved.
//

import Foundation
import RealmSwift
import Realm
class RealmManager: BaseViewModel {
    
    var serverResponseForSyncRecentPlayed: Box<ResponseMain?> = Box(nil)
    var errorResponseForSyncRecentPlayed: Box<ResponseMain?> = Box(nil)
    
    /// don't allow main thread queries.
    
    var realm = try? Realm()
    
    override init() {
        do{
            
            let documentDirectory = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask,
                                                                appropriateFor: nil, create: false)
            
            
            let url = documentDirectory.appendingPathComponent(ProjectConstants.current.getDatabaseName())
            
            
            let config = Realm.Configuration(fileURL: url,schemaVersion: 1,deleteRealmIfMigrationNeeded: true)
            //            let config = Realm.Configuration(fileURL: url,schemaVersion: 1,migrationBlock:{
            //                migration,oldversion in
            //                // this will implement at the final milestone
            //            })
            self.realm = try Realm(configuration: config)
        }catch{
            print(StringConstant.FailedDatabaseMessage + "\(error)")
        }
    }
    
    func insertSyncRecentPlayedToDB(songs: [Song]?, albums: [Album]? , artists: [Artist]?) {
        
        guard let song = songs else {return}
        guard let album = albums else {return}
        guard let artist = artists else {return}
        do {
            try realm?.write {
                realm?.add(song, update: Realm.UpdatePolicy.modified)
                realm?.add(album, update: Realm.UpdatePolicy.modified)
                realm?.add(artist, update: Realm.UpdatePolicy.modified)
            }
        }
         catch let error as NSError{
            print(error.localizedDescription,"cannot add categories to db")
        }
    }
    
    func insertCategorieToDb(categories:[Category]?){
        
        
        
        
       
        
        do {
            try realm?.write {
                guard let categoriesArray = categories else { return  }
                realm?.add(categoriesArray, update: Realm.UpdatePolicy.modified)
                
            }
            
            
            
        }
        catch let error as NSError{
            
            print(error.localizedDescription,"cannot add categories to db")
            
        }
        
        
    }
    func setUpRecentlyPlayedDB(){
        
        let songs = realm?.objects(Song.self)
        let artists = realm?.objects(Artist.self)
        let albums = realm?.objects(Album.self)
        
        
        if songs?.count ?? 0 > 0 && artists?.count ?? 0 > 0 && albums?.count ?? 0 > 0{
            
        }else{
            guard let songsArray = mDataManager.getSongsDataFromDb() else { return }
            guard let artistArray = mDataManager.getArtistsDataFromDb()  else { return }
            guard let albumArray = mDataManager.getAlbumsDataFromDb() else { return }
            
            do {
                try realm?.write {
                    realm?.add(songsArray, update: Realm.UpdatePolicy.modified)
                    realm?.add(artistArray, update: Realm.UpdatePolicy.modified)
                    realm?.add(albumArray, update: Realm.UpdatePolicy.modified)
                    print( (String(describing: realm?.configuration.fileURL?.absoluteString.description)))
                }
                
                
            }
            
            catch let error as NSError{
                
                print(error.localizedDescription,"cannot add songs db")
                
            }
            
        }
    }
    
    
    func getCategoriesFromDb()->[Category]?{
        let data = realm?.objects(Category.self)
        
        if data?.count ?? 0 > 0 {
            if let DetachedData = data?.detached{
                
                return DetachedData
            }
            
        }
        return nil
    }
    func getSongsDataFromDb()->[Song]?{
        
        let data = realm?.objects(Song.self).sorted(byKeyPath: "timeStamp", ascending: false)
        
        if let DetachedData = data?.detached{
            
            return DetachedData
        }
        return nil
    }
    
    func getArtistsDataFromDb()->[Artist]?{
        
        let data = realm?.objects(Artist.self).sorted(byKeyPath: "timeStamp", ascending: false)
        
        
        if let DetachedData = data?.detached{
            return DetachedData
        }
        return nil
    }
    
    func getAlbumDataByIdFromDb(albumId: Int?) -> Album? {
       
        if let album = getAlbumsDataFromDb() {
            for item in album {
                if item.albumID == albumId {
                    return item
                }
            }
        }
        return nil
    }
    
    func getArtistDataByIdFromDb(artistId: Int?) -> Artist? {
       
        if let artist = getArtistsDataFromDb() {
            for item in artist {
                if item.artistID == artistId {
                    return item
                }
            }
        }
        return nil
    }
    
    
    func getAlbumsDataFromDb()->[Album]?{
        
        let data = realm?.objects(Album.self).sorted(byKeyPath: "timeStamp", ascending: false)
        
        
        if let DetachedData = data?.detached{
            return DetachedData
        }
        return nil
    }
    
    func insertSongIntoDb(song:Song?){
       
        do {
            try realm?.write {
                guard let Song = song else{return }
                Song.timeStamp = Date().timeIntervalSince1970
                realm?.add(Song, update: Realm.UpdatePolicy.modified)
                
            }
            
            
        }
        
        catch let error as NSError{
            
            print(error.localizedDescription,"cannot add songs db")
            
        }
    }
    
    func insertAlbumIntoDb(album:Album?){
        
        
        do {
            try realm?.write {
                guard let Album = album else{return }
                Album.timeStamp = Date().timeIntervalSince1970
                realm?.add(Album, update: Realm.UpdatePolicy.modified)
                
            }
            
            
        }
        
        catch let error as NSError{
            
            print(error.localizedDescription,"cannot add album db")
            
        }
    }
    
    func insertArtistIntoDb(artist:Artist?){
        
        
        do {
            try realm?.write {
                guard let Artist = artist else{return }
                Artist.timeStamp = Int(Date().timeIntervalSince1970)
                realm?.add(Artist, update: Realm.UpdatePolicy.modified)
                
            }
            
            
        }
        
        catch let error as NSError{
            
            print(error.localizedDescription,"cannot add album db")
            
        }
    }
    
    func setIsFavourite(songObj:Song)->Song?{
        
         do {
             try realm?.write {
                 
                 songObj.isFavourit?.toggle()
               
             }
             
             return songObj
         }
         
         catch let error as NSError{
             
             print(error.localizedDescription,"cannot add update is favrt")
             
         }
        return nil
    }
    
    func insertLocalPathForMedia(song:Song,FilePath:String){
        
        do {
            try realm?.write {
                song.localPath = FilePath
                realm?.add(song, update: Realm.UpdatePolicy.modified)
            }
        }
        
        catch let error as NSError{
            print(error.localizedDescription,"cannot add Local path to Database")
        }
    }
    
    // Deletions Methods
    func deleteSongsDataFromDb()->[Song]?{
        
        let data = realm?.objects(Song.self).sorted(byKeyPath: "timeStamp", ascending: false)
        
        if let DetachedData = data?.detached{
            
            return DetachedData
        }
        return nil
    }
    
    func deleteArtistsDataFromDb()->[Artist]?{
        
        let data = realm?.objects(Artist.self).sorted(byKeyPath: "timeStamp", ascending: false)
        
        
        if let DetachedData = data?.detached{
            return DetachedData
        }
        return nil
    }
    
    func deleteAlbumsDataFromDb()->[Album]?{
        
        let data = realm?.objects(Album.self).sorted(byKeyPath: "timeStamp", ascending: false)
        
        
        if let DetachedData = data?.detached{
            return DetachedData
        }
        return nil
    }
}

