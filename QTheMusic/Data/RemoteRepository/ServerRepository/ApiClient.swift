//
//  ApiClient.swift
//  RexSwiftTest
//
//  Created by Nasar Iqbal on 3/21/20.
//  Copyright © 2020 Nasar Iqbal. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire
import Siren
import SkeletonView

class Apiclient:BaseApiClient {
    
    static func updateProfile(data:Authorization) -> Observable<ResponseMain>{
        var parameters : [String:NSObject] = [String:NSObject]()
        parameters[Constants.Parameters.name] = data.name as? NSObject
        parameters[Constants.Parameters.dOb] = data.dOb as? NSObject
        parameters[Constants.Parameters.address] = data.address as? NSObject
        parameters[Constants.Parameters.city] = data.city as? NSObject
        parameters[Constants.Parameters.state] = data.state as? NSObject
        parameters[Constants.Parameters.country] = data.country as? NSObject
        parameters[Constants.Parameters.zipCode] = data.zipCode as? NSObject
        parameters[Constants.Parameters.isEnableNotification] = data.isEnableNotification as? NSObject
        parameters[Constants.Parameters.isArtistUpdateEnable] = data.isArtistUpdateEnable  as? NSObject
        
        var imageProfile :[String:UIImage] = [String:UIImage]()
        imageProfile[Constants.Parameters.profile] = data.profileImage
        
        return requestImageUpload(imageData: imageProfile, parameters: parameters, urlConvertible: ApiRouter.updateProfile(jwt: ""))
        
    }
    static func savePlayist(title: String) -> Observable<ResponseMain> {
        return request(urlConvertible: ApiRouter.savePlaylist(title: title))
    }
    static func buyingHistory(data: Song, page: Int, limit: Int) -> Observable<ResponseMain> {
        return request(urlConvertible: ApiRouter.buyingHistory(data: data, page: page, limit: limit))
    }
    static func getPlaylist(page: Int, limit: Int) -> Observable<ResponseMain> {
        return request(urlConvertible: ApiRouter.getPlaylist(page: page, limit: limit))
    }
    static func deletePlaylist(playlistId: Int) -> Observable<ResponseMain> {
        return request(urlConvertible: ApiRouter.deletePlaylist(playlistId: playlistId))
    }
    static func updatePlaylist(data: Song) -> Observable<ResponseMain> {
        return request(urlConvertible: ApiRouter.updatePlaylist(data: data))
    }
    static func login(data:Authorization) -> Observable<ResponseMain>{
        return request(urlConvertible: ApiRouter.login(data: data))
    }
    static func sendOtp(data:Authorization) -> Observable<ResponseMain>{
        return request(urlConvertible: ApiRouter.sendOtp(data: data))
    }
    
    static func verifyOtp(data:Authorization) -> Observable<ResponseMain>{
        return request(urlConvertible: ApiRouter.verifyOtp(data: data))
    }
    
    static func signUp(data: Authorization) -> Observable<ResponseMain> {
        
        if data.profileImage != nil {
            var params: [String: NSObject] = [:]
            
            if let obj = data.deviceName {
                params[Constants.Parameters.deviceName] = obj as NSObject?
            }
            
            if let obj = data.deviceIdentifier {
                params[Constants.Parameters.deviceIdentifier] = obj as NSObject?
            }
            
            if let obj = data.name {
                params[Constants.Parameters.name] = obj as NSObject?
                
            }
            if let obj = data.email {
                params[Constants.Parameters.email] = obj as NSObject?
                
            }
            if let obj = data.password {
                params[Constants.Parameters.password] = obj as NSObject?
                
                
            }
            if let obj = data.dOb {
                params[Constants.Parameters.dOb] = obj as NSObject?
                
            }
            
            if data.gender != nil{
                if let obj = data.gender {
                    params[Constants.Parameters.gender] = obj as NSObject?
                }
            }
            
            if data.address != nil{
                if let obj = data.address {
                    params[Constants.Parameters.address] = obj as NSObject?
                    
                }
            }
            
            if data.city != nil {
                if let obj = data.city {
                    params[Constants.Parameters.city] = obj as NSObject?
                    
                }
            }
            
            if data.state != nil {
                if let obj = data.state {
                    params[Constants.Parameters.state] = obj as NSObject?
                    
                }
            }
            
            if data.country != nil {
                if let obj = data.country {
                    params[Constants.Parameters.country] = obj as NSObject?
                    
                }
            }
            
            if data.zipCode != nil {
                if let obj = data.zipCode {
                    params[Constants.Parameters.zipCode] = obj as NSObject?
                    
                }
            }
           
            
            if let obj = data.fcmToken {
                params[Constants.Parameters.fcmToken] = obj as NSObject?
                
            }
            if let obj = data.deviceIdentifier {
                params[Constants.Parameters.deviceIdentifier] = obj as NSObject?
                
            }
            
            if data.socialId != nil {
                if let obj = data.socialId {
                    params[Constants.Parameters.socialId] = obj as NSObject?
                }
            }
            
            if data.socialSite != nil {
                if let obj = data.socialSite {
                    params[Constants.Parameters.socialSite] = obj as NSObject?
                }
            }
           
            var imageList:[String:UIImage] = [String:UIImage]()
            imageList[Constants.Parameters.profile] =  data.profileImage
            
            return requestImageUpload(imageData: imageList, parameters: params, urlConvertible: ApiRouter.signUpWithProfile(jwt: ""))
        } else {
            
            
            return request(urlConvertible: ApiRouter.signUpWithoutProfile(data: data))
        }
    }
    
    static func resetPassword(data:Authorization)->Observable<ResponseMain>{
        return request(urlConvertible: ApiRouter.resetPassword(data: data))
    }
    
    //MARK: Logout
    static func logout(data: Authorization) ->Observable<ResponseMain> {
        return request(urlConvertible: ApiRouter.logout(data: data))
    }
    
    static func profileSetup(data:Authorization)->Observable<ResponseMain> {
        if data.profileImage != nil {
            var paremeter:[String:NSObject] = [String:NSObject]()
            if let obj = data.name{
                paremeter[Constants.Parameters.name] = obj  as NSObject?
            }
            if let obj = data.dOb{
                paremeter[Constants.Parameters.dOb] = obj  as NSObject?
            }
            if let obj = data.gender{
                paremeter[Constants.Parameters.gender] = obj  as NSObject?
            }
            if let obj = data.address{
                paremeter[Constants.Parameters.address] = obj  as NSObject?
            }
            if let obj = data.city{
                paremeter[Constants.Parameters.city] = obj  as NSObject?
            }
            if let obj = data.state{
                paremeter[Constants.Parameters.state] = obj  as NSObject?
            }
            if let obj = data.country{
                paremeter[Constants.Parameters.country] = obj  as NSObject?
            }
            if let obj = data.zipCode{
                paremeter[Constants.Parameters.zipCode] = obj  as NSObject?
            }
            
            if let obj = data.isEnableNotification{
                paremeter[Constants.Parameters.isEnableNotification] = obj  as NSObject?
                
            }
             
            if let obj = data.isArtistUpdateEnable{
                paremeter[Constants.Parameters.isArtistUpdateEnable] = obj as NSObject?
            }
            
            var imageList:[String:UIImage] = [String:UIImage]()
            imageList[Constants.Parameters.profile] =  data.profileImage
            return requestImageUpload(imageData: imageList, parameters: paremeter, urlConvertible: ApiRouter.profileSetUpWithAvatar(image: ""))
            
        }else{
            
            return request(urlConvertible: ApiRouter.profileSetUpWithoutAvatar(data: data))
            
        }
    }
    
    static func recommendedSongs(data: Song) ->Observable<ResponseMain> {
        return request(urlConvertible: ApiRouter.recommendedSongs(data: data))
    }
    
    static func songs(data:Song, page: Int, limit: Int)->Observable<ResponseMain> {
        return request(urlConvertible: ApiRouter.songs(data: data, page: page, limit: limit))
    }
    
    static func getCategories(categoryType:String?, page: Int, limit: Int)->Observable<ResponseMain>{
        
        return request(urlConvertible: ApiRouter.getCategories(categoryType: categoryType ?? CategoryType.ALL.rawValue, page: page, limit: limit))
    }
    
    static func saveInterest(categories:[Category]?)->Observable<ResponseMain> {
        
        var localOrderItems = [[String : Any]]()
        if let cat = categories{
            for items in cat{
                localOrderItems.append([Constants.Parameters.categoryId:items.categoryID])
            }}
        
        return request(urlConvertible: ApiRouter.saveInterest(interest: localOrderItems))
    }
    
    static func favouriteSong(data: Song) ->Observable<ResponseMain> {
        return request(urlConvertible: ApiRouter.favouriteSong(data: data))
    }
    
    static func searchSong(data:Song, page: Int, limit: Int)->Observable<ResponseMain>{
        return request(urlConvertible: ApiRouter.searchSong(data: data, page: page, limit: limit))
    }
    
    static func getArtitstDetail(artistDetail:Int, page: Int, limit: Int)->Observable<ResponseMain>{
        return request(urlConvertible: ApiRouter.artistDetail(artistId: artistDetail, page: page, limit: limit))
    }
    
    static func getPreOrderDetail(artistDetail:Int, page: Int, limit: Int)->Observable<ResponseMain>{
        return request(urlConvertible: ApiRouter.preOrderDetail(artistId: artistDetail, page: page, limit: limit))
    }
    
    static func followArtist(artistID:Int,isFollowed:Bool)->Observable<ResponseMain>
    {
        return request(urlConvertible: ApiRouter.followArtist(artistId: artistID, isFollowed: isFollowed))
    }
    static func getFollowingArtist()->Observable<ResponseMain>
    {
        return request(urlConvertible: ApiRouter.getFollowingArtist)
    }
    static func nextPlaySongs(data:Song, page:Int, limit:Int) -> Observable<ResponseMain> {
        return request(urlConvertible: ApiRouter.nextPlaySongs(data: data, page: page, limit: limit))
    }
    static func packagePlans(page:Int, limit:Int) -> Observable<ResponseMain> {
        return request(urlConvertible: ApiRouter.packagesPlan(page: page, limit: limit))
    }
    static func subscribeToPlan(data: Song) -> Observable<MainResponse> {
        return request(urlConvertible: ApiRouter.subscribleToPlan(data: data))
    }
    
    static func validateSubscriptionToken(token:String) -> Observable<AppStoreResponse>{
        return request(urlConvertible: ApiRouter.validateSubscription(purchaseToken: token))
    }
    
    static func syncRecentPlayed(isRecentlyPlayed: Bool, isListeningHistory: Bool, data: [Song], page: Int, limit: Int) -> Observable<ResponseMain> {
       
        var recentlyPlayedArray =  [[String:Any]]()
        var listeningHistoryArray = [[String:Any]]()
        
        for song in data {
            ///For artist album song
            if song.artistID != nil && song.albumID != nil{
                recentlyPlayedArray.append([
                    Constants.Parameters.artistId: song.artistID ?? 0,
                    Constants.Parameters.albumId: song.albumID ?? 0,
                    Constants.Parameters.songid: song.songID ?? 0
                ])
                listeningHistoryArray.append([
                    Constants.Parameters.artistId: song.artistID ?? 0,
                    Constants.Parameters.albumId: song.albumID ?? 0,
                    Constants.Parameters.songid: song.songID ?? 0
                ])
            }
            ///For artist song
            else if song.artistID != nil && song.albumID == nil{
                recentlyPlayedArray.append([
                    Constants.Parameters.artistId: song.artistID ?? 0,
                    Constants.Parameters.songid: song.songID ?? 0
                ])
                listeningHistoryArray.append([
                    Constants.Parameters.artistId: song.artistID ?? 0,
                    Constants.Parameters.songid: song.songID ?? 0
                ])
            }
            ///For album song
            else if song.albumID != nil && song.artistID == nil {
                recentlyPlayedArray.append([
                    Constants.Parameters.albumId: song.albumID ?? 0,
                    Constants.Parameters.songid: song.songID ?? 0
                ])
                listeningHistoryArray.append([
                    Constants.Parameters.albumId: song.albumID ?? 0,
                    Constants.Parameters.songid: song.songID ?? 0
                ])
            }
            ///For song
            else {
                recentlyPlayedArray.append([
                    Constants.Parameters.songid: song.songID ?? 0
                ])
                listeningHistoryArray.append([
                    Constants.Parameters.songid: song.songID ?? 0
                ])
            }
        }
        
        print("Recent Array: =\(recentlyPlayedArray) \n Listening Array: =\(listeningHistoryArray)")
        
        return request(urlConvertible: ApiRouter.syncRecentPlayed(isRecentlyPlayed: isRecentlyPlayed, isListeningHistory: isListeningHistory, recentlyPlayed: recentlyPlayedArray, listeningHistory: listeningHistoryArray, page: page, limit: limit))
    }
    
    static func syncData(isRecentlyPlayed: Bool, isListeningHistory: Bool, recentlyPlayed:[[String:Any]]?, listeningHistory: [[String:Any]]? , page: Int, limit: Int) -> Observable<ResponseMain>
    {
        return request(urlConvertible: ApiRouter.syncRecentPlayed(isRecentlyPlayed: isRecentlyPlayed, isListeningHistory: isListeningHistory, recentlyPlayed: recentlyPlayed ?? [[String:Any]](), listeningHistory: listeningHistory ?? [[String:Any]](), page: page, limit: limit))
    }
    
}
    


    
