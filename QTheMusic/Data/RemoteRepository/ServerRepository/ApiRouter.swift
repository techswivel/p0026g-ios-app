//
//  ApiRouter.swift
//  RexSwiftTest
//
//  Created by Nasar Iqbal on 3/21/20.
//  Copyright © 2020 Nasar Iqbal. All rights reserved.
//

import Foundation
import Alamofire
import Siren
import SkeletonView
enum ApiRouter: URLRequestConvertible {
    
    case login(data:Authorization)
    case sendOtp(data:Authorization)
    case signUpWithoutProfile(data: Authorization)
    case signUpWithProfile(jwt: String)
    case verifyOtp(data:Authorization)
    case resetPassword(data:Authorization)
    case logout(data: Authorization)
    case songs(data:Song,page: Int, limit: Int)
    case profileSetUpWithAvatar(image:String)
    case profileSetUpWithoutAvatar(data:Authorization?)
    case getCategories(categoryType:String, page: Int, limit: Int)
    case saveInterest(interest:[[String:Any]]?)
    case favouriteSong(data:Song)
    case searchSong(data:Song, page: Int, limit: Int)
    case updateProfile(jwt:String)
    case recommendedSongs(data:Song)
    case artistDetail(artistId:Int, page: Int, limit: Int)
    case preOrderDetail(artistId:Int,page: Int, limit: Int)
    case followArtist(artistId:Int,isFollowed:Bool)
    case nextPlaySongs(data:Song, page: Int, limit: Int)
    case syncRecentPlayed(isRecentlyPlayed: Bool, isListeningHistory: Bool, recentlyPlayed: [[String:Any]], listeningHistory: [[String:Any]], page: Int, limit: Int)
    case savePlaylist(title: String)
    case getPlaylist(page: Int, limit: Int)
    case deletePlaylist(playlistId: Int)
    case getFollowingArtist
    case updatePlaylist(data: Song)
    case buyingHistory(data: Song, page: Int, limit: Int)
    case packagesPlan(page: Int, limit: Int)
    case subscribleToPlan(data: Song)
    case validateSubscription(purchaseToken:String)
    //MARK: - URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        var urlRequest:URLRequest? = nil
        let jwtToken = "Bearer " + ((UserDefaultUtils.getProfile(key: CommonKeys.KEY_PROFILE)?.jwt ?? UserDefaultUtils.getString(key: "jwt") ?? ""))
        let ApiKey = ProjectConstants.current.getApiKey()
        switch self {
        case .validateSubscription:
            let url = try ProjectConstants.current.getSubscriptionBaseURL().asURL()
            urlRequest = URLRequest(url: (url.appendingPathComponent(path)))
            break
        default:
            let url = try ProjectConstants.current.getBaseURL().asURL()
            urlRequest = URLRequest(url: (url.appendingPathComponent(path)))
        }
        
        //Http method
        urlRequest?.httpMethod = method.rawValue
        
        // Common Headers
        
        switch self {
            
        case .updateProfile, .signUpWithProfile, .profileSetUpWithAvatar:
            urlRequest?.setValue(jwtToken, forHTTPHeaderField: Constants.HttpHeaderField.authentication.rawValue)
            urlRequest?.setValue(Constants.ContentType.json.rawValue, forHTTPHeaderField: Constants.HttpHeaderField.acceptType.rawValue)
            urlRequest?.setValue(Constants.ContentType.multipart.rawValue, forHTTPHeaderField: Constants.HttpHeaderField.contentType.rawValue)
            urlRequest?.setValue(ApiKey, forHTTPHeaderField: Constants.HttpHeaderField.apiKey.rawValue)
        case .profileSetUpWithoutAvatar:
            urlRequest?.setValue(Constants.ContentType.json.rawValue, forHTTPHeaderField: Constants.HttpHeaderField.acceptType.rawValue)
            urlRequest?.setValue(jwtToken, forHTTPHeaderField: Constants.HttpHeaderField.authentication.rawValue)
            urlRequest?.setValue(ApiKey, forHTTPHeaderField: Constants.HttpHeaderField.apiKey.rawValue)
            
        default:
            urlRequest?.setValue(Constants.ContentType.json.rawValue, forHTTPHeaderField: Constants.HttpHeaderField.acceptType.rawValue)
            urlRequest?.setValue(Constants.ContentType.json.rawValue, forHTTPHeaderField: Constants.HttpHeaderField.contentType.rawValue)
            urlRequest?.setValue(jwtToken, forHTTPHeaderField: Constants.HttpHeaderField.authentication.rawValue)
            urlRequest?.setValue(ApiKey, forHTTPHeaderField: Constants.HttpHeaderField.apiKey.rawValue)
            urlRequest?.cachePolicy = .reloadRevalidatingCacheData
        }

        //Encoding
        let encoding: ParameterEncoding = {
            switch method {
            case .get:
                return URLEncoding.default
            case .post:
                return JSONEncoding.default
            case .delete:
                return URLEncoding.default
            case .patch:
                return URLEncoding.default
            default:
                return JSONEncoding.default
            }
        }()
        return try encoding.encode(urlRequest!, with: parameters)
    }
    
    //MARK: - HttpMethod
    //This returns the HttpMethod type. It's used to determine the type if several endpoints are peresent
    private var method: HTTPMethod {
        switch self {
        
        case .login, .sendOtp, .verifyOtp, .signUpWithProfile, .signUpWithoutProfile, .profileSetUpWithAvatar, .profileSetUpWithoutAvatar, .saveInterest, .updateProfile , .recommendedSongs, .followArtist, .searchSong, .favouriteSong, .nextPlaySongs, . syncRecentPlayed, .savePlaylist, .updatePlaylist, .songs, .packagesPlan, .subscribleToPlan, .validateSubscription:
            return .post
            
        case .getCategories, .artistDetail, .preOrderDetail, .getPlaylist, .getFollowingArtist, .buyingHistory:
            return .get
            
        case .logout, .deletePlaylist:
            return .delete
            
        case .resetPassword:
            return .put
        
        }
    }
    
    //MARK: - Path
    //The path is the part following the base url
    private var path: String {
        switch self {
      
        case .login:
            return "user/login"
        case .sendOtp:
            return "user/send-otp"
        case .verifyOtp:
            return "user/verify-otp"
        case .signUpWithProfile, .signUpWithoutProfile:
            return "user/signup"
        case .resetPassword:
            return "user/reset"
        case .logout:
            return "user/logout"
        case .profileSetUpWithAvatar:
            return "user/update-profile"
        case .profileSetUpWithoutAvatar:
            return "user/update-profile"
        case .getCategories:
            return "songs/categories"
        case .saveInterest:
            return "user/save-interest"
        case .favouriteSong:
            return "songs/set-favourite-song"
        case .searchSong:
            return "songs/search"
        case .songs:
            return "songs"
        case .updateProfile:
          return "user/update-profile"
        case .recommendedSongs:
            return  "songs/recommended-songs"
        case .artistDetail:
            return "artist/artist-detail"
        case .preOrderDetail:
            return "card/pre-order-detail"
        case .followArtist:
            return "artist/artist-follow"
        case .nextPlaySongs:
            return "songs/next-play-song-list"
        case .syncRecentPlayed:
            return "songs/sync-recently-played"
        case .savePlaylist:
            return "user/save-playlist"
        case .getPlaylist:
            return "user/playlist"
        case .deletePlaylist(let playlistId):
            return "user/delete-playlist/" + String(playlistId)
        case .getFollowingArtist:
            return "artist/get-following-artist"
        case .updatePlaylist:
            return "songs/update-playlist"
        case .buyingHistory:
            return "user/buying-history"
        case .packagesPlan:
            return "card/package-plans"
        case .subscribleToPlan:
            return "card/subscribe-to-plan"
        case .validateSubscription:
            return "verifyReceipt"
        }
    }
    
    
    //MARK: - Parameters
    //This is the queries part, it's optional because an endpoint can be without parameters
    private var parameters: Parameters? {
        switch self {
        case .login(let data):
            if data.loginType == LoginType.SIMPLE.rawValue{
                return[Constants.Parameters.email:data.email ?? "",Constants.Parameters.password:data.password ?? "",Constants.Parameters.loginType:data.loginType ?? "",Constants.Parameters.fcmToken:data.fcmToken ?? "",Constants.Parameters.deviceIdentifier :data.deviceIdentifier ?? "",Constants.Parameters.deviceName:data.deviceName ?? ""]
            }else{
                
                return[Constants.Parameters.loginType:data.loginType ?? "",Constants.Parameters.fcmToken :data.fcmToken ?? "",Constants.Parameters.deviceIdentifier :data.deviceIdentifier ?? "",Constants.Parameters.deviceName:data.deviceName ?? "",Constants.Parameters.socialSite:data.socialSite ?? "" ,Constants.Parameters.accessToken : data.accessToken ?? ""]
            }
            
        case .sendOtp(let data):
            if data.otpType == OtpType.EMAIL.rawValue || data.otpType == OtpType.FORGET_PASSWORD.rawValue{
                return[Constants.Parameters.otpType:data.otpType ?? "",Constants.Parameters.email:data.email ?? "" ]
            }else {
                return[Constants.Parameters.otpType:data.otpType ?? "",Constants.Parameters.phoneNumber:data.phoneNumber ?? "" ]
            }
           
        case .verifyOtp(let data):
            if data.otpType == OtpType.EMAIL.rawValue || data.otpType == OtpType.FORGET_PASSWORD.rawValue{
                return [Constants.Parameters.otpType:data.otpType ?? "" ,Constants.Parameters.otp:"\(data.otp ?? -1)",Constants.Parameters.email:data.email ?? ""]
            }else{
                return [Constants.Parameters.otpType:data.otpType ?? "" ,Constants.Parameters.otp:"\(data.otp ?? -1)" ,Constants.Parameters.phoneNumber:data.phoneNumber ?? "" ]
            }
            
        case .signUpWithProfile(_):
            return [:]
            
        case .signUpWithoutProfile(let data):
            return [
                Constants.Parameters.name: data.name ?? "",
                Constants.Parameters.email: data.email ?? "",
                Constants.Parameters.password: data.password ?? "",
                Constants.Parameters.dOb: data.dOb ?? "",
                Constants.Parameters.gender: data.gender ?? "",
                Constants.Parameters.completeAddress: data.address ?? "",
                Constants.Parameters.city: data.city ?? "",
                Constants.Parameters.state: data.state ?? "",
                Constants.Parameters.country: data.country ?? "",
                Constants.Parameters.zipCode: data.zipCode ?? "",
                Constants.Parameters.fcmToken: data.fcmToken ?? "",
                Constants.Parameters.deviceIdentifier: data.deviceIdentifier ?? "",
                Constants.Parameters.deviceName: data.deviceName ?? ""
            ]
            
        case .resetPassword(let data):
            return [Constants.Parameters.email:data.email ?? "",Constants.Parameters.otp:data.otp ?? 0 ,Constants.Parameters.password:data.password ?? "" ]
            
        case .logout(let data):
            return [Constants.Parameters.deviceIdentifier: data.deviceIdentifier ?? ""]
    
        case .profileSetUpWithAvatar(_):
            return [:]
            
        case .profileSetUpWithoutAvatar(let data):
            var paremeter = [String:Any]()
           
            if let obj = data?.name{
                paremeter[Constants.Parameters.name] = obj
            }
            if let obj = data?.dOb{
                paremeter[Constants.Parameters.dOb] = obj
            }
            if let obj = data?.phoneNumber{
                paremeter[Constants.Parameters.phoneNumber] = obj
            }
            if let obj = data?.otp{
                paremeter[Constants.Parameters.otp] = obj
            }
             
            if let obj = data?.gender{
                paremeter[Constants.Parameters.gender] = obj
            }
            if let obj = data?.addressModObj?.completeAddress{
                paremeter[Constants.Parameters.completeAddress] = obj
            }
            if let obj = data?.addressModObj?.city{
                paremeter[Constants.Parameters.city] = obj
            }
            if let obj = data?.addressModObj?.state{
                paremeter[Constants.Parameters.state] = obj
            }
            if let obj = data?.addressModObj?.country{
                paremeter[Constants.Parameters.country] = obj
            }
            if let obj = data?.addressModObj?.zipCode{
                paremeter[Constants.Parameters.zipCode] = obj
            }
            if let obj = data?.isEnableNotification{
                paremeter[Constants.Parameters.isEnableNotification] = obj
            }
            if let obj = data?.isArtistUpdateEnable{
                paremeter[Constants.Parameters.isArtistUpdateEnable] = obj
            }
            return paremeter
        
        
        case .getCategories(let categortType, let page, let limit):
            return [Constants.Parameters.categoryType:categortType, Constants.Parameters.page: page, Constants.Parameters.limit: limit]

        
        case .saveInterest(let interest):
            return [Constants.Parameters.interests:interest ?? [[String:Any]]()]
        
        case .favouriteSong(let data):
            return
            [
                Constants.Parameters.songid: data.songID,
                Constants.Parameters.isFavourite: data.isFavourit
            ]
            
        case .searchSong(let data, let page, let limit):
            if let lanId =  data.languageId{
                return [Constants.Parameters.languageId:lanId,Constants.Parameters.query:data.query ?? "", Constants.Parameters.page: page, Constants.Parameters.limit: limit]
            }else{
                return [Constants.Parameters.query:data.query ?? "",Constants.Parameters.page: page, Constants.Parameters.limit: limit]
            }
        case .songs(let data, let page, let limit):
            if data.type == SongType.TRENDING.rawValue || data.type == SongType.FAVORITES.rawValue || data.type == SongType.PURCHASED.rawValue
            {
                return [Constants.Parameters.type:data.type ?? "",Constants.Parameters.page: page, Constants.Parameters.limit: limit]
                
            }else if data.type == SongType.PLAY_LIST.rawValue{
                return [Constants.Parameters.type:data.type ?? "",Constants.Parameters.ListId:data.playListID,Constants.Parameters.page: page, Constants.Parameters.limit: limit]
            }else{
                return [Constants.Parameters.type:data.type ?? "",Constants.Parameters.albumId:data.albumID,Constants.Parameters.page: page, Constants.Parameters.limit: limit]
            }
            

        case .updateProfile:
            return [:]
            

        case .recommendedSongs(let data):
            
            //For ARTIST ABLUM OR PURCHASED ALBUM
            if data.requestType == SongType.PURCHASED_ALBUM.rawValue || data.requestType == SongType.ARTIST_ALBUM.rawValue  {
                
                if data.requestType == SongType.PURCHASED_ALBUM.rawValue{
                    return [Constants.Parameters.requestType:data.requestType ?? ""]
                } else {
                return [Constants.Parameters.requestType:data.requestType ?? "",Constants.Parameters.artistId : data.artistID ?? 0]
                
            }
            //For RECOMMENDED SONGS, ALBUM, ARTIST
            } else if data.requestType == RequestType.RECOMMENDED.rawValue{
                return[
                    Constants.Parameters.dataType: data.type ?? "",
                    Constants.Parameters.requestType: data.requestType ?? ""
                ]
            }
            //FOR CATEGORY
            else if data.requestType == RequestType.CATEGORY.rawValue{
                return[
                    Constants.Parameters.requestType:data.requestType ?? "",
                    Constants.Parameters.dataType:data.type ?? "",
                    Constants.Parameters.categoryId:data.categoryID ?? 0
                ]
            }
            //FOR HISTORY
            else if data.requestType == RequestType.HISTORY.rawValue {
                return[Constants.Parameters.requestType:data.requestType ?? "",
                           Constants.Parameters.dataType:data.type ?? ""]
            } else {
                return [:]
            }
            
        case .artistDetail(let artistId, let page, let limit):
            return [Constants.Parameters.artistId:artistId,Constants.Parameters.page: page, Constants.Parameters.limit: limit]
    
        case .preOrderDetail(let artistId, let page, let limit):
            return [Constants.Parameters.artistId:artistId, Constants.Parameters.page: page, Constants.Parameters.limit: limit]
        
        case .followArtist(let artistId,let isFollowed):
            return [Constants.Parameters.artistId:artistId,Constants.Parameters.isFollowed:isFollowed]
        
        case .nextPlaySongs(let data, let page, let limit):
            //Case 1 PLAYLIST
            if data.playedFrom == SongType.PLAYLIST.rawValue {
                return [
                    Constants.Parameters.playedFrom: data.playedFrom ?? "",
                    Constants.Parameters.playListId: data.playListID ?? 0,
                    Constants.Parameters.currentSongId: data.currentSongId ?? 0,
                    Constants.Parameters.page: page,
                    Constants.Parameters.limit: limit
                ]
            }
            //Case 2 ALBUM || Case 7 PURCHASED_ALBUM
            else if data.playedFrom == SongType.ALBUM.rawValue || data.playedFrom == SongType.PURCHASED_ALBUM.rawValue{
                return [
                    Constants.Parameters.playedFrom: data.playedFrom ?? "",
                    Constants.Parameters.albumId: data.albumID ?? 0,
                    Constants.Parameters.currentSongId: data.currentSongId ?? 0,
                    Constants.Parameters.page: page,
                    Constants.Parameters.limit: limit
                ]
            }
            //Case 3 CATEGORY!
            else if data.playedFrom == SongType.CATEGORY.rawValue {
                return [
                    Constants.Parameters.playedFrom: data.playedFrom ?? "",
                    Constants.Parameters.categoryId: data.categoryID ?? 0,
                    Constants.Parameters.currentSongId: data.currentSongId ?? 0,
                    Constants.Parameters.page: page,
                    Constants.Parameters.limit: limit
                ]
            }
            //Case 4 FAVOURITE, Case 5 PURCHASED_SONG, Case 7 DOWNLOADED, Case 8 SEARCHED
            else if data.playedFrom == SongType.FAVOURITE.rawValue || data.playedFrom == SongType.PURCHASED_SONG.rawValue || data.playedFrom == SongType.DOWNLOADED.rawValue || data.playedFrom == SongType.SEARCHED.rawValue {
                return [
                    Constants.Parameters.playedFrom: data.playedFrom ?? "",
                    Constants.Parameters.currentSongId: data.currentSongId ?? 0,
                    Constants.Parameters.page: page,
                    Constants.Parameters.limit: limit
                ]
            } else {
                return [:]
            }
            
        case .syncRecentPlayed(let isRecentlyPlayed, let isListeningHistory, let recentlyPlayed, let listeningHistory, let page, let limit):
            return [
                Constants.Parameters.isRecentlyPlayed: isRecentlyPlayed,
                Constants.Parameters.isListeningHistory: isListeningHistory,
                Constants.Parameters.recentlyPlayed: recentlyPlayed,
                Constants.Parameters.listeningHistory: listeningHistory,
                Constants.Parameters.page: page,
                Constants.Parameters.limit: limit
            ]
        case .savePlaylist(let title):
            return [Constants.Parameters.title: title]
        case .getPlaylist(let page, let limit):
            return [Constants.Parameters.page: page,
                    Constants.Parameters.limit: limit]
        case .deletePlaylist(let playlistId):
            return [Constants.Parameters.playListId: playlistId]
        case .getFollowingArtist:
            return [:]
        case .updatePlaylist(let data):
            return [Constants.Parameters.ListId:     data.playListID,
                    Constants.Parameters.songid: data.songID,
                    Constants.Parameters.type: data.type]
        case .buyingHistory(let data, let page, let limit):
            return [ Constants.Parameters.typeOfTransection:data.typeOfTransection ??  "",
                     Constants.Parameters.page: page,
                     Constants.Parameters.limit: limit]
        case .packagesPlan(let page, let limit):
            return [Constants.Parameters.page: page,
                     Constants.Parameters.limit: limit]
        
        case .subscribleToPlan(let data):
            print(data.itemType)
            if data.itemType == SubscribeToPlan.SONG.rawValue
            {
                return [Constants.Parameters.purchaseType: data.purchaseType ?? "",
                        Constants.Parameters.purchaseToken: data.purchaseToken ?? "",
                        Constants.Parameters.itemType: data.itemType ?? "",
                        Constants.Parameters.songid: data.songID ?? 0,
                        Constants.Parameters.paidAmount: data.paidAmount ?? 0]
            }
           else if data.itemType == SubscribeToPlan.ALBUM.rawValue
            {
                return [Constants.Parameters.purchaseType: data.purchaseType,
                        Constants.Parameters.purchaseToken: data.purchaseToken,
                        Constants.Parameters.itemType: data.itemType,
                        Constants.Parameters.albumId: data.albumID,
                        Constants.Parameters.paidAmount: data.paidAmount]
            }
            else if data.itemType == SubscribeToPlan.PLAN_SUBSCRIPTION.rawValue
             {
                 return [Constants.Parameters.purchaseType: data.purchaseType,
                         Constants.Parameters.purchaseToken: data.purchaseToken,
                         Constants.Parameters.itemType: data.itemType,
                         Constants.Parameters.planId: data.planId,
                         Constants.Parameters.paidAmount: data.paidAmount]
             }
            else if data.itemType == SubscribeToPlan.PRE_ORDER_SONG.rawValue
             {
                 return [Constants.Parameters.purchaseType: data.purchaseType,
                         Constants.Parameters.purchaseToken: data.purchaseToken,
                         Constants.Parameters.itemType: data.itemType,
                         Constants.Parameters.preOrderSongId: data.preOrderSongId,
                         Constants.Parameters.paidAmount: data.paidAmount]
             }
            else if data.itemType == SubscribeToPlan.PRE_ORDER_ALBUM.rawValue
             {
                 return [Constants.Parameters.purchaseType: data.purchaseType,
                         Constants.Parameters.purchaseToken: data.purchaseToken,
                         Constants.Parameters.itemType: data.itemType,
                         Constants.Parameters.preOrderAlbumId: data.preOrderAlbumId,
                         Constants.Parameters.paidAmount: data.paidAmount]
             }
            else {
                return [:]
            }
            
        case .validateSubscription(let purchaseToken):
            return [Constants.Parameters.password:ProjectConstants.current.getSubscriptionAppSecrit(),
                    Constants.Parameters.subscriptionToken:purchaseToken]
            
            
        }
    }
}


