//
//  BaseApiClient.swift
//  RexSwiftTest
//
//  Created by Nasar Iqbal on 3/21/20.
//  Copyright © 2020 Nasar Iqbal. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire


open class BaseApiClient {
    
    internal static func request<T: Codable> (urlConvertible: URLRequestConvertible) -> Observable<T> {
        //Create an RxSwift observable, which will be the one to call the request when subscribed to
        return Observable<T>.create { observer in
            //Trigger the HttpRequest using AlamoFire (AF)
            let request = AF.request(urlConvertible).responseDecodable {
                (response: DataResponse<T,AFError>) in
                //Check the result from Alamofire's response and check if it's a success or a failure
               
                switch response.result {
                case .success(let value):
                    print("response code = \(String(describing: response.response?.statusCode))")
                    if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                        print("Data: \(utf8Text)")
                    }
                    switch response.response?.statusCode {
                    case 200,204,201:
                        observer.onNext(value)
                        observer.onCompleted()
                    case 400:
                        observer.onError(ApiError.badRequest(error: value))
                    case 401:
                        observer.onError(ApiError.unAuthorized(error: value))
                    case 402:
                        observer.onError(ApiError.paymentIssue(error: value))
                    case 403:
                        observer.onError(ApiError.forbidden(error: value))
                    case 404:
                        observer.onError(ApiError.notFound(error: value))
                    case 405:
                        observer.onError(ApiError.notAllowed(error: value))
                    case 422:
                        observer.onError(ApiError.unProcessEntity(error: value))
                    case 500:
                        observer.onError(ApiError.internalServerError(error: value))
                    default:
                        observer.onError(ApiError.defaultError(error: value))
                    }
                
                    
                case .failure(let error):
                    print(error)
                    //Something went wrong, switch on the status code and return the error
                    print("get faild response with description = \(error.localizedDescription) ")
                    let progressUpdate = ResponseMain(response: Response(status: false, data: nil, message: "Something went wrong!.", progress: nil))
                   switch response.response?.statusCode {
                    
                    case 204:
                       let progressUpdate = ResponseMain(response: Response(status: true, data: nil, message: nil, progress: nil))
                        observer.onNext(progressUpdate as! T)
                        observer.onCompleted()
                    case 200:
                        observer.onError(ApiError.onSuccess(error: progressUpdate))
                    case 400:
                        observer.onError(ApiError.badRequest(error: progressUpdate))
                    case 401:
                       let unAuth = ResponseMain(response: Response(status: true, data: nil, message: "Session Expired.", progress: nil))
                       observer.onError(ApiError.unAuthorized(error: unAuth))

                    case 402:
                        observer.onError(ApiError.paymentIssue(error: progressUpdate))
                    case 403:
                        observer.onError(ApiError.forbidden(error: progressUpdate))
                    case 404:
                        observer.onError(ApiError.notFound(error: progressUpdate))
                    case 405:
                        observer.onError(ApiError.notAllowed(error: progressUpdate))
                    case 422:
                        observer.onError(ApiError.unProcessEntity(error: progressUpdate))
                    case 500:
                        observer.onError(ApiError.internalServerError(error: progressUpdate))

                    default:
                        observer.onError(ApiError.defaultError(error: progressUpdate))
                    }
                }
            }
            
            //Finally, we return a disposable to stop the request
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    internal static func requestFileUpload<T: Codable> (imageData: [UIImage]?,video:NSURL?,parameters:[String:Any],urlConvertible: URLRequestConvertible) -> Observable<T> {
        //Create an RxSwift observable, which will be the one to call the request when subscribed to
        return Observable<T>.create { observer in
            //Trigger the HttpRequest using AlamoFire (AF)
            
            let request = AF.upload(multipartFormData: { multiPart in
                if imageData != nil{
                    for imagItem in imageData! {
                        if let imageData = imagItem.jpegData(compressionQuality: 0.6) {
                            multiPart.append(imageData, withName: "productImages[]", fileName: "file.png", mimeType: "image/png")
                        }
                        
                    }
                }
                
                if video != nil{
                    multiPart.append(video! as URL, withName: "productVideo")
                }
                
                for (key, value) in parameters {
                    multiPart.append("\(value)".data(using: String.Encoding.utf8)!, withName: key)
                }
                
            }, with: urlConvertible)
                
                .uploadProgress(queue: .main, closure: { progress in
                    //Current upload progress of file
                    let progressUpdate = ResponseMain(response: Response(status: true, data: nil, message: nil  ,progress:progress.fractionCompleted))
                    observer.onNext(progressUpdate as! T)
                    //   print("Upload Progress: \(progress.fractionCompleted)")
                })
                .responseDecodable {
                    (response: DataResponse<T,AFError>) in
                    //Check the result from Alamofire's response and check if it's a success or a failure
                    switch response.result {
                    case .success(let value):
                        print("response code = \(String(describing: response.response?.statusCode))")
                        if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                            print("Data: \(utf8Text)")
                        }
                        switch response.response?.statusCode {
                        case 200,204,201:
                            observer.onNext(value)
                            observer.onCompleted()
                        case 400:
                            observer.onError(ApiError.badRequest(error: value))
                        case 401:
                            observer.onError(ApiError.unAuthorized(error: value))
                        case 402:
                            observer.onError(ApiError.paymentIssue(error: value))
                        case 403:
                            observer.onError(ApiError.forbidden(error: value))
                        case 404:
                            observer.onError(ApiError.notFound(error: value))
                        case 405:
                            observer.onError(ApiError.notAllowed(error: value))
                        case 422:
                            observer.onError(ApiError.unProcessEntity(error: value))
                        case 500:
                            observer.onError(ApiError.internalServerError(error: value))
                        default:
                            observer.onError(ApiError.defaultError(error: value))
                        }
                    case .failure(let error):
                        print(String(describing: error.localizedDescription))
                        //Something went wrong, switch on the status code and return the error
                        print("get faild response with description = \(error.localizedDescription) ")
                        let progressUpdate = ResponseMain(response: Response(status: false, data: nil, message: "Something went wrong!.", progress: nil))
                        switch response.response?.statusCode {
                         
                         case 204:
                             observer.onNext(progressUpdate as! T)
                             observer.onCompleted()
                         case 200:
                             observer.onError(ApiError.onSuccess(error: progressUpdate))
                         case 400:
                             observer.onError(ApiError.badRequest(error: progressUpdate))
                         case 401:
                            let unAuth = ResponseMain(response: Response(status: true, data: nil, message: "Sesson Expired.", progress:nil))
                             observer.onError(ApiError.unAuthorized(error: unAuth))
                         case 402:
                             observer.onError(ApiError.paymentIssue(error: progressUpdate))
                         case 403:
                             observer.onError(ApiError.forbidden(error: progressUpdate))
                         case 404:
                             observer.onError(ApiError.notFound(error: progressUpdate))
                         case 405:
                             observer.onError(ApiError.notAllowed(error: progressUpdate))
                         case 422:
                             observer.onError(ApiError.unProcessEntity(error: progressUpdate))
                         case 500:
                             observer.onError(ApiError.internalServerError(error: progressUpdate))
                         default:
                             observer.onError(ApiError.defaultError(error: progressUpdate))
                         }
                    }
            }
            
            //Finally, we return a disposable to stop the request
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    
   internal static func requestImageUpload<T: Codable> (imageData: [String:UIImage],parameters:[String:NSObject],urlConvertible: URLRequestConvertible) -> Observable<T> {
       //Create an RxSwift observable, which will be the one to call the request when subscribed to
       return Observable<T>.create { observer in
           //Trigger the HttpRequest using AlamoFire (AF)
           let request = AF.upload(multipartFormData: { multiPart in
               for (key, value) in imageData {
                   if let imageData = value.jpegData(compressionQuality: 0.6) {
                       multiPart.append(imageData, withName: key, fileName: "file.png", mimeType: "image/png")
                   }
               }
               for (key, value) in parameters {
                   multiPart.append("\(value)".data(using: String.Encoding.utf8)!, withName: key)
               }
           }, with: urlConvertible)
               .uploadProgress(queue: .main, closure: { progress in
                   //Current upload progress of file
                   let progressUpdate = ResponseMain(response: Response(status: false, data: nil, message: nil, progress: progress.fractionCompleted))
                   print("progress \(progress.fractionCompleted)")

               })
               .responseDecodable {
                   (response: DataResponse<T,AFError>) in
                   //Check the result from Alamofire's response and check if it's a success or a failure
                   switch response.result {
                  case .success(let value):
                       print("response code = \(String(describing: response.response?.statusCode))")
                       if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                           print("Data: \(utf8Text)")
                       }
                       switch response.response?.statusCode {
                       case 200,204,201:
                           observer.onNext(value)
                           observer.onCompleted()
                       case 400:
                           observer.onError(ApiError.badRequest(error: value))
                       case 401:
                           observer.onError(ApiError.unAuthorized(error: value))
                       case 402:
                           observer.onError(ApiError.paymentIssue(error: value))
                       case 403:
                           observer.onError(ApiError.forbidden(error: value))
                       case 404:
                           observer.onError(ApiError.notFound(error: value))
                       case 405:
                           observer.onError(ApiError.notAllowed(error: value))
                       case 422:
                           observer.onError(ApiError.unProcessEntity(error: value))
                       case 500:
                           observer.onError(ApiError.internalServerError(error: value))
                       default:
                           observer.onError(ApiError.defaultError(error: value))
                       }
                   case .failure(let error):
                      
                       //Something went wrong, switch on the status code and return the error
                       print("get faild response with description = \(error.localizedDescription) ")
                       let progressUpdate = ResponseMain(response: Response(status: false, data: nil, message: "Something went wrong!.", progress: nil))
                       switch response.response?.statusCode {
                        
                        case 204:
                            observer.onNext(progressUpdate as! T)
                            observer.onCompleted()
                        case 200:
                            observer.onError(ApiError.onSuccess(error: progressUpdate))
                        case 400:
                            observer.onError(ApiError.badRequest(error: progressUpdate))
                        case 401:
                           let unAuth = ResponseMain(response: Response(status: true, data: nil, message: "Session Expired.", progress: nil))
                            observer.onError(ApiError.unAuthorized(error: unAuth))
                        case 402:
                            observer.onError(ApiError.paymentIssue(error: progressUpdate))
                        case 403:
                            observer.onError(ApiError.forbidden(error: progressUpdate))
                        case 404:
                            observer.onError(ApiError.notFound(error: progressUpdate))
                        case 405:
                            observer.onError(ApiError.notAllowed(error: progressUpdate))
                        case 422:
                            observer.onError(ApiError.unProcessEntity(error: progressUpdate))
                        case 500:
                            observer.onError(ApiError.internalServerError(error: progressUpdate))
                        default:
                            observer.onError(ApiError.defaultError(error: progressUpdate))
                        }
                   }
           }
           //Finally, we return a disposable to stop the request
           return Disposables.create {
               request.cancel()
           }
       }
   }
}
