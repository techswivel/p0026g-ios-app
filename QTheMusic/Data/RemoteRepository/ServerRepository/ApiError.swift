//
//  ApiError.swift
//  RexSwiftTest
//
//  Created by Nasar Iqbal on 3/21/20.
//  Copyright © 2020 Nasar Iqbal. All rights reserved.
//

import Foundation
enum ApiError<T: Codable>: Error {
    case onSuccess(error: T)               //Status code 200
    case badRequest(error: T)             //Status code 400
    case unAuthorized(error: T)           //Status code 401
    case paymentIssue(error: T)           //Status code 402
    case forbidden(error: T)              //Status code 403
    case notFound(error: T)               //Status code 404
    case notAllowed(error: T)             //Status code 405
    case unProcessEntity(error: T)        //Status code 422
    case internalServerError(error: T)    //Status code 500
    case defaultError(error: T)
}

