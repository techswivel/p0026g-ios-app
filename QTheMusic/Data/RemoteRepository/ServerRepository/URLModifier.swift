//
//  URLModifier.swift
//  Udeo Globe
//

//  Created by Hammad Hassan on 29/05/2020.

//  Copyright © 2020 Yasir Iqbal. All rights reserved.
//

import Foundation

class URLModifier {
    
    /// This is testing Modifier method you can replace with your requirements and add the required methods
    static func ModifiedRatingUrl(orderId: Int) -> String {
        return  "users/orders/\(orderId)/rating"
    }
}
