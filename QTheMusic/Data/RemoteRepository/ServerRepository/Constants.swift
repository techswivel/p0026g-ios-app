//
//  Constants.swift
//  RexSwiftTest
//
//  Created by Nasar Iqbal on 3/21/20.
//  Copyright © 2020 Nasar Iqbal. All rights reserved.
//

import Foundation
struct Constants {
    
    
    //The parameters (Queries) that we're gonna use
    struct Parameters {
        
        static let someConstants = "Some Constant"
        static let email = "email"
        static let phoneNumber = "phoneNumber"
        static let password = "password"
        static let completeAddress = "completeAddress"
        static let fcmToken = "fcmToken"
        static let deviceIdentifier = "deviceIdentifier"
        static let deviceName = "deviceName"
        static let accessToken = "accessToken"
        static let socialSite = "socialSite"
        static let socialId = "socialId"
        static let page = "page"
        static let limit = "limit"
        static let loginType = "loginType"
        static let otpType = "otpType"
        static let otp = "otp"
        
        static let name = "name"
        static let dOb = "dOb"
        static let gender = "gender"
        static let profile = "profile"
        static let address = "address"
        static let city = "city"
        static let state = "state"
        static let country = "country"
        static let zipCode = "zipcode"
        static let avatar = "avatar"
        static let isEnableNotification = "isEnableNotification"
        static let isArtistUpdateEnable = "isArtistUpdateEnable"
        
        static let categoryType = "categoryType"
        static let interests = "interests"
        static let categoryId  = "categoryId"
        
        static let isFavourite = "isFavourite"
        static let songid = "songId"
        
        static let languageId = "languageId"
        static let query = "queryString"
        
        static let type = "type"
        static let playListId = "playlistId"
        static let albumId = "albumId"
        static let ListId = "playListId"
        static let isRecommendedForYou = "isRecommendedForYou"
        static let isListeningHistory = "isListeningHistory"
       
        static let artistId = "artistId"
        static let isFollowed = "isFollowed"
        
        static let requestType = "requestType"
        static let dataType = "dataType"
        
        static let playedFrom    = "playedFrom"
        static let currentSongId = "currentSongId"
        
        static let isRecentlyPlayed = "isRecentlyPlayed"
        static let recentlyPlayed = "recentlyPlayed"
        static let listeningHistory = "listeningHistory"
        static let title = "title"
        static let typeOfTransection = "typeOfTransection"
        static let purchaseType = "purchaseType"
        static let purchaseToken = "purchaseToken"
        static let itemType = "itemType"
        static let planId = "planId"
        static let preOrderSongId = "preOrderSongId"
        static let preOrderAlbumId = "preOrderAlbumId"
        static let paidAmount = "paidAmount"
        static let subscriptionToken = "receipt-data"
    }
    
    //The header fields
    enum HttpHeaderField: String {
        case authentication = "Authorization"
        case contentType = "Content-Type"
        case acceptType = "Accept"
        case acceptEncoding = "Accept-Encoding"
        case apiKey = "api-key"
    }
    
    //The content type (JSON)
    enum ContentType: String {
        case json = "application/json"
        case multipart = "multipart/form-data"
    }
}
