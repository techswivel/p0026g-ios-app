//
//  DummyViewModel.swift
//  Base iOS Project
//
//  Created by Hammad Hassan on 09/02/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import Foundation
import UIKit
class AuthViewModel : BaseViewModel {
    var authData:Authorization?
    var responseMain = ResponseMain(response:nil)
    
    var responseMainResponse : Box<ResponseMain?> = Box(nil)
    var responseMainErrorResponse : Box<ResponseMain?> = Box(nil)
    var responseMainExpiryObserver : Box<ResponseMain?> = Box(nil)
    var completeResponse : Box<ResponseMain?> = Box(nil)
    //Login Password Responses
    var serverResponseForLogin: Box<ResponseMain?> = Box(nil)
    var ErrorResponseForLogin: Box<ResponseMain?> = Box(nil)
    //For SendingOtp Responses
    var serverResponseForSendOTP: Box<ResponseMain?> = Box(nil)
    var ErrorResponseForSendOTP: Box<ResponseMain?> = Box(nil)
    //For VarifyOTP Responses
    var serverResponseForValidateOTP: Box<ResponseMain?> = Box(nil)
    var ErrorResponseForValidateOTP: Box<ResponseMain?> = Box(nil)
    //For Reset Password  Responses
    var serverResponseForResetPassword: Box<ResponseMain?> = Box(nil)
    var ErrorResponseForResetPassword: Box<ResponseMain?> = Box(nil)
    
    //For Logout Response
    var serverResponseForLogout: Box<ResponseMain?> = Box(nil)
    var ErrorResponseForLogout: Box<ResponseMain?> = Box(nil)
    
    //SetUpProfile  Responses
    var serverResponseForSetUpProfile : Box<ResponseMain?> = Box(nil)
    var ErrorResponseForSetUpProfile  : Box<ResponseMain?> = Box(nil)
    
    //SetUpProfile  Responses
    var serverResponseForSignUpProfile : Box<ResponseMain?> = Box(nil)
    var ErrorResponseForSignUpProfile  : Box<ResponseMain?> = Box(nil)
    
    //Sign Up Response
    var serverResponseForSignUp: Box<ResponseMain?> = Box(nil)
    var errorResponseForSignUp: Box<ResponseMain?> = Box(nil)
    
    private let deviceIdentifire = KeyChainUtils.getUUID()
    private let deviceName = UIDevice.modelName
    
    
    //Method For Login
    func login(builder:AuthorizationBuilder)
    {
        builder.setDeviceIdentifier(deviceIdentifire ?? "")
        builder.setDeviceName(deviceName)
        
        mDataManager.login(data: builder.build())
            .subscribe(onNext: { ServerResponse in
                self.serverResponseForLogin.value = ServerResponse
                
            }, onError: { Error in
                
                let error = Error as? ApiError<ResponseMain>
                self.handleError(data: error, errorObserver: self.ErrorResponseForLogin, expireObserver: self.expireObserer)
                
            }, onCompleted: {
                self.completion.value = ResponseMain(response: nil)
                
            }).disposed(by: disposeBag)
    }
    
    //Method For Logout
    func performLogout() {
        let builder = AuthorizationBuilder()
        builder.setDeviceIdentifier(deviceIdentifire ?? "")
        
        mDataManager.logout(data: builder.build())
            .subscribe(onNext: { ServerResponse in
                self.serverResponseForLogout.value = ServerResponse
                
            }, onError: { Error in
                
                let error = Error as? ApiError<ResponseMain>
                self.handleError(data: error, errorObserver: self.ErrorResponseForLogout, expireObserver: self.expireObserer)
                
            }, onCompleted: {
                self.completion.value = ResponseMain(response: nil)
                
            }).disposed(by: disposeBag)
    }
    
    
    //Method For sending Otp
    func sendOTP(builder:AuthorizationBuilder)
    {
        builder.setDeviceIdentifier(deviceIdentifire ?? "")
        builder.setDeviceName(deviceName)
        
        mDataManager.sendOtp(data: builder.build())
            .subscribe(onNext: { ServerResponse in
                self.serverResponseForSendOTP.value = ServerResponse
                
            }, onError: { Error in
                
                let error = Error as? ApiError<ResponseMain>
                self.handleError(data: error, errorObserver: self.ErrorResponseForSendOTP, expireObserver: self.expireObserer)
                
            }, onCompleted: {
                self.completion.value = ResponseMain(response: nil)
                
            }).disposed(by: disposeBag)
    }
    
    //Method For validating Otp
    func varifyOTP(builder:AuthorizationBuilder)
    {
        builder.setDeviceIdentifier(deviceIdentifire ?? "")
        builder.setDeviceName(deviceName)
        
        mDataManager.verifyOtp(data: builder.build())
            .subscribe(onNext: { ServerResponse in
                self.serverResponseForValidateOTP.value = ServerResponse
                
            }, onError: { Error in
                
                let error = Error as? ApiError<ResponseMain>
                self.handleError(data: error, errorObserver: self.ErrorResponseForValidateOTP, expireObserver: self.expireObserer)
                
            }, onCompleted: {
                self.completion.value = ResponseMain(response: nil)
                
            }).disposed(by: disposeBag)
    }
    
    //Method For Sign Up
    func singUp(builder: AuthorizationBuilder) {
        
        builder.setDeviceIdentifier(deviceIdentifire ?? "")
        builder.setDeviceName(deviceName)
        
        mDataManager.signUp(data: builder.build())
            .subscribe(onNext: { ServerResponse in
                self.serverResponseForSignUp.value = ServerResponse
                
            }, onError: { Error in
                
                let error = Error as? ApiError<ResponseMain>
                self.handleError(data: error, errorObserver: self.errorResponseForSignUp, expireObserver: self.expireObserer)
                
            }, onCompleted: {
                self.completion.value = ResponseMain(response: nil)
                
            }).disposed(by: disposeBag)
    }
    
    //Method For Reset password
    func resetPassword(builder:AuthorizationBuilder)
    {
        mDataManager.resetPassword(data: builder.build())
            .subscribe(onNext: { ServerResponse in
                self.serverResponseForResetPassword.value = ServerResponse
                
            }, onError: { Error in
                
                let error = Error as? ApiError<ResponseMain>
                self.handleError(data: error, errorObserver: self.ErrorResponseForResetPassword, expireObserver: self.expireObserer)
                
            }, onCompleted: {
                self.completion.value = ResponseMain(response: nil)
                
            }).disposed(by: disposeBag)
    }
    
    //Method For SetUp Profile
    func setUpProfile(builder:AuthorizationBuilder)
    {
        mDataManager.profileSetup(data: builder.build())
            .subscribe(onNext: { ServerResponse in
                self.serverResponseForSetUpProfile.value = ServerResponse
                
            }, onError: { Error in
                
                let error = Error as? ApiError<ResponseMain>
                self.handleError(data: error, errorObserver: self.ErrorResponseForSetUpProfile, expireObserver: self.expireObserer)
                
            }, onCompleted: {
                self.completion.value = ResponseMain(response: nil)
                
            }).disposed(by: disposeBag)
    }
    
    //Method For SetUp Profile
    func signUpProfile(builder:AuthorizationBuilder)
    {
        builder.setDeviceIdentifier(deviceIdentifire ?? "")
        builder.setDeviceName(deviceName)
        
        
        mDataManager.profileSetup(data: builder.build())
            .subscribe(onNext: { ServerResponse in
                self.serverResponseForSignUpProfile.value = ServerResponse
                
            }, onError: { Error in
                
                let error = Error as? ApiError<ResponseMain>
                self.handleError(data: error, errorObserver: self.ErrorResponseForSignUpProfile, expireObserver: self.expireObserer)
                
            }, onCompleted: {
                self.completion.value = ResponseMain(response: nil)
                
            }).disposed(by: disposeBag)
    }
}
