//
//  SongAndArtistViewModel.swift
//  QtheMusic
//
//  Created by Assad Khan on 30/03/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
// Api call krne ka tareka with rxswift

import Foundation
class SongAndArtistViewModel:BaseViewModel{
    
    var responseMainExpiryObserver : Box<ResponseMain?> = Box(nil)
    var completeResponse : Box<ResponseMain?> = Box(nil)
    
    //Get Categories
    var serverResponseForGetCategories: Box<ResponseMain?> = Box(nil)
    var errorResponseForGetCategories: Box<ResponseMain?> = Box(nil)
    
    //Search Song
    var serverResponseForSearchSong: Box<ResponseMain?> = Box(nil)
    var errorResponseForSearchSong: Box<ResponseMain?> = Box(nil)
    
    // Song
    var serverResponseForSong: Box<ResponseMain?> = Box(nil)
    var errorResponseForSong: Box<ResponseMain?> = Box(nil)
    
    // Recommended Songs
    var serverResponseForRecommendedSong: Box<ResponseMain?> = Box(nil)
    var errorResponseForRecommendedSong: Box<ResponseMain?> = Box(nil)
    
    // Artist Detail
    var serverResponseForArtistDetail: Box<ResponseMain?> = Box(nil)
    var errorResponseForArtistDetail: Box<ResponseMain?> = Box(nil)
    
    // PreOrderDetail Detail
    var serverResponseForPreOrderDetail: Box<ResponseMain?> = Box(nil)
    var errorResponseForPreOrderDetail: Box<ResponseMain?> = Box(nil)
    
    // FollowArtist
    var serverResponseForFollowArtist: Box<ResponseMain?> = Box(nil)
    var errorResponseForFollowArtist: Box<ResponseMain?> = Box(nil)
    
    // PlayNextSong
    var serverResponseForPlayNextSong: Box<ResponseMain?> = Box(nil)
    var errorResponseForPlayNextSong: Box<ResponseMain?> = Box(nil)
    
    // GetPlaylist
    var serverResponseForGetPlaylist : Box<ResponseMain?> = Box(nil)
    var errorResponseForGetPlaylist : Box<ResponseMain?> = Box(nil)
    
    // Get Albums
    var serverResponseForGetAlbums: Box<ResponseMain?> = Box(nil)
    var errorResponseForGetAlbums : Box<ResponseMain?> = Box(nil)
    
    // Get FollowArtist
    var serverResponseForGetFollowArtist: Box<ResponseMain?> = Box(nil)
    var errorResponseForGetFollowArtist: Box<ResponseMain?> = Box(nil)
    // Get PackagesPlan
    var serverResponseForGetPackagesPlan: Box<ResponseMain?> = Box(nil)
    var errorResponseForGetPackagesPlan: Box<ResponseMain?> = Box(nil)
    // SubscribeToPlan
    var serverResponseForSubscribeToPlan: Box<MainResponse?> = Box(nil)
    var errorResponseForForSubscribeToPlan: Box<MainResponse?> = Box(nil)
    //Method For GetCategories
    func getCtegories(categoryType:String?)
    {
        
        mDataManager.getCategories(categoryType: categoryType, page: ProjectConstants.current.page, limit: ProjectConstants.current.limit)
            .subscribe(onNext: { ServerResponse in
                self.serverResponseForGetCategories.value = ServerResponse
                
            }, onError: { Error in
                let error = Error as? ApiError<ResponseMain>
                self.handleError(data: error, errorObserver: self.errorResponseForGetCategories, expireObserver: self.expireObserer)
                
            }, onCompleted: {
                self.completion.value = ResponseMain(response: nil)
                
            }).disposed(by: disposeBag)
    }
    
    //Method For GetCategories
    func searchSong(data:SongAndArtistBuilder)
    {
        mDataManager.searchSong(data: data.build(), page: ProjectConstants.current.page, limit: ProjectConstants.current.limit)
            .subscribe(onNext: { ServerResponse in
                self.serverResponseForSearchSong.value = ServerResponse
                
            }, onError: { Error in
                
                let error = Error as? ApiError<ResponseMain>
                self.handleError(data: error, errorObserver: self.errorResponseForSearchSong, expireObserver: self.expireObserer)
                
            }, onCompleted: {
                self.completion.value = ResponseMain(response: nil)
                
            }).disposed(by: disposeBag)
    }
    
    //Method For GetCategories
    func songs(data:SongAndArtistBuilder?)
    {
        
        mDataManager.songs(data: data?.build(), page: ProjectConstants.current.page, limit: ProjectConstants.current.limit)
            .subscribe(onNext: { ServerResponse in
                self.serverResponseForSong.value = ServerResponse
                
            }, onError: { Error in
                
                let error = Error as? ApiError<ResponseMain>
                self.handleError(data: error, errorObserver: self.errorResponseForSong, expireObserver: self.expireObserer)
                
            }, onCompleted: {
                self.completion.value = ResponseMain(response: nil)
                
            }).disposed(by: disposeBag)
    }
    
    func getRecommendedSongs(data:SongAndArtistBuilder?){
        mDataManager.getRecommendedSongs(data: data?.build())
            .subscribe(onNext: { ServerResponse in
                self.serverResponseForRecommendedSong.value = ServerResponse
                
            }, onError: { Error in
                
                let error = Error as? ApiError<ResponseMain>
                self.handleError(data: error, errorObserver: self.errorResponseForRecommendedSong, expireObserver: self.expireObserer)
                
            }, onCompleted: {
                self.completion.value = ResponseMain(response: nil)
                
            }).disposed(by: disposeBag)
    }
    
    func getArtistDetatil(artistId:Int?){
        mDataManager.getArtitstDetail(artistDetail: artistId ?? 0, page: ProjectConstants.current.page, limit: ProjectConstants.current.limit)
            .subscribe(onNext: { ServerResponse in
                self.serverResponseForArtistDetail.value = ServerResponse
                
            }, onError: { Error in
                
                let error = Error as? ApiError<ResponseMain>
                self.handleError(data: error, errorObserver: self.errorResponseForArtistDetail, expireObserver: self.expireObserer)
                
            }, onCompleted: {
                self.completion.value = ResponseMain(response: nil)
                
            }).disposed(by: disposeBag)
    }
    func getPackagesPlan(){
        mDataManager.getPackagesPlan(page: ProjectConstants.current.page, limit: ProjectConstants.current.limit).subscribe(onNext: { ServerResponse in
                self.serverResponseForGetPackagesPlan.value = ServerResponse
                
            }, onError: { Error in
                
                let error = Error as? ApiError<ResponseMain>
                self.handleError(data: error, errorObserver: self.serverResponseForGetPackagesPlan, expireObserver: self.expireObserer)
                
            }, onCompleted: {
                self.completion.value = ResponseMain(response: nil)
                
            }).disposed(by: disposeBag)
    }
    func subscribeToPlan(data: Song){
        mDataManager.subscribeToPlan(data: data).subscribe(onNext: { ServerResponse in
                self.serverResponseForSubscribeToPlan.value = ServerResponse
                
            }, onError: { Error in
                let error = Error as? ApiError<MainResponse>
                self.handleError(data: error, errorObserver: self.errorResponseForForSubscribeToPlan, expireObserver: self.expiredObserer)
                
            }, onCompleted: {
                self.completion.value = ResponseMain(response: nil)
                
            }).disposed(by: disposeBag)
    }
    func getPreOrderDetail(artistId:Int?){
        mDataManager.getPreOrderDetail(artistDetail: artistId ?? 0, page: ProjectConstants.current.page, limit: ProjectConstants.current.limit)
            .subscribe(onNext: { ServerResponse in
                self.serverResponseForPreOrderDetail.value = ServerResponse
                
            }, onError: { Error in
                
                let error = Error as? ApiError<ResponseMain>
                self.handleError(data: error, errorObserver: self.errorResponseForPreOrderDetail, expireObserver: self.expireObserer)
                
            }, onCompleted: {
                self.completion.value = ResponseMain(response: nil)
                
            }).disposed(by: disposeBag)
    }
    
    func followArtist(artistId:Int?,isFollowed:Bool?){
        mDataManager.followArtist(artistID: artistId ?? 0, isFollowed: isFollowed ?? false)
            .subscribe(onNext: { ServerResponse in
                self.serverResponseForFollowArtist.value = ServerResponse
                
            }, onError: { Error in
                
                let error = Error as? ApiError<ResponseMain>
                self.handleError(data: error, errorObserver: self.errorResponseForFollowArtist, expireObserver: self.expireObserer)
                
            }, onCompleted: {
                self.completion.value = ResponseMain(response: nil)
                
            }).disposed(by: disposeBag)
    }
    
    func getFollowArtist(){
        
        mDataManager.getFollowArtist()
            .subscribe(onNext: { ServerResponse in
                self.serverResponseForGetFollowArtist.value = ServerResponse
                
            }, onError: { Error in
                
                let error = Error as? ApiError<ResponseMain>
                self.handleError(data: error, errorObserver: self.errorResponseForGetFollowArtist, expireObserver: self.expireObserer)
                
            }, onCompleted: {
                self.completion.value = ResponseMain(response: nil)
                
            }).disposed(by: disposeBag)
    }
    func playNextSong(playedFrom: Song){
        mDataManager.nextPlaySongs(playedFrom: playedFrom, page: ProjectConstants.current.page, limit: ProjectConstants.current.limit)
            .subscribe(onNext: { ServerResponse in
                self.serverResponseForPlayNextSong.value = ServerResponse
                
            }, onError: { Error in
                
                let error = Error as? ApiError<ResponseMain>
                self.handleError(data: error, errorObserver: self.errorResponseForPlayNextSong, expireObserver: self.expireObserer)
                
            }, onCompleted: {
                self.completion.value = ResponseMain(response: nil)
                
            }).disposed(by: disposeBag)
    }
    
   
}
