//
//  ProfileViewModel.swift
//  QtheMusic
//
//  Created by Assad Khan on 30/03/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import Foundation



class ProfileViewModel:BaseViewModel{
    
    var responseMainExpiryObserver : Box<ResponseMain?> = Box(nil)
    var completeResponse : Box<ResponseMain?> = Box(nil)
    
    
    //Save Interest
    var serverResponseForSaveInterest: Box<ResponseMain?> = Box(nil)
    var errorResponseForSaveInterest: Box<ResponseMain?> = Box(nil)
    
    
    //Set Favourite Song
    var serverResponseForFavouriteSong: Box<ResponseMain?> = Box(nil)
    var errorResponseForFavouriteSong: Box<ResponseMain?> = Box(nil)
    
    //get Playlist
    var serverResponseForPlaylist: Box<ResponseMain?> = Box(nil)
    var errorResponseForPlaylist: Box<ResponseMain?> = Box(nil)
    
    //Update Playlist
    var serverResponseForUpdatePlaylist: Box<ResponseMain?> = Box(nil)
    var errorResponseForUpdatePlaylist: Box<ResponseMain?> = Box(nil)
    
    // save Playlist
    var serverResponseForSavePlaylist : Box<ResponseMain?> = Box(nil)
    var errorResponseForSavePlaylist : Box<ResponseMain?> = Box(nil)
    
    
    // delete playlist
    var serverResponseFordeletePlaylist : Box<ResponseMain?> = Box(nil)
    var errorResponseFordeletePlaylist : Box<ResponseMain?> = Box(nil)
    
    // buying History
    var serverResponseForBuyingHistory : Box<ResponseMain?> = Box(nil)
    var errorResponseForBuyingHistory : Box<ResponseMain?> = Box(nil)
    
    //Method For GetCategories
    func saveInterest(categories:[Category]?)
    {
        
        
        mDataManager.saveInterest(categories: categories)
            .subscribe(onNext: { ServerResponse in
                self.serverResponseForSaveInterest.value = ServerResponse
                
            }, onError: { Error in
                
                let error = Error as? ApiError<ResponseMain>
                self.handleError(data: error, errorObserver: self.errorResponseForSaveInterest, expireObserver: self.expireObserer)
                
            }, onCompleted: {
                self.completion.value = ResponseMain(response: nil)
                
            }).disposed(by: disposeBag)
    }
    
    
    
    var updateProfileServerResponse : Box<ResponseMain?> = Box(nil)
    var updateProfileErrorResponse : Box<ResponseMain?> = Box(nil)
    var updateProfileExpiryReponse : Box<ResponseMain?> = Box(nil)
    
    
    //Method For Updating User Profile
    func updateProfile(auth:Authorization)
    {
        mDataManager.updateProfile(data: auth)
            .subscribe(onNext: { ResponseMain in
                self.updateProfileServerResponse.value = ResponseMain
                
            }, onError: { Error in
                
                let error = Error as? ApiError<ResponseMain>
                self.handleError(data: error, errorObserver: self.updateProfileErrorResponse, expireObserver: self.updateProfileExpiryReponse)
                
            }, onCompleted: {
                self.completion.value = ResponseMain(response: nil)
                
            }).disposed(by: disposeBag)
    }
    
    //Method For GetCategories
    func setIsFavourite(data: Song)
    {
        mDataManager.favouriteSong(data: data)
            .subscribe(onNext: { ServerResponse in
                self.serverResponseForFavouriteSong.value = ServerResponse
                
            }, onError: { Error in
                
                let error = Error as? ApiError<ResponseMain>
                self.handleError(data: error, errorObserver: self.errorResponseForFavouriteSong, expireObserver: self.expireObserer)
                
            }, onCompleted: {
                self.completion.value = ResponseMain(response: nil)
                
            }).disposed(by: disposeBag)
    }
    
    //Method For Get Playlists
    func getPlaylist()
    {
        mDataManager.getPlaylist()
            .subscribe(onNext: { ServerResponse in
                self.serverResponseForPlaylist.value = ServerResponse
                
            }, onError: { Error in
                
                let error = Error as? ApiError<ResponseMain>
                self.handleError(data: error, errorObserver: self.errorResponseForPlaylist, expireObserver: self.expireObserer)
                
            }, onCompleted: {
                self.completion.value = ResponseMain(response: nil)
                
            }).disposed(by: disposeBag)
    }
    
    //Method For Update Playlists
    func updatePlaylist(builder:SongAndArtistBuilder)
    {
        mDataManager.updatePlaylist(data: builder.build())
            .subscribe(onNext: { ServerResponse in
                self.serverResponseForUpdatePlaylist.value = ServerResponse
                
            }, onError: { Error in
                
                let error = Error as? ApiError<ResponseMain>
                self.handleError(data: error, errorObserver: self.errorResponseForUpdatePlaylist, expireObserver: self.expireObserer)
                
            }, onCompleted: {
                self.completion.value = ResponseMain(response: nil)
                
            }).disposed(by: disposeBag)
    }

    // Method For Save Playlist
    func savePlaylist(title: String)
    {
        mDataManager.savePlaylist(title: title)
            .subscribe(onNext: { ServerResponse in
                self.serverResponseForSavePlaylist.value = ServerResponse
                
            }, onError: { Error in
                
                let error = Error as? ApiError<ResponseMain>
                self.handleError(data: error, errorObserver: self.errorResponseForSavePlaylist, expireObserver: self.expireObserer)
                
            }, onCompleted: {
                self.completion.value = ResponseMain(response: nil)
                
            }).disposed(by: disposeBag)
    }
    
    //Method For Delete Playlist
    func deletePlaylist(playlistID: Int)
    {
        mDataManager.deletePlaylist(playlistID: playlistID)
            .subscribe(onNext: { ServerResponse in
                self.serverResponseFordeletePlaylist.value = ServerResponse
                
            }, onError: { Error in
                
                let error = Error as? ApiError<ResponseMain>
                self.handleError(data: error, errorObserver: self.errorResponseFordeletePlaylist, expireObserver: self.expireObserer)
                
            }, onCompleted: {
                self.completion.value = ResponseMain(response: nil)
                
            }).disposed(by: disposeBag)
    }
    
    // Method For Getting Buying History
    func getBuyingHistory(data: Song) {
        
        mDataManager.getBuyingHistory(data: data)
            .subscribe(onNext: { ServerResponse in
                self.serverResponseForBuyingHistory.value = ServerResponse
                
            }, onError: { Error in
                
                let error = Error as? ApiError<ResponseMain>
                self.handleError(data: error, errorObserver: self.errorResponseForBuyingHistory, expireObserver: self.expireObserer)
                
            }, onCompleted: {
                self.completion.value = ResponseMain(response: nil)
                
            }).disposed(by: disposeBag)
        
    }
}
