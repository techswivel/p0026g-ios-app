//
//  AppStoreViewModel.swift
//  QtheMusic
//
//  Created by Mac on 10/05/2023.
//  Copyright © 2023 TechSwivel. All rights reserved.
//

import Foundation
import UIKit
class AppStoreViewModel : BaseViewModel {
    
    //App store response
    var successSubscriptionStatus: Box<AppStoreResponse?> = Box(nil)
    var errorSubscriptionStatus: Box<AppStoreResponse?> = Box(nil)
    var expireSubscriptionStatus: Box<AppStoreResponse?> = Box(nil)
    
    
    func getSubscriptionStatus(purchaseToken:String)
    {
        
        mDataManager.validateSubscriptionToken(token: purchaseToken)
            .subscribe(onNext: { ServerResponse in
                self.successSubscriptionStatus.value = ServerResponse
                
            }, onError: { Error in
                let error = Error as? ApiError<AppStoreResponse>
                self.handleError(data: error, errorObserver: self.errorSubscriptionStatus, expireObserver: self.expireSubscriptionStatus)
                
            }, onCompleted: {
                self.completion.value = ResponseMain(response: nil)
                
            }).disposed(by: disposeBag)
    }
}
