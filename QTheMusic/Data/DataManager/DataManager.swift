//
//  DataManager.swift
//  Udeo Globe
//
//  Created by Nasar Iqbal on 5/1/20.
//  Copyright © 2020 Yasir Iqbal. All rights reserved.
//

import UIKit
import RxSwift
import Alamofire
import SwiftUI

class DataManager:NSObject,DataManagerProtocol{
    var serverResponseForSyncRecentPlayed: Box<ResponseMain?> = Box(nil)
    var errorResponseForSyncRecentPlayed: Box<ResponseMain?> = Box(nil)
    var completion : Box<ResponseMain?> = Box(nil)
    let bag = DisposeBag()
    
    func updateProfile(data: Authorization) -> Observable<ResponseMain> {
        return Apiclient.profileSetup(data: data).observeOn(MainScheduler.instance)
    }
    
    func login(data:Authorization)->Observable<ResponseMain>{
        return Apiclient.login(data: data).observeOn(MainScheduler.instance)
    }
    
    func sendOtp(data:Authorization)->Observable<ResponseMain>{
        return Apiclient.sendOtp(data: data).observeOn(MainScheduler.instance)
    }
    
    func verifyOtp(data: Authorization) -> Observable<ResponseMain> {
        return Apiclient.verifyOtp(data: data).observeOn(MainScheduler.instance)
    }
    
    func signUp(data: Authorization) -> Observable<ResponseMain> {
        return Apiclient.signUp(data: data).observeOn(MainScheduler.instance)
    }
    
    func resetPassword(data: Authorization) -> Observable<ResponseMain> {
        return Apiclient.resetPassword(data: data).observeOn(MainScheduler.instance)
    }
    
    func logout(data: Authorization) -> Observable<ResponseMain> {
        return Apiclient.logout(data: data).observeOn(MainScheduler.instance)
    }
    
    func profileSetup(data:Authorization)->Observable<ResponseMain>{
        return Apiclient.profileSetup(data: data).observeOn(MainScheduler.instance)
    }
    
    func getCategories(categoryType:String?, page: Int, limit: Int) -> Observable<ResponseMain>{

        return Apiclient.getCategories(categoryType: categoryType, page: page, limit: limit).observeOn(MainScheduler.instance)
    }
    
    func saveInterest(categories:[Category]?)->Observable<ResponseMain>{
        return Apiclient.saveInterest(categories: categories).observeOn(MainScheduler.instance)
    }
    
    func favouriteSong(data: Song) -> Observable<ResponseMain> {
        return Apiclient.favouriteSong(data: data).observeOn(MainScheduler.instance)
    }
    func searchSong(data:Song,page: Int, limit: Int)->Observable<ResponseMain>{
        return Apiclient.searchSong(data: data,page: page, limit: limit).observeOn(MainScheduler.instance)
    }
    
    func songs(data:Song?,page: Int, limit: Int)->Observable<ResponseMain>{
        return Apiclient.songs(data: data ?? Song(),page: page, limit: limit).observeOn(MainScheduler.instance)
    }
    func getRecommendedSongs(data:Song?)->Observable<ResponseMain>{
        return Apiclient.recommendedSongs(data: data ?? Song()).observeOn(MainScheduler.instance)
    }
    
    func getArtitstDetail(artistDetail:Int, page: Int, limit: Int)->Observable<ResponseMain>{
        return Apiclient.getArtitstDetail(artistDetail: artistDetail, page: page, limit: limit).observeOn(MainScheduler.instance)
    }
    
    func getPreOrderDetail(artistDetail:Int, page: Int, limit: Int)->Observable<ResponseMain>
    {
        return Apiclient.getPreOrderDetail(artistDetail: artistDetail, page: page, limit: limit).observeOn(MainScheduler.instance)
    }
    func getPackagesPlan(page: Int, limit: Int)->Observable<ResponseMain>
    {
        return Apiclient.packagePlans(page: page, limit: limit) .observeOn(MainScheduler.instance)
    }
    
    func followArtist(artistID:Int,isFollowed:Bool)->Observable<ResponseMain>{
        return Apiclient.followArtist(artistID: artistID, isFollowed: isFollowed).observeOn(MainScheduler.instance)
    }
    func nextPlaySongs(playedFrom:Song, page: Int, limit: Int)->Observable<ResponseMain>{
        return Apiclient.nextPlaySongs(data: playedFrom, page: page, limit: limit).observeOn(MainScheduler.instance)
    }
    
    func syncRecentlyPlayedData() {
        ///Variables
        var listeningHistoryParam = [[String:Any]]()
        var recentPlayedParam = [[String:Any]]()
        var insertSongArray = [Song]()
        var insertAlbumArray = [Album]()
        var insertArtistArray = [Artist]()
        var count = 0
        let songData = getSongsDataFromDb()
            
            if let songArray = songData {
                
                ///For Listening History Array Because here no limitation in filling array
                for song in songArray {
                    
                    if  song.songID != nil && song.artistID != nil && song.albumID == nil {
                        listeningHistoryParam.append([
                            Constants.Parameters.songid: song.songID ?? 0,
                            Constants.Parameters.artistId: song.artistID ?? 0
                        ])
                        
                        let getArtistData = getRealmInstance().getArtistDataByIdFromDb(artistId: song.artistID) ?? Artist()
                        song.isSync = true
                        insertSongArray.append(song)
                        
                        getArtistData.isSync = true
                        insertArtistArray.append(getArtistData)
                    }
                    
                    if song.songID != nil && song.artistID != nil && song.albumID != nil {
                       
                        listeningHistoryParam.append([
                            Constants.Parameters.songid: song.songID ?? 0,
                            Constants.Parameters.albumId: song.albumID ?? 0,
                            Constants.Parameters.artistId: song.artistID ?? 0
                        ])

                        let getArtistData = getRealmInstance().getArtistDataByIdFromDb(artistId: song.artistID) ?? Artist()
                        let getAlbumData = getRealmInstance().getAlbumDataByIdFromDb(albumId: song.albumID) ?? Album()
                       
                        song.isSync = true
                        insertSongArray.append(song)
                        
                        getArtistData.isSync = true
                        insertArtistArray.append(getArtistData)
                        
                        getAlbumData.isSync = true
                        insertAlbumArray.append(getAlbumData)
                    }
                }
                
                ///For RecentPlayed because here fixed limiation till max 5 indexes
                for song in songArray {
                    
                    if  song.songID != nil && song.artistID != nil && song.albumID == nil {
                        recentPlayedParam.append([
                            Constants.Parameters.songid: song.songID ?? 0,
                            Constants.Parameters.artistId: song.artistID ?? 0
                        ])
                        count += 1
                        if count == 5 {break}
                    }
                  
                    if song.songID != nil && song.artistID != nil && song.albumID != nil {
                       
                        recentPlayedParam.append([
                            Constants.Parameters.songid: song.songID ?? 0,
                            Constants.Parameters.albumId: song.albumID ?? 0,
                            Constants.Parameters.artistId: song.artistID ?? 0
                        ])
                        count += 1
                        if count == 5 {break}
                    }
                }
                
                ///Flling according number of counts but max count will be 5
                recentPlayedParam = Array(recentPlayedParam.prefix(upTo: count))
                
                Apiclient.syncData(isRecentlyPlayed: recentPlayedParam.count > 0 ? true : false, isListeningHistory: listeningHistoryParam.count > 0 ? true : false, recentlyPlayed: recentPlayedParam, listeningHistory: listeningHistoryParam, page: ProjectConstants.current.page, limit: ProjectConstants.current.limit)
                    .subscribe(onNext: { ServerResponse in

                        //MARK: Insert Latest Data && Old Data into DB
                        ///For Recent Played
                        if ServerResponse.response?.data?.recentlyPlayed != nil {
                            if ServerResponse.response?.data?.recentlyPlayed?.songs?.count ?? 0 > 0 {
                                if let songs = ServerResponse.response?.data?.recentlyPlayed?.songs {
                                    for song in songs {
                                        song.isSync = true
                                        insertSongArray.append(song)
                                    }
                                }
                            }
                            
                            if ServerResponse.response?.data?.recentlyPlayed?.albums?.count ?? 0 > 0 {
                                if let albums = ServerResponse.response?.data?.recentlyPlayed?.albums {
                                    for album in albums {
                                        album.isSync = true
                                        insertAlbumArray.append(album)
                                    }
                                }
                            }
                            
                            if ServerResponse.response?.data?.recentlyPlayed?.artists?.count ?? 0 > 0 {
                                if let artists = ServerResponse.response?.data?.recentlyPlayed?.artists {
                                    for artist in artists {
                                        artist.isSync = true
                                        insertArtistArray.append(artist)
                                    }
                                }
                            }
                        }
                        ///For Listening History
                        if ServerResponse.response?.data?.listeningHistory != nil {
                            if ServerResponse.response?.data?.listeningHistory?.songs?.count ?? 0 > 0 {
                                if let songs = ServerResponse.response?.data?.listeningHistory?.songs {
                                    for song in songs {
                                        song.isSync = true
                                        insertSongArray.append(song)
                                    }
                                }
                            }
                            
                            if ServerResponse.response?.data?.listeningHistory?.albums?.count ?? 0 > 0 {
                                if let albums = ServerResponse.response?.data?.listeningHistory?.albums {
                                    for album in albums {
                                        album.isSync = true
                                        insertAlbumArray.append(album)
                                    }
                                }
                            }
                            
                            if ServerResponse.response?.data?.listeningHistory?.artists?.count ?? 0 > 0 {
                                if let artists = ServerResponse.response?.data?.listeningHistory?.artists {
                                    for artist in artists {
                                        artist.isSync = true
                                        insertArtistArray.append(artist)
                                    }
                                }
                            }
                        }
                        
                        ///Inserting into database
                        self.getRealmInstance().insertSyncRecentPlayedToDB(songs: insertSongArray, albums: insertAlbumArray, artists: insertArtistArray)
                       
                    }, onError: { Error in
                        let error = Error as? ApiError<ResponseMain>
                        print(error!)
                    }, onCompleted: {
                        self.completion.value = ResponseMain(response: nil)
                        
                    }).disposed(by: bag)
            }
    }
    
    func getPlaylist()->Observable<ResponseMain>{
        return Apiclient.getPlaylist(page: ProjectConstants.current.page, limit: ProjectConstants.current.limit).observeOn(MainScheduler.instance)
    }
    func updatePlaylist(data:Song)->Observable<ResponseMain>{
        return Apiclient.updatePlaylist(data: data).observeOn(MainScheduler.instance)
    }
    
     
    func getFollowArtist()->Observable<ResponseMain>
    {
        return Apiclient.getFollowingArtist().observeOn(MainScheduler.instance)
    }

    func savePlaylist(title: String)->Observable<ResponseMain>{
        return Apiclient.savePlayist(title: title).observeOn(MainScheduler.instance)
    }
    func subscribeToPlan(data: Song)->Observable<MainResponse>{
        return Apiclient.subscribeToPlan(data: data).observeOn(MainScheduler.instance)
    }
    
    func validateSubscriptionToken(token:String) -> Observable<AppStoreResponse>{
        return Apiclient.validateSubscriptionToken(token: token).observeOn(MainScheduler.instance)
    }
    
    func deletePlaylist(playlistID:Int)->Observable<ResponseMain>{
        return Apiclient.deletePlaylist(playlistId: playlistID).observeOn(MainScheduler.instance)
    }

    func getBuyingHistory(data: Song) -> Observable<ResponseMain> {
        return Apiclient.buyingHistory(data: data, page: ProjectConstants.current.page, limit: ProjectConstants.current.limit) .observeOn(MainScheduler.instance)
    }
    //DATABASE
    func getSongsDataFromDb()->[Song]?{
        return getRealmInstance().getSongsDataFromDb()
    }
    
    func getAlbumsDataFromDb()->[Album]?{
        return getRealmInstance().getAlbumsDataFromDb()
    }
    
    func getArtistsDataFromDb()->[Artist]?
    {
        return getRealmInstance().getArtistsDataFromDb()
    }
    
    
    func setUpRecentlyPlayedDB(){
        getRealmInstance().setUpRecentlyPlayedDB()
    }
    
    
    func insertCategorieToDb(categories:[Category]?){
        getRealmInstance().insertCategorieToDb(categories:categories)
    }
    func getCategoriesFromDb()->[Category]?{
        return getRealmInstance().getCategoriesFromDb()
    }
    
    func insertAlbumIntoDb(album:Album?){
        getRealmInstance().insertAlbumIntoDb(album: album)
    }
    
    func insertArtistIntoDb(artist:Artist?) {
        getRealmInstance().insertArtistIntoDb(artist: artist)
    }
    
    func insertSongIntoDb(song:Song?){
        getRealmInstance().insertSongIntoDb(song: song)
    }
    func setIsFavourite(songObj:Song)->Song?{
        getRealmInstance().setIsFavourite(songObj: songObj)
    }
    func insertLocalPathForMedia(song:Song,FilePath:String){
        getRealmInstance().insertLocalPathForMedia(song: song, FilePath: FilePath)
    }
    
    
    // Deletion
    func deleteSongsDataFromDb() -> [Song]?
    {
        return delRealmInstance().deleteSongsDataFromDb()
    }
    
    func deleteAlbumsDataFromDb() -> [Album]?
    {
        return delRealmInstance().deleteAlbumsDataFromDb()
    }
    
    func deleteArtistDataFromDb() -> [Artist]?
    {
        return delRealmInstance().deleteArtistsDataFromDb()
    }
    
    
    private func getRealmInstance() -> RealmManager{
        return RealmManager()
    }
    
    private func delRealmInstance() -> RealmManager{
        return RealmManager()
    }
}
