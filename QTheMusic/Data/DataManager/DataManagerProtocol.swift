//
//  DataManagerProtocol.swift
//  Udeo Globe
//
//  Created by Nasar Iqbal on 5/5/20.
//  Copyright © 2020 Yasir Iqbal. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire

protocol DataManagerProtocol{
    
    func login(data:Authorization)->Observable<ResponseMain>
    func sendOtp(data:Authorization)->Observable<ResponseMain>
    func verifyOtp(data:Authorization)->Observable<ResponseMain>
    func signUp(data:Authorization)->Observable<ResponseMain>
    func resetPassword(data:Authorization)->Observable<ResponseMain>
    func logout(data: Authorization) ->Observable<ResponseMain>
    func profileSetup(data:Authorization)->Observable<ResponseMain>
    func getCategories(categoryType:String?, page: Int, limit: Int)->Observable<ResponseMain>
    func saveInterest(categories:[Category]?)->Observable<ResponseMain>
    func favouriteSong(data: Song) -> Observable<ResponseMain>
    
    func searchSong(data:Song,page: Int, limit: Int)->Observable<ResponseMain>
    
    func songs(data:Song?,page: Int, limit: Int)->Observable<ResponseMain>
    func getRecommendedSongs(data:Song?)->Observable<ResponseMain>
    func getArtitstDetail(artistDetail:Int, page: Int, limit: Int)->Observable<ResponseMain>
    func getPreOrderDetail(artistDetail:Int, page: Int, limit: Int)->Observable<ResponseMain>
    func followArtist(artistID:Int,isFollowed:Bool)->Observable<ResponseMain>

    func setIsFavourite(songObj:Song)->Song?
    func nextPlaySongs(playedFrom:Song, page: Int, limit: Int)->Observable<ResponseMain>
    func getPlaylist()->Observable<ResponseMain>
    func updatePlaylist(data:Song)->Observable<ResponseMain>
    
    func savePlaylist(title:String)->Observable<ResponseMain>
    func deletePlaylist(playlistID:Int)->Observable<ResponseMain>
    
    func getFollowArtist()->Observable<ResponseMain>
    func getPackagesPlan(page: Int, limit: Int)->Observable<ResponseMain>
    func getBuyingHistory(data: Song)->Observable<ResponseMain>
    func syncRecentlyPlayedData()
    func subscribeToPlan(data: Song)->Observable<MainResponse>
    func validateSubscriptionToken(token:String) -> Observable<AppStoreResponse>
    //DATABASE
    func setUpRecentlyPlayedDB()
    func insertCategorieToDb(categories:[Category]?)
    func getSongsDataFromDb()->[Song]?
    func getAlbumsDataFromDb()->[Album]?
    func getArtistsDataFromDb()->[Artist]?
    
    func deleteSongsDataFromDb()->[Song]?
    func deleteAlbumsDataFromDb()->[Album]?
    func deleteArtistDataFromDb()->[Artist]?
    
    func getCategoriesFromDb()->[Category]?
    func updateProfile(data:Authorization) -> Observable<ResponseMain>
    
    func insertAlbumIntoDb(album:Album?)
    func insertArtistIntoDb(artist:Artist?)
    func insertSongIntoDb(song:Song?)
    func insertLocalPathForMedia(song:Song,FilePath:String)
    
}
