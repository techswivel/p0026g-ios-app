//
//  DownloadManager.swift
//  MultiDownload
//
//  Created by KA TO on 2020/04/08.
//


import Foundation
import os
import NotificationCenter

enum DownloadError: Error {
    case packetFetchError(String) // Error occured while downloading
    case wrongOrder(String) // Add complete block with wrong order
}

@objc protocol DownloadProcessProtocol {
    
    /// Share downloding progress with outside
    ///
    /// - parameter percent:  Download percentage of current file
    func downloadingProgress(_ percent: Float, fileName: String)
    
    /// Get called when download complete
    func downloadSucceeded(_ fileName: String)
    
    /// Get called when error occured while download.
    ///
    /// - parameter error: Download error occurred by URLSession's download task
    func downloadWithError(_ error: Error?, fileName: String)
    ///

    
}

/// Manager of asynchronous download `Operation` objects
@objcMembers class DownloadManager: NSObject {
    
    
    private var mViewModel = DownloadManagerViewModel()
    internal var song:Song?
    var onProgress: ((Double) -> ())?
    var onCompleted: ((URL) -> ())?
    
    private let mDataManager:DataManager = DataManager()
    static var shared = DownloadManager()
    
    /// Dictionary of operations, keyed by the `taskIdentifier` of the `URLSessionTask`
    fileprivate var operations = [Int: DownloadOperation]()
    
    /// Set  download count that can execute at the same time.
    /// Default is 20 to make a serial download.
    public static var maxOperationCount = 20
    
    /// Serial NSOperationQueue for downloads
    private let queue: OperationQueue = {
        let _queue = OperationQueue()
        _queue.name = "download"
        _queue.maxConcurrentOperationCount = maxOperationCount
        return _queue
    }()
    
    /// Delegate-based NSURLSession for DownloadManager
    lazy var session: URLSession = {
        let config = URLSessionConfiguration.background(withIdentifier: "\(Bundle.main.bundleIdentifier!).background")
        ///the property 'isDiscretionary' is commented now. If you set this property to true, you tell the system that you basically don't care when the file is downloaded. So the system will try to optimise the download as much as possible,
        //config.isDiscretionary = true
        
        ///the 'sessionSendsLaunchEvents' to true to have your app woken up by the system when the download completes.
        config.sessionSendsLaunchEvents = true
        config.shouldUseExtendedBackgroundIdleMode = true
        config.allowsCellularAccess = true
        return URLSession(configuration: config, delegate: self, delegateQueue: nil)
    }()
    
    /// Track the download process
    var  processDelegate : DownloadProcessProtocol?
    
    /// Add download links
    ///
    /// - parameter url: The file's download URL
    ///
    /// - returns:  A downloadOperation of the operation that was queued
    @discardableResult
    @objc func addDownload(_ url: URL) -> DownloadOperation {
        let operation = DownloadOperation(session: session, url: url)
        operations[operation.task.taskIdentifier] = operation
        queue.addOperation(operation)
        return operation
    }

    /// Cancel all queued operations
    func cancelAll() {
        queue.cancelAllOperations()
    
    }
    
    
    
    
}

//MARK: - URL Session Delegate
extension DownloadManager : URLSessionDelegate {
    
    /// Did Finish Event From BackGround
    func urlSessionDidFinishEvents(forBackgroundURLSession session: URLSession) {
    }
}



// MARK: URLSessionDownloadDelegate methods

extension DownloadManager :URLSessionDownloadDelegate {
    /// Download Progress
    func urlSession(_: URLSession, downloadTask: URLSessionDownloadTask, didWriteData _: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
       
        if let downloadUrl = downloadTask.originalRequest!.url {
            let percent = Double(totalBytesWritten)/Double(totalBytesExpectedToWrite)
            DispatchQueue.main.async { [self] in
                processDelegate?.downloadingProgress(Float(percent), fileName:  downloadUrl.lastPathComponent)
            }
        }

    }
    
    /// Did Finished Downloading
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        os_log("Download finished: %@",log:OSLog.default ,type: .info, location.absoluteString)
        
       
        
        
        /// delegate to inform about download succeeded
        if let downloadUrl = downloadTask.originalRequest!.url {
            DispatchQueue.main.async { [self] in
                processDelegate?.downloadSucceeded(downloadUrl.lastPathComponent)
            }
        }

        
        
        
        
        do {
            let manager = FileManager.default
            let destinationURL = try manager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            .appendingPathComponent(downloadTask.originalRequest!.url!.lastPathComponent)
            if manager.fileExists(atPath:  destinationURL.path) {
                try manager.removeItem(at: destinationURL)
            }
            try manager.moveItem(at: location, to: destinationURL)
            DispatchQueue.main.async {
                self.mViewModel.insertLocalPathForMedia(song: self.song ?? Song(), FilePath: destinationURL.absoluteString)
            }
           
            
        }
        catch {
            print("\(error)")
        }
        
        
    }
    
    
    
    func urlSession(_: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        if let error = error {
            os_log("Download error: %@", type: .error, String(describing: error))
        } else {
            os_log("Task finished: %@",log:OSLog.default, type: .info, task)
        }
                let key = task.taskIdentifier
                operations[key]?.trackDownloadByOperation(session, task: task, didCompleteWithError: error)
                operations.removeValue(forKey: key)
        
                if let downloadUrl = task.originalRequest!.url, error != nil {
                    DispatchQueue.main.async { [self] in
                        processDelegate?.downloadWithError(error, fileName: downloadUrl.lastPathComponent)
                    }
                }
    }
}







