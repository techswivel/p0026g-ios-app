//
//  DownloadManagerViewModel.swift
//  QtheMusic
//
//  Created by Assad Khan on 13/09/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import Foundation

class DownloadManagerViewModel:BaseViewModel{

    func insertLocalPathForMedia(song:Song,FilePath:String){
        mDataManager.insertLocalPathForMedia(song: song, FilePath: FilePath)
    }
}
