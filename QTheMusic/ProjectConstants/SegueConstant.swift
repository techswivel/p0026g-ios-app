//
//  SegueConstant.swift
//  Base iOS Project
//
//  Created by Mac on 13/01/2021.
//  Copyright © 2021 TechSwivel. All rights reserved.
//

import Foundation

//MARK: - Segue Constant
let SEGUE_SPLASH_TO_HOME = "goToHome"
let SEGUE_HOME_TO_SPLASH = "goToSplash"

let SEGUE_SPLASH_TO_SIGNINSCREEN = "goToSignInScreen"
let SEGUE_SIGNIN_TO_FORGETPASSWORD = "goToForgetPasswordScreen"
let SEGUE_PROFILE_SETTING = "goToProfileSetting"
let SEGUE_PROFILE_UPDATE = "goToProfileUpdate"
let SEGUE_TERMS_PRIVACY_SCREEN = "goToTermsAndPrivacyScreen"

