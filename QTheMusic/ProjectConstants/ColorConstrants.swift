//
//  ColorConstrants.swift
//  Base iOS Project
//
//  Created by Mac on 13/01/2021.
//  Copyright © 2021 TechSwivel. All rights reserved.
//

import Foundation
import UIKit


public class ColorConstrants {
    
    static let AQUA_THEME = UIColor.init(rgb: 0x10B2B9)
    static let FACEBOOK_COLOR = UIColor.init(rgb: 0x4267B2)
    static let GOOGLE_COLOR = UIColor.init(rgb: 0xD9393F)
    static let TEXTFIELD_BORDER_COLOR = UIColor.init(rgb: 0x575757)
    static let PRIMARY_COLOR = UIColor.init(rgb: 0xBA011A)
    static let BACKGROUND_GRADIENT_COLOR = UIColor.init(rgb: 0x545454)
    static let RED_THEME = UIColor.init(rgb: 0xBA011A)
    static let UNSELECTEDCELL_INTERESTCOLOR = UIColor.init(rgb: 0x545454)
    static let SILVER_COLOR = UIColor.init(rgb: 0xB1B1B1)
    static let FOLLOWINGBTN_COLOR = UIColor.init(rgb: 0x28B446)
    
    
}
