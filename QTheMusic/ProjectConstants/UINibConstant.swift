//
//  UINibConstant.swift
//  QtheMusic
//
//  Created by Assad Khan on 14/03/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import Foundation
class UINibConstant{
    static let ActivityIndicatorNib = "ActivityIndicatorView"
    static let paymentFailedNib = "PaymentFailedView"
    static let paymentSuccessNib = "PaymentSuccessView"
    static let CalendarViewNib = "CalendarView"
    static let selectGenderNib = "SelectGender"
    static let paymentTypeNib = "PaymentType"
    static let WhyWeAreAskingNib = "WhyWeAreAsking"
    
    static let InterestCollectionViewCellNib = "InterestCollectionViewCell"
    static let InterestCollectionViewCellIdentifier = "InterestCollectionViewCellIdentifier"

    static let RecommendedForYouView = "RecommendedForYouView"
    static let YourMoodView = "YourMoodView"
    static let TrendingSongsView = "TrendingSongsView"

    static let SearchSongsCellIdentifier = "SearchSongsCellIdentifier"
    static let SearchingSongTableViewCellNib = "SearchingSongTableViewCell"
    
    static let ArtistCollectionViewCellNib = "ArtistCollectionViewCell"
    static let AlbumCollectionViewCellNib = "AlbumCollectionViewCell"
    static let SongsTableViewCellNib = "SongsTableViewCell"
    
    static let ArtistCollectionViewCellIdentifier = "ArtistCollectionViewCellIdentifier"
    static let AlbumCollectionViewCellIdentifier = "AlbumCollectionViewCellIdentifier"
    static let SongsTableViewCellIdentifier = "SongsTableViewCellIdentifier"
    
    static let RecentlyPlayedView = "RecentlyPlayedView"
    
    static let ListeningHistoryView = "ListeningHistoryView"
    

    static let CategoriesCollectionViewCellNib = "CategoriesCollectionViewCell"
    static let CategoriesCollectionCellIdentifier = "categoriesCell"
    
    static let SongsCollectionViewCellNib = "SongsCollectionViewCell"
    static let SongsCollectionViewCellIdentifier = "SongsCollectionViewCellIdentifier"
    
    static let ProfileListTableViewCellNib = "ProfileListTableViewCell"
    static let ProfileListTableViewCellIdentifier = "profileListCell"
    
    static let PhoneNumberView = "PhoneNumberView"
    static let ChangeUserNameView = "ChangeUserNameView"
    static let ChangeAddressView = "ChangeAddressView"

    static let CategoryCollectionViewCellNib = "CategoryCollectionViewCell"
    static let CategoryCollectionViewCellIdentifier = "CategoryCollectionViewCellIdentifier"
    
    static let PreOrderSongsTableViewCellNib = "PreOrderSongsTableViewCell"
    static let PreOrderSongsTableViewCellIdentifier = "PreOrderSongsTableViewCellIdentifier"
    
    static let SongAndAlbumUnlockingScreenNib = "SongAndAlbumUnlockingScreen"
    
    static let PlayListCellIdentifier = "PlayListCellIdentifier"
    static let PlayListTableViewCell = "PlayListTableViewCell"
    
    static let AllPlaylistViewNib = "AllPlaylistView"
    static let DownloadScreenNib = "DownloadScreen"

    static let Addplaylist = "AddPlaylistView"
    static let PlaylistCellnib = "PlaylistTableCell"
    static let PlaylistCellIdentifier = "PlaylistTableCellidentifier"
    
    static let SonglisttableCellnib = "SonglistTableCell"
    static let SonglisttableCellIdentifier = "SonglistTabelCellidentifier"
    
    static let FollowingArtistTableCellnib = "FollowingArtistTableCell"
    static let FollowingArtistTableCellIdentifier = "FollowingArtistTableCellidentifier"
    
    static let PurchasedAlbumcellnib = "PurchasedAlbumcell"
    static let PurchasedAlbumcellIdentifier = "PurchasedAlbumcellidentifier"
    
    static let ListeningSongTableCellnib = "ListeningSongTableCell"
    static let ListeningSongTableCellidentifier = "ListeningSongTableCellIdentifier"
    
    static let ListeningTitleCellnib = "ListeningTitleCollectionCell"
    static let ListeningTitleCellidentifier = "ListeningTitleCollectionCellIdentifier"
    
    static let DownloadingSongsCellnib = "DownloadingSongsCell"
    static let DownloadingSongsCellidentifier = "DownloadingSongsCellIdentifier"
    
    static let BuyingSongCellnib = "BuyingSongCell"
    static let BuyingSongCellidentifier = "BuyingSongCellIdentifier"
    
    static let BuyingAlbumCellnib = "BuyingAlbumCell"
    static let BuyingAlbumCellidentifier = "BuyingAlbumCellIdentifier"
    
    static let AlbumSongCellnib = "AlbumSongCell"
    static let AlbumSongCellidentifier = "AlbumSongCellIdentifier"
    
    static let subscriptionPlanCellnib = "SubsciptionPlanCell"
    static let SavedCardCellnib = "SavedCardCell"
}

