//
//  StringConstant.swift
//  Base iOS Project
//
//  Created by Mac on 13/01/2021.
//  Copyright © 2021 TechSwivel. All rights reserved.
//

import Foundation

//MARK: - String Constant
class StringConstant{
    
    
let WelcomeMessage = "welcome to base project"
    static let AlertButtonYes = "Yes"
    static let AlertButtonNo = "NO"
    static let AlertOkAction = "OK"
    static let ErrorAlert = "Something went wrong!"
    static let Ok = "OK"
    static let validationError = "Validation Error"
    
    ///Validation Class String Constants.
    static let inValidEmail = "InvalidEmail"
    static let invalidFirstLetterCaps = "First Letter should be capital"
    static let invalidPhone = "Invalid Phone"
    static let invalidAlphabeticString = "Invalid String"
    static let inValidPSW = "Invalid Password"
    static let emptyPhone = "Empty Phone"
    static let emptyEmail = "Empty Email"
    static let emptyFirstLetterCaps = "Empty Name"
    static let emptyAlphabeticString = "Empty String"
    static let emptyPSW = "Empty Password"
    static let emailIsMissing = "Email is missing"
    static let emailISNotValid = "Email is invalid"
    static let passwordIsMissing = "Password is missing"
    static let confirmPasswordIsMissing = "Confirm password is missing"
    static let passwordIsNotValid = "Your password must include one uppercase letter, and must include one lower letter, and must include number, and a special chracter"
    static let newPassDoesntMatchConfirmPass = "New password doesn't match confirm password"
    ///Validation Enum
    static let firstNameValidation  = "* Please provide first name."
    static let lastNameValidation = "* Please provide last name."
    static let emailValidation = "* Please provide valid email."
    static let signInPasswordValidation = "* Please enter your password."
    static let signUpPasswordValidation = "* Password should be at least 6 character long."
    static let confirmPasswordValidation = "* Password does not match"
    static let FailedDatabaseMessage = "failed to init database cauesed by "
    static let email = "Email"
    static let password = "Password"
    static let confirmPassword = "Confirm Password"
    
    static let visibiltyOnImage = "eye"
    static let visibiltyOffImage = "closeEye"
    
    
    
    static let thirty  = "30"
    static let resendCode = "Resend Code "
    static let resendIn = "Resend In "
    
    static let otpVerfication = "OTP Verification"
    static let otpIsMissing = "OTP code is missing"
    static let otpMustBe5Char =  "OTP code must be 5 characters long."
    static let otpMessage  = "OTP sent successfully!"
    
    static let numberAllowedOnly = "Numbers allowed only"
    static let name = "Name"
    static let dob = "Date of Birth"
    static let gender = "Gender"
    static let address = "Address"
    static let city = "City"
    static let state = "State"
    static let country  = "Country"
    static let zipCode = "Zipcode"
    
    static let Male = "Male"
    static let FeMale = "Female"
    static let NonBinary = "Non-binary"
    static let NoAnswer = "No answer"
    static let nameIsRequired = "Name is required"
    static let dobIsRequired = "Date of birth is required"
    
    static let aTitle = "Title"
    static let tDelete = "Delete"
    
   //Update Alert
    
    static let updatePlaylist = "Update Playlist!"
    static let titleName = "Change your list title."
    
    static let updateBtn = "Update"
    static let cancelBtn = "Cancel"
    
    static let tfplaceholder = "Update Title name"
    
    //SocialSignInandSignUpViewController
    static let failedToLoginWithFacebook = "Failed to login with Facebook"
    static let failedToGetFacebookAccess = "Failed To Get Facebook Access Token"
    static let trackingAccessIsRequired = "Tracking access is required "
    static let trackingAccessMessage = "If you want to Continue with Facebook then you have to allow tracking in app setting. We do not collect your any data. It's just required data for creating your social account. "
    static let settings = "Setting"
    static let cancel = "Cancel"
    static let couldNotGetDataFromServer = "Couldn't get data from server, Try again later!"
    static let failedToGetGoogleAccess = "Failed To Get Google Access Token"
    static let operationCAnceledSuccessfully = "Operation cancel successfully."
    static let failedToGetFirebaseToken = "Failed To Get Firebase Access Token"
    static let appleSignupIsnotAvailable = "Apple login is not available below ios 13!"
    static let maximumFiveInterestCanBeSelected = "Maximum 5 topics can be selected."
    static let atleastSelectTwoInterest = "Atleast select two topics"
    
    static let all = "All"
    
    static let profilePlaceHolder = "profilePlaceHolder"
    static let underDevelopment = "Under Development"
    static let follow = "Follow"
    static let following = "Following"

    static let emailNotFound = "No email found"
    static let sessionExpiryAlerTitle = "Session Expire!."
    static let sessionExpiryAlerMsssage = "Your session has been expire you need to re-login."
    
    static let pickCountry = "Pick Country"
   static let nothingToUpdate = "Nothing to update.!"
    
    static let songs = "Songs"
    
    
    
    static let playImage = "playWhite"
    static let zeroTime = "00:00"
    static let pauseImage = "pauseWhite"
    static let stopped = "Stopped"
    static let titleOfAudioKey = "TitleOfAudioKey"
    static let paused = "Paused"
    
    static let noLyricsFound = "No lyrics available."
    static let couldntPlayAudioFile = "Couldn't play audio file"
    static let playingFrom = "PLAYING FROM "
    
    
    
    static let catTypeKey = "catTypeKey"
    static let saveSongKey = "saveSongKey"
    static let saveIndexKey = "saveIndexKey"
    static let saveSongArrayKey = "saveSongArrayKey"
    static let isPlayingFromKey = "isPlayingFromKey"
    static let isPlayingKey = "isPlayingKey"
    static let ResumingMiniPlayerKey  = "ResumingMiniPlayer"
    
    static let audioFullTimeKey = "audioFullTimeKey"
    static let audioCurrentStatusBarStatusKey = "audioCurrentStatusBarStatusKey"
    static let resumeplayeratCurrentPositionKey = "resumeplayeratCurrentPositionKey"
    static let thisIsPremiumAlbum = "This is a Premium Album."
    static let unlockPremiumAlbum = "Unlock this song by paying the album creator."
    static let thisIsPremiumSong = "This is a Premium song."
    static let unlockPremiumSong = " Unlock this song by paying the song creator."
    
    
    //MARK: - Local Notification
    static let backGroundDownloadNotificationTitle = "Download complete!"
    static let backGroundDownloadNotificationBody = "Your background transfer has completed."
    static let backGroundDownloadNotificationIdentifier = "TransferComplete"
    
    //MARK: - Sync Status
    static let enqueued = "ENQUEUED"
    static let running = "RUNNING"
    
    static let thisFeaturesAvailable = "This Feature is only available for Premium users."
    static let addToFavorite  = "Add to Favorite"
    static let removeFromFavorite = "Remove from Favorite"
    static let selectPlayList = "Select Playlist"
    
    static let productisNotAvailabe = "product is not available"
    static let completeAddress = "Please Add Your Complete Address in Profile"
    public static let Unknown_Case = "Unknown Error Occured.. Try Again"
    public static let Invalid_Client = "InvalidClient.. Try Again"
    public static let Payment_Cancelled = "Your Payment Has Been Cancelled"
    public static let Invalid_Payment = "Your Payment is Invalid"
    public static let Payment_Not_Allowed = "Your Payment is not Allowed"
    public static let Product_Not_Available = "The Selected Product is not Available"
    public static let Cloud_Service_Permission_Denied = "Cloud Service Permission Denied"
    public static let Cloud_Service_Network_Connection_Failed = "Cloud Service Network Connection Failed"
    public static let Something_Went_Wrong = "Something Went Wrong.. Try Again"
    public static let Restored = "Your Payment is Restored"
    public static let Suspended = "Your Payment is Suspended"
    public static let Payment_Failed = " Payment Failed"
    public static let addToPlaylist = "Add To Playlist"
    public static let removeFromPlaylist = "Remove From Playlist"
    public static let premium = "PREMIUM"
    public static let purchased = "PURCHASED"
    public static let premiumSongs = "Premium Songs Cannot be added To Playlist"
    public static let favouriteSongs = "Premium Songs Cannot be added To Favourite"
}

