//
//  ProjectConstants.swift
//  Base iOS Project
//
//  Created by Hammad Hassan on 23/11/2020.
//  Copyright © 2020 TechSwivel. All rights reserved.
//

import Foundation
import RxSwift

class ProjectConstants {
    
    static let current = ProjectConstants()
    
    let ACCEPTANCE_URL = "https://acceptance.qthemusic.app/api/v1/"
    let STAGING_URL = "https://staging.qthemusic.app/api/v1/"
    let Development_URL = "https://staging.qthemusic.app/api/v1/"
    let PRODUCTION_URL = "https://production.qthemusic.app/api/v1/"
    
    let SANDBOX_APP_PURCHASE_URL = "https://sandbox.itunes.apple.com/"
    let PRODUCTION_APP_PURCHASE_URL = "https://buy.itunes.apple.com/"
    
    private let STAGING_DATABASE_NAME = "staging.realm"
    private let ACCEPTANCE_DATABASE_NAME = "acceptance.realm"
    private let DEVELOPMENT_DATABASE_NAME = "development.realm"
    private let PRODUCTION_DATABASE_NAME = "production.realm"
    
    let KEY_CHAIN_SHARED_GROUP = "H8KC4GDBS6.99Q7SK7UH92.deviceIdentifier.QtheMusickeychainGroup"
    
    let API_STAGING_KEY = "eb366ea0-8479-4cfb-9b2a-d4780b75d9ae"
    let API_DEVELOPMENT_KEY = "d4864608-2426-410d-bff7-6c599bb7df83"
    let API_ACCEPTANCE_KEY = "4871083f-30d4-494e-8934-3b06acb0fdeb"
    let API_PRODUCTION_KEY = "eb366ea0-8479-4cfb-9b2a-d4780b75d9ae"
    
    ///in-app purchase shared keys
    let APP_SECRIT = "c7228eac974a4d7ebe165574d9e3956f"
    let STAGING_APP_SECRIT = "5a4a1bdf0cd047368917b235dd7fbb3a"
    let ACCEPTANCE_APP_SECRIT = "7b10d81f5d924881869e4a08cd57aaa9"
    
    let GOOGLE_KEY = "Some Google Key"
    let GOOGLE_CLIENT_ID = "Google Client Id."

    let STRIPE_TEST_KEY = "Stripe Test Key"
    let STRIPE_LIVE_KEY = "Stripe Live Key"
    
    //Privacy and policy URL
    let PRIVACY_AND_POLICY_URL = "Privacy And Policy Url"
    // Defaul Message String when Failed in Error
    let DEFAULT_ERROR_MESSAGE = "Sorry! Data couldn't be retrieved from Server."
    let DEFAULT_DELETE_ERROR_MESSAGE = "Sorry! Data couldn't be deleted from Server."
    let DEFAULT_POST_ERROR_MESSAGE = "Sorry! Data couldn't be posted to Server."
    
    
    let GOOGLE_CLIENT_ID_STAGING = "74605093159-k7idp16mtord75qp7kt7f4iachkk313h.apps.googleusercontent.com"
    let GOOGLE_CLIENT_ID_DEVELOPMENT = "74605093159-8sfuk9fceen4dsi8e8a585gqmc63l3p5.apps.googleusercontent.com"
    let GOOGLE_CLIENT_ID_ACCEPTANCE = "345540049924-gb6k5328p577pbr1p1v53lfk0e7770qo.apps.googleusercontent.com"
    let GOOGLE_CLIENT_ID_PRODUCTION = "716441253502-o7jc1u1sg2f4a4ossaich1ist02s5hkq.apps.googleusercontent.com"
    
    
    let countDownTimeForOtp:Double = 30
    
    let MIN_VIDEO_LENGTH_IN_SECOND = 5
    let MAX_VIDEO_LENGTH_IN_SECOND = 25 // in second
    
    
    // Pages limit
    let page = 1
    let limit = 300
    
    /// Firebase Messages Topic
    let FIRE_BASE_MESSAGES_TOPIC = "iOS_Users"
    let FREE_PLAN_USER = "free_plan_user"
    
    ///Set Value For Timer Of  Sync Recent Playaed Endpoint
    func getTimerValue() -> (TimeInterval,TimeInterval) {
        if ConfigurationManager.currentEnvironment == Environment.Acceptance {

            let DEFAULT_SYNC_TIMMER_VALUE:TimeInterval = 900
            let SYNC_FETCH_TIME_VALUE:TimeInterval = 900
           return (DEFAULT_SYNC_TIMMER_VALUE, SYNC_FETCH_TIME_VALUE)
            
        }
        else if ConfigurationManager.currentEnvironment == Environment.Staging {
            let DEFAULT_SYNC_TIMMER_VALUE:TimeInterval = 60
            let SYNC_FETCH_TIME_VALUE:TimeInterval = 60
           return (DEFAULT_SYNC_TIMMER_VALUE, SYNC_FETCH_TIME_VALUE)
        }
        else if ConfigurationManager.currentEnvironment == Environment.Development {
            let DEFAULT_SYNC_TIMMER_VALUE:TimeInterval = 900
            let SYNC_FETCH_TIME_VALUE:TimeInterval = 900
           return (DEFAULT_SYNC_TIMMER_VALUE, SYNC_FETCH_TIME_VALUE)
        }
        else if ConfigurationManager.currentEnvironment == Environment.Production {
            let DEFAULT_SYNC_TIMMER_VALUE:TimeInterval = 900
            let SYNC_FETCH_TIME_VALUE:TimeInterval = 900
           return (DEFAULT_SYNC_TIMMER_VALUE, SYNC_FETCH_TIME_VALUE)
        }
        else {
            //Staging
            let DEFAULT_SYNC_TIMMER_VALUE:TimeInterval = 60
            let SYNC_FETCH_TIME_VALUE:TimeInterval = 60
           return (DEFAULT_SYNC_TIMMER_VALUE, SYNC_FETCH_TIME_VALUE)
        }
    }
    
    func getBaseURL() -> String! {
        
        
        if ConfigurationManager.currentEnvironment == Environment.Acceptance {
            return self.ACCEPTANCE_URL
        }
        else if ConfigurationManager.currentEnvironment == Environment.Staging {
            return self.STAGING_URL
        }
        else if ConfigurationManager.currentEnvironment == Environment.Development {
            return self.Development_URL
        }
        else if ConfigurationManager.currentEnvironment == Environment.Production {
            return self.PRODUCTION_URL
        }
        
        return self.STAGING_URL
    }
    
    func getApiKey() -> String {
        
        if ConfigurationManager.currentEnvironment == Environment.Acceptance {
            return self.API_ACCEPTANCE_KEY
        }
        else if ConfigurationManager.currentEnvironment == Environment.Staging {
            return self.API_STAGING_KEY
        }
        else if ConfigurationManager.currentEnvironment == Environment.Development {
            return self.API_DEVELOPMENT_KEY
        }
        else if ConfigurationManager.currentEnvironment == Environment.Production {
            return self.API_PRODUCTION_KEY
        }
        
        return self.API_PRODUCTION_KEY
    }
    
    func getDatabaseName() -> String! {
        
        if ConfigurationManager.currentEnvironment == Environment.Acceptance {
            return self.ACCEPTANCE_DATABASE_NAME
        }
        else if ConfigurationManager.currentEnvironment == Environment.Staging {
            return self.STAGING_DATABASE_NAME
        }
        else if ConfigurationManager.currentEnvironment == Environment.Development {
            return self.DEVELOPMENT_DATABASE_NAME
        }
        else if ConfigurationManager.currentEnvironment == Environment.Production {
            return self.PRODUCTION_DATABASE_NAME

        }
        
        return ""
    }
    
    func getSubscriptionBaseURL() -> String! {
        
        
        if ConfigurationManager.currentEnvironment == Environment.Acceptance {
            return self.SANDBOX_APP_PURCHASE_URL
        }
        else if ConfigurationManager.currentEnvironment == Environment.Staging {
            return self.SANDBOX_APP_PURCHASE_URL
        }
        else if ConfigurationManager.currentEnvironment == Environment.Development {
            return self.SANDBOX_APP_PURCHASE_URL
        }
        else if ConfigurationManager.currentEnvironment == Environment.Production {
            return self.SANDBOX_APP_PURCHASE_URL
        }
        
        return ""
    }
    
    func getSubscriptionAppSecrit() -> String! {
        
        
        if ConfigurationManager.currentEnvironment == Environment.Acceptance {
            return self.ACCEPTANCE_APP_SECRIT
        }
        else if ConfigurationManager.currentEnvironment == Environment.Staging {
            return self.STAGING_APP_SECRIT
        }
        else if ConfigurationManager.currentEnvironment == Environment.Development {
            return self.STAGING_APP_SECRIT
        }
        else if ConfigurationManager.currentEnvironment == Environment.Production {
            return self.APP_SECRIT
        }
        
        return ""
    }
}
