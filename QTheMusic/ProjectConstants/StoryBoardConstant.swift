//
//  StoryBoardConstant.swift
//  QtheMusic
//
//  Created by Assad Khan on 04/04/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import Foundation
import UIKit

class StoryBoardConstant{
    
    static let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
    static let AuthStoryBoard = UIStoryboard(name: "Auth", bundle: nil)
    static let storyBoardAuth = "Auth"
    static let storyBoardMain = "Main"
    static let tabbarRootVC = "TabBarRootVC"
    static let splashVC = "SplashScreen"
    static let yourInterestScreen = "YourInterestScreen"
    static let ServerSettingScreen  = "ServerSettingScreen"
    static let OTPScreenController = "OTPScreen"
    static let resetPasswordScreen = "ResetPasswordScreen"
    static let ProfileSetUpScreenVc = "ProfileSetUpScreen"
    static let ServerSettingScreenVC = "serverSettingScreen"
    
    static let searchingScreenVC = "SearchingScreen"
    

    static let SignInVC = "SignInVC"
    static let SignUPVC = "SignUPVC"

    
    static let AlbumDetailVc = "AlbumDetailVc"
    

    static let CategoryDetailVC  = "CategoryDetailVC"

    static let ArtistDetailViewController = "ArtistDetailViewController"
    static let SongsDetailViewController = "SongsDetailViewController"

    static let AllAlbumsVC = "AllAlbumsVC"

    static let PreOrderVC = "PreOrderVC"
    
    static let UnderDevelopmentVC = "UnderDevelopmentVC"
    static let LyricsVC = "LyricsVC"
    
    static let miniPlayerVC = "MNPVC"
    
    static let ScreenOverLayVC  = "ScreenOverLayVC"
    
    static let playlistVC = "playlistViewController"
    
    static let purchasedsongVC = "purchasedSongVC"
    
    static let purchasedalbumVC  = "purchasedAlbumVC"
    
    static let followingartistVC = "followingArtistVC"
    
    static let favouritesongVC = "favouriteSongsVC"
    
    static let songslistScreen = "songPlaylistS"
    
    static let artistdetailVC = "artistDetailVC"
    
    static let listeningHistoryVC = "listeningHistory"
    
    static let downloadedSongVC = "downloadedSongs"
    
    static let downloadingSongsVC = "DownloadingSongsVC"
    
    static let buyingHistoryVC = "BuyingHistoryVC"
    
    static let subscriptionPlanVC = "SubscriptionPlanViewController"
    
}


