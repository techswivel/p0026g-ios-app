//
//  FontConstants.swift
//  Base iOS Project
//
//  Created by Mac on 13/01/2021.
//  Copyright © 2021 TechSwivel. All rights reserved.
//

import Foundation
import UIKit

public class FontConstants {
    
    static func getManropeRegular(size : CGFloat) -> UIFont {
        return UIFont.init(name: "Manrope", size: size)!
    }
}
