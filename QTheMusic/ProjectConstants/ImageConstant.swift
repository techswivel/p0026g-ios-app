//
//  ImageConstant.swift
//  Base iOS Project
//
//  Created by Mac on 13/01/2021.
//  Copyright © 2021 TechSwivel. All rights reserved.
//

import Foundation
import UIKit

public class ImageConstant {
    
    static func getEyeOffImage(size : CGFloat) -> UIImage {
        return UIImage(named: "visibility_on")!
    }
    static let emailIcon = "Message"
    static let passwordIcon = "Lock"
    static let PROFILE_PLACE_HOLDER = "profilePlaceHolder"
    static let noImagePlaceHolder = "noImagePlaceHolder"
    static let followPlusIcon  = "plusFollow"
    static let isFvrt = "heartFill"
    static let isNotFvrt = "heart"
    
}
