//
//  Enums.swift
//  QtheMusic
//
//  Created by Assad Khan on 15/03/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import Foundation

enum LoginType:String{
    case SIMPLE = "SIMPLE"
    case SOCIAL = "SOCIAL"
}

enum socialSiteEnum:String{
    case FACEBOOK = "FACEBOOK"
    case GOOGLE = "GMAIL"
    case APPLE = "APPLE"
}

enum OtpType:String{
    case EMAIL = "EMAIL"
    case PHONE_NUMBER = "PHONE_NUMBER"
    case FORGET_PASSWORD = "FORGET_PASSWORD"
}

enum DateFormatEnum {
    case FORMAT_HOMESCREEN_TOOLBAR
    case CALANDAR_DATE_FORMAT
    case CALANDAR_YEAR_FORMAT
    case CALANDAR_MONTHYEAR_FORMAT
    case CALANDAR_DAY_FORMAT
    case RELEASING_DATE
    
}
enum SelectGenderEnum:String{
    case MALE = "Male"
    case NON_BINARY = "Non-binary"
    case NOT_ANSWERED = "No answer"
    case FEMALE = "Female"
}

enum CategoryType:String{
    case RECOMMENDED = "RECOMMENDED"
    case ALL = "ALL"
    case INTEREST = "INTERESTS"
}

enum SongType :String{
    case TRENDING = "TRENDING"
    case PLAY_LIST = "PLAY_LIST"
    case FAVORITES = "FAVOURITES"
    case PURCHASED = "PURCHASED"
    case ALBUM = "ALBUM"
    case ARTIST = "ARTIST"
    case DOWNLOADED = "DOWNLOADED"
    case PURCHASED_ALBUM = "PURCHASED_ALBUM"
    case ARTIST_ALBUM = "ARTIST_ALBUM"
    case PLAYLIST = "PLAYLIST"
    case FAVOURITE = "FAVOURITE"
    case CATEGORY = "CATEGORY"
    case SEARCHED = "SEARCHED"
    case PURCHASED_SONG = "PURCHASED_SONG"
}

enum SongRecommendType: String {
    case  SONGS =  "SONGS"
    case  ALBUM = "ALBUM"
    case  ARTIST = "ARTIST"
}

enum RequestType: String {
    case RECOMMENDED = "RECOMMENDED"
    case HISTORY = "HISTORY"
    case CATEGORY = "CATEGORY"
    case PURCHASED_ALBUM = "PURCHASED_ALBUM"
    case ARTIST_ALBUM = "ARTIST_ALBUM"
    
}


enum SongStatus :String{
    case FREE = "FREE"
    case PREMIUM  = "PREMIUM"
    case PURCHASED = "PURCHASED"
    case DOWNLOADED = "DOWNLOADED"
    case DELETED = "DELETED"
}

enum SearchSongType:String{
    case  SONG =  "SONG"
    case  ALBUM = "ALBUM"
    case  ARTIST = "ARTIST"
}
enum RecentlyPlayedCatEnum:String{
    case Songs = "Songs"
    case Albums = "Albums"
    case Artists = "Artists"
}

enum PlaylistEnum:String{
    case ADD = "ADD"
    case REMOVE = "REMOVE"
}

enum TermsAndPrivacyEnum{
    case TermsAndCondition
    case PrivacyPolicy
    
    var title : String {
        switch self{
        case .TermsAndCondition:
            return "Terms & Conditions"
        case .PrivacyPolicy :
            return "Privacy Policy"
        }
    }
    
    var description : String{
        switch self{
        case .TermsAndCondition:
            return "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum. passages, and more recently with desktop mobile publishing software like Aldus PageMaker."
        case .PrivacyPolicy:
            return "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum. passages, and more recently with desktop mobile publishing software like Aldus PageMaker."
        }
    }
    
    
}


enum UserProfileUpdateEnum : String{
  case IS_NOTIFICATION_ENABLED = "isNotificationEnabled"
  case IS_ARTIST_UPDATE_ENABLED = "isArtistEnabled"
}



enum IsPlayingFrom : String{
    case ALBUM = "ALBUM"
    case SEARCHED = "SEARCHED"
    case CATEGORY = "CATEGORY"
    case ARTIST = "ARTIST"
    case FAVOURITE = "FAVOURITE"
    case PURCHASED_SONG = "PURCHASED_SONG"
    case PURCHASED_ALBUM = "PURCHASED_ALBUM"
    case DOWNLOADED = "DOWNLOADED"
    case PLAYLIST = "PLAYLIST"
}

enum PlayNextSongFromEnum:String{
    
    case ALBUM = "PLAYING FROM ALBUM"
    case SEARCHED = "PLAYING FROM SEARCHED"
    case TRENDING = "PLAYING FROM TRENDING"
    case CATEGORY = "PLAYING FROM CATEGORY"
    case FAVOURITE = "PLAYING FROM FAVOURITE"
    case PURCHASED_SONG = "PLAYING FROM PURCHASED_SONG"
    case PURCHASED_ALBUM = "PLAYING FROM PURCHASED_ALBUM"
    case DOWNLOADED = "PLAYING FROM DOWNLOADED"
    case PLAYLIST = "PLAYING FROM PLAYLIST"
    
    
}

enum SubscriptionStatus {
    case ACTIVE
    case DE_ACTIVE
    
    var value : String {
        switch self{
        case .ACTIVE:
            return "1"
        case . DE_ACTIVE :
            return "0"
        }
    }
}

enum SubscriptionGracePeriod {
    case ACTIVE
    case DE_ACTIVE
    
    var value : String {
        switch self{
        case .ACTIVE:
            return "1"
        case . DE_ACTIVE :
            return "0"
        }
    }
}

enum SubscribeToPlan:String{
    
    case SONG = "SONG"
    case ALBUM = "ALBUM"
    case PLAN_SUBSCRIPTION = "PLAN_SUBSCRIPTION"
    case PRE_ORDER_SONG = "PRE_ORDER_SONG"
    case PRE_ORDER_ALBUM = "PRE_ORDER_ALBUM"
    
}
