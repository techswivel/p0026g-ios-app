//
//  DownloadedSongViewModel.swift
//  QtheMusic
//
//  Created by Muhammad Tanzeel Ur Rehman on 12/01/2023.
//  Copyright © 2023 TechSwivel. All rights reserved.
//

import Foundation
import UIKit

class DownloadedSongViewModel : BaseViewModel {
    
    var song:Song?
    
    var albumsArray  = [Album]()
    var songsArray = [Song]()
    var songBuilder : SongAndArtistBuilder = SongAndArtistBuilder()
}
