//
//  DownloadedViewController.swift
//  QtheMusic
//
//  Created by Muhammad Tanzeel Ur Rehman on 12/01/2023.
//  Copyright © 2023 TechSwivel. All rights reserved.
//

import Foundation
import UIKit

class DownloadedSongViewController : BaseViewController {
    
    
    @IBOutlet weak var downloadSonglbl : UILabel!
    @IBOutlet weak var listenWithPremiumbtn : UIButton!{
        didSet {
            self.listenWithPremiumbtn.layer.cornerRadius = self.listenWithPremiumbtn.frame.height / 2
        }
    }
    
    private var mViewModel = DownloadedSongViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addGradientBackground()
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.view.addGradientBackground()
        
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        self.popBackNavigationWithTransition()
    }
    
    @IBAction func listenWithPremiumBtnPressed(_ sender: UIButton) {
        
        goToUnderDevelopementScreen()
        
    }
    
    //MARK: - Go TO Downloaded Song VC
    func goToDownloadingSongsScreen() {
        
        let downloadingSongVC = UIStoryboard.init(name: StoryBoardConstant.storyBoardMain, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.downloadingSongsVC) as! DownloadingSongsViewController
        self.navigationController?.pushViewController(downloadingSongVC, animated: true)
    }
}
