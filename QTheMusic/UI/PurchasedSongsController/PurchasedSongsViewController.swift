//
//  PurchasedSongsViewController.swift
//  QtheMusic
//
//  Created by Muhammad Tanzeel Ur Rehman on 08/12/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import Foundation
import UIKit

class PurchasedSongsViewController: BaseViewController {
    
    @IBOutlet weak var emptyResponseLbl: UILabel!
    
    @IBOutlet weak var purchasedSongLbl: UILabel!
    
    @IBOutlet weak var purchasedSongTable: UITableView!{
        didSet{
            purchasedSongTable.delegate = self
            purchasedSongTable.dataSource = self
        }
    }
    
    private var mViewModel = PurchasedSongViewModel()
    private var mProfileViewModel = ProfileViewModel()
    private var mSongAndArtistViewModel = SongAndArtistViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        emptyResponseLbl.isHidden = true
        self.view.addGradientBackground()
        setBindigServerResponse()
        
        purchasedSongTable.register(UINib(nibName: UINibConstant.SongsTableViewCellNib, bundle: nil), forCellReuseIdentifier: UINibConstant.SongsTableViewCellIdentifier)
        
        mViewModel.songBuilder.settype(SongType.PURCHASED.rawValue)
        showActivityIndicator()
        mSongAndArtistViewModel.songs(data: mViewModel.songBuilder)
    }
    
    
    @IBAction func backBtnPressed(_ sender: Any) {
        self.popBackNavigationWithTransition()
    }
    
    private func setBindigServerResponse() {
        
        self.bindingServerResponse()
        self.bindingServerErrorResponse()
        self.bindingServerCompletionResponse()
        
    }
    
}

extension PurchasedSongsViewController : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mViewModel.songsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = purchasedSongTable.dequeueReusableCell(withIdentifier: UINibConstant.SongsTableViewCellIdentifier, for: indexPath) as! SongsTableViewCell
        
        if self.mViewModel.songsArray.count != 0 {
            cell.showAnimation()
            DispatchQueue.main.asyncAfter(deadline: .now() + 4.0) {
                cell.hideAnimation()
                cell.setUpCell(data: self.mViewModel.songsArray[indexPath.row])
             }
        }
        cell.selectionStyle = .none
        return cell
    }
    
}
// MARK: Binding

extension PurchasedSongsViewController{
    
    
    private func bindingServerResponse() {
        self.mSongAndArtistViewModel.serverResponseForSong.bind {
            if ($0 != nil) {
                
                if $0?.response?.status ?? false {
                    self.removeActivityIndicator()
                    
                    self.mViewModel.songsArray = $0?.response?.data?.songs ?? self.mViewModel.songsArray
                    if self.mViewModel.songsArray.count == 0
                    {
                        self.emptyResponseLbl.isHidden = false
                    }
                    else
                    {
                        self.emptyResponseLbl.isHidden = true
                    }
                    self.purchasedSongTable.reloadData()
                }
                else if $0?.response?.status == false {
                    
                    Utils.showErrorAlert(vc: self, message: $0?.response?.message)
                    self.removeActivityIndicator()
                    
                }
            }
        }
    }
    
    //MARK: - Binding Error Response
    private func bindingServerErrorResponse() {
        self.mSongAndArtistViewModel.errorResponseForSong.bind {
            if ($0 != nil) {
                Utils.showErrorAlert(vc: self, message: $0?.response?.message)
            }
            
            self.removeActivityIndicator()
        }
        
        
    }
    
    //MARK: - Binding Complete Response
    private func bindingServerCompletionResponse(){
        self.mSongAndArtistViewModel.completion.bind {
            if ($0 != nil) {
                
                
            }
        }
        
    }
}
