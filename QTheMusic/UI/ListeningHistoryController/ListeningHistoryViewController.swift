//
//  ListeningHistoryViewController.swift
//  QtheMusic
//
//  Created by Muhammad Tanzeel Ur Rehman on 12/01/2023.
//  Copyright © 2023 TechSwivel. All rights reserved.
//

import Foundation
import UIKit
import SkeletonView

class ListeningHistoryViewController : BaseViewController {
    @IBOutlet weak var emptyResponseLbl: UILabel!
    
    
    @IBOutlet weak var listeningHistorylbl : UILabel!
    

    @IBOutlet weak var recentlyPlayedContainerView : UIView!
    
    private var mViewModel = ListeningHistoryViewModel()
    private var mSongAndArtistViewModel = SongAndArtistViewModel()
    private var recentlyPlayedView:ListeningHistoryView!

     var tabbarViewController : CustomTabbarController?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        emptyResponseLbl.isHidden = true
        self.view.addGradientBackground()
        setbindingServerResponse()
        
        loadNib()
        
        mViewModel.songbuilder.setRequestType(RequestType.RECOMMENDED.rawValue)
        mViewModel.songbuilder.settype(SongRecommendType.SONGS.rawValue)
        mSongAndArtistViewModel.getRecommendedSongs(data: mViewModel.songbuilder)
        guard let View = self.recentlyPlayedView else{return}
        View.reloadDataBase()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.view.addGradientBackground()
        view.layoutSkeletonIfNeeded()
        
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        self.popBackNavigationWithTransition()
    }
    
    
    private func setbindingServerResponse(){

        self.bindingServerResponse()
        self.bindingServerErrorResponse()
        self.bindingServerCompletionResponse()

    }
    
    
    private func loadNib(){
        //Listening History View
        self.recentlyPlayedView = UINib(nibName: UINibConstant.ListeningHistoryView, bundle: .main).instantiate(withOwner: nil, options: nil).first as? ListeningHistoryView
        self.recentlyPlayedView.frame = self.recentlyPlayedContainerView.bounds
        
        self.recentlyPlayedView.setDelegate(delegate: self)
        self.recentlyPlayedView.setNavigationController(navCon: self.navigationController)
        
        self.recentlyPlayedView.navigationController = self.navigationController
        self.recentlyPlayedView.tabbarViewController = self.tabbarViewController
        self.recentlyPlayedView.vc = self
        self.recentlyPlayedContainerView.addSubview(recentlyPlayedView)
        
    }
    
}

extension ListeningHistoryViewController {

    //MARK: - Binding Server Response
    private func bindingServerResponse() {
        self.mSongAndArtistViewModel.serverResponseForRecommendedSong.bind {
            if ($0 != nil) {

                if $0?.response?.status ?? false {

                   
                    defer{

                    }


                    if !($0?.response?.data?.Languages?.isEmpty ?? true){

                        self.mViewModel.languagesArray = $0?.response?.data?.Languages ?? [Language]()
                        self.mViewModel.languagesArray? .insert(Language(languageID: nil, languageTitle: StringConstant.all, isSelected: true), at: 0)
                    }

                    self.mViewModel.songArray =  $0?.response?.data?.songs ?? [Song]()
                    if self.mViewModel.songArray?.count == 0
                    {
                        self.emptyResponseLbl.isHidden = false
                    }
                    else
                    {
                        self.emptyResponseLbl.isHidden = true
                    }
                }
                else if $0?.response?.status == false {
                    print("Response for searching song is false")
                    Utils.showErrorAlert(vc: self, message: $0?.response?.message)
                    self.removeActivityIndicator()

                }
            }
        }
    }

    //MARK: - Binding Error Response
    private func bindingServerErrorResponse() {
        self.mSongAndArtistViewModel.errorResponseForRecommendedSong.bind {
            if ($0 != nil) {
                Utils.showErrorAlert(vc: self, message: $0?.response?.message)
            }

            self.removeActivityIndicator()
        }


    }

    //MARK: - Binding Complete Response
    private func bindingServerCompletionResponse(){
        self.mSongAndArtistViewModel.completion.bind {
            if ($0 != nil) {


            }
        }

    }
}

//MARK: - Listening Songs View Delegates
extension ListeningHistoryViewController : ListeningHistorySongsDelegate {
    func listeningSongsErrorResponse(error: String?) {
        Utils.showErrorAlert(vc: self, message: error)
    }
    
    func listeningSongsExpiryReponse() {
        self.performExpirationInCaseOfBoxBinding()
    }
    
    
}
