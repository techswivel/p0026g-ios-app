//
//  ListeningHistoryViewModel.swift
//  QtheMusic
//
//  Created by Muhammad Tanzeel Ur Rehman on 12/01/2023.
//  Copyright © 2023 TechSwivel. All rights reserved.
//

import Foundation

class ListeningHistoryViewModel : BaseViewModel {
    
    var languagesArray : [Language]?
    var songArray : [Song]?
    var languageId :Int? = nil
    var isAnimatable = false
    var isAnimatableLanguages = false
    var songbuilder : SongAndArtistBuilder = SongAndArtistBuilder()
    
    var categoryType :String?
    var album : Album?
    var song:Song?
    var artist: Artist?
    
    func insertDataIntoRecentlyPlayed() {
        if let CategoryType = categoryType {
            if  CategoryType == SongRecommendType.ALBUM.rawValue{
                mDataManager.insertSongIntoDb(song: song)
                mDataManager.insertAlbumIntoDb(album: album)
            }else if CategoryType == SongRecommendType.SONGS.rawValue{
                mDataManager.insertSongIntoDb(song: song)
            } else if CategoryType == RecentlyPlayedCatEnum.Artists.rawValue {
                mDataManager.insertArtistIntoDb(artist: artist)
                mDataManager.insertSongIntoDb(song: song)
                mDataManager.insertAlbumIntoDb(album: album)
            }
        }
    }
    
    
}
