//
//  AlbumDetailViewModel.swift
//  QtheMusic
//
//  Created by Assad Khan on 18/04/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import Foundation


class AlbumDetailViewModel:BaseViewModel{
    
    var songsArray:[Song]?
    var songBuilder :SongAndArtistBuilder?
    var song:Song?
    var album:Album?
    var artist: Artist?
    var category:String?
    var playedFrom:String?
    var purchaseToken = String()
    let builder = SongAndArtistBuilder()
    func setParams() -> Song {
        builder.setpurchaseType("APPLE_PAY")
        builder.setpurchaseToken(purchaseToken)
        builder.setItemType("ALBUM")
        builder.setalbumId(self.album?.albumID)
        builder.setPaidAmount(self.album?.price)
        song = builder.build()
        return song ?? Song()
    }
    
    
    
}
