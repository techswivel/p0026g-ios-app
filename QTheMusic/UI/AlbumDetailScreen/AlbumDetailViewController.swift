//
//  AlbumDetailViewController.swift
//  QtheMusic
//
//  Created by Assad Khan on 18/04/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import UIKit
import SwiftUI
import StoreKit
protocol AlbumDetailViewControllerProtocol{
    func setSongData(data:SongAndArtistBuilder?)
    func setAlbumData(data:Song?)
    func setAlbum(data: Album?)
    func setCategoryType(category: String?)
    func setPlayedFrom(playedFrom:String?)
    func setTabBarVC(tabBarVC:UITabBarController)
}
class AlbumDetailViewController: BaseViewController {
    
    @IBOutlet weak var backgroundBlurImageView: UIImageView!{
        didSet{
            backgroundBlurImageView.blurImageViewWithTransparentBlackColor()
        }
    }
    @IBOutlet weak var coverPhotoActivityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var albumNameLabel: UILabel!
    @IBOutlet weak var backgroundImageActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var albumCoverImageView: UIImageView!
    
    @IBOutlet weak var listenWithPremiumBtn: UIButton!{
        didSet{
            listenWithPremiumBtn.roundCorner(radius: 20)
        }
    }
    
    @IBOutlet weak var premiumView: UIView!{
        didSet{
            premiumView.roundCorner(radius:( Float(premiumView.frame.height) / 2))
        }
    }
    
    
    @IBOutlet weak var numberOfSongsLabel: UILabel!
    
    @IBOutlet weak var numberOfSongsSubHeadingLabel: UILabel!
    @IBOutlet weak var songsTableView: UITableView!{
        didSet{
            songsTableView.delegate = self
            songsTableView.dataSource = self
        }
    }
    
    @IBOutlet weak var playAllBtn: UIButton!{
        didSet{
            playAllBtn.roundCorner(radius: 20)
        }
    }
    @IBOutlet weak var songsLabel: UILabel!
    private var productIdentifiers: Set<String> = []
    private var successView: PaymentSuccessView!
    private var failedView: PaymentFailedView!
    private var mViewModel = AlbumDetailViewModel()
    private var mSongAndArtistViewModel = SongAndArtistViewModel()
    private var tabBarVC:UITabBarController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        productIdentifiers.removeAll()
        self.setUpViewBeforeHittingApi()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        SKPaymentQueue.default().remove(self)
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        self.popBackControllerWithOutAnimation()
    }
    
    @IBAction func playAllBtnPressed(_ sender: Any) {
        self.addTransitionOnNavigationControler()
      let vc = UIStoryboard.init(name: StoryBoardConstant.storyBoardMain, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.UnderDevelopmentVC) as? UnderDevelopmentViewController
        
      self.navigationController?.pushViewController(vc!, animated: false)
    }
    
    @IBAction func listenPremiumBtnPressed(_ sender: Any) {
        fetchProducts()
        self.showActivityIndicator()
    }
    private func setUpViewBeforeHittingApi(){
        self.registerNibs()
        self.bindingServerResponse()
        self.bindingServerResponseForSubscribeToPlan()
        self.bindingServerErrorResponse()
        self.bindingServerErrorResponseForSubscribeToPlan()
        self.bindingServerCompletionResponse()
        
        self.startShimmer()
        
        self.mViewModel.songBuilder?.settype(SongType.ALBUM.rawValue)
        self.mSongAndArtistViewModel.songs(data: self.mViewModel.songBuilder)
    }
    
    private func fetchProducts()
    {
        let productIdentifier: String = self.mViewModel.album?.sku ?? ""
        print(productIdentifier)
        productIdentifiers.insert(productIdentifier)
            let productRequest = SKProductsRequest(productIdentifiers: productIdentifiers)
            productRequest.delegate = self
            productRequest.start()
    }
    
    private func registerNibs(){
        
        
        songsTableView.register(UINib(nibName: UINibConstant.SongsTableViewCellNib, bundle: nil),forCellReuseIdentifier: UINibConstant.SongsTableViewCellIdentifier)
        
        self.failedView = UINib(nibName:    UINibConstant.paymentFailedNib, bundle: .main).instantiate(withOwner: nil, options: nil).first as? PaymentFailedView
        self.failedView.frame = self.view.bounds
        self.failedView.delegate = self
        // Success View Nib
        self.successView = UINib(nibName:    UINibConstant.paymentSuccessNib, bundle: .main).instantiate(withOwner: nil, options: nil).first as? PaymentSuccessView
        self.successView.frame = self.view.bounds
        self.successView.delegate = self
        
    }
    private func startShimmer(){
        backgroundBlurImageView.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        albumCoverImageView.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        numberOfSongsLabel.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        songsLabel.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        numberOfSongsSubHeadingLabel.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        coverPhotoActivityIndicator.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        backgroundImageActivityIndicator.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        albumNameLabel.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        
    }
    private func stopAnimation(){
        backgroundBlurImageView.hideSkeleton()
        albumCoverImageView.hideSkeleton()
        numberOfSongsLabel.hideSkeleton()
        songsLabel.hideSkeleton()
        numberOfSongsSubHeadingLabel.hideSkeleton()
        coverPhotoActivityIndicator.hideSkeleton()
        backgroundImageActivityIndicator.hideSkeleton()
        albumNameLabel.hideSkeleton()
    }
    
    private func setUpViewAfterHittingApi(song:[Song]?){
        guard let Song = song else {
            return
        }
        
        self.stopAnimation()
        
        
    }
    
}
extension AlbumDetailViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.mViewModel.songsArray?.count ?? 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: UINibConstant.SongsTableViewCellIdentifier,for: indexPath) as! SongsTableViewCell
        cell.selectionStyle = .none
        
        if !(self.mViewModel.songsArray?.isEmpty ?? true){
            cell.hideAnimation()
            cell.setUpCell(data: self.mViewModel.songsArray?[indexPath.row])
            
        }else{
            cell.showAnimation()
        }
        
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
        let tabBarVC = self.tabBarVC as? CustomTabbarController
        
        if self.mViewModel.songsArray?[indexPath.row].songStatus == SongStatus.PREMIUM.rawValue{
            let vc = UIStoryboard.init(name: StoryBoardConstant.storyBoardMain, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.SongsDetailViewController) as? SongsDetailViewController
            self.mViewModel.category == nil ? vc?.setPlayingCategoryType(categoryType: RecentlyPlayedCatEnum.Albums.rawValue) : vc?.setPlayingCategoryType(categoryType: RecentlyPlayedCatEnum.Artists.rawValue)
            vc?.setAlbumData(album: self.mViewModel.album)
            vc?.setSongData(song: self.mViewModel.songsArray?[indexPath.row])
            vc?.setIndexPath(row: indexPath.row)
            vc?.setCompleteSongsArray(songsArray: self.mViewModel.songsArray)
            vc?.labelArtist(lblArtist: self.mViewModel.artist)
            vc?.setIsPlayingFromString(isplayingFromString: "\(StringConstant.playingFrom)ALBUM")
            UserDefaultUtils.setBool(value : false, key: CommonKeys.IsPlayingFromNotification)
            self.navigationController?.pushViewController(vc!, animated: false)
            tabBarVC?.removeMiniPlayer()
        }
        else {
            let vc = UIStoryboard.init(name: StoryBoardConstant.storyBoardMain, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.SongsDetailViewController) as? SongsDetailViewController
            self.mViewModel.category == nil ? vc?.setPlayingCategoryType(categoryType: RecentlyPlayedCatEnum.Albums.rawValue) : vc?.setPlayingCategoryType(categoryType: RecentlyPlayedCatEnum.Artists.rawValue)
            vc?.setAlbumData(album: self.mViewModel.album)
            vc?.setSongData(song: self.mViewModel.songsArray?[indexPath.row])
            vc?.setIndexPath(row: indexPath.row)
            vc?.setCompleteSongsArray(songsArray: self.mViewModel.songsArray)
            vc?.setIsFromMiniPlayer(isFromMiniPlayer: false)
            vc?.labelArtist(lblArtist: self.mViewModel.artist)
            ///Handling for nextPlaySong
            if mViewModel.playedFrom != nil {
                ///For nextPlaySong
                vc?.setIsPlayingFromString(isplayingFromString: "\(StringConstant.playingFrom)CATEGORY")
            } else {
                ///For Normal Case
                vc?.setIsPlayingFromString(isplayingFromString: "\(StringConstant.playingFrom)ALBUM")
            }
            UserDefaultUtils.setBool(value : false, key: CommonKeys.IsPlayingFromNotification)
            self.navigationController?.pushViewController(vc!, animated: false)
            print("Tabbarcontroller here \(tabBarVC)")
            tabBarVC?.setPlayingCategoryType(categoryType: RecentlyPlayedCatEnum.Albums.rawValue)
            tabBarVC?.setSongData(song: self.mViewModel.songsArray?[indexPath.row])
            tabBarVC?.setIndexPath(row: indexPath.row)
            tabBarVC?.setCompleteSongsArray(songsArray: self.mViewModel.songsArray)
            tabBarVC?.setIsPlayingFromString(isplayingFromString: "\(StringConstant.playingFrom)ALBUM")
            tabBarVC?.addMiniPlayer()
            
        }
    }
    
}

extension AlbumDetailViewController{
    
    //MARK: - Binding Server Response
    private func bindingServerResponse() {
        self.mSongAndArtistViewModel.serverResponseForSong.bind {
            if ($0 != nil) {
                
                if $0?.response?.status ?? false {
                    
                    
                    defer{
                        
                        self.songsTableView.reloadData()
                        
                    }
                    self.mViewModel.songsArray = $0?.response?.data?.songs
                    self.setUpViewAfterHittingApi(song: $0?.response?.data?.songs )
                    
                    
                    
                    self.numberOfSongsLabel.text = "\(self.mViewModel.songsArray?.count ?? 0) Songs"
                    self.numberOfSongsSubHeadingLabel.text = "\(self.mViewModel.songsArray?.count ?? 0) Songs"
                    
                    
                    self.albumNameLabel.text = "\(self.mViewModel.album?.albumTitle ?? "")"
                    self.albumCoverImageView.loadImage(imageUrl: self.mViewModel.album?.albumCoverImageURL ?? "", placeHolderName: ImageConstant.noImagePlaceHolder , activiyIndicator: self.coverPhotoActivityIndicator)
                    self.backgroundBlurImageView.loadImage(imageUrl: self.mViewModel.album?.albumCoverImageURL ?? "", placeHolderName: ImageConstant.noImagePlaceHolder , activiyIndicator: self.backgroundImageActivityIndicator)
                    print(self.mViewModel.album?.albumCoverImageURL,"URL")
                    print("Album status \(self.mViewModel.album?.albumStatus)")
                    
                    
                    
                    
                    if $0?.response?.data?.albumStatus == SongStatus.PREMIUM.rawValue{
                        self.premiumView.isHidden = false
                        self.listenWithPremiumBtn.isHidden = false
                        self.playAllBtn.isHidden = true
                    }else{
                        self.premiumView.isHidden = true
                        self.listenWithPremiumBtn.isHidden = true
                        self.playAllBtn.isHidden = false
                    }
                     
                   
                }
                else if $0?.response?.status == false {
                    print("Response for searching song is false")
                    Utils.showErrorAlert(vc: self, message: $0?.response?.message)
                    self.removeActivityIndicator()
                    
                }
            }
        }
    }
    
    //MARK: - Binding Error Response
    private func bindingServerErrorResponse() {
        self.mSongAndArtistViewModel.errorResponseForSong.bind {
            if ($0 != nil) {
                Utils.showErrorAlert(vc: self, message: $0?.response?.message)
            }
            
            self.removeActivityIndicator()
        }
        
        
    }
    
    //MARK: - Binding Complete Response
    private func bindingServerCompletionResponse(){
        self.mSongAndArtistViewModel.completion.bind {
            if ($0 != nil) {
                
                
            }
        }
        
    }
    private func bindingServerResponseForSubscribeToPlan() {
        self.mSongAndArtistViewModel.serverResponseForSubscribeToPlan.bind {
            if ($0 != nil) {
                
                if $0?.response?.status ?? false {
                    self.removeActivityIndicator()
                    self.successView
                        .alpha = 0.0
                    self.view.addSubview(self.successView)
                    UIView.animate(withDuration: 0.25) { () -> Void in
                        self.successView.alpha = 1.0
                        self.mViewModel.songBuilder?.settype(SongType.ALBUM.rawValue)
                        self.mSongAndArtistViewModel.songs(data: self.mViewModel.songBuilder)
                    }
                    
                }
                else if $0?.response?.status == false {
                   
                    self.removeActivityIndicator()
                    
                }
            }
        }
    }
    private func bindingServerErrorResponseForSubscribeToPlan() {
        self.mSongAndArtistViewModel.errorResponseForForSubscribeToPlan.bind {
            if ($0 != nil) {
                self.failedView.setFailedMsg(errorLbl: StringConstant.Payment_Failed)
                Utils.showErrorAlert(vc: self, message: $0?.response?.message)
                self.failedView
                    .alpha = 0.0
                self.view.addSubview(self.failedView)
                UIView.animate(withDuration: 0.25) { () -> Void in
                    self.failedView.alpha = 1.0
                }
            }
            
            self.removeActivityIndicator()
        }
    }
}
extension AlbumDetailViewController:AlbumDetailViewControllerProtocol{
    func setPlayedFrom(playedFrom: String?) {
        if let type = playedFrom {
            self.mViewModel.playedFrom = type
        }
    }
    
    func setAlbum(data: Album?) {
        if let album = data {
            self.mViewModel.album = album
        }
    }
    
    func setTabBarVC(tabBarVC: UITabBarController) {
        self.tabBarVC = tabBarVC
    }
    
    
    
    func setAlbumData(data: Song?) {
        if let Data = data {
            
        }
    }
    
    func setCategoryType(category: String?) {
        if let Category = category {
            self.mViewModel.category = Category
        }
    }
    
    func setSongData(data: SongAndArtistBuilder?) {
        self.mViewModel.songBuilder = data
        
    }
    
    
}

extension AlbumDetailViewController: ArtistLabelDetailViewControllerProtocol {
    func labelArtist(lblArtist: Artist?) {
        if let Data = lblArtist {
            self.mViewModel.artist = Data
        }
    }
    
     
}
extension AlbumDetailViewController: SKProductsRequestDelegate, SKPaymentTransactionObserver {
    func productsRequest (_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        print(response.products.count)
        
        if let oProducts = response.products.first
        {
            print("product is available")
            print(productIdentifiers)
            self.purchase(aproduct: oProducts)
        }
        else
        {
            Utils.showErrorAlert(vc: self, message: StringConstant.productisNotAvailabe)
    
        }
        
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions {
            switch transaction.transactionState
            {
            case .purchasing:
                print("customer is in process of purchase")
            case .purchased:
                print("Purchased")
                SKPaymentQueue.default().finishTransaction(transaction)
                productIdentifiers.removeAll()
                getPurchaseToken()
                self.mSongAndArtistViewModel.subscribeToPlan(data: self.mViewModel.setParams())
            case .failed:
                     if let error = transaction.error as? SKError {
                         switch error.code {
                         case .unknown:
                             self.failedView.setFailedMsg(errorLbl: StringConstant.Unknown_Case)
                         case .clientInvalid:
                             self.failedView.setFailedMsg(errorLbl: StringConstant.Invalid_Client)
                         case .paymentCancelled:
                             self.failedView.setFailedMsg(errorLbl: StringConstant.Payment_Cancelled)
                         case .paymentInvalid:
                             self.failedView.setFailedMsg(errorLbl: StringConstant.Invalid_Payment)
                         case .paymentNotAllowed:
                             self.failedView.setFailedMsg(errorLbl: StringConstant.Payment_Not_Allowed)
                         case .storeProductNotAvailable:
                             self.failedView.setFailedMsg(errorLbl: StringConstant.Product_Not_Available)
                         case .cloudServicePermissionDenied:
                             self.failedView.setFailedMsg(errorLbl: StringConstant.Cloud_Service_Permission_Denied)
                         case .cloudServiceNetworkConnectionFailed:
                             self.failedView.setFailedMsg(errorLbl: StringConstant.Cloud_Service_Network_Connection_Failed)
                         default:
                             self.failedView.setFailedMsg(errorLbl: StringConstant.Something_Went_Wrong)
                         }
                         SKPaymentQueue.default().finishTransaction(transaction)
                         productIdentifiers.removeAll()
                         self.failedView.alpha = 0.0
                         self.view.addSubview(failedView)
                         UIView.animate(withDuration: 0.25) { () -> Void in
                             self.failedView.alpha = 1.0
                         }
                         self.removeActivityIndicator()
                     }
                
            case .restored:
                self.failedView.setFailedMsg(errorLbl: StringConstant.Restored)
            case .deferred:
                self.failedView.setFailedMsg(errorLbl: StringConstant.Suspended)
            default : break
            }
        }
    }
    
    
   private func purchase(aproduct: SKProduct)
    {
        if SKPaymentQueue.canMakePayments()
        {
        let payment = SKPayment(product: aproduct)
        SKPaymentQueue.default().add(self)
        SKPaymentQueue.default().add(payment)
        }
    }
   private func getPurchaseToken()
    {
    if let url = Bundle.main.appStoreReceiptURL,
       let data = try? Data(contentsOf: url) {
          var receiptBase64 = data.base64EncodedString()
        self.mViewModel.purchaseToken = receiptBase64
        
    }
    }
}

extension AlbumDetailViewController: SuccessViewDelegate
{
    func cancelView() {
        self.successView.removeFromSuperview()
    }
    
    
}
extension AlbumDetailViewController: FailedViewDelegate
{
    func cancel() {
        self.failedView.removeFromSuperview()
    }
    
    
}
