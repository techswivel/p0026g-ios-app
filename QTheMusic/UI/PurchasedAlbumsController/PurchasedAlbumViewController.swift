//
//  PurchasedAlbumViewController.swift
//  QtheMusic
//
//  Created by Muhammad Tanzeel Ur Rehman on 08/12/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import Foundation
import UIKit

class PurchasedAlbumViewController: BaseViewController{
    
    @IBOutlet weak var emptyResponseLbl: UILabel!
    
    @IBOutlet weak var purchasedAlbumlbl: UILabel!
    
    @IBOutlet weak var purchasedAlbumCollection: UICollectionView!{
        didSet{
            purchasedAlbumCollection.delegate = self
            purchasedAlbumCollection.dataSource = self
        }
    }
    
    private var mViewModel = PurchasedAlbumViewModel()
    private var collectionFlowLayoutForAlbum : UICollectionViewFlowLayout!
    private var mSongAndArtistViewModel = SongAndArtistViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addGradientBackground()
        setBindigServerResponse()
        setEmptyResponse()
        purchasedAlbumCollection.register(UINib(nibName: UINibConstant.PurchasedAlbumcellnib, bundle: nil), forCellWithReuseIdentifier: UINibConstant.PurchasedAlbumcellIdentifier)
        mViewModel.songBuilder.setRequestType(RequestType.PURCHASED_ALBUM.rawValue)
        mSongAndArtistViewModel.getRecommendedSongs(data: mViewModel.songBuilder)
        setupFlowLayout()
        
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        self.popBackNavigationWithTransition()
    }
    private func setEmptyResponse(){
    if mViewModel.albumsArray.count == 0
    {
        emptyResponseLbl.isHidden = false
    }
    else
    {
        emptyResponseLbl.isHidden = true
    }
    }
    // 2 columns collecction layout
    private func setupFlowLayout(){
        
        if collectionFlowLayoutForAlbum == nil {
            let width = (self.view.frame.width - (48) ) / 2
            let height = purchasedAlbumCollection.frame.width * 0.6
            //using two as two column are to be presented
            collectionFlowLayoutForAlbum = UICollectionViewFlowLayout()
            
            collectionFlowLayoutForAlbum.itemSize = CGSize(width: width, height: height)
            
            collectionFlowLayoutForAlbum.scrollDirection = .vertical
            collectionFlowLayoutForAlbum.minimumLineSpacing = 0
            collectionFlowLayoutForAlbum.minimumInteritemSpacing = 0
            purchasedAlbumCollection.setCollectionViewLayout(collectionFlowLayoutForAlbum, animated: true)
        }
    }
    
    private func setBindigServerResponse() {
        
        self.bindingServerResponse()
        self.bindingServerErrorResponse()
        self.bindingServerCompletionResponse()
        
        
    }
    
    private func setUpViewAfterHittingApi(album: [Album]?){
        guard let Album = album else {
            return
        }
    }

    
    //MARK: - Go TO Album Detail VC
   private func goToAlbumDetailScreen() {
        
        let AlbumDetailVC = UIStoryboard.init(name: StoryBoardConstant.storyBoardMain, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.AlbumDetailVc) as! AlbumDetailViewController
        self.navigationController?.pushViewController(AlbumDetailVC, animated: true)
    }
    
}

extension PurchasedAlbumViewController : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 4
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        goToAlbumDetailScreen()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mViewModel.albumsArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = purchasedAlbumCollection.dequeueReusableCell(withReuseIdentifier: UINibConstant.PurchasedAlbumcellIdentifier, for: indexPath) as! PurchasedAlbumcell
        if mViewModel.albumsArray.count != 0
        {
            cell.showAnimation()
            DispatchQueue.main.asyncAfter(deadline: .now() + 4.0) {
                cell.hideAnimation()
                cell.setUpCell(data: self.mViewModel.albumsArray[indexPath.row])
             }
        }
        
        return cell
    }
    
}
extension PurchasedAlbumViewController{
    
    //MARK: - Binding Server Response
    private func bindingServerResponse() {
        self.mSongAndArtistViewModel.serverResponseForGetAlbums.bind {
            if ($0 != nil) {
               
                
                if $0?.response?.status ?? false {
                    
                    
                    defer{
                        
                        self.purchasedAlbumCollection.reloadData()

                    }
                    self.mViewModel.albumsArray = $0?.response?.data?.albums ?? self.mViewModel.albumsArray
                    self.setUpViewAfterHittingApi(album: $0?.response?.data?.albums)
                    self.purchasedAlbumCollection.reloadData()
                    
                    if $0?.response?.data?.albumStatus == SongStatus.PREMIUM.rawValue{
                       
                        self.mViewModel.albumsArray = $0?.response?.data?.albums ?? self.mViewModel.albumsArray
                        self.setUpViewAfterHittingApi(album: $0?.response?.data?.albums)
                        self.purchasedAlbumCollection.reloadData()
                        print("Albums Data")
                    }else{
                        self.mViewModel.albumsArray = $0?.response?.data?.albums ?? self.mViewModel.albumsArray
                        self.setUpViewAfterHittingApi(album: $0?.response?.data?.albums)
                        self.purchasedAlbumCollection.reloadData()
                       
                        print("Error in getting Albums")
                    }
                    
                    
                }
                else if $0?.response?.status == false {
                    print("Response for getting Albums is false")
                    Utils.showErrorAlert(vc: self, message: $0?.response?.message)
                    self.removeActivityIndicator()
                    
                }
            }
        }
    }
   
    //MARK: - Binding Error Response
    private func bindingServerErrorResponse() {
        self.mSongAndArtistViewModel.errorResponseForGetAlbums.bind {
            if ($0 != nil) {
                Utils.showErrorAlert(vc: self, message: $0?.response?.message)
            }
            
            self.removeActivityIndicator()
        }
        
        
    }
    
    //MARK: - Binding Complete Response
    private func bindingServerCompletionResponse(){
        self.mSongAndArtistViewModel.completion.bind {
            if ($0 != nil) {
                
                
            }
        }
        
    }
}
