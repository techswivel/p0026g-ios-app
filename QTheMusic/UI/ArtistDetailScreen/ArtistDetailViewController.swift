//
//  ArtistDetailViewController.swift
//  QtheMusic
//
//  Created by Assad Khan on 28/04/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import UIKit
import SDWebImage

protocol ArtistDetailViewControllerProtocol{
    func setArtistId(artistId:Int?)
    func setTabBarVC(tabBarVC : UITabBarController)
}

protocol ArtistLabelDetailViewControllerProtocol{
    func labelArtist(lblArtist: Artist?)
}

protocol AlbumLabelDetaillViewControllerProtocol {
    func lebelAlbum(lblAlbum: Album)
}

class ArtistDetailViewController: BaseViewController {
    
    @IBOutlet weak var followBtnActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var listenWithPremiumBtn: UIButton!{
        didSet{
            listenWithPremiumBtn.roundCorner(radius: 17)
        }
    }
    @IBOutlet weak var songsTableView: UITableView!{
        didSet{
            songsTableView.delegate = self
            songsTableView.dataSource = self
        }
    }
    @IBOutlet weak var dotLAbel: UILabel!
    @IBOutlet weak var playAllBtn: UIButton!{
        didSet{
            playAllBtn.roundCorner(radius: 17)
        }
    }
    @IBOutlet weak var viewAllBtn: UIButton!
    @IBOutlet weak var followingBtn: UIButton!{
        didSet{
            followingBtn.roundCorner(radius: 17)
            followingBtn.layer.borderColor = ColorConstrants.FOLLOWINGBTN_COLOR.cgColor
            followingBtn.layer.borderWidth = 1
        }
    }
    @IBOutlet weak var preOrderView: UIView!{
        didSet{
            preOrderView.roundCorner(radius: 18)
        }
    }
    @IBOutlet weak var preOrderViewHeightContraint: NSLayoutConstraint!//static height is 130
    @IBOutlet weak var preOrderArtistDetailLabel: UILabel!
    @IBOutlet weak var allMusicBtmClrView: UIView!
    @IBOutlet weak var artistNameLabel: UILabel!
    @IBOutlet weak var numberOfSongsArtistLabel: UILabel!
    @IBOutlet weak var aboutArtistBtmClrView: UIView!
    @IBOutlet weak var numberOfAlbumLabel: UILabel!
    @IBOutlet weak var backgroundImageActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var artistActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var artistBackgroundImageView: UIImageView!
    @IBOutlet weak var artistImageView: UIImageView!{
        didSet{
            artistImageView.roundCorner(radius: Float(artistImageView.frame.height) / 2)
        }
    }
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var allMusicContainerView: UIView!
    @IBOutlet weak var allMusicBtn: UIButton!
    @IBOutlet weak var aboutArtistBtn: UIButton!
    @IBOutlet weak var textContainerView: UIView!
    @IBOutlet weak var artistDetailTextView: UITextView!
    @IBOutlet weak var numberOfSongsLabel: UILabel!
    @IBOutlet weak var albumCollectionView: UICollectionView!{
        didSet{
            albumCollectionView.delegate = self
            albumCollectionView.dataSource  = self
        }
    }
    
    var artistName = ""
    private var dynamicTableViewHeight:CGFloat?
    private var collectionViewFlowLayout : UICollectionViewFlowLayout!
    private var mViewModel = ArtistDetailViewModel()
    private var mSongAndArtistViewModel = SongAndArtistViewModel()
    private var tabBarVC : UITabBarController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        artistNameLabel.text = artistName
        self.setUpView()
        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        setupFlowLayout()
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        self.popBackNavigationWithTransition()
    }
    
    // MARK: - AllMusicBtnAction
    @IBAction func allMusicBtnAction(_ sender: Any) {
        
        aboutArtistBtmClrView.backgroundColor = UIColor.black
        aboutArtistBtn.setTitleColor(UIColor.lightGray, for: .normal)
        allMusicBtn.setTitleColor(ColorConstrants.RED_THEME, for: .normal)
        allMusicBtmClrView.backgroundColor = ColorConstrants.RED_THEME
        
        allMusicContainerView.isHidden = false
        textContainerView.isHidden = true
        self.artistDetailTextView.isScrollEnabled =  true
        if let Dheight = self.dynamicTableViewHeight{
            tableViewHeightConstraint.constant = 12000000000
            tableViewHeightConstraint.constant = Dheight + 100
            self.containerView.layoutSubviews()
            self.containerView.layoutIfNeeded()
            self.view.layoutSubviews()
            self.view.layoutIfNeeded()
        }
    }
    
    // MARK: - ArtistDetailBtnAction
    @IBAction func artistDetailBtnAction(_ sender: Any) {
        aboutArtistBtmClrView.backgroundColor = ColorConstrants.RED_THEME
        aboutArtistBtn.setTitleColor(ColorConstrants.RED_THEME, for: .normal)
        allMusicBtn.setTitleColor(UIColor.lightGray, for: .normal)
        allMusicBtmClrView.backgroundColor = UIColor.black
        
        
        tableViewHeightConstraint.constant = 12000000000
        self.tableViewHeightConstraint.constant  = self.artistDetailTextView.contentSize.height + 100
        self.allMusicContainerView.isHidden = true
        self.textContainerView.isHidden = false
        self.containerView.layoutSubviews()
        self.containerView.layoutIfNeeded()
        self.view.layoutSubviews()
        self.view.layoutIfNeeded()
        
        self.artistDetailTextView.isScrollEnabled =  false
        
        
    }
    
    
    // MARK: - FollowingBtnPressed
    @IBAction func followingBtnPressed(_ sender: Any) {
        
        if self.mViewModel.isFollowing ?? false{
            self.followBtnActivityIndicator.startAnimating()
            self.mSongAndArtistViewModel.followArtist(artistId: self.mViewModel.artistId, isFollowed: false)
            
        }else
        {
            self.followBtnActivityIndicator.startAnimating()
            self.mSongAndArtistViewModel.followArtist(artistId: self.mViewModel.artistId, isFollowed: true)
        }
        
    }
    
    
    // MARK: - PlayAllBtnPressed
    @IBAction func playAllBtnPressed(_ sender: Any) {
        self.addTransitionOnNavigationControler()
        let vc = UIStoryboard.init(name: StoryBoardConstant.storyBoardMain, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.UnderDevelopmentVC) as? UnderDevelopmentViewController
        self.navigationController?.pushViewController(vc!, animated: false)
    }
    
    
    // MARK: - PreOrderNowBtnPressed
    @IBAction func preOrderNowBtnPressed(_ sender: Any) {
        
        
        self.addTransitionOnNavigationControler()
        let vc = UIStoryboard.init(name: StoryBoardConstant.storyBoardMain, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.PreOrderVC) as? PreOrderSongViewController
        vc?.setArtistId(artistId: self.mViewModel.artistId)
        self.navigationController?.pushViewController(vc!, animated: false)
    }
    
    // MARK: - ViewAllBtnPressed
    @IBAction func viewAllBtnPressed(_ sender: Any) {
        
        
        self.addTransitionOnNavigationControler()
        let vc = UIStoryboard.init(name: StoryBoardConstant.storyBoardMain, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.AllAlbumsVC) as? AllAlbumsViewController
        vc?.albumsArray = self.mViewModel.albumsArray
        vc?.artistName = self.artistNameLabel.text
        self.navigationController?.pushViewController(vc!, animated: false)
        
    }
    
    
    // MARK: - SetUpView
    private func setUpView(){
        self.registerNibs()
        
        
        tableViewHeightConstraint.constant = 12000000000
        textContainerView.translatesAutoresizingMaskIntoConstraints = false
        textContainerView.bottomAnchor.constraint(equalTo: self.containerView.bottomAnchor,constant: 25).isActive = true
        
        allMusicContainerView.isHidden = false
        textContainerView.isHidden = true
        viewAllBtn.isEnabled = false
        
        followBtnActivityIndicator.hidesWhenStopped = true
        followBtnActivityIndicator.stopAnimating()
        self.preOrderView.isHidden = true
        self.preOrderViewHeightContraint.constant = 0
        
        self.aboutArtistBtn.isEnabled = false
        
        self.startShimmerEffect()
        
        
        self.bindingServerResponse()
        self.bindingServerErrorResponse()
        self.bindingServerCompletionResponse()
        
        self.bindingServerResponseForFollowArtist()
        self.bindingServerErrorResponseForFollowArtist()
        
        
        self.mSongAndArtistViewModel.getArtistDetatil(artistId: self.mViewModel.artistId)
        
        
        artistBackgroundImageView.blurImageViewWithTransparentBlackColor()
        artistNameLabel.isHidden = true
        numberOfSongsArtistLabel.isHidden = true
        dotLAbel.isHidden = true
        numberOfAlbumLabel.isHidden = true
    }
    
    // MARK: - RegisterNibs
    private func registerNibs(){
        songsTableView.register(UINib(nibName: UINibConstant.SongsTableViewCellNib, bundle: nil),forCellReuseIdentifier: UINibConstant.SongsTableViewCellIdentifier)
        
        albumCollectionView.register(UINib(nibName: UINibConstant.AlbumCollectionViewCellNib, bundle: nil), forCellWithReuseIdentifier: UINibConstant.AlbumCollectionViewCellIdentifier)
        
        
    }
    private func startShimmerEffect(){
        
        artistActivityIndicator.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        artistImageView.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        backgroundImageActivityIndicator.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        artistBackgroundImageView.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        artistNameLabel.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        numberOfAlbumLabel.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        numberOfSongsLabel.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        numberOfSongsArtistLabel.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        dotLAbel.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        
        
        
    }
    private func endShimmerEffect(){
        artistActivityIndicator.hideSkeleton()
        artistImageView.hideSkeleton()
        backgroundImageActivityIndicator.hideSkeleton()
        artistBackgroundImageView.hideSkeleton()
        artistNameLabel.hideSkeleton()
        numberOfAlbumLabel.hideSkeleton()
        numberOfSongsLabel.hideSkeleton()
        numberOfSongsArtistLabel.hideSkeleton()
        dotLAbel.hideSkeleton()
    }
    
    private func setUpTableViewHeight(){
        
        tableViewHeightConstraint.constant = self.songsTableView.contentSize.height + 100
        
        dynamicTableViewHeight = self.songsTableView.contentSize.height + 100
    }
    
    // MARK: - setupFlowLayout
    private func setupFlowLayout(){
        
        if collectionViewFlowLayout == nil{
            
            let width = (albumCollectionView.frame.width - 60) / 2
            let height = albumCollectionView.frame.width * 0.62
            //using two as two column are to be presented
            collectionViewFlowLayout = UICollectionViewFlowLayout()
            
            collectionViewFlowLayout.itemSize = CGSize(width: width, height: height)
            
            collectionViewFlowLayout.scrollDirection = .horizontal
            collectionViewFlowLayout.minimumLineSpacing = 20
            collectionViewFlowLayout.minimumInteritemSpacing = 20
            albumCollectionView.setCollectionViewLayout(collectionViewFlowLayout, animated: true)
        }
        
    }
    // MARK: -isFollowingBtnAnimation
    private func isFollowingBtnAnimation() {
        if self.mViewModel.isFollowing == true{
            self.mViewModel.isFollowing = false
            UIView.transition(with: followingBtn, duration: 0.5, options: .transitionFlipFromLeft, animations: {
                let myNormalAttributedTitle = NSAttributedString(string: StringConstant.follow,
                                                                 attributes: [NSAttributedString.Key.foregroundColor : ColorConstrants.FOLLOWINGBTN_COLOR])
                self.followingBtn.setAttributedTitle(myNormalAttributedTitle, for: .normal)
                self.followingBtn.setImage(UIImage(named:ImageConstant.followPlusIcon), for: .normal)
                
            }, completion: nil)
        }else{
            UIView.transition(with: followingBtn, duration: 0.5, options: .transitionFlipFromRight, animations: {
                self.mViewModel.isFollowing = true
                let myNormalAttributedTitle = NSAttributedString(string: StringConstant.following,
                                                                 attributes: [NSAttributedString.Key.foregroundColor : ColorConstrants.FOLLOWINGBTN_COLOR])
                self.followingBtn.setAttributedTitle(myNormalAttributedTitle, for: .normal)
                self.followingBtn.setImage(UIImage(), for: .normal)
            }, completion: nil)
        }
    }
    // MARK: - setupFollowingBtnView
    private func setupFollowingBtnView(){
        if self.mViewModel.isFollowing ?? false{
            let myNormalAttributedTitle = NSAttributedString(string: StringConstant.following,
                                                             attributes: [NSAttributedString.Key.foregroundColor : ColorConstrants.FOLLOWINGBTN_COLOR])
            self.followingBtn.setAttributedTitle(myNormalAttributedTitle, for: .normal)
            self.followingBtn.setImage(UIImage(), for: .normal)
        }else{
            let myNormalAttributedTitle = NSAttributedString(string: StringConstant.follow,
                                                             attributes: [NSAttributedString.Key.foregroundColor : ColorConstrants.FOLLOWINGBTN_COLOR])
            self.followingBtn.setAttributedTitle(myNormalAttributedTitle, for: .normal)
            self.followingBtn.setImage(UIImage(named:ImageConstant.followPlusIcon), for: .normal)
        }
    }
}

extension ArtistDetailViewController{
    
    //MARK: - Binding Server Response
    private func bindingServerResponse() {
        self.mSongAndArtistViewModel.serverResponseForArtistDetail.bind {
            if ($0 != nil) {
                
                if $0?.response?.status ?? false {
                
                    defer{
                        self.endShimmerEffect()
                        self.viewAllBtn.isEnabled = true
                        self.aboutArtistBtn.isEnabled = true
                        self.albumCollectionView.reloadData()
                        self.songsTableView.reloadData()
                        self.setUpTableViewHeight()
                        self.artistNameLabel.isHidden = false
                        self.numberOfSongsArtistLabel.isHidden = false
                        self.dotLAbel.isHidden = false
                        self.numberOfAlbumLabel.isHidden = false
                    }
                    
                    let artistDetail = $0?.response?.data?.artistDetails
                    
                    self.mViewModel.isFollowing = artistDetail?.isUserFollowed
                    self.mViewModel.lablesArtist?.artistName = artistDetail?.artistName
                    self.mViewModel.lablesArtist?.artistID = self.mViewModel.artistId ?? 0
                    self.mViewModel.lablesArtist?.artistCoverImageURL = artistDetail?.artistCoverImageURL
                    
                    
                    self.artistDetailTextView.text = artistDetail?.aboutArtist
                    self.mViewModel.songsArray = artistDetail?.songs
                    self.mViewModel.albumsArray = artistDetail?.albums
                    self.numberOfSongsLabel.text = "\(artistDetail?.totalSongs ?? 0) Songs"
                    self.numberOfSongsArtistLabel.text = "\(artistDetail?.songs?.count ?? 0) Songs"
                    self.numberOfAlbumLabel.text = "\(artistDetail?.albums?.count ?? 0) Albums"
                    self.artistNameLabel.text = artistDetail?.artistName
                    self.artistImageView.loadImage(imageUrl: artistDetail?.artistCoverImageURL ?? "", placeHolderName: ImageConstant.noImagePlaceHolder, activiyIndicator:  self.artistActivityIndicator)
                    self.artistBackgroundImageView.loadImage(imageUrl: artistDetail?.artistCoverImageURL ?? "", placeHolderName: ImageConstant.noImagePlaceHolder, activiyIndicator:  self.backgroundImageActivityIndicator)
                    
                    if artistDetail?.isPrimumArtist ?? false{
                        self.listenWithPremiumBtn.isHidden = false
                        self.playAllBtn.isHidden = true
                        self.followingBtn.isHidden = true
                        
                    }else{
                        self.listenWithPremiumBtn.isHidden = true
                        self.playAllBtn.isHidden = false
                        self.followingBtn.isHidden = false
                    }
                    
                    if artistDetail?.isPreOrderAvailble ?? false{
                        UIView.animate(withDuration: 3) {
                            self.preOrderView.isHidden = false
                            self.preOrderArtistDetailLabel.text = "\(self.artistNameLabel.text ?? "") is going to release new songs."
                            self.preOrderViewHeightContraint.constant = 130
                        }
                        
                    }else{
                        UIView.animate(withDuration: 3) {
                            self.preOrderView.isHidden = true
                            self.preOrderViewHeightContraint.constant = 0
                            
                        }
                    }
                    
                    self.setupFollowingBtnView()
                }
                else if $0?.response?.status == false {
                    print("Response for ArtistDetailViewController is false")
                    Utils.showErrorAlert(vc: self, message: $0?.response?.message)
                    self.removeActivityIndicator()
                    
                }
            }
        }
    }
    
    
    //MARK: - Binding Error Response
    private func bindingServerErrorResponse() {
        self.mSongAndArtistViewModel.errorResponseForArtistDetail.bind {
            if ($0 != nil) {
                Utils.showErrorAlert(vc: self, message: $0?.response?.message)
            }
            
            self.removeActivityIndicator()
        }
        
        
    }
    
    
    
    //MARK: - Binding Server Response For Follow Artist
    private func bindingServerResponseForFollowArtist() {
        self.mSongAndArtistViewModel.serverResponseForFollowArtist.bind {
            if ($0 != nil) {
                
                if $0?.response?.status ?? false {
                    self.isFollowingBtnAnimation()
                    self.followBtnActivityIndicator.stopAnimating()
                }
                else if $0?.response?.status == false {
                    self.followBtnActivityIndicator.stopAnimating()
                    print("Response for ArtistDetailViewController is false")
                    Utils.showErrorAlert(vc: self, message: $0?.response?.message)
                    self.removeActivityIndicator()
                    
                }
            }
        }
    }
    
    //MARK: - Binding Error Response For Following Artist
    private func bindingServerErrorResponseForFollowArtist() {
        self.mSongAndArtistViewModel.errorResponseForFollowArtist.bind {
            if ($0 != nil) {
                Utils.showErrorAlert(vc: self, message: $0?.response?.message)
            }
            self.followBtnActivityIndicator.stopAnimating()
            self.removeActivityIndicator()
        }
        
        
    }
    
    
    //MARK: - Binding Complete Response
    private func bindingServerCompletionResponse(){
        self.mSongAndArtistViewModel.completion.bind {
            if ($0 != nil) {
                self.removeActivityIndicator()
            }
        }
        
    }
}
extension ArtistDetailViewController:UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.mViewModel.songsArray?.count ?? 3
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: UINibConstant.SongsTableViewCellIdentifier, for: indexPath) as! SongsTableViewCell
        
        if !(self.mViewModel.songsArray?.isEmpty ??  true){
            cell.hideAnimation()
            cell.setUpCell(data: self.mViewModel.songsArray?[indexPath.row])
            
        }else{
            cell.showAnimation()
        }
        cell.selectionStyle = .none
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let tabBarVC = self.tabBarVC as? CustomTabbarController
        
        if self.mViewModel.songsArray?[indexPath.row].songStatus == SongStatus.PREMIUM.rawValue{
            let vc = UIStoryboard.init(name: StoryBoardConstant.storyBoardMain, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.SongsDetailViewController) as? SongsDetailViewController
            vc?.setPlayingCategoryType(categoryType: RecentlyPlayedCatEnum.Artists.rawValue)
            vc?.setSongData(song: self.mViewModel.songsArray?[indexPath.row])
            vc?.setIndexPath(row: indexPath.row)
            vc?.setCompleteSongsArray(songsArray: self.mViewModel.songsArray)
            vc?.setIsPlayingFromString(isplayingFromString: "\(StringConstant.playingFrom)ARTIST")
            vc?.setArtistData(artist: self.mViewModel.lablesArtist ?? Artist())
            UserDefaultUtils.setBool(value : false, key: CommonKeys.IsPlayingFromNotification)
            self.navigationController?.pushViewController(vc!, animated: false)
            tabBarVC?.removeMiniPlayer()
        }else{
            
            
            let vc = UIStoryboard.init(name: StoryBoardConstant.storyBoardMain, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.SongsDetailViewController) as? SongsDetailViewController
            vc?.setPlayingCategoryType(categoryType: RecentlyPlayedCatEnum.Artists.rawValue)
            vc?.setSongData(song: self.mViewModel.songsArray?[indexPath.row])
            vc?.setIndexPath(row: indexPath.row)
            vc?.setCompleteSongsArray(songsArray: self.mViewModel.songsArray)
            vc?.setIsPlayingFromString(isplayingFromString: "\(StringConstant.playingFrom)ARTIST")
            vc?.setIsFromMiniPlayer(isFromMiniPlayer: false)
            vc?.setArtistData(artist: self.mViewModel.lablesArtist ?? Artist())
            UserDefaultUtils.setBool(value : false, key: CommonKeys.IsPlayingFromNotification)
            self.navigationController?.pushViewController(vc!, animated: false)
            
            print("Tabbarcontroller here \(tabBarVC)")
            tabBarVC?.setPlayingCategoryType(categoryType: RecentlyPlayedCatEnum.Songs.rawValue)
            tabBarVC?.setSongData(song: self.mViewModel.songsArray?[indexPath.row])
            tabBarVC?.setIndexPath(row: indexPath.row)
            tabBarVC?.setCompleteSongsArray(songsArray: self.mViewModel.songsArray)
            tabBarVC?.setIsPlayingFromString(isplayingFromString: "\(StringConstant.playingFrom)ARTIST")
            
            tabBarVC?.addMiniPlayer()
            
        }
    }
    
    
}
extension  ArtistDetailViewController:UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.mViewModel.albumsArray?.count ?? 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: UINibConstant.AlbumCollectionViewCellIdentifier, for: indexPath) as! AlbumCollectionViewCell
        if !(self.mViewModel.albumsArray?.isEmpty ?? true){
            cell.hideAnimation()
            // MARK: -View all btn Enabling
            self.viewAllBtn.isEnabled = true
            cell.setUpCell(data: self.mViewModel.albumsArray?[indexPath.row])
            
        }else{
            cell.showAnimation()
        }
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.addTransitionOnNavigationControler()
        let vc = UIStoryboard.init(name: StoryBoardConstant.storyBoardMain, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.AlbumDetailVc) as? AlbumDetailViewController
        let builder = SongAndArtistBuilder()
        builder.setalbumId(self.mViewModel.albumsArray?[indexPath.row].albumID)
        builder.setalbumTitle(self.mViewModel.albumsArray?[indexPath.row].albumTitle)
        builder.setalbumCoverImageURL(self.mViewModel.albumsArray?[indexPath.row].albumCoverImageURL)
        builder.setalbumStatus(self.mViewModel.albumsArray?[indexPath.row].albumStatus)
        builder.setNumberOfSongs(self.mViewModel.albumsArray?[indexPath.row].numberOfSongs)
        builder.setTimeStamp(Date().timeIntervalSince1970)
        vc?.setSongData(data: builder)
        vc?.setAlbumData(data: builder.build())
        vc?.setTabBarVC(tabBarVC: self.tabBarVC ?? UITabBarController())
        vc?.setAlbum(data: self.mViewModel.albumsArray?[indexPath.row])
        vc?.setCategoryType(category: RecentlyPlayedCatEnum.Artists.rawValue)
        //Artist pass
        vc?.labelArtist(lblArtist: self.mViewModel.lablesArtist)
        self.navigationController?.pushViewController(vc!, animated: false)
    }
    
    
}

extension ArtistDetailViewController:ArtistDetailViewControllerProtocol{
    func setTabBarVC(tabBarVC: UITabBarController) {
        self.tabBarVC = tabBarVC
    }
    
    func setArtistId(artistId: Int?) {
        self.mViewModel.artistId = artistId
    }
    
    
}


extension ArtistDetailViewController : AlbumLabelDetaillViewControllerProtocol{
    
    func lebelAlbum(lblAlbum: Album) {
        mViewModel.labelsAlbum = lblAlbum
        numberOfAlbumLabel.text = lblAlbum.numberOfAlbum
    }
}
