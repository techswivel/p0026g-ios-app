//
//  ArtistDetailViewModel.swift
//  QtheMusic
//
//  Created by Assad Khan on 11/05/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import Foundation


class ArtistDetailViewModel:BaseViewModel{
    
    var lablesArtist : Artist?
    var labelsAlbum : Album?
    var artistDetail: ArtistDetails?
    
    var albumsArray  : [Album]?
    var songsArray:[Song]?
    
    var artistId:Int?
    var isFollowing:Bool?
    func getAlbumsDataFromDb() {
        if let Data = mDataManager.getAlbumsDataFromDb(){
            self.albumsArray = Data
        }
    }
    
}
