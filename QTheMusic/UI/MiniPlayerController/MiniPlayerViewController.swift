//
//  MiniPlayerViewController.swift
//  QtheMusic
//
//  Created by Assad Khan on 27/07/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import UIKit
import LinearProgressView
import AVFoundation
import AVKit
import MediaPlayer

protocol MiniPlayerViewControllerProtocol{
    func setDelegate(delegate:MiniPlayerViewControllerDelegate)
    func setIsPlayingFromMiniPlayer(isFromMiniPlayerVC:Bool)
    func setTabBarVC(tabBarVC:UITabBarController)
}
protocol MiniPlayerViewControllerDelegate{
    func navigateToMainAudioPlayer(isplaying:Bool, category: String)
}
class MiniPlayerViewController: UIViewController {
    @IBOutlet weak var songImageView: UIImageView!{
        didSet{
            songImageView.roundCornerImageView()
        }
    }
    @IBOutlet weak var songTitleLabel: UILabel!
    @IBOutlet weak var songPlayBtn: UIButton!
    @IBOutlet weak var songDurationProgressView: LinearProgressView!
    @IBOutlet weak var songImageActtivityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var navigateToMainPlayerBtn: UIButton!
    internal var mViewModel = MiniPlayerViewModel()
    private var delegate : MiniPlayerViewControllerDelegate?
    private var isPlayingFromMiniPlayer = false
    private var tabBarVC:UITabBarController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
     
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("View will appear resuming\(UserDefaultUtils.getBool(key: StringConstant.ResumingMiniPlayerKey))")
        if UserDefaultUtils.getBool(key: StringConstant.ResumingMiniPlayerKey){
           
            self.resumingMiniPlayer()
        }else{
            self.setupView()
            
        }
       
      
    }
    
    @objc func handleSwipe(gesture: UISwipeGestureRecognizer) {
        // handling code
        let tbVC = self.tabBarVC as? CustomTabbarController
        tbVC?.removeMiniPlayer()
        AudioPlayer.shared.stopPlaying()
       
    }
    @IBAction func navigateToAudioPlayerBtnPresed(_ sender: Any) {
        //Delegate to tabbar controller and push vc
        print("Cat Type: \(self.mViewModel.categoryType ?? "")")
        self.delegate?.navigateToMainAudioPlayer(isplaying: self.mViewModel.isPlaying ?? false, category: self.mViewModel.categoryType ?? "")
        
    }
    
  
    @IBAction func playBtnPressed(_ sender: Any) {
        //If resuming it via mini player
        self.selectedSongInMediaPlayer()
    }
    
    private func selectedSongInMediaPlayer() {
        if AudioPlayer.shared.aVPlayer == nil {
            DispatchQueue.main.async {
                let url = NSURL(string:  (UserDefaultUtils.getSavedSongData(key: StringConstant.saveSongKey)!).songAudioURL ?? ""  )
                AudioPlayer.shared.setDelegate(delegate: self)
                if let URLForSong = url
                {
                    self.songPlayBtn.isEnabled = false
                    
                    AudioPlayer.shared.downloadFileFromURL(url: URLForSong, false)
                }
            }
            
        
            return
        }
        
        if self.mViewModel.isPlaying ?? false{
            self.mViewModel.isPlaying = false
            
            
            AudioPlayer.shared.displayLink?.invalidate()
            AudioPlayer.shared.aVPlayer?.pause()
            
            songPlayBtn.setImage(UIImage(named: StringConstant.playImage), for: .normal)
            self.syncMediaCenterRemoteControlAudioRateTitle(title: StringConstant.paused, Rate: 0)
            AudioPlayer.shared.setPlaybackFalse()
            AudioPlayer.shared.syncSeekBarTimeInRemoteControlCenter()
        }else{
            self.mViewModel.isPlaying = true
            AudioPlayer.shared.aVPlayer?.prepareToPlay()
            AudioPlayer.shared.aVPlayer?.play()
            
            songPlayBtn.setImage(UIImage(named: StringConstant.pauseImage), for: .normal)
            AudioPlayer.shared.instentiateCDisplayLink()
            AudioPlayer.shared.syncSeekBarTimeInRemoteControlCenter()
            AudioPlayer.shared.setPlaybacktrue()
            
            if UserDefaultUtils.isValueNil(key: StringConstant.titleOfAudioKey) != nil{
                let title = UserDefaultUtils.getString(key: StringConstant.titleOfAudioKey)
                self.syncMediaCenterRemoteControlAudioRateTitle(title: title, Rate: 1)
            }
            AudioPlayer.shared.syncSeekBarTimeInRemoteControlCenter()
        }
    }
    
    internal func setupView(){
        
        self.selectedSongInMediaPlayer()
       
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.handleSwipe))
            swipeLeft.direction = .left
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.handleSwipe))
        swipeRight.direction = .right
        
            self.navigateToMainPlayerBtn.addGestureRecognizer(swipeLeft)
        self.navigateToMainPlayerBtn.addGestureRecognizer(swipeRight)

      

      
        self.songTitleLabel.text = "\(self.mViewModel.song?.songTitle ?? "No Title")"
        
        self.songPlayBtn.isEnabled = false
        self.navigateToMainPlayerBtn.isEnabled = false
        self.songImageView.loadImage(imageUrl: self.mViewModel.song?.coverImageURL ?? "", placeHolderName: ImageConstant.noImagePlaceHolder, activiyIndicator: self.songImageActtivityIndicator)
        
        self.songDurationProgressView.setProgress(0, animated: true)
        
            //If its previously playing any audio then stop
            AudioPlayer.shared.stopPlaying()
            DispatchQueue.main.async {
                let url = NSURL(string:  self.mViewModel.song?.songAudioURL ?? "")
                AudioPlayer.shared.setDelegate(delegate: self)
                if let URLForSong = url,let imageURL = URL(string:  "\(self.mViewModel.song?.albumCoverImageURL ?? "")" ){
                    
                    self.songPlayBtn.isEnabled = false
                    
                    AudioPlayer.shared.downloadFileFromURL(url: URLForSong, false)
                    AudioPlayer.shared.playingFileName = "\(self.mViewModel.song?.songTitle ?? "" )"
                    AudioPlayer.shared.playingAlbumName = "\(self.mViewModel.song?.albumName ?? "")"
                    
                   
                    
                    self.getData(from: imageURL ) { data, response, error in
                            guard let data = data, error == nil else { return }
                            DispatchQueue.main.async() { [weak self] in
                                AudioPlayer.shared.PlayingFileImage = UIImage(data: data)
                            }
                    }
                    
                   
                }
           
        }
        
        
    }

    internal func resumingMiniPlayer(){
        
        
        UserDefaultUtils.setBool(value: true, key: StringConstant.resumeplayeratCurrentPositionKey)
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.handleSwipe))
            swipeLeft.direction = .left
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.handleSwipe))
        swipeRight.direction = .right
        
            self.navigateToMainPlayerBtn.addGestureRecognizer(swipeLeft)
            self.navigateToMainPlayerBtn.addGestureRecognizer(swipeRight)

      

      
        self.songTitleLabel.text = "\(self.mViewModel.song?.songTitle ?? "No Title")"
        
       
        self.songImageView.loadImage(imageUrl: self.mViewModel.song?.coverImageURL ?? "", placeHolderName: ImageConstant.noImagePlaceHolder, activiyIndicator: self.songImageActtivityIndicator)
       
        
        print(UserDefaultUtils.getInteger(key: StringConstant.audioFullTimeKey))
        print(UserDefaultUtils.getInteger(key: StringConstant.audioCurrentStatusBarStatusKey))
        print("DATA")
        self.songDurationProgressView.maximumValue = Float(UserDefaultUtils.getInteger(key: StringConstant.audioFullTimeKey))
        self.songDurationProgressView.setProgress(Float(UserDefaultUtils.getInteger(key: StringConstant.audioCurrentStatusBarStatusKey) ), animated: true)
        
        
        
            
            DispatchQueue.main.async {
               
              
                if let imageURL = URL(string:  "\(self.mViewModel.song?.albumCoverImageURL ?? "")" ){
                    
                    
                    
                    
                    AudioPlayer.shared.playingFileName = "\(self.mViewModel.song?.songTitle ?? "" )"
                    AudioPlayer.shared.playingAlbumName = "\(self.mViewModel.song?.albumName ?? "")"
                    
                   
                    
                    self.getData(from: imageURL ) { data, response, error in
                            guard let data = data, error == nil else { return }
                            DispatchQueue.main.async() { [weak self] in
                                AudioPlayer.shared.PlayingFileImage = UIImage(data: data)
                            }
                    }
                    
                   
                }
           
        }
        
        
    }
    private func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    private func syncMediaCenterRemoteControlAudioRateTitle(title:String,Rate:Int){
        AudioPlayer.shared.nowPlayingInfo?[MPMediaItemPropertyTitle] = title
        AudioPlayer.shared.nowPlayingInfo?[MPNowPlayingInfoPropertyPlaybackRate] = Rate
        MPNowPlayingInfoCenter.default().nowPlayingInfo =  AudioPlayer.shared.nowPlayingInfo
        
    }
    internal func setDelegateToVCAndSetUpViewOnAppearing(){
        AudioPlayer.shared.setDelegate(delegate: self)
        self.songDurationProgressView.setProgress(Float(AudioPlayer.shared.aVPlayer?.currentTime ?? TimeInterval(UserDefaultUtils.getInteger(key: StringConstant.audioCurrentStatusBarStatusKey))), animated: true)
        print("\n iscoming to mini player \(self.isPlayingFromMiniPlayer)")

       
        let tbVC = self.tabBarVC as? CustomTabbarController
        self.mViewModel.isPlaying = tbVC?.mViewModel.isPlaying
        if tbVC?.mViewModel.isPlaying ?? false{
            songPlayBtn.setImage(UIImage(named: StringConstant.pauseImage), for: .normal)
        }else{
            songPlayBtn.setImage(UIImage(named: StringConstant.playImage), for: .normal)
            
            
        }
        
        
        guard let Text = AudioPlayer.shared.playingFileName else {return}
        
        self.songTitleLabel.text = Text
        
        if let url = AudioPlayer.shared.songsArray?[AudioPlayer.shared.indexPath ?? 0].coverImageURL{
            self.songImageView.loadImage(imageUrl: url , placeHolderName: ImageConstant.noImagePlaceHolder, activiyIndicator: self.songImageActtivityIndicator)}else{
                print("Couldnot find url \n")
            }
       
    }
    fileprivate func saveDataToSharedPrefrences() {
        UserDefaultUtils.setString(value: self.mViewModel.categoryType ?? "", key: StringConstant.catTypeKey)
        UserDefaultUtils.saveSongData(value: self.mViewModel.song ?? Song(), key: StringConstant.saveSongKey)
        UserDefaultUtils.setInteger(value: AudioPlayer.shared.indexPath ?? -1, key: StringConstant.saveIndexKey)
        UserDefaultUtils.saveSongArray(value:AudioPlayer.shared.songsArray ?? [Song](), key: StringConstant.saveSongArrayKey)
        UserDefaultUtils.setString(value: "\(self.mViewModel.isplayingFromString ?? "")", key: StringConstant.isPlayingFromKey)
       
    }
}
extension MiniPlayerViewController:SongsDetailProtocol{
    func setIsFromMiniPlayer(isFromMiniPlayer: Bool) {
        
    }
    
    func setCompleteSongsArray(songsArray: [Song]?) {
        if let songsArray = songsArray {
            self.mViewModel.songsArray = songsArray
        }
    }
    
    func setIndexPath(row: Int?) {
        if let Row = row{
            self.mViewModel.indexPathRow = Row
        }
    }
    
    func setIsPlayingFromString(isplayingFromString:String?) {
        if let string = isplayingFromString{
            self.mViewModel.isplayingFromString = string
        }
    }
    
    func setPlayingCategoryType(categoryType: String?) {
        if let Data = categoryType{
            self.mViewModel.categoryType = Data
        }
    }
    
    func setSongData(song: Song?) {
        if let Data = song{
            self.mViewModel.song = Data
        }
    }
    
    func setAlbumData(album: Album?) {
        if let Data = album{
            self.mViewModel.album = Data
        }
    }
    
    
}
extension MiniPlayerViewController : AudioPlayerPlayDelegate
{
    func onNextOrPreviousSong(song: Song) {
        self.mViewModel.song = song
        self.saveDataToSharedPrefrences()
        self.songPlayBtn.isEnabled = false
        self.songDurationProgressView.setProgress(0, animated: true)
    }
    
    
    func playerIsPlaying() {
        print("Player is playing")
        DispatchQueue.main.async {
        self.mViewModel.isPlaying = true
        self.songPlayBtn.isEnabled = true
            self.navigateToMainPlayerBtn.isEnabled = true
        self.songPlayBtn.setImage(UIImage(named: StringConstant.pauseImage), for: .normal)
            if !UserDefaultUtils.getBool(key: StringConstant.resumeplayeratCurrentPositionKey){
                self.songDurationProgressView.setProgress(0, animated: true)}
            self.songDurationProgressView.maximumValue = Float(CGFloat( ((self.mViewModel.songsArray?[ self.mViewModel.indexPathRow ?? 0].songDuration as? Int ?? 0) / 1000)))
        }
        
        
    }
    
    func playerCatchedException() {
        print("Exceoption")
        self.songPlayBtn.setImage(UIImage(named: StringConstant.playImage), for: .normal)
        Utils.showToast(view: self.view, message: StringConstant.couldntPlayAudioFile)
        songPlayBtn.isEnabled = false
        
    }
    
    func isUpdatingSeekBarView(currentTime: TimeInterval) {
      
        if AudioPlayer.shared.songsArray?[AudioPlayer.shared.indexPath ?? 0].songStatus == SongStatus.PREMIUM.rawValue{
            
        }else{
        if UserDefaultUtils.getBool(key: StringConstant.resumeplayeratCurrentPositionKey){
            UserDefaultUtils.setBool(value: false, key: StringConstant.resumeplayeratCurrentPositionKey)
            print("\(TimeInterval(UserDefaultUtils.getInteger(key: StringConstant.audioCurrentStatusBarStatusKey)))")
            AudioPlayer.shared.aVPlayer?.currentTime = TimeInterval(UserDefaultUtils.getInteger(key: StringConstant.audioCurrentStatusBarStatusKey))
            AudioPlayer.shared.aVPlayer?.prepareToPlay()
            AudioPlayer.shared.aVPlayer?.play()
            
           
        }}
        self.songPlayBtn.isEnabled = true
        self.navigateToMainPlayerBtn.isEnabled = true
        self.songPlayBtn.setImage(UIImage(named: StringConstant.pauseImage) , for: .normal)
        self.songDurationProgressView.setProgress(Float(CGFloat(currentTime)), animated: true)
        self.mViewModel.maxValueOfSeekBar  = CGFloat(currentTime)
        self.mViewModel.isPlaying = true
        if  self.mViewModel.indexPathRow != -1{//handling case:If audio file is not selected and play buton is pressed
            self.songDurationProgressView.maximumValue = (Float(CGFloat((self.mViewModel.songsArray?[ self.mViewModel.indexPathRow ?? 0].songDuration as? Int ?? 0) / 1000)))
    }
    }


    func stopUpdatingSeekBarView() {
        print("Stopped")
            self.mViewModel.isPlaying = false
            self.songPlayBtn.setImage(UIImage(named: StringConstant.playImage), for: .normal)
            if  self.mViewModel.indexPathRow != -1{
                
                self.songDurationProgressView.setProgress(Float((self.mViewModel.songsArray?[ self.mViewModel.indexPathRow ?? 0].songDuration as? Int ?? 0)), animated: true)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                    
                    self.songDurationProgressView.setProgress(0, animated: true)
                })
            }
        AudioPlayer.shared.playNextSong(playNext: true)
        guard let Text = AudioPlayer.shared.playingFileName else {return}
        
        self.songTitleLabel.text = Text
        if let url = AudioPlayer.shared.songsArray?[AudioPlayer.shared.indexPath ?? 0].coverImageURL{
            self.songImageView.loadImage(imageUrl: url , placeHolderName: ImageConstant.noImagePlaceHolder, activiyIndicator: self.songImageActtivityIndicator)}else{
                print("Couldnot find url \n")
            }
        
    }
    
    func applicationWillEnterBackgroundObs() {
        
    }
    
    func applicationWillBecomeActiveObs() {
        
    }
    
    func onStateChange(isPlaying: Bool, imageName: String) {
        print("On state changed")
        self.mViewModel.isPlaying = isPlaying
        self.songPlayBtn.setImage(UIImage(named: imageName), for: .normal)
    }
}
extension MiniPlayerViewController:MiniPlayerViewControllerProtocol{
    func setTabBarVC(tabBarVC:UITabBarController) {
        self.tabBarVC = tabBarVC
    }
    
    func setIsPlayingFromMiniPlayer(isFromMiniPlayerVC: Bool) {
        print(isFromMiniPlayerVC,"Setting is playing from vc in delegate")
        self.isPlayingFromMiniPlayer = isFromMiniPlayerVC
    }
    
    func setDelegate(delegate: MiniPlayerViewControllerDelegate) {
        self.delegate = delegate
    }
}

