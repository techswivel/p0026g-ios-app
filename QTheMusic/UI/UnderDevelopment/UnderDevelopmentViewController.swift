//
//  UnderDevelopmentViewController.swift
//  QtheMusic
//
//  Created by Assad Khan on 20/05/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import UIKit

class UnderDevelopmentViewController: BaseViewController {
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        view.addGradientBackground()
    }
    
    @IBAction func onClickBackBtn(_ sender: Any) {
        popBackNavigationWithTransition()
    }
}
