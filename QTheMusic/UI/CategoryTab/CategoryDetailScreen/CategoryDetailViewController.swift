//
//  CategoryDetailViewController.swift
//  QtheMusic
//
//  Created by Assad Khan on 27/04/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import UIKit


protocol CategoryDetailViewControllerProtocol{
    func setCategoryData(category:Category?)
    func setTabbarVC(tabBarVC:UITabBarController)
}

class CategoryDetailViewController: BaseViewController {
    
    @IBOutlet weak var albumNameLabel: UILabel!
    
    @IBOutlet weak var numberOfSongsLabel: UILabel!
    @IBOutlet weak var numberOfAlbumLabel: UILabel!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var albumCoverImageView: UIImageView!{
        didSet{
            albumCoverImageView.blurImageViewWithTransparentBlackColor()
        }
    }
    
    @IBOutlet weak var backBtn: UIButton!
    
    @IBOutlet weak var playingCategoryCollectionView: UICollectionView!{
        didSet{
            playingCategoryCollectionView.dataSource = self
            playingCategoryCollectionView.delegate = self
        }
    }
    
    @IBOutlet weak var songsTableView: UITableView!{
        didSet{
            songsTableView.dataSource = self
            songsTableView.delegate = self
        }
    }
    
    @IBOutlet weak var albumAndArtistCollectionView: UICollectionView!
    {
        didSet{
            albumAndArtistCollectionView.dataSource = self
            albumAndArtistCollectionView.delegate = self
        }
    }
    private var mViewModel = CategoryDetailViewModel()
    private var selectedCat:String?
    private var collectionViewFlowLayout : UICollectionViewFlowLayout!
    private var collectionViewFlowLayoutForArtist : UICollectionViewFlowLayout!
    private var collectionViewFlowLayoutForCat : UICollectionViewFlowLayout!
    private var tabBarVC :UITabBarController?
    private var mSongAndArtistViewModel = SongAndArtistViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUpView()
    }
    
    
    @IBAction func backBtnPressed(_ sender: Any) {
        self.popBackNavigationWithTransition()
    }
    
    private func setUpView(){
        self.registerNib()
        
        albumCoverImageView.loadImage(imageUrl: self.mViewModel.category?.categoryImageURL ?? "", placeHolderName: ImageConstant.noImagePlaceHolder, activiyIndicator: activityIndicator)
        albumNameLabel.text = "\(self.mViewModel.category?.categoryTitle ?? "")"
        numberOfAlbumLabel.text = "\(self.mViewModel.category?.totalAlbums ?? -1) Albums"
        numberOfSongsLabel.text = "\(self.mViewModel.category?.totalSongs ?? -1) Songs"
    }
    private func registerNib(){
        
        
        albumAndArtistCollectionView.translatesAutoresizingMaskIntoConstraints = false
        albumAndArtistCollectionView.bottomAnchor.constraint(equalTo: self.songsTableView.bottomAnchor).isActive = true
        albumAndArtistCollectionView.trailingAnchor.constraint(equalTo: self.songsTableView.trailingAnchor).isActive = true
        playingCategoryCollectionView.register(UINib(nibName: UINibConstant.InterestCollectionViewCellNib, bundle: nil), forCellWithReuseIdentifier: UINibConstant.InterestCollectionViewCellIdentifier)
        
        albumAndArtistCollectionView.register(UINib(nibName: UINibConstant.AlbumCollectionViewCellNib, bundle: nil), forCellWithReuseIdentifier: UINibConstant.AlbumCollectionViewCellIdentifier)
        
        albumAndArtistCollectionView.register(UINib(nibName: UINibConstant.ArtistCollectionViewCellNib, bundle: nil), forCellWithReuseIdentifier: UINibConstant.ArtistCollectionViewCellIdentifier)
        
        songsTableView.register(UINib(nibName: UINibConstant.SongsTableViewCellNib, bundle: nil),forCellReuseIdentifier: UINibConstant.SongsTableViewCellIdentifier)
        
        self.bindingServerResponse()
        self.bindingServerErrorResponse()
        self.bindingServerCompletionResponse()
        
        
        self.selectedCat = RecentlyPlayedCatEnum.Songs.rawValue
        self.mViewModel.recentlyPlayedCategories[0].isSelected = true
        
        self.mViewModel.songsArray = nil
        let builder = SongAndArtistBuilder()
        builder.setRequestType(RequestType.CATEGORY.rawValue)
        builder.settype(SongRecommendType.SONGS.rawValue)
        builder.setCategoryId(self.mViewModel.category?.categoryID)
        self.mSongAndArtistViewModel.getRecommendedSongs(data: builder)
        
        setupFlowLayout()
        self.songsTableView.isHidden = false
        self.albumAndArtistCollectionView.isHidden = true
        
        
        
        playingCategoryCollectionView.translatesAutoresizingMaskIntoConstraints = false
        playingCategoryCollectionView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor,constant: 25).isActive = true
        playingCategoryCollectionView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor,constant: -25).isActive = true
        
        
        
        songsTableView.translatesAutoresizingMaskIntoConstraints = false
        songsTableView.leadingAnchor.constraint(equalTo: playingCategoryCollectionView.leadingAnchor).isActive = true
        songsTableView.trailingAnchor.constraint(equalTo: playingCategoryCollectionView.trailingAnchor).isActive = true
        
        albumAndArtistCollectionView.translatesAutoresizingMaskIntoConstraints = false
        albumAndArtistCollectionView.leadingAnchor.constraint(equalTo: songsTableView.leadingAnchor).isActive = true
        albumAndArtistCollectionView.trailingAnchor.constraint(equalTo: songsTableView.trailingAnchor).isActive = true
        
        
    }
    
    
    
    // MARK: - Collection View Flow Layout
    private func setupFlowLayout(){
        
        if selectedCat == RecentlyPlayedCatEnum.Albums.rawValue{
            
            let width = (albumAndArtistCollectionView.frame.width - 16) / 2
            let height = albumAndArtistCollectionView.frame.width * 0.62
            //using two as two column are to be presented
            collectionViewFlowLayoutForArtist = UICollectionViewFlowLayout()
            
            collectionViewFlowLayoutForArtist.itemSize = CGSize(width: width, height: height)
            
            collectionViewFlowLayoutForArtist.scrollDirection = .vertical
            collectionViewFlowLayoutForArtist.minimumLineSpacing = 8
            collectionViewFlowLayoutForArtist.minimumInteritemSpacing = self.mViewModel.interItemSpacing
            albumAndArtistCollectionView.setCollectionViewLayout(collectionViewFlowLayoutForArtist, animated: true)
        }
        else if self.selectedCat == RecentlyPlayedCatEnum.Artists.rawValue {
            
            let width = (albumAndArtistCollectionView.frame.width - (5 * self.mViewModel.interItemSpacing)) / 3
            let height = albumAndArtistCollectionView.frame.width * 0.4
            //using two as two column are to be presented
            collectionViewFlowLayout = UICollectionViewFlowLayout()
            
            collectionViewFlowLayout.itemSize = CGSize(width: width, height: height)
            
            collectionViewFlowLayout.scrollDirection = .vertical
            collectionViewFlowLayout.minimumLineSpacing = 8
            collectionViewFlowLayout.minimumInteritemSpacing = self.mViewModel.interItemSpacing
            albumAndArtistCollectionView.setCollectionViewLayout(collectionViewFlowLayout, animated: true)
        }
        if collectionViewFlowLayoutForCat == nil{
            
            
            
            collectionViewFlowLayoutForCat = UICollectionViewFlowLayout()
            
            collectionViewFlowLayoutForCat.itemSize = CGSize(width: 100, height: 30)
            
            collectionViewFlowLayoutForCat.scrollDirection = .horizontal
            collectionViewFlowLayoutForCat.minimumLineSpacing = 10
            collectionViewFlowLayoutForCat.minimumInteritemSpacing = self.mViewModel.interItemSpacing
            playingCategoryCollectionView.setCollectionViewLayout(collectionViewFlowLayoutForCat, animated: true)
        }
        
    }
}
// MARK: -CollectionView Delegate
extension CategoryDetailViewController : UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView{
        case playingCategoryCollectionView:
            return self.mViewModel.recentlyPlayedCategories.count
        case albumAndArtistCollectionView:
            if self.selectedCat == RecentlyPlayedCatEnum.Songs.rawValue{
                return   self.mViewModel.songsArray?.count ?? 3
            }else if self.selectedCat == RecentlyPlayedCatEnum.Albums.rawValue{
                return  self.mViewModel.albumsArray?.count ?? 3
            }else{
                return  self.mViewModel.artistArray?.count ?? 3
            }
        default :
            return 0
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView{
        case playingCategoryCollectionView:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: UINibConstant.InterestCollectionViewCellIdentifier, for: indexPath) as! InterestCollectionViewCell
            
            cell.setUpCellForMenu(data: self.mViewModel.recentlyPlayedCategories[indexPath.row])
            
            return cell
        case albumAndArtistCollectionView:
            
            if self.selectedCat == RecentlyPlayedCatEnum.Artists.rawValue{
                
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: UINibConstant.ArtistCollectionViewCellIdentifier, for: indexPath) as! ArtistCollectionViewCell
                if !(self.mViewModel.artistArray?.isEmpty ?? true){
                    cell.hideAnimation()
                    cell.setUpCell(data: self.mViewModel.artistArray?[indexPath.row])
                    
                }else{
                    cell.showAnimation()
                }
                
                return cell
                
                
            }else if self.selectedCat == RecentlyPlayedCatEnum.Albums.rawValue{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: UINibConstant.AlbumCollectionViewCellIdentifier, for: indexPath) as! AlbumCollectionViewCell
                
                if !(self.mViewModel.albumsArray?.isEmpty ?? true){
                    cell.hideAnimation()
                    cell.setUpCell(data: self.mViewModel.albumsArray?[indexPath.row])
                    
                }else{
                    cell.showAnimation()
                }
                return cell
            }
            else{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: UINibConstant.ArtistCollectionViewCellIdentifier, for: indexPath) as! ArtistCollectionViewCell
                
                
                return cell
            }
            
        default:
            
            
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: UINibConstant.ArtistCollectionViewCellIdentifier, for: indexPath) as! ArtistCollectionViewCell
            
            
            return cell
            
        }
        
    }
    
    
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        switch collectionView{
        case playingCategoryCollectionView:
            
            
            self.selectedCat = self.mViewModel.recentlyPlayedCategories[indexPath.row].menuItem ?? ""
            
            if self.mViewModel.recentlyPlayedCategories[indexPath.row].isSelected == true {
                
                
            }else{
                
                
                for items in self.mViewModel.recentlyPlayedCategories{
                    items.isSelected = false
                }
                
                self.mViewModel.recentlyPlayedCategories[indexPath.row].isSelected = true
                
                
                
                if self.selectedCat == RecentlyPlayedCatEnum.Songs.rawValue{
                    
                    songsTableView.isHidden = false
                    albumAndArtistCollectionView.isHidden = true
                    self.mViewModel.songsArray = nil
                    let builder = SongAndArtistBuilder()
                    builder.setRequestType(RequestType.CATEGORY.rawValue)
                    builder.settype(SongRecommendType.SONGS.rawValue)
                    builder.setCategoryId(self.mViewModel.category?.categoryID)
                    self.mSongAndArtistViewModel.getRecommendedSongs(data: builder)
                    
                    self.songsTableView.reloadData()
                    
                    
                    
                }else if self.selectedCat == RecentlyPlayedCatEnum.Albums.rawValue{
                    songsTableView.isHidden = true
                    albumAndArtistCollectionView.isHidden = false
                    
                    self.mViewModel.albumsArray = nil
                    let builder = SongAndArtistBuilder()
                    builder.setRequestType(RequestType.CATEGORY.rawValue)
                    builder.settype(SongRecommendType.ALBUM.rawValue)
                    builder.setCategoryId(self.mViewModel.category?.categoryID)
                    self.mSongAndArtistViewModel.getRecommendedSongs(data: builder)
                    
                    
                    self.albumAndArtistCollectionView.reloadData()
                    
                    setupFlowLayout()
                    
                }else{
                    songsTableView.isHidden = true
                    albumAndArtistCollectionView.isHidden = false
                    
                    self.mViewModel.artistArray = nil
                    let builder = SongAndArtistBuilder()
                    builder.setRequestType(RequestType.CATEGORY.rawValue)
                    builder.settype(SongRecommendType.ARTIST.rawValue)
                    builder.setCategoryId(self.mViewModel.category?.categoryID)
                    self.mSongAndArtistViewModel.getRecommendedSongs(data: builder)
                    
                    
                    self.albumAndArtistCollectionView.reloadData()
                    
                    setupFlowLayout()
                    
                }
                
                
            }
            self.playingCategoryCollectionView.reloadData()
            
        case albumAndArtistCollectionView:
            if self.selectedCat == RecentlyPlayedCatEnum.Albums.rawValue{
                self.addTransitionOnNavigationControler()
                let vc = UIStoryboard.init(name: StoryBoardConstant.storyBoardMain, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.AlbumDetailVc) as? AlbumDetailViewController
                let builder = SongAndArtistBuilder()
                builder.setalbumId(self.mViewModel.albumsArray?[indexPath.row].albumID)
                builder.setalbumTitle(self.mViewModel.albumsArray?[indexPath.row].albumTitle)
                builder.setalbumCoverImageURL(self.mViewModel.albumsArray?[indexPath.row].albumCoverImageURL)
                builder.setalbumStatus(self.mViewModel.albumsArray?[indexPath.row].albumStatus)
                builder.setNumberOfSongs(self.mViewModel.albumsArray?[indexPath.row].numberOfSongs)
                builder.setTimeStamp(Date().timeIntervalSince1970)
                vc?.setSongData(data: builder)
                vc?.setAlbumData(data: builder.build())
                vc?.setAlbum(data: self.mViewModel.albumsArray?[indexPath.row])
                vc?.setCategoryType(category: SongType.CATEGORY.rawValue)
                vc?.setPlayedFrom(playedFrom: SongType.CATEGORY.rawValue)
                vc?.setTabBarVC(tabBarVC: self.tabBarVC ?? UITabBarController())
            
                self.navigationController?.pushViewController(vc!, animated: false)
            }
            else if self.selectedCat == RecentlyPlayedCatEnum.Artists.rawValue{
                self.addTransitionOnNavigationControler()
                let vc = UIStoryboard.init(name: StoryBoardConstant.storyBoardMain, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.ArtistDetailViewController) as? ArtistDetailViewController
                vc?.setArtistId(artistId: self.mViewModel.artistArray?[indexPath.row].artistID)
                vc?.setTabBarVC(tabBarVC: self.tabBarVC ?? UITabBarController())
                self.navigationController?.pushViewController(vc!, animated: false)
                
            }
        default:
            print("Default")
        }
        
        
    }
    
}

extension CategoryDetailViewController{
    
    //MARK: - Binding Server Response
    private func bindingServerResponse() {
        self.mSongAndArtistViewModel.serverResponseForRecommendedSong.bind {
            if ($0 != nil) {
                
                if $0?.response?.status ?? false {
                    
                    
                    defer{
                        
                        self.songsTableView.reloadData()
                        self.albumAndArtistCollectionView.reloadData()
                        
                    }
                    if let Songs = $0?.response?.data?.songs{
                        self.mViewModel.songsArray = Songs
                    }
                    if let Artist = $0?.response?.data?.artist{
                        self.mViewModel.artistArray = Artist
                    }
                    
                    if let Albums = $0?.response?.data?.albums{
                        self.mViewModel.albumsArray = Albums
                    }
                    
                    
                    
                }
                else if $0?.response?.status == false {
                    print("Response for searching song is false")
                    Utils.showErrorAlert(vc: self, message: $0?.response?.message)
                    self.removeActivityIndicator()
                    
                }
            }
        }
    }
    
    //MARK: - Binding Error Response
    private func bindingServerErrorResponse() {
        self.mSongAndArtistViewModel.errorResponseForRecommendedSong.bind {
            if ($0 != nil) {
                Utils.showErrorAlert(vc: self, message: $0?.response?.message)
            }
            
            self.removeActivityIndicator()
        }
        
        
    }
    
    //MARK: - Binding Complete Response
    private func bindingServerCompletionResponse(){
        self.mSongAndArtistViewModel.completion.bind {
            if ($0 != nil) {
                
                
            }
        }
        
    }
}
extension CategoryDetailViewController : UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.mViewModel.songsArray?.count ?? 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: UINibConstant.SongsTableViewCellIdentifier, for: indexPath) as! SongsTableViewCell
        
        if !(self.mViewModel.songsArray?.isEmpty ??  true){
            cell.hideAnimation()
            cell.setUpCell(data: self.mViewModel.songsArray?[indexPath.row])
            
        }else{
            cell.showAnimation()
        }
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let tabBarVC = self.tabBarVC as? CustomTabbarController
        
        if self.mViewModel.songsArray?[indexPath.row].songStatus == SongStatus.PREMIUM.rawValue{
            let vc = UIStoryboard.init(name: StoryBoardConstant.storyBoardMain, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.SongsDetailViewController) as? SongsDetailViewController
            vc?.setPlayingCategoryType(categoryType: self.selectedCat)
            vc?.setSongData(song: self.mViewModel.songsArray?[indexPath.row])
            vc?.setIndexPath(row: indexPath.row)
            vc?.setCompleteSongsArray(songsArray: self.mViewModel.songsArray)
            vc?.setIsPlayingFromString(isplayingFromString: "\(StringConstant.playingFrom)CATEGORY")
            self.navigationController?.pushViewController(vc!, animated: false)
            tabBarVC?.removeMiniPlayer()
        }else{
            
            
            let vc = UIStoryboard.init(name: StoryBoardConstant.storyBoardMain, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.SongsDetailViewController) as? SongsDetailViewController
            vc?.setPlayingCategoryType(categoryType: self.selectedCat)
            vc?.setSongData(song: self.mViewModel.songsArray?[indexPath.row])
            vc?.setIndexPath(row: indexPath.row)
            vc?.setCompleteSongsArray(songsArray: self.mViewModel.songsArray)
            vc?.setIsPlayingFromString(isplayingFromString: "\(StringConstant.playingFrom)CATEGORY")
            vc?.setIsFromMiniPlayer(isFromMiniPlayer: false)
            tabBarVC?.setIsPlayingFromString(isplayingFromString: "\(StringConstant.playingFrom)CATEGORY")
            self.navigationController?.pushViewController(vc!, animated: false)
            
            print("Tabbarcontroller here \(tabBarVC)")
            tabBarVC?.setPlayingCategoryType(categoryType: self.selectedCat)
            tabBarVC?.setSongData(song: self.mViewModel.songsArray?[indexPath.row])
            tabBarVC?.setIndexPath(row: indexPath.row)
            tabBarVC?.setCompleteSongsArray(songsArray: self.mViewModel.songsArray)
            tabBarVC?.setIsPlayingFromString(isplayingFromString: "\(StringConstant.playingFrom)CATEGORY")
            
            tabBarVC?.addMiniPlayer()
        }
    }
}

extension CategoryDetailViewController : CategoryDetailViewControllerProtocol{
    func setTabbarVC(tabBarVC: UITabBarController) {
        self.tabBarVC = tabBarVC
    }
    
    func setCategoryData(category: Category?) {
        if let Data = category{
            self.mViewModel.category = Data
        }
    }
    
    
}
