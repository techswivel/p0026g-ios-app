//
//  CategoryDetailViewModel.swift
//  QtheMusic
//
//  Created by Assad Khan on 27/04/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import Foundation
import UIKit

class CategoryDetailViewModel :BaseViewModel
{
    var category:Category?
    var songsArray : [Song]?
    var artistArray : [Artist]?
    var albumsArray  :[Album]?
    
    let nunmberOfItemPerRow : CGFloat = 3
    let lineSpacing : CGFloat = 1
    let interItemSpacing : CGFloat = 2
    
    var recentlyPlayedCategories = [
        RecentlyPlayedMenu(isSelected: false, menuItem: RecentlyPlayedCatEnum.Songs.rawValue),
        RecentlyPlayedMenu(isSelected: false, menuItem: RecentlyPlayedCatEnum.Albums.rawValue),
        RecentlyPlayedMenu(isSelected: false, menuItem: RecentlyPlayedCatEnum.Artists.rawValue)
    ]
    
    
}
