//
//  CategoryViewController.swift
//  QtheMusic
//
//  Created by Assad Khan on 26/04/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import UIKit
import Foundation

class CategoryViewController: BaseViewController {

    @IBOutlet weak var categoryCollectionView: UICollectionView!{
        didSet{
            categoryCollectionView.delegate = self
            categoryCollectionView.dataSource = self
        }
       
    }
    @IBOutlet weak var bottomConstraintFromSafeArea: NSLayoutConstraint!
    
    
    private var collectionViewFlowLayoutForArtist : UICollectionViewFlowLayout!
    private var mViewModel = CategoryViewModel()
    private var mSongAndArtistViewModel = SongAndArtistViewModel()
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
        self.bottomSafeAreaConstraintForVC = self.bottomConstraintFromSafeArea
                        
    }
    private func setupView(){
        self.registerNib()
        
        self.mViewModel.getCategoriesFromDB()
        self.categoryCollectionView.reloadData()
        
        self.bindingServerResponse()
        self.bindingServerErrorResponse()
        self.bindingServerCompletionResponse()
        
        self.mSongAndArtistViewModel.getCtegories(categoryType: CategoryType.ALL.rawValue)
    }
    private func registerNib()
    {
        categoryCollectionView.register(UINib(nibName: UINibConstant.CategoryCollectionViewCellNib, bundle: nil), forCellWithReuseIdentifier: UINibConstant.CategoryCollectionViewCellIdentifier)
        setupFlowLayout()
        
        
    }
    
    private func setupFlowLayout(){
       
        if collectionViewFlowLayoutForArtist == nil {
            let width = (self.view.frame.width - (58) ) / 2
        let height = categoryCollectionView.frame.width * 0.3
        //using two as two column are to be presented
        collectionViewFlowLayoutForArtist = UICollectionViewFlowLayout()
        
        collectionViewFlowLayoutForArtist.itemSize = CGSize(width: width, height: height)
        
        collectionViewFlowLayoutForArtist.scrollDirection = .vertical
        collectionViewFlowLayoutForArtist.minimumLineSpacing = 0
        collectionViewFlowLayoutForArtist.minimumInteritemSpacing = 0
            categoryCollectionView.setCollectionViewLayout(collectionViewFlowLayoutForArtist, animated: true)
        }
    }
}
extension CategoryViewController:UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       return self.mViewModel.categoriesArray?.count ?? 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: UINibConstant.CategoryCollectionViewCellIdentifier, for: indexPath) as! CategoryCollectionViewCell
        
        if !(self.mViewModel.categoriesArray?.isEmpty ?? true){
           
            cell.stopAnimation()
            cell.setUpCell(data: self.mViewModel.categoriesArray?[indexPath.row])
            
        }else{
           
            cell.showShimmerEffect()
        }

       
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.addTransitionOnNavigationControler()
        let vc = UIStoryboard.init(name: StoryBoardConstant.storyBoardMain, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.CategoryDetailVC) as? CategoryDetailViewController
        vc?.setCategoryData(category: self.mViewModel.categoriesArray?[indexPath.row])
        vc?.setTabbarVC(tabBarVC: self.tabBarController ?? UITabBarController())
        self.navigationController?.pushViewController(vc!, animated: false)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
       return 4
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
}
extension CategoryViewController{
    
    //MARK: - Binding Server Response
    private func bindingServerResponse() {
        self.mSongAndArtistViewModel.serverResponseForGetCategories.bind {
            if ($0 != nil) {
                
                if $0?.response?.status ?? false {
                    
                    defer{
                        self.categoryCollectionView.reloadData()
                        self.mViewModel.insertCategorieToDb()
                    }
                    if !($0?.response?.data?.category?.isEmpty ?? true){
                    
                        self.mViewModel.categoriesArray = $0?.response?.data?.category
                    
                     }
                }
                else if $0?.response?.status == false {
                    print("Response for all categories is false")
                    Utils.showErrorAlert(vc: self, message: $0?.response?.message)
                    self.removeActivityIndicator()
                    
                }
            }
        }
    }
    
    //MARK: - Binding Error Response
    private func bindingServerErrorResponse() {
        self.mSongAndArtistViewModel.errorResponseForGetCategories.bind {
            if ($0 != nil) {
                Utils.showErrorAlert(vc: self, message: $0?.response?.message)
            }
            
            self.removeActivityIndicator()
        }
        
        
    }
    
    //MARK: - Binding Complete Response
    private func bindingServerCompletionResponse(){
        self.mSongAndArtistViewModel.completion.bind {
            if ($0 != nil) {
                
                
            }
        }
        
    }
}
