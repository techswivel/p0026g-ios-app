//
//  CategoryViewModel.swift
//  QtheMusic
//
//  Created by Assad Khan on 26/04/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import Foundation
import UIKit


class CategoryViewModel:BaseViewModel{
    let nunmberOfItemPerRow : CGFloat = 3
    let lineSpacing : CGFloat = 1
    let interItemSpacing : CGFloat = 4
    
    var categoriesArray : [Category]?
    
    func getCategoriesFromDB(){
        
        categoriesArray =    mDataManager.getCategoriesFromDb()
        
    }
    func insertCategorieToDb(){
        mDataManager.insertCategorieToDb(categories: categoriesArray)
    }
    
}
