//
//  ProfileScreenViewModel.swift
//  QtheMusic
//
//  Created by Hammad Hassan on 29/04/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import Foundation


class ProfileScreenViewModel : BaseViewModel {
    
    var ProfileListData = [ProfileList(ProfileListName: ProfileListEnum.Playlists.name, ProfileListIcon: ProfileListEnum.Playlists.icon),
    ProfileList(ProfileListName: ProfileListEnum.Favorite_Songs.name
        , ProfileListIcon: ProfileListEnum.Favorite_Songs.icon),
    ProfileList(ProfileListName: ProfileListEnum.Purchased_Albums.name, ProfileListIcon: ProfileListEnum.Purchased_Albums.icon),
    ProfileList(ProfileListName: ProfileListEnum.Purchased_Songs.name, ProfileListIcon: ProfileListEnum.Purchased_Songs.icon),
    ProfileList(ProfileListName: ProfileListEnum.Following_Artists.name, ProfileListIcon: ProfileListEnum.Following_Artists.icon),
    ProfileList(ProfileListName: ProfileListEnum.Listening_History.name, ProfileListIcon: ProfileListEnum.Listening_History.icon),
    ProfileList(ProfileListName: ProfileListEnum.Downloaded.name, ProfileListIcon: ProfileListEnum.Downloaded.icon),
    ProfileList(ProfileListName: ProfileListEnum.Buying_History.name, ProfileListIcon: ProfileListEnum.Buying_History.icon),
    ProfileList(ProfileListName: ProfileListEnum.Your_Interests.name, ProfileListIcon: ProfileListEnum.Your_Interests.icon)]
    
    
    var authorization:Authorization?
    
    
}

struct ProfileList {
    let ProfileListName : String
    let ProfileListIcon : String
}


enum ProfileListEnum {
  case Playlists
  case Favorite_Songs
  case Purchased_Albums
  case Purchased_Songs
  case Following_Artists
  case Listening_History
  case Downloaded
  case Buying_History
  case Your_Interests
  case Server_Settings
    
    var name : String{
        switch self {
        case .Playlists:
            return "Playlists"
        case .Favorite_Songs:
            return "Favorite Songs"
        case .Purchased_Albums:
            return "Purchased Albums"
        case .Purchased_Songs:
            return "Purchased Songs"
        case .Following_Artists:
            return "Following Artists"
        case .Listening_History:
            return "Listening History"
        case .Downloaded:
            return "Downloaded"
        case .Buying_History:
            return "Buying History"
        case .Your_Interests:
            return "Your Interests"
        case .Server_Settings:
            return "Server Settings"
        }
    }
    
    var icon : String {
        switch self {
        case .Playlists:
           return "playlistIcon"
        case .Favorite_Songs:
          return "favoriteSongIcon"
        case .Purchased_Albums:
           return "puchasedAlbumsIcon"
        case .Purchased_Songs:
           return "purchasedSongsIcon"
        case .Following_Artists:
           return "followingArtistsIcon"
        case .Listening_History:
           return "listeningHistoryIcon"
        case .Downloaded:
            return "downloadedIcon"
        case .Buying_History:
           return "buyingHistoryIcon"
        case .Your_Interests:
           return "yourInterestsIcon"
        case .Server_Settings:
            return "settingIcon"
        }
    }
    
}
