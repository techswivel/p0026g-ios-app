//
//  TermsAndPrivacyPolicyViewController.swift
//  QtheMusic
//
//  Created by Hammad Hassan on 18/05/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import UIKit
protocol TermsAndPrivacyPolicyDelegateImp{
    func setData(navTitle:String,description:String)
}

class TermsAndPrivacyPolicyViewController: BaseViewController {

    @IBOutlet weak var navBarTitleLabel: UILabel!
    
    @IBOutlet weak var textView: UITextView!
    
    private var mViewModel = TermsAndPrivacyViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navBarTitleLabel.text = self.mViewModel.navTitle
        self.textView.text = self.mViewModel.desc
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.popBackController()
    }
    
}

extension TermsAndPrivacyPolicyViewController: TermsAndPrivacyPolicyDelegateImp {
    
    func setData(navTitle: String, description: String) {
        self.mViewModel.navTitle = navTitle
        self.mViewModel.desc = description
    }
    
    
}
