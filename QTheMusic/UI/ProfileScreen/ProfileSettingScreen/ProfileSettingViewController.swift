//
//  ProfileSettingViewController.swift
//  QtheMusic
//
//  Created by Hammad Hassan on 10/05/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import UIKit
import AudioToolbox
import Alamofire


class ProfileSettingViewController: BaseViewController {

    @IBOutlet weak var listenWithPremiumView: UIView!
    @IBOutlet weak var listenWithPremiumButton: UIButton!{
        didSet {
            self.listenWithPremiumButton.layer.cornerRadius = self.listenWithPremiumButton.frame.height / 2
        }
    }
    @IBOutlet weak var currentPlanLabel: UILabel!
    @IBOutlet weak var currentPlanPriceLabel: UILabel!
    
    @IBOutlet weak var currentPlanView: UIView!{
        didSet{
            self.currentPlanView.layer.cornerRadius = 12
        }
    }
    
    @IBOutlet weak var userProfileView: UIView!{
        didSet{
            self.userProfileView.layer.cornerRadius = 12
        }
    }
    @IBOutlet weak var userProfileImage: UIImageView!{
        didSet{
            self.userProfileImage.layer.cornerRadius = self.userProfileImage.frame.height / 2
        }
    }
    @IBOutlet weak var userProfileName: UILabel!
   
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    @IBOutlet weak var enableNotificationSwitch: UISwitch!{
        didSet{
            self.enableNotificationSwitch.set(width: 35, height: 22)
            self.enableNotificationSwitch.onTintColor = ColorConstrants.RED_THEME
            
            
        }
    }
    @IBOutlet weak var followingArtistSwitch: UISwitch!{
        didSet{
            self.followingArtistSwitch.set(width: 35, height: 22)
            self.followingArtistSwitch.onTintColor = ColorConstrants.RED_THEME
            
        }
    }
    
    @IBOutlet weak var logutButton: UIButton!{
        didSet{
            self.logutButton.layer.cornerRadius = 12
        }
    }
    
    @IBOutlet weak var appVersionLabel: UILabel!
    
    private var mViewModel = ProfileSettingViewModel()
    private var profileNetworkViewModel = ProfileViewModel()
    private var authViewModel = AuthViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.bindingAllReponse()
        // Do any additional setup after loading the view.
        self.mViewModel.authorization = UserDefaultUtils.getProfile(key: CommonKeys.KEY_PROFILE)
       
        self.UpdateProfileView()
        // binding logout
        self.bindingLogoutResponse()
        self.bindingLogoutErrorObserver()
        self.bindingCompletionResponse()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        self.UpdateProfileView()
    }
    
  
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.view.addGradientBackground()
       // self.loadNibViews()
    }
    
    
    //MARK: - prepare for segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SEGUE_TERMS_PRIVACY_SCREEN {
            let destVC = segue.destination as! TermsAndPrivacyPolicyViewController
            if self.mViewModel.selectedOption == TermsAndPrivacyEnum.TermsAndCondition.title {
                destVC.setData(navTitle: TermsAndPrivacyEnum.TermsAndCondition.title, description: TermsAndPrivacyEnum.PrivacyPolicy.description)
            }else if self.mViewModel.selectedOption == TermsAndPrivacyEnum.PrivacyPolicy.title{
                destVC.setData(navTitle: TermsAndPrivacyEnum.PrivacyPolicy.title, description: TermsAndPrivacyEnum.PrivacyPolicy.description)
            }
            
            
            
        }
    

        
    }

    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.popBackController()
    }
    
    
    @IBAction func listenWithPremiumButtonPressed(_ sender: UIButton) {
        
        self.listenWithPremiumView.isHidden = true
        self.currentPlanView.isHidden = false
        UIView.animate(withDuration: 0.3) {
            
            self.view.layoutSubviews()
        }
        
        
    }
    
    @IBAction func currentPlanChangeButtonPressed(_ sender: UIButton) {
        let vc = UIStoryboard.init(name: StoryBoardConstant.storyBoardMain, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.subscriptionPlanVC) as? SubscriptionPlanViewController
        self.navigationController?.pushViewController(vc!, animated: true)
        
      //  Utils.showToast(view: self.view, message: StringConstant.underDevelopment)
    }
    
    
    @IBAction func enabledNotificationSwitchedPressed(_ sender: UISwitch) {
        self.showActivityIndicator()
        if mViewModel.authorization?.notification?.isNotificationEnabled == true
        {
            mViewModel.SelectedKey = false
           self.mViewModel.authorization?.isEnableNotification = "false"
            self.mViewModel.authorization?.isArtistUpdateEnable = "false"
            followingArtistSwitch.isOn = false
            followingArtistSwitch.isUserInteractionEnabled = false
    
        }
        else
        {
            mViewModel.SelectedKey = false
            self.mViewModel.authorization?.isEnableNotification = "true"
            followingArtistSwitch.isOn = false
            followingArtistSwitch.isUserInteractionEnabled = true
        }
        let auth = self.mViewModel.updateUserProfileData()
        self.profileNetworkViewModel.updateProfile(auth: auth)
         
    }
    
    @IBAction func followingArtistSwitchedPressed(_ sender: UISwitch) {
        self.showActivityIndicator()
        if mViewModel.authorization?.notification?.isNotificationEnabled == true && mViewModel.authorization?.notification?.isArtistUpdateEnabled == true
        {
            mViewModel.SelectedKey = true
            self.mViewModel.authorization?.isArtistUpdateEnable = "false"
            
        }
        else if mViewModel.authorization?.notification?.isNotificationEnabled == true && mViewModel.authorization?.notification?.isArtistUpdateEnabled == false
        {
                mViewModel.SelectedKey = false
                self.mViewModel.authorization?.isArtistUpdateEnable = "true"
        }
        let auth = self.mViewModel.updateUserProfileData()
        self.profileNetworkViewModel.updateProfile(auth: auth)
         
    }
    
    @IBAction func termsAndConditionButtonPressed(_ sender: UIButton) {
        self.mViewModel.selectedOption = TermsAndPrivacyEnum.TermsAndCondition.title
        self.performSegue(withIdentifier: SEGUE_TERMS_PRIVACY_SCREEN, sender: nil)
    }
    
    
    @IBAction func privacyPolicyButtonPressed(_ sender: UIButton) {
        self.mViewModel.selectedOption = TermsAndPrivacyEnum.PrivacyPolicy.title
        self.performSegue(withIdentifier: SEGUE_TERMS_PRIVACY_SCREEN, sender: nil)
    }
    
    @IBAction func profileButtonPressed(_ sender: UIButton) {
        self.performSegue(withIdentifier: SEGUE_PROFILE_UPDATE, sender: self)
    }
    
    
    
    @IBAction func logoutButtonPressed(_ sender: UIButton) {
        self.showActivityIndicator()
        self.authViewModel.performLogout()
    }
    
    private func UpdateProfileView(){
        
        self.mViewModel.authorization = UserDefaultUtils.getProfile(key: CommonKeys.KEY_PROFILE)
        
        self.userProfileName.text = self.mViewModel.authorization?.name
        
        if let avatar = self.mViewModel.authorization?.avatar {
           
            self.userProfileImage.loadImage(imageUrl: avatar, placeHolderName: ImageConstant.noImagePlaceHolder, activiyIndicator: activityIndicator)
           
           
        }else {
            self.userProfileImage.image = UIImage(named: StringConstant.profilePlaceHolder)
        }
        if mViewModel.authorization?.notification?.isNotificationEnabled == false
        {
            enableNotificationSwitch.isOn = false
            followingArtistSwitch.isOn = false
            followingArtistSwitch.isUserInteractionEnabled = false
        }
        else if mViewModel.authorization?.notification?.isNotificationEnabled == true && mViewModel.authorization?.notification?.isArtistUpdateEnabled == true && mViewModel.SelectedKey == true
        {
            enableNotificationSwitch.isOn = true
            followingArtistSwitch.isOn = false
            followingArtistSwitch.isUserInteractionEnabled = true
        }
        else if mViewModel.authorization?.notification?.isNotificationEnabled == true && mViewModel.authorization?.notification?.isArtistUpdateEnabled == false
        {
            enableNotificationSwitch.isOn = true
            followingArtistSwitch.isOn = false
            
        }
        else if mViewModel.authorization?.notification?.isNotificationEnabled == true && mViewModel.authorization?.notification?.isArtistUpdateEnabled == true && mViewModel.SelectedKey == false
        {
            enableNotificationSwitch.isOn = true
            followingArtistSwitch.isOn = true
            followingArtistSwitch.isUserInteractionEnabled = true
        }
        if self.mViewModel.authorization?.subscription != nil{
            self.listenWithPremiumButton.isHidden = true
            self.currentPlanView.isHidden = false
            UIView.animate(withDuration: 0.3) {
              //  self.profileListTopConstraint.constant = 30
              //  self.currentPlanViewHeightConstraint.constant = 120
                self.view.layoutSubviews()
            }
            self.currentPlanLabel.text = self.mViewModel.authorization?.subscription?.planTitle
            self.currentPlanPriceLabel.text = "\(self.mViewModel.authorization?.subscription?.planPrice ?? 0) / \(self.mViewModel.authorization?.subscription?.planDuration ?? "")"
        }else{
            UIView.animate(withDuration: 0.3) {
                       
                     //  self.currentPlanViewHeightConstraint.constant = 0
                       self.currentPlanView.isHidden = true
                       
                     //  self.listenWithPremiumButton.isHidden = false
                    //   self.profileListTopConstraint.constant = 50
                       self.view.layoutSubviews()
                       
                   }
        }
       
    }
    
 
    private func bindingAllReponse(){
        self.bindingUpdateProfileObserver()
        self.bindingUpdateProfileErrorObserver()
        self.bindingUpdateProfileExpiryObserver()
    }
    
    
    private func bindingUpdateProfileObserver(){
        self.profileNetworkViewModel.updateProfileServerResponse.bind{
            if ($0 != nil){
                self.removeActivityIndicator()
                if $0?.response?.status ?? false {
                    guard let authData = $0?.response?.data?.auth else {return }
                    print("AuthData:\(authData.toString())")
                    UserDefaultUtils.saveProfile(value: (authData), key: CommonKeys.KEY_PROFILE)
                   self.UpdateProfileView()
                }else {
                    Utils.showErrorAlert(vc: self, message:$0?.response?.message ?? "" )
                }
            }
        }
    }
    
    private func bindingUpdateProfileErrorObserver(){
        self.profileNetworkViewModel.updateProfileErrorResponse.bind {
            if ($0 != nil){
                self.removeActivityIndicator()
                Utils.showAlert(vc: self, message: $0?.response?.message ?? "")
            }
        }
    }
    
    private func bindingUpdateProfileExpiryObserver(){
        self.profileNetworkViewModel.updateProfileExpiryReponse.bind {
            if ($0 != nil){
                self.removeActivityIndicator()
                self.performExpirationInCaseOfBoxBinding()
            }
        }
    }
    
    
}

    //MARK: EXTENSION - Binding Log Out Session
extension ProfileSettingViewController {
    func bindingLogoutResponse() {
        self.authViewModel.serverResponseForLogout.bind {
            if ($0 != nil) {
                if $0?.response?.status == true {
                    self.clearApplicationSession()
                    self.removeActivityIndicator()
                }  else {
                    Utils.showAlert(vc: self, message: $0?.response?.message ?? "")
                    self.removeActivityIndicator()
                }
            }
        }
    }
    
    func bindingLogoutErrorObserver() {
        self.authViewModel.ErrorResponseForLogout.bind {
            if ($0 != nil) {
                
                Utils.showAlert(vc: self, message: $0?.response?.message ?? "")
            }
            self.removeActivityIndicator()
        }
    }
    
    func bindingCompletionResponse() {
        self.authViewModel.completion.bind {
            if ($0 != nil) {
                self.removeActivityIndicator()
            }
        }
    }
}

