//
//  ProfileSettingViewModel.swift
//  QtheMusic
//
//  Created by Hammad Hassan on 18/05/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import Foundation

class ProfileSettingViewModel : BaseViewModel {
    
    var selectedOption : String?
    var authorization:Authorization?
    var profileUpdateSelection : String?
    var SelectedKey = Bool()
    let builder = AuthorizationBuilder()
    
    func updateUserProfileData() ->Authorization {
        builder.setemail(self.authorization?.email)
        builder.setavatar(self.authorization?.avatar)
        builder.setname(self.authorization?.name)
        builder.setdOb(self.authorization?.dOb ?? 0)
        builder.setgender(self.authorization?.gender)
        builder.setaddressModObj(self.authorization?.addressModObj)
        builder.setisEnableNotification(self.authorization?.isEnableNotification)
        builder.setisArtistUpdateEnable(self.authorization?.isArtistUpdateEnable)
       return builder.build()
    }

    
    
}
