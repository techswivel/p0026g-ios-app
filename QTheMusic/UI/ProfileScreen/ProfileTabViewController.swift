//
//  ProfileTabViewController.swift
//  QtheMusic
//
//  Created by Assad Khan on 11/04/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import UIKit

class ProfileTabViewController: BaseViewController {
    
    
    @IBOutlet weak var currentPlanView: UIView!{
        didSet{
            self.currentPlanView.layer.cornerRadius = 12
        }
    }
    @IBOutlet weak var currentPlanTitle: UILabel!
    @IBOutlet weak var planPriceLabel: UILabel!
    /// Defualt Height is = 100
    @IBOutlet weak var currentPlanViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var safeAreaBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var listenWithPremiumButton: UIButton!{
        didSet {
            self.listenWithPremiumButton.layer.cornerRadius = self.listenWithPremiumButton.frame.height / 2
        }
    }
    
    /// when button Listen With Premium Showed then top constraint = 50
    /// if current plan View is showed then top constraint is 10
    @IBOutlet weak var profileListtableView: UITableView!{
        didSet{
            self.profileListtableView.dataSource = self
            self.profileListtableView.delegate = self
            self.profileListtableView.separatorStyle = .none
        }
    }
    
    @IBOutlet weak var profileListTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var activityIdicator: UIActivityIndicatorView!
    
    @IBOutlet weak var userProfileImage: UIImageView!{
        didSet{
            self.userProfileImage.layer.cornerRadius = self.userProfileImage.frame.height / 2
        }
    }
    
    @IBOutlet weak var userNameLabel: UILabel!
    
    private var mViewModel = ProfileScreenViewModel()
    private var mAppStoreViewmodel: AppStoreViewModel = AppStoreViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.registerNibCell()
        
        self.updateProfileView()
        
        if ConfigurationManager.currentEnvironment != Environment.Production {
            self.mViewModel.ProfileListData.append( ProfileList(ProfileListName: ProfileListEnum.Server_Settings.name, ProfileListIcon: ProfileListEnum.Server_Settings.icon))
            
        }
        
        self.bottomSafeAreaConstraintForVC = self.safeAreaBottomConstraint
        self.bindAppStoreSuccessResponse()
        self.bindAppStoreErrorResponse()
        self.bindAppStoreCompleteResponse()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.view.addGradientBackground()
        
        super.updateViewConstraints()
        self.tableViewHeightConstraint.constant = self.profileListtableView.contentSize.height
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.mViewModel.authorization = UserDefaultUtils.getProfile(key: CommonKeys.KEY_PROFILE)
        self.updateProfileView()
        var receiptData: String? {
            guard
                let url = Bundle.main.appStoreReceiptURL,
                let data = try? Data(contentsOf: url)
            else { return nil }
            return data.base64EncodedString()
        }
        
        if receiptData != nil{
            self.mAppStoreViewmodel.getSubscriptionStatus(purchaseToken: receiptData ?? "")
        }else{
            debugPrint("Receipt data is nill")
        }
    }
    
    
    @IBAction func listenWithPremiumButtonPressed(_ sender: UIButton) {
        if self.mViewModel.authorization?.subscription == nil{
            let vc = UIStoryboard.init(name: StoryBoardConstant.storyBoardMain, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.subscriptionPlanVC) as? SubscriptionPlanViewController
            self.navigationController?.pushViewController(vc!, animated: true)
        }
    }
    @IBAction func planChangeButtonPressed(_ sender: UIButton) {
        
        let vc = UIStoryboard.init(name: StoryBoardConstant.storyBoardMain, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.subscriptionPlanVC) as? SubscriptionPlanViewController
        self.navigationController?.pushViewController(vc!, animated: true)

    }
    
    
    
    private func registerNibCell(){
        
        profileListtableView.register(UINib(nibName: UINibConstant.ProfileListTableViewCellNib, bundle: nil),forCellReuseIdentifier: UINibConstant.ProfileListTableViewCellIdentifier)
        
    }
    
    @IBAction func profileSettingButtonPressed(_ sender: UIButton) {
        self.performSegue(withIdentifier: SEGUE_PROFILE_SETTING, sender: self)
    }
    
    
    private func updateProfileView(){
        
        self.userNameLabel.text = self.mViewModel.authorization?.name
        
        
        if let avatar = self.mViewModel.authorization?.avatar {
            
            self.userProfileImage.loadImage(imageUrl: avatar, placeHolderName: ImageConstant.noImagePlaceHolder, activiyIndicator: self.activityIdicator)
            
            
        }else {
            self.userProfileImage.image = UIImage(named: StringConstant.profilePlaceHolder)
        }
        
        
        if self.mViewModel.authorization?.subscription != nil{
            self.listenWithPremiumButton.isHidden = true
            self.currentPlanView.isHidden = false
            UIView.animate(withDuration: 0.3) {
                self.profileListTopConstraint.constant = 30
                self.currentPlanViewHeightConstraint.constant = 120
                self.view.layoutSubviews()
            }
            self.currentPlanTitle.text = self.mViewModel.authorization?.subscription?.planTitle
            self.planPriceLabel.text = "\(self.mViewModel.authorization?.subscription?.planPrice ?? 0) / \(self.mViewModel.authorization?.subscription?.planDuration ?? "")"
        }else{
            UIView.animate(withDuration: 0.3) {
                       
                       self.currentPlanViewHeightConstraint.constant = 0
                       self.currentPlanView.isHidden = true
                       
                       self.listenWithPremiumButton.isHidden = false
                       self.profileListTopConstraint.constant = 50
                       self.view.layoutSubviews()
                       
                   }
        }
        
        
    }
    
    //MARK: - Go TO playlist VC
    func goToPlaylistScreen() {
        
        let playlistVC = UIStoryboard.init(name: StoryBoardConstant.storyBoardMain, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.playlistVC) as! PlaylistViewController
        self.navigationController?.pushViewController(playlistVC, animated: true)
    }
    
    //MARK: - Go TO Favourite VC
    func goToFavouriteScreen() {
        
        let favoriteVC = UIStoryboard.init(name: StoryBoardConstant.storyBoardMain, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.favouritesongVC) as! FavoriteSongsViewController
        self.navigationController?.pushViewController(favoriteVC, animated: true)
    }
    //MARK: - Go TO Album Detail VC
    func goToAlbumDetailScreen() {
        
        let AlbumDetailVC = UIStoryboard.init(name: StoryBoardConstant.storyBoardMain, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.AlbumDetailVc) as! AlbumDetailViewController
        self.navigationController?.pushViewController(AlbumDetailVC, animated: true)
    }
    //MARK: - Go TO Purchased Song VC
    func goToPurchasedSongScreen() {
        
        let purchaseSongVC = UIStoryboard.init(name: StoryBoardConstant.storyBoardMain, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.purchasedsongVC) as! PurchasedSongsViewController
        self.navigationController?.pushViewController(purchaseSongVC, animated: true)
    }
    //MARK: - Go TO Following Artist VC
    func goToFollowingArtistScreen() {
        
        let followingAVC = UIStoryboard.init(name: StoryBoardConstant.storyBoardMain, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.followingartistVC) as! FollowingArtistViewController
        self.navigationController?.pushViewController(followingAVC, animated: true)
    }
    //MARK: - Go TO Purchased Song VC
    func goToPurchasedAlbumScreen() {
        
        let purchaseAlbumVC = UIStoryboard.init(name: StoryBoardConstant.storyBoardMain, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.purchasedalbumVC) as! PurchasedAlbumViewController
        self.navigationController?.pushViewController(purchaseAlbumVC, animated: true)
    }
    //MARK: - Go TO Purchased Song VC
    func goToArtistDetailScreen() {
        
        let aristDeatilVC = UIStoryboard.init(name: StoryBoardConstant.storyBoardMain, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.ArtistDetailViewController) as! ArtistDetailViewController
        self.navigationController?.pushViewController(aristDeatilVC, animated: true)
    }
    //MARK: - Go TO listening History VC
    func goToListeningHistoryScreen() {
        
        let listeningHistoryVC = UIStoryboard.init(name: StoryBoardConstant.storyBoardMain, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.listeningHistoryVC) as! ListeningHistoryViewController
        listeningHistoryVC.tabbarViewController = self.tabBarController as? CustomTabbarController
        self.navigationController?.pushViewController(listeningHistoryVC, animated: true)
    }
    //MARK: - Go TO Downloaded Song VC
    func goToDownloadedSongsScreen() {
        
        let downloadedSongVC = UIStoryboard.init(name: StoryBoardConstant.storyBoardMain, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.downloadedSongVC) as! DownloadedSongViewController
        self.navigationController?.pushViewController(downloadedSongVC, animated: true)
    }
    //MARK: - Go TO Buying history VC
    func goToBuyingHistoryScreen() {
        
        let buyingHistoryVC = UIStoryboard.init(name: StoryBoardConstant.storyBoardMain, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.buyingHistoryVC) as! BuyingHistoryViewController
        self.navigationController?.pushViewController(buyingHistoryVC, animated: true)
    }
    //MARK: - Go TO Downloading Song VC
    func goToDownloadingSongsScreen() {
        
        let downloadingSongVC = UIStoryboard.init(name: StoryBoardConstant.storyBoardMain, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.downloadingSongsVC) as! DownloadingSongsViewController
        self.navigationController?.pushViewController(downloadingSongVC, animated: true)
    }

}



extension ProfileTabViewController : UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.mViewModel.ProfileListData.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: UINibConstant.ProfileListTableViewCellIdentifier, for: indexPath) as! ProfileListTableViewCell
        cell.setProfileData(data: self.mViewModel.ProfileListData[indexPath.row])
        
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch self.mViewModel.ProfileListData[indexPath.row].ProfileListName {
        case ProfileListEnum.Playlists.name:
            goToPlaylistScreen()
        case ProfileListEnum.Favorite_Songs.name:
            goToFavouriteScreen()
        case ProfileListEnum.Purchased_Albums.name:
            goToPurchasedAlbumScreen()
        case ProfileListEnum.Purchased_Songs.name:
            goToPurchasedSongScreen()
        case ProfileListEnum.Following_Artists.name:
            goToFollowingArtistScreen()
        case ProfileListEnum.Listening_History.name:
            self.goToListeningHistoryScreen()
        case ProfileListEnum.Downloaded.name:
            if mViewModel.authorization?.subscription == nil{
                self.goToDownloadedSongsScreen()
            }else {
                self.goToDownloadingSongsScreen()
            }
        case ProfileListEnum.Buying_History.name:
            self.goToBuyingHistoryScreen()
        case ProfileListEnum.Your_Interests.name:
            self.goToInterestSelectionScreen()
            
        case ProfileListEnum.Server_Settings.name:
            
            let serverSettingScreen = StoryBoardConstant.mainStoryBoard.instantiateViewController(withIdentifier: "serverSettingScreens") as! ServerSettingViewController
            
            self.navigationController?.pushViewController(serverSettingScreen, animated: true)
            
        default:
            print("Default")
            Utils.showToast(view: self.view, message: "Under Development")
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
}


//MARK: - Subscription Status checking
extension ProfileTabViewController {
    private func bindAppStoreSuccessResponse(){
        self.mAppStoreViewmodel.successSubscriptionStatus.bind{
            if $0 != nil{
                switch $0?.status{
                case 0:
                    // success
                    self.handleAppStoreResponse(response: $0)
                    break
                case 21000:
                    // other then post request
                    debugPrint("Response fail for store Api code 2100: \(String(describing: $0?.status))")
                    break
                case 21001:
                    // invalid token
                    debugPrint("Response fail for store Api code 21001: \(String(describing: $0?.status))")
                    break
                default:
                    debugPrint("Response fail for store Api code default: \(String(describing: $0?.status))")
                    break
                }
            }
        }
    }
    
    
    private func bindAppStoreErrorResponse(){
        self.mAppStoreViewmodel.errorSubscriptionStatus.bind{
            if $0 != nil{
                Utils.showErrorAlert(vc: self, message: StringConstant.ErrorAlert)
            }
        }
    }
    
    private func bindAppStoreCompleteResponse(){
        self.mAppStoreViewmodel.completion.bind{
            if $0 != nil{
                debugPrint("complete Api request ")
            }
        }
    }
    
    
    private func handleAppStoreResponse(response:AppStoreResponse?){
        let now = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss VV"
        let expire = formatter.date(from: response?.latestReceiptInfo?.first?.expiresDate ?? "") as NSDate?
        if response?.pendingRenewalInfo?.first?.autoRenewStatus == SubscriptionStatus.ACTIVE.value {
            self.listenWithPremiumButton.isHidden = true
                  self.currentPlanView.isHidden = false
                  UIView.animate(withDuration: 0.3) {
                      self.profileListTopConstraint.constant = 30
                      self.currentPlanViewHeightConstraint.constant = 120
                      self.view.layoutSubviews()
                  }
            print("User has active subscription")
        }else if response?.pendingRenewalInfo?.first?.isInBillingRetryPeriod == SubscriptionGracePeriod.ACTIVE.value {
            self.listenWithPremiumButton.isHidden = true
                  self.currentPlanView.isHidden = false
                  UIView.animate(withDuration: 0.3) {
                      self.profileListTopConstraint.constant = 30
                      self.currentPlanViewHeightConstraint.constant = 120
                      self.view.layoutSubviews()
                  }
            print("User has active subscription but in grace period ")
        }else if now <= expire as? Date ?? Date() {
            self.listenWithPremiumButton.isHidden = true
                  self.currentPlanView.isHidden = false
                  UIView.animate(withDuration: 0.3) {
                      self.profileListTopConstraint.constant = 30
                      self.currentPlanViewHeightConstraint.constant = 120
                      self.view.layoutSubviews()
                  }
            print("User has deActive subscription but remain his time ")
        }else {
            let data = UserDefaultUtils.getProfile(key: CommonKeys.KEY_PROFILE)
            data?.subscription = nil
            UserDefaultUtils.saveProfile(value: data ?? AuthorizationBuilder().build(), key: CommonKeys.KEY_PROFILE)
            self.updateProfileView()
            print("User subscription has been deActive ")
        }
    }
    
    
}
