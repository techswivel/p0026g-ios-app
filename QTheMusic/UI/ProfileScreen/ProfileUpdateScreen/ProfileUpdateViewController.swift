//
//  ProfileUpdateViewController.swift
//  QtheMusic
//
//  Created by Hammad Hassan on 13/05/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import UIKit

class ProfileUpdateViewController: BaseViewController {

    @IBOutlet weak var profileImage: UIImageView!{
        didSet{
            self.profileImage.layer.cornerRadius = self.profileImage.frame.height / 2
        }
    }
    @IBOutlet weak var profileImageAddButton: UIButton!{
        didSet{
            self.profileImageAddButton.layer.cornerRadius = self.profileImageAddButton.frame.height  / 2
        }
    }
    
    @IBOutlet weak var emailView: UIView!{
        didSet{
            self.emailView.layer.cornerRadius = 12
        }
    }
    
    @IBOutlet weak var phoneNoView: UIView!{
        didSet{
            self.phoneNoView.layer.cornerRadius = 12
        }
    }
    
    @IBOutlet weak var nameView: UIView!{
        didSet{
            self.nameView.layer.cornerRadius = 12
        }
    }
    
    @IBOutlet weak var DOBView: UIView!{
        didSet{
            self.DOBView.layer.cornerRadius = 12
        }
    }
    
    @IBOutlet weak var genderView: UIView!{
        didSet{
            self.genderView.layer.cornerRadius = 12
        }
    }
    
    @IBOutlet weak var addressView: UIView!{
        didSet{
            self.addressView.layer.cornerRadius = 12
        }
    }
    
    //email outlet
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var emailVerificationImage: UIImageView!
    
    //Phone Outlet
    @IBOutlet weak var phoneNoLabel: UILabel!
    
    //Name OutLet
    @IBOutlet weak var namelabel: UILabel!
    
    // DOB Outlet
    @IBOutlet weak var dobLabel: UILabel!
    
    // Gender Outlet
    @IBOutlet weak var genderLabel: UILabel!
    
    // Address Outlet
    @IBOutlet weak var addressLabel: UILabel!
    
    @IBOutlet weak var phoneAddButton: UIButton!
    
    @IBOutlet weak var phoneVerifyIcon: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    private var phoneNumberView : PhoneNumberView!
    private var userNameView : ChangeUserNameView!
    private var calendarView: CalendarView!
    private var selectGenderView : SelectGender!
    private var changeAddressView : ChangeAddressView!
    
    private var mViewModel = ProfileUpdateViewModel()
    
    private var profileNetworkViewModel = ProfileViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mViewModel.authorization = UserDefaultUtils.getProfile(key: CommonKeys.KEY_PROFILE)
        print("AuthModel :\(self.mViewModel.authorization?.toString())")
        // Do any additional setup after loading the view.
        self.loadNibViews()
        self.setupUserProfileData()
        self.bindingAllReponse()
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.view.addGradientBackground()
       
    }
    

    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.popBackController()
    }
    
    @IBAction func profileImageAddButtonPressed(_ sender: UIButton) {
        
        //Attachment handler is used to specify number of photos which can be selected, showing user camera and library options and managing image after user select's one.
        AttachmentHandler.shared.imageSelectionLimit = 1
        AttachmentHandler.shared.showAttachmentActionSheet(vc: self, type: AttachmentHandler.AttachmentFileType.image)
        AttachmentHandler.shared.imagePickedBlock = {
            image in
            self.profileImage.image = image.first
            self.mViewModel.profilePic = image.first
            self.mViewModel.profilePic?.pngData()
            self.mViewModel.profileImage = true
            self.mViewModel.phoneNumberBool = false
            let auth = self.mViewModel.updateUserProfileData()
            self.profileNetworkViewModel.updateProfile(auth: auth)
    }
    }
    @IBAction func addPhoneNoButtonPressed(_ sender: UIButton) {
        self.view.addSubview(self.phoneNumberView)
        
    }
    
    @IBAction func nameChangeButtonPressed(_ sender: UIButton) {
        self.userNameView.setData(data: self.mViewModel.authorization)
        self.view.addSubview(self.userNameView)
        
    }
    
    @IBAction func dobChangeButtonPressed(_ sender: UIButton) {
        self.view.endEditing(true)
        
        
        if let dob = self.mViewModel.authorization?.dOb{
            self.calendarView.setDateOnDatePicker(dobTimeStamp: Double(dob) ?? 0.0)
        }
        
        self.calendarView.alpha = 0.0
        self.view.addSubview(calendarView)
        UIView.animate(withDuration: 0.25) { () -> Void in
            self.calendarView.alpha = 1.0
        }
    }
    
    @IBAction func genderChangeButtonPressed(_ sender: UIButton) {
        
        if self.mViewModel.selectedGender == SelectGenderEnum.MALE.rawValue{
            selectGenderView.setMaleCheckMark()
        }else if self.mViewModel.selectedGender == SelectGenderEnum.FEMALE.rawValue{
            selectGenderView.setFeMaleCheckMark()
        }else if self.mViewModel.selectedGender == SelectGenderEnum.NON_BINARY.rawValue{
            selectGenderView.setNonBinaryCheckMark()
        }else if self.mViewModel.selectedGender == SelectGenderEnum.NOT_ANSWERED.rawValue{
            selectGenderView.setNoAnswerCheckMark()
        }else{
            selectGenderView.setMaleCheckMark()
        }
        
        self.view.endEditing(true)
        selectGenderView.alpha = 0
        self.view.addSubview(selectGenderView)
        UIView.animate(withDuration: 0.25) { () -> Void in
            self.selectGenderView.alpha = 1.0
        }
    }
    
    @IBAction func addressChangeButtonPressed(_ sender: UIButton) {
        
        self.changeAddressView.setAddressData(data: self.mViewModel.authorization)
        self.view.addSubview(self.changeAddressView)
    }
    
    
    
    private func loadNibViews(){
        //PhoneNumber View
        self.phoneNumberView = UINib(nibName: UINibConstant.PhoneNumberView, bundle: .main).instantiate(withOwner: nil, options: nil).first as? PhoneNumberView
        self.phoneNumberView.frame = self.view.bounds
        self.phoneNumberView.setDelegateAndVC(delegate: self, vc: self)
        
        // User Name View
        self.userNameView = UINib(nibName: UINibConstant.ChangeUserNameView, bundle: .main).instantiate(withOwner: nil, options: nil).first as? ChangeUserNameView
        self.userNameView.frame = self.view.bounds
        self.userNameView.setDelegate(delegate: self)
        
        
        /// CalendarView From Nib
        self.calendarView = UINib(nibName:    UINibConstant.CalendarViewNib, bundle: .main).instantiate(withOwner: nil, options: nil).first as? CalendarView
        self.calendarView.frame = self.view.bounds
        self.calendarView.delegate = self
        
        /// Select Gender View
        self.selectGenderView = UINib(nibName:    UINibConstant.selectGenderNib, bundle: .main).instantiate(withOwner: nil, options: nil).first as? SelectGender
        self.selectGenderView.frame = self.view.bounds
        self.selectGenderView.setDelegate(delegate: self)
        
        
        self.changeAddressView = UINib(nibName:    UINibConstant.ChangeAddressView, bundle: .main).instantiate(withOwner: nil, options: nil).first as? ChangeAddressView
        self.changeAddressView.setDelegate(delegate: self)
        self.changeAddressView.frame = self.view.bounds
    }
    
    private func setupUserProfileData(){
        
        self.mViewModel.authorization = UserDefaultUtils.getProfile(key: CommonKeys.KEY_PROFILE)
    
        if let avatar = self.mViewModel.authorization?.avatar {
           
            self.profileImage.loadImage(imageUrl: avatar, placeHolderName: ImageConstant.noImagePlaceHolder, activiyIndicator: activityIndicator)
           
           
        }else {
            self.profileImage.image = UIImage(named: StringConstant.profilePlaceHolder)
        }
             
        
        
        if let email = self.mViewModel.authorization?.email {
            self.emailLabel.text = email
        }else {
            emailLabel.text = StringConstant.emailNotFound
            self.emailVerificationImage.isHidden = true
        }
       
        if let phone = self.mViewModel.authorization?.phoneNumber {
            self.phoneNoLabel.text = phone
            self.phoneAddButton.isHidden = true
            self.phoneVerifyIcon.isHidden = false
        }else {
            self.phoneAddButton.isHidden = false
            self.phoneVerifyIcon.isHidden = true
        }
        
        if let name = self.mViewModel.authorization?.name{
            self.namelabel.text = name
            
        }
        
        if let dob = self.mViewModel.authorization?.dOb{
            self.dobLabel.text = Utils.dateConverstion(timeStamp:Double(dob) ?? 0.0, format: .FORMAT_HOMESCREEN_TOOLBAR)
        }
        if let gender = self.mViewModel.authorization?.gender{
            self.genderLabel.text = gender
        }
         
        if let address = self.mViewModel.authorization?.addressModObj?.completeAddress{
            self.addressLabel.text = address
        }
        
        
        
    }
    
    private func updateUserDefaults(){
        
        let authData = self.mViewModel.updateUserProfileData()
        UserDefaultUtils.removeValueForKey(key: CommonKeys.KEY_PROFILE)
         UserDefaultUtils.saveProfile(value: authData, key: CommonKeys.KEY_PROFILE)
        
        self.setupUserProfileData()
    }
     
    private func goToOTPVerification(){
        //OTPScreen
        
        let otpVerificationVC = StoryBoardConstant.AuthStoryBoard.instantiateViewController(withIdentifier: StoryBoardConstant.OTPScreenController) as! OtpVerificationViewController
        let authBuilder = AuthorizationBuilder()
        authBuilder.setphoneNumber(self.mViewModel.phoneNumber)
        authBuilder.setOtpType(OtpType.PHONE_NUMBER.rawValue)
        otpVerificationVC.setOtpSCreenData(data: authBuilder)
        otpVerificationVC.setDelegate(delegate: self)
        
        self.navigationController?.pushViewController(otpVerificationVC, animated: true)
    }
    
    
    private func bindingAllReponse(){
        self.bindingUpdateProfileObserver()
        self.bindingUpdateProfileErrorObserver()
        self.bindingUpdateProfileExpiryObserver()
    }
    
    
    private func bindingUpdateProfileObserver(){
        self.profileNetworkViewModel.updateProfileServerResponse.bind{
            if ($0 != nil){
                self.removeActivityIndicator()
                if $0?.response?.status ?? false {
                    guard let authData = $0?.response?.data?.auth else {return }
                    print("AuthData:\(authData.toString())")
                        UserDefaultUtils.saveProfile(value: (authData), key: CommonKeys.KEY_PROFILE)
                        print(UserDefaultUtils.getProfile(key: CommonKeys.KEY_PROFILE)?.toString())
                        self.setupUserProfileData()
                }else {
                    Utils.showErrorAlert(vc: self, message:$0?.response?.message ?? "" )
                }
            }
        }
    }
    
    private func bindingUpdateProfileErrorObserver(){
        self.profileNetworkViewModel.updateProfileErrorResponse.bind {
            if ($0 != nil){
                self.removeActivityIndicator()
                Utils.showAlert(vc: self, message: $0?.response?.message ?? "")
            }
        }
    }
    
    private func bindingUpdateProfileExpiryObserver(){
        self.profileNetworkViewModel.updateProfileExpiryReponse.bind {
            if ($0 != nil){
                self.removeActivityIndicator()
                self.performExpirationInCaseOfBoxBinding()
            }
        }
    }

    
}

//MARK: - Change Phone Number Delegate
extension ProfileUpdateViewController : PhoneNumberViewDelegate {
    func updatePhoneNumber(number: String) {
        self.mViewModel.phoneNumberBool = true
        self.mViewModel.profileImage = false
        self.mViewModel.phoneNumber = number
        self.phoneNumberView.removeFromSuperview()
        self.goToOTPVerification()
        
    }
    
    func closeView() {
        self.phoneNumberView.removeFromSuperview()
    }
    
    
}

//MARK: - Update User Name Delegate
extension ProfileUpdateViewController: UpdateUserNameDelegate{
    func updateName(name: String) {
        self.mViewModel.phoneNumberBool = false
        self.namelabel.text = name
        self.mViewModel.authorization?.name = name
        self.userNameView.removeFromSuperview()
        
        
        self.showActivityIndicator()
        self.mViewModel.phoneNumberBool = false
        self.mViewModel.profileImage = false
        let auth = self.mViewModel.updateUserProfileData()
        self.profileNetworkViewModel.updateProfile(auth: auth)
        
        
    }
    
    func closeUpdateNameView() {
        self.userNameView.removeFromSuperview()
    }
    
    
    
}



//MARK: - Calendar View Delegate Methods
extension ProfileUpdateViewController : CalendarViewDelegate {
    func Ok(date: Date) {
        self.dobLabel.text = Utils.dateConverstion(timeStamp: date.timeIntervalSince1970, format: .FORMAT_HOMESCREEN_TOOLBAR)
        
        let formetter =  DateFormatter()
        formetter.timeZone = TimeZone(abbreviation: "GMT")
        //GMT TIME STAMP FOR SERVER
       
        self.mViewModel.dOBTimeStamp =  Int(formetter.date(from: self.dobLabel.text ?? "")?.timeIntervalSince1970 ?? -1)
        
        self.mViewModel.authorization?.dOb = Int(date.timeIntervalSince1970)
        
        
        self.calendarView.removeFromSuperview()
        
        self.showActivityIndicator()
        self.mViewModel.profileImage = false
        self.mViewModel.phoneNumberBool = false
        print(self.mViewModel.phoneNumberBool)
        let auth = self.mViewModel.updateUserProfileData()
        self.profileNetworkViewModel.updateProfile(auth: auth)
    }
    
    func cancel() {
        self.calendarView.removeFromSuperview()
    }
    
    
}


// MARK: -SelectGenderDelegate
extension ProfileUpdateViewController:SelectGenderDelegate{
    
    func updateBtnPressed(selectedOption: String?) {
        self.mViewModel.phoneNumberBool = false
        self.mViewModel.selectedGender = selectedOption
        
        self.mViewModel.authorization?.gender = selectedOption
  
        if self.mViewModel.selectedGender == SelectGenderEnum.MALE.rawValue{
            self.genderLabel.text = StringConstant.Male
        }else if self.mViewModel.selectedGender == SelectGenderEnum.FEMALE.rawValue{
            self.genderLabel.text = StringConstant.FeMale
        }else if self.mViewModel.selectedGender == SelectGenderEnum.NON_BINARY.rawValue{
            self.genderLabel.text = StringConstant.NonBinary
        }else if self.mViewModel.selectedGender == SelectGenderEnum.NOT_ANSWERED.rawValue{
            self.genderLabel.text = StringConstant.NoAnswer
        }

        self.selectGenderView.removeFromSuperview()
        
        
        self.showActivityIndicator()
        self.mViewModel.phoneNumberBool = false
        self.mViewModel.profileImage = false
        let auth = self.mViewModel.updateUserProfileData()
        self.profileNetworkViewModel.updateProfile(auth: auth)
    }
    
    func closeBtnPressed() {
        self.selectGenderView.removeFromSuperview()
    }
    
    
}

//MARK: - Change Address View Delegate
extension ProfileUpdateViewController: ChangeAddressViewDelegate {

    func closePressed() {
        self.changeAddressView.removeFromSuperview()
    }
    
    
    func updatePressed(address: String, city: String, state: String, country: String, zipCode: String) {
        self.mViewModel.phoneNumberBool = false
        self.addressLabel.text = address
        let addressObjc = Address(completeAddress: address, city: city, state: state, country: country, zipCode: zipCode)
   
        self.mViewModel.authorization?.addressModObj = addressObjc
        
        self.changeAddressView.removeFromSuperview()
        
        self.showActivityIndicator()
        self.mViewModel.phoneNumberBool = false
        self.mViewModel.profileImage = false
        let auth = self.mViewModel.updateUserProfileData()
        self.profileNetworkViewModel.updateProfile(auth: auth)
        
    }
    
    
}

extension ProfileUpdateViewController : OtpVerificationViewControllerDelegate {
    func phoneNumberSucccessResponse() {
        self.phoneNoLabel.text = self.mViewModel.phoneNumber
        self.phoneAddButton.isHidden = true
        self.mViewModel.authorization?.phoneNumber = self.mViewModel.phoneNumber
        self.mViewModel.authorization?.otp = UserDefaults.standard.integer(forKey: "otpText")
        self.showActivityIndicator()
        let auth = self.mViewModel.updateUserProfileData()
        self.profileNetworkViewModel.updateProfile(auth: auth)
    }
}
