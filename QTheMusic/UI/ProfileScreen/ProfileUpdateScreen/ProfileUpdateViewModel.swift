//
//  ProfileUpdateViewModel.swift
//  QtheMusic
//
//  Created by Hammad Hassan on 17/05/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import Foundation
import UIKit

class ProfileUpdateViewModel : BaseViewModel{
    
    var selectedGender:String?
    var profilePic:UIImage?
    var dOBTimeStamp :Int?
    var authorization:Authorization?
    var phoneNumber : String?
    var phoneNumberBool = Bool()
    var profileImage = Bool()
    let builder = AuthorizationBuilder()
    func updateUserProfileData() -> Authorization {
        if phoneNumberBool == true
        {
            builder.setemail(self.authorization?.email)
            builder.setProfileImage(UIImage(named: ""))
            builder.setphoneNumber(self.authorization?.phoneNumber)
            builder.setOtp(self.authorization?.otp)
            builder.setname(self.authorization?.name)
           builder.setdOb(self.authorization?.dOb ?? 0)
           builder.setgender(self.authorization?.gender)
           builder.setjwt(self.authorization?.jwt)
           builder.setaddressModObj(self.authorization?.addressModObj)
        }
        else if profileImage == true
        {
            builder.setProfileImage(profilePic)
            builder.setdOb(Int(""))
        }
        else
        {
            builder.setemail(self.authorization?.email)
            builder.setProfileImage(UIImage(named: ""))
            builder.setname(self.authorization?.name)
            builder.setdOb(self.authorization?.dOb ?? 0)
            builder.setphoneNumber("")
            builder.setOtp(Int(""))
            builder.setgender(self.authorization?.gender)
            builder.setjwt(self.authorization?.jwt)
            builder.setaddressModObj(self.authorization?.addressModObj)
        }
       return builder.build()
        
    }
}
