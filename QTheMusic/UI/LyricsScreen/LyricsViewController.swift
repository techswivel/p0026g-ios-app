//
//  LyricsViewController.swift
//  QtheMusic
//
//  Created by Assad Khan on 04/07/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import UIKit

protocol LyricsViewProtocol{
    func setLyricsText(text:String?)
}


class LyricsViewController: BaseViewController {
    
    @IBOutlet weak var scrollViewCon: UIScrollView!{didSet{
        scrollViewCon.layer.cornerRadius = 8
        scrollViewCon.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
    }}
    @IBOutlet weak var lyricsContainerView: UIView!{
        didSet{
            lyricsContainerView.layer.cornerRadius = 8
            lyricsContainerView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]//maxmaxy is right bottom corner//maxXminY is top right corner//minXmaxY is bottom left corner
        }
    }
    @IBOutlet weak var lyricsTextView: UITextView!
    
    private var mViewModel = LyricsViewModel()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
    }
    
    
    
    @IBAction func lyricsBtnPressed(_ sender: Any) {
        
        self.popBackNavigationWithTransition()
        
        
    }
    
    
    // MARK: -setupView
    private func setupView()
    {
        if let Text = self.mViewModel.lyricsText{
        let range = (Text as NSString).range(of: Text)
        let mutableAttributedString = NSMutableAttributedString.init(string: Text)
        mutableAttributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.white, range: range)
        lyricsTextView.attributedText = mutableAttributedString
        }else{
            let range = (StringConstant.noLyricsFound as NSString).range(of: StringConstant.noLyricsFound)
            let mutableAttributedString = NSMutableAttributedString.init(string: StringConstant.noLyricsFound)
            mutableAttributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.white, range: range)
            lyricsTextView.attributedText = mutableAttributedString
        }
    }
    
};extension LyricsViewController:LyricsViewProtocol{
    func setLyricsText(text: String?) {
        if let Text = text{
            self.mViewModel.lyricsText = Text
        }}
    
}
