//
//  ProfileSignUpViewController.swift
//  QtheMusic
//
//  Created by Assad Khan on 18/03/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialTextControls_OutlinedTextFields
import Firebase
import FirebaseMessaging

protocol ProfileSignUpViewControllerProtocol{
    func setAuthorizationData(auth:Authorization?, socialSite: String?)
}

class ProfileSignUpViewController: BaseViewController {

    @IBOutlet weak var backgroundView: UIView!
    
    @IBOutlet weak var profileImageView: UIImageView!{
        didSet{
            profileImageView.roundCorner(radius: 50)
        }
    }
    
    @IBOutlet weak var addBtn: UIButton!{
        didSet{
            addBtn.roundCorner(radius: 15)
        }
    }
    @IBOutlet weak var profileActivityndicator: UIActivityIndicatorView!
    @IBOutlet weak var nameTextField: MDCOutlinedTextField!{
        didSet{
            nameTextField.placeholder = StringConstant.name
            
            nameTextField.setFloatingLabelColor(UIColor.white, for: .normal)
            nameTextField.setFloatingLabelColor(ColorConstrants.PRIMARY_COLOR, for: .editing)
            nameTextField.setFloatingLabelColor(UIColor.white, for: .disabled)
            
            nameTextField.setNormalLabelColor(UIColor.white, for: .normal)
            nameTextField.setNormalLabelColor(UIColor.white, for: .editing)
            nameTextField.setNormalLabelColor(UIColor.white, for: .disabled)
            
            nameTextField.label.text = StringConstant.name
            
            nameTextField.setTextColor(UIColor.white, for: .editing)
            nameTextField.setTextColor(UIColor.white, for: .disabled)
            nameTextField.setTextColor(UIColor.white, for: .normal)
            
            
            nameTextField.setLeadingAssistiveLabelColor(UIColor.red, for: .normal)
            nameTextField.setLeadingAssistiveLabelColor(UIColor.red, for: .disabled)
            nameTextField.setLeadingAssistiveLabelColor(UIColor.red, for: .editing)
            
            nameTextField.setOutlineColor(UIColor.white, for: .normal)
            nameTextField.setOutlineColor(ColorConstrants.PRIMARY_COLOR, for: .editing)
        }
    }
    
    @IBOutlet weak var dObTextField: MDCOutlinedTextField!{
        didSet{
            dObTextField.placeholder = StringConstant.dob
            
            dObTextField.setFloatingLabelColor(UIColor.white, for: .normal)
            dObTextField.setFloatingLabelColor(ColorConstrants.PRIMARY_COLOR, for: .editing)
            dObTextField.setFloatingLabelColor(UIColor.white, for: .disabled)
            
            dObTextField.setNormalLabelColor(UIColor.white, for: .normal)
            dObTextField.setNormalLabelColor(UIColor.white, for: .editing)
            dObTextField.setNormalLabelColor(UIColor.white, for: .disabled)
            
            dObTextField.label.text = StringConstant.dob
            
            dObTextField.setTextColor(UIColor.white, for: .editing)
            dObTextField.setTextColor(UIColor.white, for: .disabled)
            dObTextField.setTextColor(UIColor.white, for: .normal)
            
            
            dObTextField.setLeadingAssistiveLabelColor(UIColor.red, for: .normal)
            dObTextField.setLeadingAssistiveLabelColor(UIColor.red, for: .disabled)
            dObTextField.setLeadingAssistiveLabelColor(UIColor.red, for: .editing)
            
            dObTextField.setOutlineColor(UIColor.white, for: .normal)
            dObTextField.setOutlineColor(ColorConstrants.PRIMARY_COLOR, for: .editing)
        }
    }
    
    @IBOutlet weak var genderTextField: MDCOutlinedTextField!{
        didSet{
            genderTextField.placeholder = StringConstant.gender
            
            genderTextField.setFloatingLabelColor(UIColor.white, for: .normal)
            genderTextField.setFloatingLabelColor(ColorConstrants.PRIMARY_COLOR, for: .editing)
            genderTextField.setFloatingLabelColor(UIColor.white, for: .disabled)
            
            genderTextField.setNormalLabelColor(UIColor.white, for: .normal)
            genderTextField.setNormalLabelColor(UIColor.white, for: .editing)
            genderTextField.setNormalLabelColor(UIColor.white, for: .disabled)
            
            genderTextField.label.text = StringConstant.gender
            
            genderTextField.setTextColor(UIColor.white, for: .editing)
            genderTextField.setTextColor(UIColor.white, for: .disabled)
            genderTextField.setTextColor(UIColor.white, for: .normal)
            
            
            genderTextField.setLeadingAssistiveLabelColor(UIColor.red, for: .normal)
            genderTextField.setLeadingAssistiveLabelColor(UIColor.red, for: .disabled)
            genderTextField.setLeadingAssistiveLabelColor(UIColor.red, for: .editing)
            
            genderTextField.setOutlineColor(UIColor.white, for: .normal)
            genderTextField.setOutlineColor(ColorConstrants.PRIMARY_COLOR, for: .editing)
        }
    }
    
    @IBOutlet weak var addressTextField: MDCOutlinedTextField!{
        didSet{
            addressTextField.placeholder = StringConstant.address
            
            addressTextField.setFloatingLabelColor(UIColor.white, for: .normal)
            addressTextField.setFloatingLabelColor(ColorConstrants.PRIMARY_COLOR, for: .editing)
            addressTextField.setFloatingLabelColor(UIColor.white, for: .disabled)
            
            addressTextField.setNormalLabelColor(UIColor.white, for: .normal)
            addressTextField.setNormalLabelColor(UIColor.white, for: .editing)
            addressTextField.setNormalLabelColor(UIColor.white, for: .disabled)
            
            addressTextField.label.text = StringConstant.address
            
            addressTextField.setTextColor(UIColor.white, for: .editing)
            addressTextField.setTextColor(UIColor.white, for: .disabled)
            addressTextField.setTextColor(UIColor.white, for: .normal)
            
            
            addressTextField.setLeadingAssistiveLabelColor(UIColor.red, for: .normal)
            addressTextField.setLeadingAssistiveLabelColor(UIColor.red, for: .disabled)
            addressTextField.setLeadingAssistiveLabelColor(UIColor.red, for: .editing)
            
            addressTextField.setOutlineColor(UIColor.white, for: .normal)
            addressTextField.setOutlineColor(ColorConstrants.PRIMARY_COLOR, for: .editing)
        }
    }
    
    
    @IBOutlet weak var cityTextField: MDCOutlinedTextField!{
        didSet{
            cityTextField.placeholder = StringConstant.city
            
            cityTextField.setFloatingLabelColor(UIColor.white, for: .normal)
            cityTextField.setFloatingLabelColor(ColorConstrants.PRIMARY_COLOR, for: .editing)
            cityTextField.setFloatingLabelColor(UIColor.white, for: .disabled)
            
            cityTextField.setNormalLabelColor(UIColor.white, for: .normal)
            cityTextField.setNormalLabelColor(UIColor.white, for: .editing)
            cityTextField.setNormalLabelColor(UIColor.white, for: .disabled)
            
            cityTextField.label.text = StringConstant.city
            
            cityTextField.setTextColor(UIColor.white, for: .editing)
            cityTextField.setTextColor(UIColor.white, for: .disabled)
            cityTextField.setTextColor(UIColor.white, for: .normal)
            
            
            cityTextField.setLeadingAssistiveLabelColor(UIColor.red, for: .normal)
            cityTextField.setLeadingAssistiveLabelColor(UIColor.red, for: .disabled)
            cityTextField.setLeadingAssistiveLabelColor(UIColor.red, for: .editing)
            
            cityTextField.setOutlineColor(UIColor.white, for: .normal)
            cityTextField.setOutlineColor(ColorConstrants.PRIMARY_COLOR, for: .editing)
        }
    }
    
    
    @IBOutlet weak var stateTextField: MDCOutlinedTextField!{
        didSet{
            stateTextField.placeholder = StringConstant.state
            
            stateTextField.setFloatingLabelColor(UIColor.white, for: .normal)
            stateTextField.setFloatingLabelColor(ColorConstrants.PRIMARY_COLOR, for: .editing)
            stateTextField.setFloatingLabelColor(UIColor.white, for: .disabled)
            
            stateTextField.setNormalLabelColor(UIColor.white, for: .normal)
            stateTextField.setNormalLabelColor(UIColor.white, for: .editing)
            stateTextField.setNormalLabelColor(UIColor.white, for: .disabled)
            
            stateTextField.label.text = StringConstant.state
            
            stateTextField.setTextColor(UIColor.white, for: .editing)
            stateTextField.setTextColor(UIColor.white, for: .disabled)
            stateTextField.setTextColor(UIColor.white, for: .normal)
            
            
            stateTextField.setLeadingAssistiveLabelColor(UIColor.red, for: .normal)
            stateTextField.setLeadingAssistiveLabelColor(UIColor.red, for: .disabled)
            stateTextField.setLeadingAssistiveLabelColor(UIColor.red, for: .editing)
            
            stateTextField.setOutlineColor(UIColor.white, for: .normal)
            stateTextField.setOutlineColor(ColorConstrants.PRIMARY_COLOR, for: .editing)
        }
    }
    
    @IBOutlet weak var countryTextField: MDCOutlinedTextField!{
        didSet{
            countryTextField.placeholder = StringConstant.country
            
            countryTextField.setFloatingLabelColor(UIColor.white, for: .normal)
            countryTextField.setFloatingLabelColor(ColorConstrants.PRIMARY_COLOR, for: .editing)
            countryTextField.setFloatingLabelColor(UIColor.white, for: .disabled)
            
            countryTextField.setNormalLabelColor(UIColor.white, for: .normal)
            countryTextField.setNormalLabelColor(UIColor.white, for: .editing)
            countryTextField.setNormalLabelColor(UIColor.white, for: .disabled)
            
            countryTextField.label.text = StringConstant.country
            
            countryTextField.setTextColor(UIColor.white, for: .editing)
            countryTextField.setTextColor(UIColor.white, for: .disabled)
            countryTextField.setTextColor(UIColor.white, for: .normal)
            
            
            countryTextField.setLeadingAssistiveLabelColor(UIColor.red, for: .normal)
            countryTextField.setLeadingAssistiveLabelColor(UIColor.red, for: .disabled)
            countryTextField.setLeadingAssistiveLabelColor(UIColor.red, for: .editing)
            
            countryTextField.setOutlineColor(UIColor.white, for: .normal)
            countryTextField.setOutlineColor(ColorConstrants.PRIMARY_COLOR, for: .editing)
        }
    }
    
    
    @IBOutlet weak var zipCodeTextField: MDCOutlinedTextField!{
        didSet{
            zipCodeTextField.placeholder = StringConstant.zipCode
            
            zipCodeTextField.setFloatingLabelColor(UIColor.white, for: .normal)
            zipCodeTextField.setFloatingLabelColor(ColorConstrants.PRIMARY_COLOR, for: .editing)
            zipCodeTextField.setFloatingLabelColor(UIColor.white, for: .disabled)
            
            zipCodeTextField.setNormalLabelColor(UIColor.white, for: .normal)
            zipCodeTextField.setNormalLabelColor(UIColor.white, for: .editing)
            zipCodeTextField.setNormalLabelColor(UIColor.white, for: .disabled)
            
            zipCodeTextField.label.text = StringConstant.zipCode
            
            zipCodeTextField.setTextColor(UIColor.white, for: .editing)
            zipCodeTextField.setTextColor(UIColor.white, for: .disabled)
            zipCodeTextField.setTextColor(UIColor.white, for: .normal)
            
            
            zipCodeTextField.setLeadingAssistiveLabelColor(UIColor.red, for: .normal)
            zipCodeTextField.setLeadingAssistiveLabelColor(UIColor.red, for: .disabled)
            zipCodeTextField.setLeadingAssistiveLabelColor(UIColor.red, for: .editing)
            
            zipCodeTextField.setOutlineColor(UIColor.white, for: .normal)
            zipCodeTextField.setOutlineColor(ColorConstrants.PRIMARY_COLOR, for: .editing)
        }
    }
    
    @IBOutlet weak var letsGoBtn: UIButton!{
        didSet{
            letsGoBtn.roundCorner(radius: 12)
        }
    }
    private var calendarView: CalendarView!
    private var selectGenderView : SelectGender!
    private var WhyWeAreAskingView : WhyWeAreAsking!
    private var mViewModel = ProfileSignUpViewModel()
    private var mAuthViewModel = AuthViewModel()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpView()
        // Do any additional setup after loading the view.
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        backgroundView.addGradientBackground()
    }
    
    //MARK: - Tap Gesture on Calendar View
    @objc func handleTapOnCalendar(_ sender: UITapGestureRecognizer? = nil) {
        self.calendarView.removeFromSuperview()
    }
    @IBAction func letgoBtnPressed(_ sender: Any) {
        let builder = AuthorizationBuilder()
        
        nameTextField.resignFirstResponder()
        dObTextField.resignFirstResponder()
        genderTextField.resignFirstResponder()
        addressTextField.resignFirstResponder()
        cityTextField.resignFirstResponder()
        stateTextField.resignFirstResponder()
        countryTextField.resignFirstResponder()
        zipCodeTextField.resignFirstResponder()
        
        
        if nameTextField.text?.count ?? 0 > 0 {
            
            builder.setname(nameTextField.text)
            nameTextField.leadingAssistiveLabel.text = ""
        }else{
            nameTextField.leadingAssistiveLabel.text = StringConstant.nameIsRequired
        }
        if dObTextField.text?.count ?? 0 > 0 {
            
            builder.setdOb(Int(self.mViewModel.dOBTimeStamp ?? 0))
            dObTextField.leadingAssistiveLabel.text = ""
        }else{
            dObTextField.leadingAssistiveLabel.text = StringConstant.dobIsRequired
        }
        
        
        guard nameTextField.text?.count ?? 0 > 0 else{return}
        guard dObTextField.text?.count ?? 0 > 0 else{return}
        
        
        if self.mViewModel.profilePic != nil {
            builder.setProfileImage(self.mViewModel.profilePic)
        }
        
        
        if genderTextField.text != nil{
            builder.setgender(self.mViewModel.selectedGender)
        }
        
        if addressTextField.text != nil {
            builder.setAddress(addressTextField.text)
        }
        
        if cityTextField.text != nil {
            builder.setcity(cityTextField.text)
        }
        
        if stateTextField.text != nil
        {
            builder.setstate(stateTextField.text)
        }
        
        if countryTextField.text != nil {
            builder.setcountry(countryTextField.text)
        }
        
        if zipCodeTextField.text != nil {
            builder.setzipCode(zipCodeTextField.text)
        }
        
        if self.mViewModel.authorization?.socialSite != nil {
            builder.setsocialId(self.mViewModel.authorization?.socialId)
        }
        
        if let token = Messaging.messaging().fcmToken {
            self.showActivityIndicator()
            if self.mViewModel.authorization?.socialId != nil{
                builder.setsocialId(mViewModel.authorization?.socialId)
                builder.setSocialSite(mViewModel.authorization?.socialSite)
            }
            builder.setFcmToken(token)
            builder.setemail(mViewModel.authorization?.email)
            builder.setPassword(mViewModel.authorization?.password)
            self.mAuthViewModel.singUp(builder: builder)
        }
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        self.popBackController()
    }
    
    @IBAction func addBtnPressed(_ sender: Any) {
        //Attachment handler is used to specify number of photos which can be selected, showing user camera and library options and managing image after user select's one.
        AttachmentHandler.shared.imageSelectionLimit = 1
        AttachmentHandler.shared.showAttachmentActionSheet(vc: self, type: AttachmentHandler.AttachmentFileType.image)
        AttachmentHandler.shared.imagePickedBlock = {
            image in
            self.profileImageView.image = image.first
            self.mViewModel.profilePic = image.first
        }
        
    }
  
    @IBAction func addCalenderBtnPresed(_ sender: Any) {
        dObTextField.leadingAssistiveLabel.text = ""
        self.view.endEditing(true)
        
        self.calendarView.frame = self.view.bounds
        self.calendarView.alpha = 0.0
        self.view.addSubview(calendarView)
        UIView.animate(withDuration: 0.25) { () -> Void in
            self.calendarView.alpha = 1.0
        }
        
    }
    
    @IBAction func selectGenderBtnPressed(_ sender: Any) {
        
        genderTextField.leadingAssistiveLabel.text = ""
        self.selectGenderView.alpha = 0.0
        selectGenderView.setDelegate(delegate: self)
        
        
        if self.mViewModel.selectedGender == SelectGenderEnum.MALE.rawValue{
            selectGenderView.setMaleCheckMark()
        }else if self.mViewModel.selectedGender == SelectGenderEnum.FEMALE.rawValue{
            selectGenderView.setFeMaleCheckMark()
        }else if self.mViewModel.selectedGender == SelectGenderEnum.NON_BINARY.rawValue{
            selectGenderView.setNonBinaryCheckMark()
        }else if self.mViewModel.selectedGender == SelectGenderEnum.NOT_ANSWERED.rawValue{
            selectGenderView.setNoAnswerCheckMark()
        }else{
            selectGenderView.setMaleCheckMark()
        }
        
        self.view.endEditing(true)
        selectGenderView.alpha = 0
        self.view.addSubview(selectGenderView)
        UIView.animate(withDuration: 0.25) { () -> Void in
            self.selectGenderView.alpha = 1.0
        }
    }
    @IBAction func whyWeAreAskingBtnPressed(_ sender: Any) {
       
        WhyWeAreAskingView.setDelegate(delegate: self)
        self.view.endEditing(true)
        WhyWeAreAskingView.alpha = 0
        self.view.addSubview(self.WhyWeAreAskingView)
        UIView.animate(withDuration: 0.25) { () -> Void in
            self.WhyWeAreAskingView.alpha = 1.0
        }
    }
    
    
    
    private func setUpView(){
        self.registerNib()
        
        
        self.bindingServerResponse()
        self.bindingServerErrorResponse()
        self.bindingServerCompletionResponse()
        
        nameTextField.delegate = self
        
        if self.mViewModel.authorization != nil {
            if let name = self.mViewModel.authorization?.name{
                nameTextField.text = name
            }
            
            if let dob = self.mViewModel.authorization?.dOb{
                dObTextField.text = Utils.dateConverstion(timeStamp: Double(dob) ?? 0.0, format: .FORMAT_HOMESCREEN_TOOLBAR)
                
                self.mViewModel.dOBTimeStamp = Int(dob)
            }
            
            if let gender = self.mViewModel.authorization?.gender {
                genderTextField.text = gender
               
                // MARK: - In future c response from server may be enum is different

                self.mViewModel.selectedGender = gender
            }
            
            if let socialPic = self.mViewModel.authorization?.avatar {
                self.profileImageView.loadImage(imageUrl: socialPic, placeHolderName: ImageConstant.PROFILE_PLACE_HOLDER, activiyIndicator: self.profileActivityndicator)
                self.mViewModel.profilePic = self.profileImageView.image
                
            }
            
        }
        
       
        
    }
    private func registerNib(){
        /// CalendarView From Nib
        self.calendarView = UINib(nibName:    UINibConstant.CalendarViewNib, bundle: .main).instantiate(withOwner: nil, options: nil).first as? CalendarView
        
        let calendarTap = UITapGestureRecognizer(target: self, action: #selector(handleTapOnCalendar(_:)))
        calendarView.delegate = self
        self.calendarView.blurView.addGestureRecognizer(calendarTap)
        
        self.selectGenderView = UINib(nibName:    UINibConstant.selectGenderNib, bundle: .main).instantiate(withOwner: nil, options: nil).first as? SelectGender
        self.selectGenderView.frame = self.view.bounds
        
        self.WhyWeAreAskingView = UINib(nibName:    UINibConstant.WhyWeAreAskingNib, bundle: .main).instantiate(withOwner: nil, options: nil).first as? WhyWeAreAsking
        self.WhyWeAreAskingView.frame = self.view.bounds
        
    }
}

extension ProfileSignUpViewController{
    //MARK: - Binding Server Response
    private func bindingServerResponse() {
        self.mAuthViewModel.serverResponseForSignUp.bind {
            if ($0 != nil) {
                print("Status check \($0?.response?.status)")
                if $0?.response?.status ?? false {

                    guard let authData = $0?.response?.data?.auth else {return }
                    UserDefaultUtils.setString(value: $0?.response?.data?.auth?.jwt ?? "", key: "jwt")
                    UserDefaultUtils.saveProfile(value: (authData), key: CommonKeys.KEY_PROFILE)
                    let vc = UIStoryboard.init(name: StoryBoardConstant.storyBoardAuth, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.yourInterestScreen) as? InterestSelectionViewController
                    vc!.selectedState = false
                    self.navigationController?.pushViewController(vc!, animated: true)
                    
                    self.removeActivityIndicator()
                    }
                else if $0?.response?.status == false {
                    print("Response for setup Profile is false")
                    self.removeActivityIndicator()

                }
            }
        }
    }

    //MARK: - Binding Error Response
    private func bindingServerErrorResponse() {
        self.mAuthViewModel.errorResponseForSignUp.bind {
            if ($0 != nil) {
                Utils.showErrorAlert(vc: self, message: $0?.response?.message)
            }

            self.removeActivityIndicator()
        }


    }

    //MARK: - Binding Complete Response
    private func bindingServerCompletionResponse(){
        self.mAuthViewModel.completion.bind {
            if ($0 != nil) {
                //firebase auth takes time so on success of firebase auth only remove activity indicator,so app doesn't freazes
            }
        }
    }
}

//MARK: - Calendar View Delegate Methods
extension ProfileSignUpViewController : CalendarViewDelegate {
    func Ok(date: Date) {
        self.dObTextField.text = Utils.dateConverstion(timeStamp: date.timeIntervalSince1970, format: .FORMAT_HOMESCREEN_TOOLBAR)
        
        let formetter =  DateFormatter()
        formetter.timeZone = TimeZone(abbreviation: "GMT")
        formetter.dateFormat = "d MMMM YYYY"
        
        //GMT TIME STAMP FOR SERVER
        self.mViewModel.dOBTimeStamp =  Int(formetter.date(from: self.dObTextField.text ?? "")?.timeIntervalSince1970 ?? 0)
        dObTextField.leadingAssistiveLabel.text = ""
        self.calendarView.removeFromSuperview()
    }
    
    func cancel() {
        self.calendarView.removeFromSuperview()
    }
    
    
}
// MARK: -SelectGenderDelegate
extension ProfileSignUpViewController:SelectGenderDelegate{
    func updateBtnPressed(selectedOption: String?) {
        self.mViewModel.selectedGender = selectedOption
        
        if self.mViewModel.selectedGender == SelectGenderEnum.MALE.rawValue{
            self.genderTextField.text = StringConstant.Male
        }else if self.mViewModel.selectedGender == SelectGenderEnum.FEMALE.rawValue{
            self.genderTextField.text = StringConstant.FeMale
        }else if self.mViewModel.selectedGender == SelectGenderEnum.NON_BINARY.rawValue{
            self.genderTextField.text = StringConstant.NonBinary
        }else if self.mViewModel.selectedGender == SelectGenderEnum.NOT_ANSWERED.rawValue{
            self.genderTextField.text = StringConstant.NoAnswer
        }
        genderTextField.leadingAssistiveLabel.text = ""
        
        self.selectGenderView.removeFromSuperview()
    }
    
    func closeBtnPressed() {
        self.selectGenderView.removeFromSuperview()
    }
    
    
}
// MARK: -WhyWeAreAskingDelegate

extension ProfileSignUpViewController:WhyWeAreAskingDelegate{
    func closeWhyWeAskingView() {
        self.WhyWeAreAskingView.removeFromSuperview()
    }
    
    
}
extension ProfileSignUpViewController:ProfileSignUpViewControllerProtocol{
    func setAuthorizationData(auth: Authorization?, socialSite: String?) {
        self.mViewModel.authorization = auth
    }
}
//MARK: - UITEXTFIELD Delagate Methods.
extension ProfileSignUpViewController : UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.clearsOnInsertion = false
        textField.clearsOnBeginEditing = false
        switch textField {
        case nameTextField:
            nameTextField.leadingAssistiveLabel.text = ""
            
        default:
            print("no field found")
        }
        return true
    }}

