//
//  NoInternetViewController.swift
//  QtheMusic
//
//  Created by Assad Khan on 01/04/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import UIKit
import Lottie


class NoInternetViewController: BaseViewController {
    @IBOutlet weak var backgrroundView: UIView!
    
    @IBOutlet weak var lottieAnimationView: AnimationView!
    
    @IBOutlet weak var goToDownloadBtn: UIButton!{
        didSet{
            goToDownloadBtn.roundCorner(radius: 15)
        }
    }
    
    private var mViewModel = NoInternetViewModel()



    override func viewDidLoad() {
        super.viewDidLoad()

        lottieAnimationView.loopMode = .loop
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        backgrroundView.addGradientBackground()
    }

    override func viewWillAppear(_ animated: Bool) {
        lottieAnimationView.play()
    }
    override func viewWillDisappear(_ animated: Bool) {
        lottieAnimationView.stop()
    }

}
