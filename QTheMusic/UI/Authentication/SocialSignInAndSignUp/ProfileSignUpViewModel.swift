//
//  ProfileSignUpViewModel.swift
//  QtheMusic
//
//  Created by Assad Khan on 18/03/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import Foundation
import UIKit

class ProfileSignUpViewModel :BaseViewModel{
    var selectedGender:String?
    var profilePic:UIImage?
    var dOBTimeStamp :Int?
    var authorization:Authorization?
}
