//
//  SocialSignInandSignUpViewController.swift
//  eloumo
//
//  Created by macbook on 27/09/2021.
//  Copyright © 2021 TechSwivel. All rights reserved.
//

import UIKit
import Firebase
import FirebaseMessaging
import FBSDKLoginKit
import GoogleSignIn
import AuthenticationServices
import AppTrackingTransparency
import FirebaseAuth

protocol SocialSignInandSignUpViewProtocol{
    func setDelegate(delegate:SocialSignInandSignUpViewDelegate)
}
protocol SocialSignInandSignUpViewDelegate{
    func socialUserDontExists(auth:Authorization?, socialSite:String?)
}


//this view controller has social signin and signup methods for(facebook,google,apple) and is inhertited by view controllers having social login options
class SocialSignInandSignUpViewController: BaseViewController {
    
    
    private let loginManager = LoginManager()
    private let mAuthViewModel = AuthViewModel()
    
    private var delegate : SocialSignInandSignUpViewDelegate?
    private var socialSite:String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let appleData = KeyChainWrapperUtils.shared.getAppleUserProfiles()
        print("appleData:\(appleData)")
        //Setting up Google Presentation View
        GIDSignIn.sharedInstance().presentingViewController = self
        //Setting up Apple keychain data
        
        /// Binding Server Response
        self.bindingServerResponse()
        self.bindingServerErrorResponse()
        self.bindingServerCompletionResponse()
    }
    internal func facebookSignIn(){
        
        /*This ATTrackingManger status is applied due to Newly Addition of App transprency Request where user's can allow aur restricted any thirdparties to collect data. Due to which if user do not allow to track then App was not able to communicate with Facebook Server and in result it was causing app to crash */
        /// So here we get status Of tracking if user allow tracking then he can login otherwise showing alert to user to allow tracking to use feature of social login with Facebook.
        /// So how we handled the app crash
        if #available(iOS 14, *){
            ATTrackingManager.requestTrackingAuthorization { status in
                switch status {
                case .authorized:
                    // Tracking authorization dialog was shown
                    // and we are authorized
                    print("Authorized")
                    
                    
                    
                    if AccessToken.current != nil {
                        // Access token available -- user already logged in
                        self.loginManager.logOut()
                        self.startFacebookLoginFlow()
                        
                        
                    } else {
                        self.startFacebookLoginFlow()
                        
                    }
                    
                    
                    
                case .denied:
                    
                    // shown and permission is denied
                    print("Denied")
                    //need to take it to background thread as this att method runs on background thread
                    DispatchQueue.main.async {
                        self.displayTrackingAccessAlert()
                    }
                    
                    
                case .notDetermined:
                    // Tracking authorization dialog has not been shown
                    print("Not Determined")
                case .restricted:
                    DispatchQueue.main.async {
                        self.displayTrackingAccessAlert()
                    }
                @unknown default:
                    print("Unknown")
                }
                
                
                
            }
            
            
        }else {
            
            // early versions
            if AccessToken.current != nil {
                // Access token available -- user already logged in
                loginManager.logOut()
                startFacebookLoginFlow()
                
                
            } else {
                startFacebookLoginFlow()
                
            }
            
        }
        
    }
    private func startFacebookLoginFlow(){
        self.socialSite = socialSiteEnum.FACEBOOK.rawValue
        loginManager.logIn(
            permissions: ["public_profile", "email"],
            from: self)
        {
            [weak self]
            (result,error) in
            
            // Check for error
            guard error == nil else {
                // Error occurred
                //we can forcely unwrap self view controller because we are sure its inherited by other view controller
                Utils.showAlert(vc: self!, message: error?.localizedDescription ?? StringConstant.failedToLoginWithFacebook)
                
                print(error!.localizedDescription)
                return
            }
            
            // Check for cancel
            guard let result = result, !result.isCancelled else {
                
                print("User cancelled login")
                
                return
            }
            
            Profile.loadCurrentProfile { (profile, error) in
                self?.showActivityIndicator()
                if let token = Messaging.messaging().fcmToken {
                    print("\(token) fcm token")
                    
                    let builder = AuthorizationBuilder()
                    builder.setAccessToken(AccessToken.current?.tokenString ?? "")
                    builder.setSocialSite(socialSiteEnum.FACEBOOK.rawValue)
                    builder.setFcmToken(token)
                    builder.setloginType(LoginType.SOCIAL.rawValue)
                    self?.mAuthViewModel.login(builder: builder)
                    
                    
                }else{
                    print("Error:\(error?.localizedDescription)")
                    //we can forcely unwrap self view controller because we are sure its inherited by other view controller
                    Utils.showAlert(vc: self!, message: StringConstant.failedToGetFacebookAccess)
                    
                    self?.removeActivityIndicator()
                    
                    
                }
            }
        }
    }
    
    
    internal func googleSignIn() {
        self.socialSite = socialSiteEnum.GOOGLE.rawValue
        GIDSignIn.sharedInstance().signOut()
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().signIn()
    }
    
    internal func appleSignIn(){
        self.socialSite = socialSiteEnum.APPLE.rawValue
        if #available(iOS 13.0, *) {
            let appleIDProvider = ASAuthorizationAppleIDProvider()
            let request = appleIDProvider.createRequest()
            request.requestedScopes = [.fullName, .email]
            let authorizationController = ASAuthorizationController(authorizationRequests: [request])
            authorizationController.delegate = self
            authorizationController.performRequests()
        } else {
            Utils.showAlert(vc: self, message: StringConstant.appleSignupIsnotAvailable)
            // Fallback on earlier versions
        }
        
    }
    //MARK: - SignIn with Apple if user alreadysaved in KeyChain
    internal func SignInWithAppleIfIdAlreadySaved(userProfile:AppleUserProfile?){
        
        self.showActivityIndicator()
        if let token = Messaging.messaging().fcmToken {
            
          
            
            if let userIdentifier = userProfile?.userIdentifier {
                
                let builder = AuthorizationBuilder()
                builder.setAccessToken(userIdentifier)
                builder.setFcmToken(token)
                builder.setSocialSite( socialSiteEnum.APPLE.rawValue)
                builder.setname("\(userProfile?.firstName) \(userProfile?.lastName)")
                builder.setemail(userProfile?.email)
                builder.setloginType(LoginType.SOCIAL.rawValue)
                self.mAuthViewModel.login(builder: builder)
            }
            
            
        }else{
            self.removeActivityIndicator()
            Utils.showAlert(vc: self, message: StringConstant.failedToGetFirebaseToken)
            
        }
    }
    //MARK: - Tracking Access Alert
    internal func  displayTrackingAccessAlert() {
        
        Utils.showAlert(vc: self,title: StringConstant.trackingAccessIsRequired ,message: StringConstant.trackingAccessMessage,titleYesButton:StringConstant.settings  ,titleNoButton:StringConstant.cancel,onYesPress: {
            
            // Open the Settings app
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
            
            
        }, onNoPress: {
            
            
        })
        
    }
}
extension SocialSignInandSignUpViewController{
    //MARK: - Binding Server Response
    private func bindingServerResponse() {
        self.mAuthViewModel.serverResponseForLogin.bind {
            if ($0 != nil) {
                print("Status check \($0?.response?.status)")
                if $0?.response?.status ?? false {

                    if $0?.response?.data?.auth != nil {
                        let auth :Authorization? = $0?.response?.data?.auth
                        guard let authData = $0?.response?.data?.auth else {return }
                       
                        UserDefaultUtils.saveProfile(value: (authData), key: CommonKeys.KEY_PROFILE)
    
                        if authData.isInterestsSet ?? false{
                            
                            self.goToMainStoryboard()
                        }else {
                            let vc = UIStoryboard.init(name: StoryBoardConstant.storyBoardAuth, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.yourInterestScreen) as? InterestSelectionViewController
                            self.navigationController?.pushViewController(vc!, animated: true)
                            
                        }
                        

                    }else{
                        Utils.showAlert(vc: self, message: StringConstant.couldNotGetDataFromServer)
                        self.removeActivityIndicator()
                    }


                }else if $0?.response?.status == false {
                    print("Response for SocialLogin is false")
                    self.delegate?.socialUserDontExists(auth: $0?.response?.data?.auth, socialSite: self.socialSite)
                    self.removeActivityIndicator()

                }
            }
        }
    }

    //MARK: - Binding Error Response
    private func bindingServerErrorResponse() {
        self.mAuthViewModel.ErrorResponseForLogin.bind {
            if ($0 != nil) {
                Utils.showErrorAlert(vc: self, message: $0?.response?.message)
            }

            self.removeActivityIndicator()
        }


    }

    //MARK: - Binding Complete Response
    private func bindingServerCompletionResponse(){
        self.mAuthViewModel.completion.bind {
            if ($0 != nil) {
                //firebase auth takes time so on success of firebase auth only remove activity indicator,so app doesn't freazes

            }
        }

    }
}
//MARK: - Google SignIn Delegates
extension SocialSignInandSignUpViewController:GIDSignInDelegate{
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        if (error == nil) {
            self.showActivityIndicator()
            if let token = Messaging.messaging().fcmToken {
                
                let builder = AuthorizationBuilder()
                builder.setAccessToken(user.authentication.accessToken)
                builder.setSocialSite(socialSiteEnum.GOOGLE.rawValue)
                builder.setFcmToken(token)
                builder.setloginType(LoginType.SOCIAL.rawValue)
                self.mAuthViewModel.login(builder: builder)
                
            }else{
                self.removeActivityIndicator()

                Utils.showAlert(vc: self, message: StringConstant.failedToGetGoogleAccess)

            }
        
    }
        else {
            self.removeActivityIndicator()
            
            Utils.showAlert(vc: self, message: error.localizedDescription)
            
            
        }
        
        
        
    }
    
}
//Setting up Apple Authentication delegates
@available(iOS 13.0, *)
extension SocialSignInandSignUpViewController:ASAuthorizationControllerDelegate,ASAuthorizationControllerPresentationContextProviding{
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as?  ASAuthorizationAppleIDCredential {
            let userIndentifier = appleIDCredential.user
            print("Response:\(appleIDCredential)")
            print("UserIdentifier:\(userIndentifier)")
            
            let firstName = appleIDCredential.fullName?.givenName
            let lastName = appleIDCredential.fullName?.familyName
            print("fullName:\(String(describing: firstName))")
            print("lastName:\(String(describing: lastName))")
            let email = appleIDCredential.email
            print("email:\(String(describing: email))")
            
            if let authorizationCode = appleIDCredential.authorizationCode{
                let string = String(data: authorizationCode, encoding: .utf8)
                print("auth code :\(String(describing:string))")
            }
            
            if let idToken = appleIDCredential.identityToken{
                let string = String(data: idToken, encoding: .utf8)
                print("idToken :\(String(describing:string))")
            }
            
            
            /// if these variables are not null then it means user is signin in for first time.
            if firstName != nil && lastName != nil && email != nil {
                print("Apple id data is not saved before")
                let appleKeyChainUser = AppleUserProfile(firstName: firstName, lastName: lastName, email: email, userIdentifier: userIndentifier)
                /// we are adding data into keychain but in keychain we are cross checking for existing user.
                KeyChainWrapperUtils.shared.addAppleUserProfileToList(userProfile: appleKeyChainUser)
                
                
                self.showActivityIndicator()
                if let token = Messaging.messaging().fcmToken {
                    
                    let builder = AuthorizationBuilder()
                    builder.setAccessToken(userIndentifier)
                    builder.setFcmToken(token)
                    builder.setSocialSite(socialSiteEnum.APPLE.rawValue)
                    builder.setname("\(firstName) \(lastName)")
                    builder.setemail(email)
                    builder.setloginType(LoginType.SOCIAL.rawValue)
                    self.mAuthViewModel.login(builder: builder)
                    
                }else{
                    self.removeActivityIndicator()
                    Utils.showAlert(vc: self, message: StringConstant.failedToGetFirebaseToken)
                    
                    
                }
                
                
            }
            /// When These variables are null then its mean device is already signIn with Apple and that id is stored in keychain
            else {
                
                print("apple id data is already stored.")
                
                let appleData = KeyChainWrapperUtils.shared.getAppleUserProfiles()
                if let user = appleData {
                    print(user.count,"User count")
                    for i in 0..<user.count {
                        if userIndentifier == user[i].userIdentifier {
                            self.SignInWithAppleIfIdAlreadySaved(userProfile: user[i])
                            print("Already saved")
                            continue
                            
                        }
                    }
                }
                
                
            }
            
            
            
        }
        
        
    }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        // Handle error.
        print("AppleSignInError:\(error.localizedDescription)")
        Utils.showAlert(vc: self, message: StringConstant.operationCAnceledSuccessfully)
    }
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        
        // we can forcely unwrap as it can not be null,their will always be a window displayed unless app is not terminated
        self.view.window!
    }
}
extension SocialSignInandSignUpViewController:SocialSignInandSignUpViewProtocol{
    func setDelegate(delegate: SocialSignInandSignUpViewDelegate) {
        self.delegate = delegate
    }
    
    
}
