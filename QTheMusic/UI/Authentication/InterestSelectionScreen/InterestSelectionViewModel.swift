//
//  InterestSelectionViewModel.swift
//  QtheMusic
//
//  Created by Assad Khan on 29/03/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import Foundation


class InterestSelectionViewModel:BaseViewModel{
    
    var categoryArray = [Category]()
    var selectedArray = [Category]()
    var retrieveSelectedArray = [Category]()
    var finalSelectedArray = [Category]()
}
