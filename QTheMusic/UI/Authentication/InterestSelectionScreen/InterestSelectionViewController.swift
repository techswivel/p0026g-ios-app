//
//  InterestSelectionViewController.swift
//  QtheMusic
//
//  Created by Assad Khan on 29/03/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import UIKit

class InterestSelectionViewController: BaseViewController {
    
    
    @IBOutlet weak var crossBtn: UIButton!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var interestCollectionView: UICollectionView!{
        didSet{
            interestCollectionView.dataSource = self
            interestCollectionView.delegate = self
        }}
    
    @IBOutlet weak var letsGoBtn: UIButton!{
        didSet{
            letsGoBtn.roundCorner(radius: 15)
        }
    }
    
    private var mSongAndArtistViewModel = SongAndArtistViewModel()
    private var mProfileViewModel = ProfileViewModel()
    private var mViewModel = InterestSelectionViewModel()
    var selectedState = Bool()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpView()
    
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        backgroundView.addGradientBackground()
    }
    
    @IBAction func onClickCrossBtn(_ sender: Any) {
        self.popBackNavigationWithTransition()
    }
    @IBAction func letsGoBtnPressed(_ sender: Any) {
        if mViewModel.selectedArray.count < 2
        {
            Utils.showToast(view: self.view, message: StringConstant.atleastSelectTwoInterest)
        }
        else
        {
            self.showActivityIndicator()
            self.mProfileViewModel.saveInterest(categories: self.mViewModel.selectedArray)
        }
        }
    
    private func setUpView() {
        self.registerNib()
        bindingServerResponse()
        bindingServerErrorResponse()
        bindingServerCompletionResponse()
        
        //SaveInterest
        bindingServerResponseForSaveInterest()
        bindingServerErrorResponseForSaveInterest()
        bindingServerCompletionResponseForSaveInterest()
        
        self.showActivityIndicator()
        if selectedState == true
        {
            crossBtn.isHidden = false
        self.mSongAndArtistViewModel.getCtegories(categoryType: CategoryType.INTEREST.rawValue)
        }
        else
        {
            crossBtn.isHidden = true
            self.mSongAndArtistViewModel.getCtegories(categoryType: CategoryType.ALL.rawValue)
        }
        
    }
    
    private func registerNib() {
        interestCollectionView.register(UINib(nibName: UINibConstant.InterestCollectionViewCellNib, bundle: nil), forCellWithReuseIdentifier: UINibConstant.InterestCollectionViewCellIdentifier)
    }
}

// MARK: -InterestSelectionViewController
extension InterestSelectionViewController : UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.mViewModel.categoryArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: UINibConstant.InterestCollectionViewCellIdentifier ,for: indexPath) as!
        InterestCollectionViewCell
        if !(self.mViewModel.categoryArray.isEmpty ?? true)
            {
            cell.setUpCell(category: self.mViewModel.categoryArray[indexPath.row])
            if mViewModel.selectedArray.count == 0
            {
                self.mViewModel.selectedArray.append( self.mViewModel.categoryArray[indexPath.row])
                mViewModel.selectedArray = mViewModel.categoryArray.filter ({ $0.isSelected == true})
            }
        }
        
        return cell
        
    }
    func centerItemsInCollectionView(cellWidth: Double, numberOfItems: Double, spaceBetweenCell: Double, collectionView: UICollectionView) -> UIEdgeInsets {
        let totalWidth = cellWidth * numberOfItems
        let totalSpacingWidth = spaceBetweenCell * (numberOfItems - 1)
        let leftInset = (collectionView.frame.width - CGFloat(totalWidth + totalSpacingWidth)) / 2
        let rightInset = leftInset
        return UIEdgeInsets(top: 0, left: leftInset, bottom: 0, right: rightInset)
    }
    
    
    //MARK: - UIcollectionView Flow Layout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let string = NSString(string: self.mViewModel.categoryArray[indexPath.row].categoryTitle ?? "" )
        let font = FontConstants.getManropeRegular(size: 12)
        let size = string.size(withAttributes: [NSAttributedString.Key.font : font])
        
        return CGSize(width: (size.width + 25), height: 30)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: UINibConstant.InterestCollectionViewCellIdentifier ,for: indexPath) as!
        InterestCollectionViewCell
        if self.mViewModel.categoryArray[indexPath.row].isSelected ?? false
        {            self.mViewModel.categoryArray[indexPath.row].isSelected = false
           self.mViewModel.selectedArray.removeAll(where: { $0.categoryID ==  self.mViewModel.categoryArray[indexPath.row].categoryID })
        }else{
            if self.mViewModel.selectedArray.count >= 5{
                Utils.showToast(view: self.view, message: StringConstant.maximumFiveInterestCanBeSelected)

            }else{
                self.mViewModel.categoryArray[indexPath.row].isSelected = true
                if selectedState == false
                {
                self.mViewModel.selectedArray.append( self.mViewModel.categoryArray[indexPath.row])
                }
                else
                    {
                    self.mViewModel.selectedArray.append( self.mViewModel.categoryArray[indexPath.row])
                    mViewModel.selectedArray = mViewModel.categoryArray.filter ({ $0.isSelected == true})
                    }
                }
            }
            
        self.interestCollectionView.reloadData()
    }
}
extension InterestSelectionViewController{
    //MARK: - Binding Server Response
    private func bindingServerResponse() {
        self.mSongAndArtistViewModel.serverResponseForGetCategories.bind {
            if ($0 != nil) {
                print("Status check \($0?.response?.status)")
                if $0?.response?.status ?? false {
                    
                    
                    defer{
                        self.interestCollectionView.reloadData()
                        self.removeActivityIndicator()
                    }
                    self.mViewModel.categoryArray = $0?.response?.data?.category ?? [Category]()
                }
                else if $0?.response?.status == false {
                    print("Response for Interest selection screen is false")
                    self.removeActivityIndicator()
                    
                }
            }
        }
    }
    
    //MARK: - Binding Error Response
    private func bindingServerErrorResponse() {
        self.mSongAndArtistViewModel.errorResponseForGetCategories.bind {
            if ($0 != nil) {
                Utils.showErrorAlert(vc: self, message: $0?.response?.message)
            }
            
            self.removeActivityIndicator()
        }
        
        
    }
    //MARK: - Binding Server Response For saving Interest
    private func bindingServerResponseForSaveInterest() {
        self.mProfileViewModel.serverResponseForSaveInterest.bind {
            if ($0 != nil) {
                print("Status check \($0?.response?.status)")
                if $0?.response?.status ?? false {
                    let authData = UserDefaultUtils.getProfile(key: CommonKeys.KEY_PROFILE)
                    authData?.isInterestsSet = true
                    if let data = authData {
                        UserDefaultUtils.saveProfile(value: data, key: CommonKeys.KEY_PROFILE)
                    }
                    self.goToMainStoryboard()
                    self.removeActivityIndicator()
                    
                }
                else if $0?.response?.status == false {
                    print("Response for Interest save interest screen is false")
                    self.removeActivityIndicator()
                    
                }
            }
        }
    }
    
    //MARK: - Binding Error Response
    private func bindingServerErrorResponseForSaveInterest() {
        self.mProfileViewModel.errorResponseForSaveInterest.bind {
            if ($0 != nil) {
                Utils.showErrorAlert(vc: self, message: $0?.response?.message)
            }
            
            self.removeActivityIndicator()
        }
        
        
    }
    //MARK: - Binding Complete Response
    private func bindingServerCompletionResponse(){
        self.mProfileViewModel.completion.bind {
            if ($0 != nil) {
                
                
            }
        }
        
    }
    //MARK: - Binding Complete Response
    private func bindingServerCompletionResponseForSaveInterest(){
        self.mSongAndArtistViewModel.completion.bind {
            if ($0 != nil) {
                
                
            }
        }
        
    }
}
