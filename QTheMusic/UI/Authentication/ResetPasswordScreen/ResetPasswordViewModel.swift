//
//  ResetPasswordViewModel.swift
//  QtheMusic
//
//  Created by Assad Khan on 17/03/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import Foundation
import UIKit

class ResetPasswordViewModel:BaseViewModel{
    
    var authorization:Authorization?
    var newPasswordVisibility :Bool = false
    var confirmPasswordVisibility :Bool = false
    let visibilityOnImage = UIImage(named: StringConstant.visibiltyOnImage)
    let visibilityOffImage = UIImage(named: StringConstant.visibiltyOffImage)
}

