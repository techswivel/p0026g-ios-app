//
//  ResetPasswordViewController.swift
//  QtheMusic
//
//  Created by Assad Khan on 11/03/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialTextControls_OutlinedTextFields

protocol ResetPasswordViewControllerProtocol{
    func setData(data:AuthorizationBuilder?)
}



class ResetPasswordViewController: BaseViewController {

    @IBOutlet weak var resetPasswordBtn: UIButton!{
        didSet{
            resetPasswordBtn.roundCorner(radius: 12)
        }
    }
    
    @IBOutlet weak var passwordTextField: MDCOutlinedTextField!{
        didSet{
            passwordTextField.placeholder = StringConstant.password
            
            passwordTextField.setFloatingLabelColor(UIColor.white, for: .normal)
            passwordTextField.setFloatingLabelColor(ColorConstrants.PRIMARY_COLOR, for: .editing)
            passwordTextField.setFloatingLabelColor(UIColor.white, for: .disabled)
            
            passwordTextField.setNormalLabelColor(UIColor.white, for: .normal)
            passwordTextField.setNormalLabelColor(UIColor.white, for: .editing)
            passwordTextField.setNormalLabelColor(UIColor.white, for: .disabled)
            
            passwordTextField.label.text = StringConstant.password
            
            passwordTextField.setTextColor(UIColor.white, for: .editing)
            passwordTextField.setTextColor(UIColor.white, for: .disabled)
            passwordTextField.setTextColor(UIColor.white, for: .normal)
            
            
            passwordTextField.setLeadingAssistiveLabelColor(UIColor.red, for: .normal)
            passwordTextField.setLeadingAssistiveLabelColor(UIColor.red, for: .disabled)
            passwordTextField.setLeadingAssistiveLabelColor(UIColor.red, for: .editing)
            
            passwordTextField.setOutlineColor(UIColor.white, for: .normal)
            passwordTextField.setOutlineColor(ColorConstrants.PRIMARY_COLOR, for: .editing)
        }
    }
    @IBOutlet weak var confirmPasswordTextfield: MDCOutlinedTextField!{
        didSet{
            confirmPasswordTextfield.placeholder = StringConstant.confirmPassword
            
            confirmPasswordTextfield.setFloatingLabelColor(UIColor.white, for: .normal)
            confirmPasswordTextfield.setFloatingLabelColor(ColorConstrants.PRIMARY_COLOR, for: .editing)
            confirmPasswordTextfield.setFloatingLabelColor(UIColor.white, for: .disabled)
            
            confirmPasswordTextfield.setNormalLabelColor(UIColor.white, for: .normal)
            confirmPasswordTextfield.setNormalLabelColor(UIColor.white, for: .editing)
            confirmPasswordTextfield.setNormalLabelColor(UIColor.white, for: .disabled)
            
            confirmPasswordTextfield.label.text = StringConstant.confirmPassword
            
            confirmPasswordTextfield.setTextColor(UIColor.white, for: .editing)
            confirmPasswordTextfield.setTextColor(UIColor.white, for: .disabled)
            confirmPasswordTextfield.setTextColor(UIColor.white, for: .normal)
            
            
            confirmPasswordTextfield.setLeadingAssistiveLabelColor(UIColor.red, for: .normal)
            confirmPasswordTextfield.setLeadingAssistiveLabelColor(UIColor.red, for: .disabled)
            confirmPasswordTextfield.setLeadingAssistiveLabelColor(UIColor.red, for: .editing)
            
            confirmPasswordTextfield.setOutlineColor(UIColor.white, for: .normal)
            confirmPasswordTextfield.setOutlineColor(ColorConstrants.PRIMARY_COLOR, for: .editing)
        }
    }
    @IBOutlet weak var newPasswordVisibilityBtn: UIButton!
    @IBOutlet weak var confirmPasswordVisibilityBtn: UIButton!
    
    @IBOutlet weak var backgroundView: UIView!
    
    private var mViewModel = ResetPasswordViewModel()
    private var mAuthViewModel = AuthViewModel()
        
    override func viewDidLoad() {
        super.viewDidLoad()

       
        self.setUpView()
        // Do any additional setup after loading the view.
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        backgroundView.addGradientBackground()
    }
    @IBAction func passwordVisibilityBtnPressed(_ sender: Any) {
        
        if self.mViewModel.newPasswordVisibility == true {
            self.mViewModel.newPasswordVisibility = false
            self.newPasswordVisibilityBtn.setImage(self.mViewModel.visibilityOnImage, for: .normal)
            self.passwordTextField.isSecureTextEntry = true
        }
        else {
            self.mViewModel.newPasswordVisibility = true
            self.newPasswordVisibilityBtn.setImage(self.mViewModel.visibilityOffImage, for: .normal)
            self.passwordTextField.isSecureTextEntry = false
        }
        
        
    }
    @IBAction func confirmPassworVisibilityBtnPressed(_ sender: Any) {
        
        if self.mViewModel.confirmPasswordVisibility == true {
            self.mViewModel.confirmPasswordVisibility = false
            self.confirmPasswordVisibilityBtn.setImage(self.mViewModel.visibilityOnImage, for: .normal)
            self.confirmPasswordTextfield.isSecureTextEntry = true
        }
        else {
            self.mViewModel.confirmPasswordVisibility = true
            self.confirmPasswordVisibilityBtn.setImage(self.mViewModel.visibilityOffImage, for: .normal)
            self.confirmPasswordTextfield.isSecureTextEntry = false
        }
        
    }
    
    @IBAction func resetPasswordBtnPressed(_ sender: Any) {
        
        
        
        //Making textfields out of focus
        passwordTextField.resignFirstResponder()
        confirmPasswordTextfield.resignFirstResponder()
        
        //checking if password is empty or not
        if passwordTextField.text?.count ?? 0 > 0 {
            passwordTextField.leadingAssistiveLabel.text = ""
        }else{
            passwordTextField.leadingAssistiveLabel.text  = StringConstant.passwordIsMissing
        }
        //checking if password is empty or not
        if confirmPasswordTextfield.text?.count ?? 0 > 0 {
            
            confirmPasswordTextfield.leadingAssistiveLabel.text = ""
        }else{
         
            confirmPasswordTextfield.leadingAssistiveLabel.text = StringConstant.confirmPasswordIsMissing
        }
        guard passwordTextField.text?.count ?? 0  > 0 else{return }
        guard let newPassword = passwordTextField.text else{return}
        //checking if password is not empty but valid or not
        if newPassword.count > 0 && newPassword.isValidPassword() == false{
            
            passwordTextField.leadingAssistiveLabel.text  = StringConstant.passwordIsNotValid
            return
           
        }else{
            passwordTextField.leadingAssistiveLabel.text  = ""
        }
        // checking if confirm password is not empty but matches newpassword textfield text or not
        if confirmPasswordTextfield.text?.count ?? 0 > 0 && confirmPasswordTextfield.text == newPassword{
      
            confirmPasswordTextfield.leadingAssistiveLabel.text  = ""
        }else if confirmPasswordTextfield.text?.count ?? 0 > 0 && confirmPasswordTextfield.text != newPassword{
            
           
            confirmPasswordTextfield.leadingAssistiveLabel.text  = StringConstant.newPassDoesntMatchConfirmPass
        }
        guard confirmPasswordTextfield.text?.count ?? 0 >  0 else {return}
        guard passwordTextField.text?.count ?? 0 > 0  else {return}
        guard let confirmPassword = confirmPasswordTextfield.text else{return }
        
        // Case Sign up
        if newPassword == confirmPassword && self.mViewModel.authorization?.otpType == OtpType.EMAIL.rawValue {
            
            let vc = UIStoryboard.init(name: StoryBoardConstant.storyBoardAuth, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.ProfileSetUpScreenVc) as? ProfileSignUpViewController
            
            
            let builder = AuthorizationBuilder()
            
            if mViewModel.authorization?.socialSite == socialSiteEnum.APPLE.rawValue || mViewModel.authorization?.socialSite == socialSiteEnum.FACEBOOK.rawValue || mViewModel.authorization?.socialSite == socialSiteEnum.GOOGLE.rawValue {
                builder.setSocialSite(mViewModel.authorization?.socialSite)
            }
            
            builder.setemail(self.mViewModel.authorization?.email)
            builder.setPassword(self.passwordTextField.text)
            builder.setsocialId(self.mViewModel.authorization?.socialId)
            vc?.setAuthorizationData(auth: builder.build(), socialSite: nil)
            self.navigationController?.pushViewController(vc!, animated: true)
            
            return
        }
        
        // Case Forgot Password
        if newPassword == confirmPassword && self.mViewModel.authorization?.otpType == OtpType.FORGET_PASSWORD.rawValue{
            
            let builder = AuthorizationBuilder()
            builder.setemail(self.mViewModel.authorization?.email)
            builder.setOtp(self.mViewModel.authorization?.otp)
            builder.setPassword(confirmPassword)
            self.showActivityIndicator()
            self.mAuthViewModel.resetPassword(builder: builder)
        }
    }

    @IBAction func backBtnPressed(_ sender: Any) {
        self.popBackController()
    }
   
    
    private func setUpView(){
        self.registerNib()
        
        passwordTextField.delegate = self
        confirmPasswordTextfield.delegate = self
        
        self.bindingServerResponseForResetPassword()
        self.bindingServerErrorResponseForResetPassword()
        self.bindingServerCompletionResponse()
        
    }
    
    private func registerNib(){
        
    }
}

extension ResetPasswordViewController:ResetPasswordViewControllerProtocol{
    func setData(data: AuthorizationBuilder?) {
        if let Data = data{
            self.mViewModel.authorization = Data.build()
        }
    }
}

extension ResetPasswordViewController{
    //MARK: - Binding Server Response
    private func bindingServerResponseForResetPassword() {
        self.mAuthViewModel.serverResponseForResetPassword.bind {
            if ($0 != nil) {
                
                if $0?.response?.status ?? false {
                    
                    ///Profile Setup
                    if self.mViewModel.authorization?.otpType == OtpType.EMAIL.rawValue{
                        
                        let vc = UIStoryboard.init(name: StoryBoardConstant.storyBoardAuth, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.ProfileSetUpScreenVc) as? ProfileSignUpViewController
                        
                        vc?.setAuthorizationData(auth: nil, socialSite: nil)
                        self.navigationController?.pushViewController(vc!, animated: true)
                        
                        
                    }else{
                        //Forget Password
                        let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController];
                        for desiredViewController in viewControllers {
                            if (desiredViewController is SignInViewController) {
                                self.navigationController?.popToViewController(desiredViewController, animated: true)
                            }
                            
                        }
                        
                    }
                   
                    self.removeActivityIndicator()
                }else if $0?.response?.status == false {
                    Utils.showAlert(vc: self, message: $0?.response?.message ?? "" )
                    print("Response is False in reset password")
                    self.removeActivityIndicator()
                    
                }
                
            }
        }
    }
    
    //MARK: - Binding Error Response
    private func bindingServerErrorResponseForResetPassword() {
        self.mAuthViewModel.ErrorResponseForResetPassword.bind {
            if ($0 != nil) {
                Utils.showErrorAlert(vc: self, message: $0?.response?.message)
            }
            
            self.removeActivityIndicator()
        }
        
        
    }
    
    //MARK: - Binding Complete Response
    private func bindingServerCompletionResponse(){
        self.mAuthViewModel.completion.bind {
            if ($0 != nil) {
                
            }
        }
        
    }
}
//MARK: - UITEXTFIELD Delagate Methods.
extension ResetPasswordViewController : UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.clearsOnInsertion = false
        textField.clearsOnBeginEditing = false
    
        switch textField {
        case passwordTextField:
            passwordTextField.leadingAssistiveLabel.text = ""
            
            
        case confirmPasswordTextfield:
            confirmPasswordTextfield.leadingAssistiveLabel.text = ""
            
        default:
            print("no field found")
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        switch textField {
        case passwordTextField:
            confirmPasswordTextfield.becomeFirstResponder()
        case confirmPasswordTextfield:
            self.view.endEditing(true)
        default:
            print("no field found")
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
       let nsString:NSString? = textField.text as NSString?
       let updatedString = nsString?.replacingCharacters(in:range, with:string);

       textField.text = updatedString;


       //Setting the cursor at the right place
       let selectedRange = NSMakeRange(range.location + string.count, 0)
       let from = textField.position(from: textField.beginningOfDocument, offset:selectedRange.location)
       let to = textField.position(from: from!, offset:selectedRange.length)
       textField.selectedTextRange = textField.textRange(from: from!, to: to!)

       //Sending an action
        textField.sendActions(for: UIControl.Event.editingChanged)

       return false;
   }
}

extension ResetPasswordViewController:ProfileSignUpViewControllerProtocol{
    func setAuthorizationData(auth: Authorization?, socialSite: String?) {
        self.mViewModel.authorization = auth
        self.mViewModel.authorization?.socialSite = socialSite
    }
}
