//
//  SplashScreenViewModel.swift
//  QtheMusic
//
//  Created by Assad Khan on 05/04/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import Foundation


class SplashScreenViewModel :BaseViewModel{
    
    func setUpRecentlyPlayedDb(){
        mDataManager.setUpRecentlyPlayedDB()
    }
    
}
