//
//  SplashScreenViewController.swift
//  QtheMusic
//
//  Created by Assad Khan on 14/03/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import UIKit

class SplashScreenViewController: BaseViewController {
    @IBOutlet weak var loaderActivityIndicator: UIActivityIndicatorView!
    
    private var mViewModel = SplashScreenViewModel()
    
    override func viewDidLoad() {
        
        mViewModel.setUpRecentlyPlayedDb()

        
        loaderActivityIndicator.startAnimating()
        super.viewDidLoad()
        
        if UserDefaultUtils.getProfile(key: CommonKeys.KEY_PROFILE) != nil {
            DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                
               if UserDefaultUtils.getProfile(key: CommonKeys.KEY_PROFILE)?.isInterestsSet ?? false {
                   self.goToMainStoryboard()
               }else{
                   let vc = UIStoryboard.init(name: StoryBoardConstant.storyBoardAuth, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.yourInterestScreen) as? InterestSelectionViewController
                   self.navigationController?.pushViewController(vc!, animated: true)
               }
                
            }
        }else{
            DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                self.performSegue(withIdentifier: SEGUE_SPLASH_TO_SIGNINSCREEN, sender: nil)
            }
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        loaderActivityIndicator.stopAnimating()
    }
    

}
