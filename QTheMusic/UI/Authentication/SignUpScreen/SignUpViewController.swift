//
//  SignUpViewController.swift
//  QtheMusic
//
//  Created by Assad Khan on 11/03/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialTextControls_OutlinedTextFields
import Firebase
import FirebaseMessaging


class SignUpViewController: SocialSignInandSignUpViewController {

    @IBOutlet weak var emailTextField: MDCOutlinedTextField!{
        didSet{
            emailTextField.placeholder = StringConstant.email
            
            emailTextField.setFloatingLabelColor(UIColor.white, for: .normal)
            emailTextField.setFloatingLabelColor(ColorConstrants.PRIMARY_COLOR, for: .editing)
            emailTextField.setFloatingLabelColor(UIColor.white, for: .disabled)
            
            emailTextField.setNormalLabelColor(UIColor.white, for: .normal)
            emailTextField.setNormalLabelColor(UIColor.white, for: .editing)
            emailTextField.setNormalLabelColor(UIColor.white, for: .disabled)
            
            emailTextField.label.text = StringConstant.email
            
            emailTextField.setTextColor(UIColor.white, for: .editing)
            emailTextField.setTextColor(UIColor.white, for: .normal)
            emailTextField.setTextColor(UIColor.white, for: .disabled)
            
            
            emailTextField.setLeadingAssistiveLabelColor(UIColor.red, for: .normal)
            emailTextField.setLeadingAssistiveLabelColor(UIColor.red, for: .disabled)
            emailTextField.setLeadingAssistiveLabelColor(UIColor.red, for: .editing)
            
            emailTextField.setOutlineColor(UIColor.white, for: .normal)
            emailTextField.setOutlineColor(ColorConstrants.PRIMARY_COLOR, for: .editing)
        }
    }
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var orBackGroundView: UIView!{
        didSet{
            orBackGroundView.layer.borderColor = UIColor.white.cgColor
            orBackGroundView.layer.borderWidth = 1
            orBackGroundView.roundCorner(radius: 15)
        }
    }
    @IBOutlet weak var sendcodeBtn: UIButton!{
        didSet{
            sendcodeBtn.roundCorner(radius: 12)
        }
    }
    
    
    @IBOutlet weak var emailWarning: UIImageView!
    private var mViewModel = SignUpViewModel()
    private var mAuthViewModel = AuthViewModel()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        // Do any additional setup after loading the view.
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        backgroundView.addGradientBackground()
    }
    @IBAction func fbBtnPressed(_ sender: Any) {
        self.facebookSignIn()
    }
    
    @IBAction func appleBtnPressed(_ sender: Any) {
        self.appleSignIn()
    }
    
    @IBAction func googleBtnPressed(_ sender: Any) {
        self.googleSignIn()
    }
    
    
    @IBAction func backBtnPressed(_ sender: Any) {
        
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.fade
        self.navigationController?.view.layer.add(transition, forKey: nil)
        
        if #available(iOS 13.0, *) {
            self.navigationController?.popViewController(animated: false)
        }else{
           self.navigationController?.popViewController(animated: false)
        }

        
    }
    
    @IBAction func sendCodeBtnPressed(_ sender: Any) {
        
        
        emailTextField.resignFirstResponder()
       
        self.mViewModel.validationMsg = ""
        
        //Checking if email is empty or not
        if emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            
            
            self.mViewModel.validationMsg.append(contentsOf: ", \(StringConstant.emailIsMissing)")
            emailWarning.isHidden = false
            
        }else {

            emailTextField.leadingAssistiveLabel.text = ""
            emailWarning.isHidden = true
            
        }
        //Checking if email is not empty weather it is valid or not
        if emailTextField.text?.isValidEmail() == false && emailTextField.text?.count ?? 0 > 0{
            
            
            
            self.mViewModel.validationMsg.append(contentsOf: ", \(StringConstant.emailISNotValid)")
            emailWarning.isHidden = false
            
            
        }else if emailTextField.text?.isValidEmail() == true && emailTextField.text?.count ?? 0 > 0{
            emailTextField.leadingAssistiveLabel.text = ""
            
            emailWarning.isHidden = true
            
        }
        
        
        
            
        if self.mViewModel.validationMsg.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
                
            Utils.showActionSheet(vc: self, title: StringConstant.validationError, message: self.mViewModel.validationMsg.removePunctuationIfInStart(), noBtnTile: StringConstant.Ok) {
                    
                }
     
                return
            }
        
        guard let email = emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) else{return }
        if   email.count  > 0 && email.isValidEmail() {
            self.showActivityIndicator()
           
            
                //Perform Action
                let builder = AuthorizationBuilder()
                builder.setOtpType(OtpType.EMAIL.rawValue)
                builder.setemail(email)
               
                self.mAuthViewModel.sendOTP(builder: builder)
            
        }
    }
    private func setUpView(){
        self.regisNib()
        
        emailTextField.delegate = self
        
        self.bindingServerResponseForSendOtp()
        self.bindingServerErrorResponseForSendOtp()
        self.bindingServerCompletionResponse()
        
        
        self.setDelegate(delegate: self)
        
        // For Social Sign up
        if let email = mViewModel.authorization?.email {
            self.emailTextField.text = mViewModel.authorization?.email
        }
        
    }
    private func regisNib(){
        
    }
}

extension SignUpViewController{
    //MARK: - Binding Server Response
    private func bindingServerResponseForSendOtp() {
        self.mAuthViewModel.serverResponseForSendOTP.bind {
            if ($0 != nil) {
                
                if $0?.response?.status ?? false {
                    
                    let vc = UIStoryboard.init(name: StoryBoardConstant.storyBoardAuth, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.OTPScreenController) as? OtpVerificationViewController
                    let authBuilder = AuthorizationBuilder()
                    authBuilder.setemail(self.emailTextField.text)
                    authBuilder.setOtpType(OtpType.EMAIL.rawValue)
                    authBuilder.setsocialId(self.mViewModel.authorization?.socialId)
                    authBuilder.setSocialSite(self.mViewModel.authorization?.socialSite)
                    vc?.setOtpSCreenData(data: authBuilder)
                    self.navigationController?.pushViewController(vc!, animated: true)
                    self.removeActivityIndicator()
                }else if $0?.response?.status == false {
                    Utils.showAlert(vc: self, message: $0?.response?.message ?? "" )
                    print("Response is False in signUp OTP")
                    self.removeActivityIndicator()
                    
                }
                
            }
        }
    }
    
    //MARK: - Binding Error Response
    private func bindingServerErrorResponseForSendOtp() {
        self.mAuthViewModel.ErrorResponseForSendOTP.bind {
            if ($0 != nil) {
                Utils.showErrorAlert(vc: self, message: $0?.response?.message)
            }
            
            self.removeActivityIndicator()
        }
        
        
    }
    
    //MARK: - Binding Complete Response
    private func bindingServerCompletionResponse(){
        self.mAuthViewModel.completion.bind {
            if ($0 != nil) {
                
            }
        }
        
    }
}


//MARK: - UITEXTFIELD Delagate Methods.
extension SignUpViewController : UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.clearsOnInsertion = false
        textField.clearsOnBeginEditing = false
        switch textField {
        case emailTextField:
            emailTextField.leadingAssistiveLabel.text = ""
            emailWarning.isHidden = true
        
            
        default:
            print("no field found")
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        switch textField {
        case emailTextField:
            self.view.endEditing(true)
        
            
        default:
            print("no field found")
        }
        return true
    }
}
extension SignUpViewController:SocialSignInandSignUpViewDelegate{
    
    func socialUserDontExists(auth: Authorization?, socialSite:String?) {
        let vc = UIStoryboard.init(name: StoryBoardConstant.storyBoardAuth, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.ProfileSetUpScreenVc) as? ProfileSignUpViewController
        
        vc?.setAuthorizationData(auth: auth, socialSite: socialSite)
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    
}

extension SignUpViewController:ProfileSignUpViewControllerProtocol{
   
    func setAuthorizationData(auth: Authorization?, socialSite: String?) {
        self.mViewModel.authorization = auth
        self.mViewModel.authorization?.socialSite = socialSite
    }
}
