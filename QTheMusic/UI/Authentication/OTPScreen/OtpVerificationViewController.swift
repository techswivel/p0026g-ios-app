//
//  OtpVerificationViewController.swift
//  QtheMusic
//
//  Created by Assad Khan on 14/03/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import UIKit
import SGDigitTextField
import SwiftUI

protocol OtpVerificationViewControllerProtocol{
    func setOtpSCreenData(data:AuthorizationBuilder?)
}
protocol OtpVerificationViewControllerDelegate{
    func phoneNumberSucccessResponse()
}
protocol OtpVerificationViewDelegateImp{
    func setDelegate(delegate:OtpVerificationViewControllerDelegate)
}


class OtpVerificationViewController: BaseViewController {
    @IBOutlet weak var emailPhoneLabel: UILabel!
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var otpTextField: SGDigitTextField!
    @IBOutlet weak var resendInLabel: UILabel!
    @IBOutlet weak var doneBtn: UIButton!{
        didSet{
            doneBtn.roundCorner(radius: 12)
        }
    }
    @IBOutlet weak var backgroundView: UIView!
  
    @IBOutlet weak var resendBtn: UIButton!
    
    
    @IBOutlet weak var bottomEmailLabel: UILabel!
    @IBOutlet weak var bottomEmailHeightConstraint: NSLayoutConstraint!
    
    
    
    
    
    
    private var timer : Timer!
    private var counterTime : TimeInterval!
    
    private var delegate : OtpVerificationViewControllerDelegate?
    var isFromPhoneNoVerification = false
    var phoneNumber : String?
    private var mViewModel = OtpVerificationViewModel()
    private var mAuthViewModel = AuthViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpView()
        // Do any additional setup after loading the view.
        
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        //Resetting otp textfield on view did disappear
        otpTextField.reset()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        backgroundView.addGradientBackground()
    }
    @IBAction func doneBtnPressed(_ sender: Any) {
        
        
        guard let  otpTextfield = otpTextField.text else{return}
        if !(otpTextfield.count > 0)  {
            Utils.showAlert(vc: self, message: StringConstant.otpIsMissing)
            return
            
        }
        guard let num = Int("\(otpTextField.text ?? "invalid")") else{  Utils.showAlert(vc: self, message: StringConstant.numberAllowedOnly); return }
        
        
        if otpTextfield.count != 5{
            
            Utils.showAlert(vc: self, message: StringConstant.otpMustBe5Char)
            return
        }
        //Verify otp
        self.verifyOtp()
    }
    @IBAction func resendCodeBtnPressed(_ sender: Any) {
        if self.counterTime == 0 {
            self.startTimer()
            //trigger
            
            if self.mViewModel.authorization?.otpType == OtpType.EMAIL.rawValue{
                
                let builder = AuthorizationBuilder()
                builder.setemail(self.mViewModel.authorization?.email)
                builder.setOtpType(OtpType.EMAIL.rawValue)
                
                self.showActivityIndicator()
                self.mAuthViewModel.sendOTP(builder: builder)
            }
            else if self.mViewModel.authorization?.otpType == OtpType.PHONE_NUMBER.rawValue{
                let builder = AuthorizationBuilder()
                builder.setphoneNumber(self.mViewModel.authorization?.phoneNumber)
                builder.setOtpType(OtpType.PHONE_NUMBER.rawValue)
                self.showActivityIndicator()
                self.mAuthViewModel.sendOTP(builder: builder)
                
            }else if self.mViewModel.authorization?.otpType == OtpType.FORGET_PASSWORD.rawValue{
                let builder = AuthorizationBuilder()
                builder.setemail(self.mViewModel.authorization?.email)
                builder.setOtpType(OtpType.FORGET_PASSWORD.rawValue)
               
                self.showActivityIndicator()
                self.mAuthViewModel.sendOTP(builder: builder)
            }
        }
        
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        self.popBackController()
        
    }
    private func setUpView(){
        print("setupVIew")
        self.registerNib()
        if self.mViewModel.authorization?.otpType == OtpType.EMAIL.rawValue{
        self.emailPhoneLabel.text =  self.mViewModel.authorization?.email
            
        
            if   self.emailPhoneLabel.calculateMaxLines() > 1 {
                bottomEmailLabel.text = self.mViewModel.authorization?.email
                bottomEmailLabel.isHidden = false
                bottomEmailHeightConstraint.constant = 13.8
                emailPhoneLabel.isHidden = true
                
            }else{
                
                bottomEmailLabel.isHidden = true
                bottomEmailHeightConstraint.constant = 0
                let range = ("\(self.mViewModel.authorization?.email ?? "")" as NSString).range(of: "\(self.mViewModel.authorization?.email ?? "")")
                let mutableAttributedString = NSMutableAttributedString.init(string: "\(self.mViewModel.authorization?.email ?? "")")
                mutableAttributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: ColorConstrants.RED_THEME, range: range)
                emailPhoneLabel.attributedText = mutableAttributedString
                emailPhoneLabel.isHidden = false
               
            }
            
        }else if self.mViewModel.authorization?.otpType == OtpType.PHONE_NUMBER.rawValue{
            self.emailPhoneLabel.text = self.mViewModel.authorization?.phoneNumber
        }
        
        startTimer()
        resendBtn.isEnabled = false
        
        /// Binding Server Response
        //Binding response for otp request
        self.bindingServerResponseForSendingOtp()
        //Binding response for validating otp
        self.bindingServerResponseValidationOfOtp()
        
        //Error responsess
        self.bindingServerErrorResponseForsSendingOtp()
        self.bindingServerErrorResponseValidationOfOtp()
        
        //Completion Response
        self.bindingServerCompletionResponse()
        
    }
    private func registerNib(){
        
    }
    // MARK: - Verify Otp
    private func verifyOtp(){
        if self.mViewModel.authorization?.otpType == OtpType.EMAIL.rawValue{
            
            let builder = AuthorizationBuilder()
            builder.setemail(self.mViewModel.authorization?.email)
            builder.setOtpType(OtpType.EMAIL.rawValue)
            builder.setOtp(Int("\(self.otpTextField.text ?? "")"))
            self.showActivityIndicator()
            self.mAuthViewModel.varifyOTP(builder: builder)
        }
        else if self.mViewModel.authorization?.otpType == OtpType.PHONE_NUMBER.rawValue{
            let builder = AuthorizationBuilder()
            builder.setphoneNumber(self.mViewModel.authorization?.phoneNumber)
            builder.setOtpType(OtpType.PHONE_NUMBER.rawValue)
            builder.setOtp(Int("\(self.otpTextField.text ?? "")"))
            
            self.showActivityIndicator()
            self.mAuthViewModel.varifyOTP(builder: builder)
            
        }else if self.mViewModel.authorization?.otpType == OtpType.FORGET_PASSWORD.rawValue{
            let builder = AuthorizationBuilder()
            builder.setOtpType(OtpType.FORGET_PASSWORD.rawValue)
            builder.setOtp(Int(self.otpTextField.text ?? ""))
            builder.setemail(self.mViewModel.authorization?.email)
            self.showActivityIndicator()
            self.mAuthViewModel.varifyOTP(builder: builder)
        }
    }
    
    func startTimer() {
        
        self.counterTime = ProjectConstants.current.countDownTimeForOtp
        
        self.resendInLabel.text = StringConstant.resendIn
        self.timerLabel.text = StringConstant.thirty
        self.timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { [weak self] (timer) in
            
            if self == nil {
                return
            }
            
            self!.counterTime -= 1
            
            let formatter = DateComponentsFormatter()
            formatter.allowedUnits = [.minute, .second]
            formatter.unitsStyle = .positional
            
            let timeString = formatter.string(from: TimeInterval(self!.counterTime))!
            
            UIView.setAnimationsEnabled(false)
            self?.timerLabel.text = timeString
            UIView.setAnimationsEnabled(true)
            
            if self!.counterTime == 0 {
                
                self?.resendBtn.isEnabled = true
                self?.resendInLabel.text = StringConstant.resendCode
                self?.timerLabel.text = ""
                self!.timer.invalidate()
                self!.timer = nil
            }
            
        }
        
        
    }
}
extension OtpVerificationViewController{
    //MARK: - Binding Server Response
    
    //sending otp to either email or phonenumber
    //MARK: - bindingServerResponseForSendingOtp
    private func bindingServerResponseForSendingOtp() {
        self.mAuthViewModel.serverResponseForSendOTP.bind {
            if ($0 != nil) {
                print(self.mViewModel.authorization?.otpType)
                print(self.mViewModel.authorization?.phoneNumber)
                print("Status check \($0?.response?.status)")
                if $0?.response?.status ?? false {
                    self.view.endEditing(true)
                    Utils.showToast(view: self.view, message: StringConstant.otpMessage)
                    print("Response True is \($0?.response?.message)")
                    self.removeActivityIndicator()
                    
                }else if $0?.response?.status == false {
                    Utils.showAlert(vc: self, message: $0?.response?.message ?? "" )
                    print("Response for sending otp to email or phoneNumber is False")
                    self.removeActivityIndicator()
                    
                }
            }}
    }
    //MARK: - Binding Error Response
    private func bindingServerErrorResponseForsSendingOtp() {
        self.mAuthViewModel.ErrorResponseForSendOTP.bind {
            if ($0 != nil) {
                Utils.showErrorAlert(vc: self, message: $0?.response?.message)
            }
            
            self.removeActivityIndicator()
        }
        
        
    }
    //Validation of otp
    ///bindingServerResponseValidationOfOtp
    private func bindingServerResponseValidationOfOtp() {
        self.mAuthViewModel.serverResponseForValidateOTP.bind {
            if ($0 != nil) {
                print("Status check \($0?.response?.status)")
                if $0?.response?.status ?? false {
                    UserDefaults.standard.set(self.otpTextField.text, forKey: "otpText")
                    //Signup
                    if self.mViewModel.authorization?.otpType == OtpType.EMAIL.rawValue{
                       
                        let vc = UIStoryboard.init(name: StoryBoardConstant.storyBoardAuth, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.resetPasswordScreen) as? ResetPasswordViewController
                        let authBuilder = AuthorizationBuilder()
                        authBuilder.setOtpType(OtpType.EMAIL.rawValue)
                        authBuilder.setemail(self.mViewModel.authorization?.email)
                        authBuilder.setOtp(Int("\(self.otpTextField.text ?? "")"))
                        authBuilder.setsocialId(self.mViewModel.authorization?.socialId)
                        authBuilder.setSocialSite(self.mViewModel.authorization?.socialSite)
                        vc?.setData(data: authBuilder)
                        
                        self.navigationController?.pushViewController(vc!, animated: true)
                    }
                    else if self.mViewModel.authorization?.otpType == OtpType.PHONE_NUMBER.rawValue{
                    
                        self.delegate?.phoneNumberSucccessResponse()
                        
                        self.popBackController()
                    }
                    //ForgetPassword
                    else if self.mViewModel.authorization?.otpType == OtpType.FORGET_PASSWORD.rawValue{
                        
                        let vc = UIStoryboard.init(name: StoryBoardConstant.storyBoardAuth, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.resetPasswordScreen) as? ResetPasswordViewController
                        let authBuilder = AuthorizationBuilder()
                        authBuilder.setOtpType(OtpType.FORGET_PASSWORD.rawValue)
                        authBuilder.setemail(self.mViewModel.authorization?.email)
                        authBuilder.setOtp(Int("\(self.otpTextField.text ?? "")"))
                        vc?.setData(data: authBuilder)
                        self.navigationController?.pushViewController(vc!, animated: true)
                    }
                
                    
                    self.removeActivityIndicator()
                    
                    
                    
                    
                }else if $0?.response?.status == false {
                    Utils.showAlert(vc: self, message: $0?.response?.message ?? "" )
                    self.otpTextField.reset()
                    self.removeActivityIndicator()
                    print("Response for validating otp is False ")
                    
                    
                }}
        }
    }
    //MARK: - Binding Error Response
    private func bindingServerErrorResponseValidationOfOtp() {
        self.mAuthViewModel.ErrorResponseForValidateOTP.bind {
            if ($0 != nil) {
                Utils.showErrorAlert(vc: self, message: $0?.response?.message)
            }
            self.otpTextField.reset()
            self.removeActivityIndicator()
        }
        
        
    }
    
    
    //MARK: - Binding Complete Response
    private func bindingServerCompletionResponse(){
        self.mAuthViewModel.completion.bind {
            if ($0 != nil) {
                //firebase auth takes time so on success of firebase auth only remove activity indicator,so app doesn't freazes
                
            }
        }
        
    }
}
extension OtpVerificationViewController:OtpVerificationViewControllerProtocol{
    func setOtpSCreenData(data: AuthorizationBuilder?) {
        if let Data = data {
            self.mViewModel.authorization = Data.build()
            self.mAuthViewModel.sendOTP(builder: Data)
        }
    }
    
    
}

extension OtpVerificationViewController : OtpVerificationViewDelegateImp{
    func setDelegate(delegate: OtpVerificationViewControllerDelegate) {
        self.delegate = delegate
    }
}
