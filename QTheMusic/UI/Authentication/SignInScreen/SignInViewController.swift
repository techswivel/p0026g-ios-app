//
//  SignInViewController.swift
//  QtheMusic
//
//  Created by Assad Khan on 10/03/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import UIKit
import Firebase
import FirebaseMessaging
import Foundation
//import FirebaseAuth
class SignInViewController: SocialSignInandSignUpViewController {

    @IBOutlet weak var backgroundImageView: UIImageView!{
        didSet{
            backgroundImageView.blurImageViewWithTransparentBlackColor()
        }
    }
    @IBOutlet weak var textfieldsBackgroundViw: UIView!{
        didSet{
            textfieldsBackgroundViw.roundCorner(radius: 10)
        }
    }
    @IBOutlet weak var emailTextField: UITextField!{
        didSet{
            emailTextField.setIcon(#imageLiteral(resourceName: ImageConstant.emailIcon))
            emailTextField.attributedPlaceholder = NSAttributedString(
                string: StringConstant.email,
                attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        }
    }
    @IBOutlet weak var passwordTextField: PasswordTextField!{
        didSet{
            passwordTextField.setIcon(#imageLiteral(resourceName: ImageConstant.passwordIcon))
            passwordTextField.attributedPlaceholder = NSAttributedString(
                string: StringConstant.password,
                attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        }
    }
    @IBOutlet weak var visibleEyeBtn: UIButton!
    
    @IBOutlet weak var forgetPasswordLabel: UILabel!
    @IBOutlet weak var signInBtn: UIButton!{
        didSet{
            signInBtn.roundCorner(radius: 12)
        }
    }
    
    @IBOutlet weak var orBackgroundView: UIView!{
        didSet{
            orBackgroundView.layer.borderColor = UIColor.white.cgColor
            orBackgroundView.layer.borderWidth = 1
            orBackgroundView.roundCorner(radius: 15)
        }
    }
    @IBOutlet weak var emailWarning: UIImageView!
    @IBOutlet weak var passwordWarning: UIImageView!
    @IBOutlet weak var passwordWarningHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var emailWarningHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var emailWarningLabel: UILabel!
    @IBOutlet weak var passwordWarningLabel: UILabel!
    
    private var mAuthViewModel = AuthViewModel()
    private var mViewModel = SignInViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
       
        // Do any additional setup after loading the view.
    }
    
    
    @objc func labelTapped(_ sender: UITapGestureRecognizer) {
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.fade
        self.navigationController?.view.layer.add(transition, forKey: nil)
        
        let vc = UIStoryboard.init(name: StoryBoardConstant.storyBoardAuth, bundle: Bundle.main).instantiateViewController(withIdentifier: "goToForgetPasswordScreen") as? ForgotPasswordViewController
        self.navigationController?.pushViewController(vc!, animated: false)
        }
    
    
    @IBAction func visibilityEyeBtnPressed(_ sender: Any) {
        if self.mViewModel.isVisible == true {
            self.mViewModel.isVisible = false
            self.visibleEyeBtn.setImage(self.mViewModel.visibilityOnImage, for: .normal)
            self.passwordTextField.isSecureTextEntry = true
        }
        else {
            self.mViewModel.isVisible = true
            self.visibleEyeBtn.setImage(self.mViewModel.visibilityOffImage, for: .normal)
            self.passwordTextField.isSecureTextEntry = false
        }
        
    }
    @IBAction func signInBtnPressed(_ sender: Any) {
        
        emailTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
        
        self.mViewModel.validationMsg = ""
       
        //Checking if email is empty or not
        if emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            
            
            emailWarning.isHidden = false
            self.mViewModel.validationMsg.append(contentsOf: ", \(StringConstant.emailIsMissing)")
            
            
        }else {
            emailWarningLabel.layer.borderWidth = 0
            emailWarningLabel.text = StringConstant.emailIsMissing
            emailWarning.isHidden = true
            
        }
        //Checking if email is not empty weather it is valid or not
        if emailTextField.text?.isValidEmail() == false && emailTextField.text?.count ?? 0 > 0{
           
            emailWarning.isHidden = false
            self.mViewModel.validationMsg.append(contentsOf: ", \(StringConstant.emailISNotValid)")
            
        }else if emailTextField.text?.isValidEmail() == true && emailTextField.text?.count ?? 0 > 0{
            emailWarningLabel.text = StringConstant.emailISNotValid
            emailWarningHeightConstraint.constant = 0
            emailWarning.isHidden = true
            
        }
        //checking if password is empty or not
        if passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            
            passwordWarning.isHidden = false
            self.mViewModel.validationMsg.append(contentsOf: ", \(StringConstant.passwordIsMissing)")
            
        }else {
            passwordWarningLabel.text = StringConstant.passwordIsMissing
            passwordWarningHeightConstraint.constant = 0
            passwordWarning.isHidden = true
        }
        
        if passwordTextField.text?.isValidPassword() == false && passwordTextField.text?.count ?? 0 > 0{
            
            passwordWarning.isHidden = false
            self.mViewModel.validationMsg.append(contentsOf: ", \(StringConstant.passwordIsNotValid)")
            
        }else if passwordTextField.text?.isValidPassword() == true && passwordTextField.text?.count ?? 0 > 0{
            passwordWarningLabel.text = StringConstant.passwordIsNotValid
            passwordWarningHeightConstraint.constant = 0
            passwordWarning.isHidden = true
            
        }
        
        
        
        if self.mViewModel.validationMsg.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            
            Utils.showActionSheet(vc: self, title: StringConstant.validationError, message: self.mViewModel.validationMsg.removePunctuationIfInStart(), noBtnTile: StringConstant.Ok) {
                
            }
 
            return
        }
        
        
        guard let email = emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) else{return }
        guard let password = passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) else{return }
        
        if   email.count  > 0 && email.isValidEmail() && password.isValidPassword() && password.count  > 0{
            self.showActivityIndicator()
            if let token = Messaging.messaging().fcmToken{
                //Perform Action
                
                
                let builder = AuthorizationBuilder()
                builder.setloginType(LoginType.SIMPLE.rawValue)
                builder.setFcmToken(token)
                builder.setemail(email)
                builder.setPassword(password)
                self.mAuthViewModel.login(builder: builder)
            }
        }
        
    }
    
    @IBAction func fbBtnPressed(_ sender: Any) {
        
        self.facebookSignIn()
    }
    @IBAction func appleBtnPressed(_ sender: Any) {
        self.appleSignIn()
    }
    
    @IBAction func googleBtnPressed(_ sender: Any) {
        self.googleSignIn()
    }
    
    @IBAction func signUpBtnPressed(_ sender: Any) {
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.fade
        self.navigationController?.view.layer.add(transition, forKey: nil)
        
        let vc = UIStoryboard.init(name: StoryBoardConstant.storyBoardAuth, bundle: Bundle.main).instantiateViewController(withIdentifier: "SignUPVC") as? SignUpViewController
        self.navigationController?.pushViewController(vc!, animated: false)
    }
    
    private func setUpView(){
        self.registerNib()
        
        
        passwordTextField.delegate = self
        emailTextField.delegate = self
        
        self.forgetPasswordLabel.isUserInteractionEnabled = true
        let TapGesture = UITapGestureRecognizer(target: self, action: #selector(self.labelTapped(_:)))

        self.forgetPasswordLabel.addGestureRecognizer(TapGesture)
        
        self.bindingServerResponseForSignIn()
        self.bindingServerErrorResponseForSignIN()
        self.bindingServerCompletionResponse()
        
        //Setting delegate of SocialSignInSignUp view
        //The class is using social methods so is inhertited from SocialSignInandSignUpViewController
        self.setDelegate(delegate: self)
    }
    private func registerNib(){}
}
extension SignInViewController{
    //MARK: - Binding Server Response
    private func bindingServerResponseForSignIn() {
        self.mAuthViewModel.serverResponseForLogin.bind {
            if ($0 != nil) {
                
                if $0?.response?.status ?? false {
                    //if sign in with phone number respose is a success take user to otp verification screen
                    if $0?.response?.data?.auth != nil{
                        guard let authData = $0?.response?.data?.auth else {return }
                        UserDefaultUtils.setString(value: $0?.response?.data?.auth?.jwt ?? "", key: "jwt")
                         UserDefaultUtils.saveProfile(value: (authData), key: CommonKeys.KEY_PROFILE)
                        if authData.isInterestsSet ?? false{
                            self.goToMainStoryboard()
                        }else{
                            let vc = UIStoryboard.init(name: StoryBoardConstant.storyBoardAuth, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.yourInterestScreen) as? InterestSelectionViewController
                            self.navigationController?.pushViewController(vc!, animated: true)
                            
                        }
                    }
                    print("Response True is \($0?.response?.message)")
                    self.removeActivityIndicator()
                }else if $0?.response?.status == false {
                    Utils.showAlert(vc: self, message: $0?.response?.message ?? "" )
                    print("Response is False in Sign in phone number")
                    self.removeActivityIndicator()
                    
                }}
        }
    }
    
    //MARK: - Binding Error Response
    private func bindingServerErrorResponseForSignIN() {
        self.mAuthViewModel.ErrorResponseForLogin.bind {
            if ($0 != nil) {
                Utils.showErrorAlert(vc: self, message: $0?.response?.message)
            }
            
            self.removeActivityIndicator()
        }
        
        
    }
    
    //MARK: - Binding Complete Response
    private func bindingServerCompletionResponse(){
        self.mAuthViewModel.completion.bind {
            if ($0 != nil) {
                
                
            }
        }
        
    }
}
//MARK: - UITEXTFIELD Delagate Methods.
extension SignInViewController : UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.clearsOnInsertion = false
        textField.clearsOnBeginEditing = false
        switch textField {
        case emailTextField:
            emailWarningHeightConstraint.constant = 0
            emailWarning.isHidden = true
        case passwordTextField:
            passwordWarningHeightConstraint.constant = 0
            passwordWarning.isHidden = true
            
        default:
            print("no field found")
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        switch textField {
        case emailTextField:
            passwordTextField.becomeFirstResponder()
        case passwordTextField:
            self.view.endEditing(true)
            
        default:
            print("no field found")
        }
        return true
    }
}
//MARK: - SocialSignInandSignUpViewDelegate Extension
extension SignInViewController:SocialSignInandSignUpViewDelegate{
    
  
    func socialUserDontExists(auth: Authorization?, socialSite: String?) {
        if socialSite == socialSiteEnum.APPLE.rawValue {
            let vc = UIStoryboard.init(name: StoryBoardConstant.storyBoardAuth, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.resetPasswordScreen) as? ResetPasswordViewController
            vc?.setAuthorizationData(auth: auth, socialSite: socialSite)
            self.navigationController?.pushViewController(vc!, animated: true)
            
        } else {
            let vc = UIStoryboard.init(name: StoryBoardConstant.storyBoardAuth, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.SignUPVC) as? SignUpViewController
            vc?.setAuthorizationData(auth: auth, socialSite: socialSite)
            self.navigationController?.pushViewController(vc!, animated: true)
        }
    }
}
