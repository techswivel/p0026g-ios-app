//
//  SignInViewModel.swift
//  QtheMusic
//
//  Created by Assad Khan on 15/03/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import Foundation
import UIKit

class SignInViewModel:BaseViewModel{
    var isVisible :Bool = false
    let visibilityOnImage = UIImage(named: StringConstant.visibiltyOnImage)
    let visibilityOffImage = UIImage(named: StringConstant.visibiltyOffImage)
    var validationMsg = ""
}
