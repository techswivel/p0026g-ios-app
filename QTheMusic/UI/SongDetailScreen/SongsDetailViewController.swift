//
//  SongsDetailViewController.swift
//  QtheMusic
//
//  Created by Assad Khan on 28/04/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import UIKit
import RangeSeekSlider
import AVFoundation
import AVKit
import MediaPlayer
import StoreKit
protocol SongDetailProtocolForMiniPlayer{
    func setDelegateForMiniPlayer(delegate:SongDeatilDelegateForMiniPlayer)
}
protocol SongDeatilDelegateForMiniPlayer{
    func setIsPlaying(isPlaying:Bool)
}
protocol SongsDetailProtocol{
    func setPlayingCategoryType(categoryType:String?)
    func setSongData(song:Song?)
    func setAlbumData(album:Album?)
    func setIndexPath(row:Int?)
    func setCompleteSongsArray(songsArray :[Song]?)
    func setIsFromMiniPlayer(isFromMiniPlayer:Bool)
}

protocol ArtistDetailProtocol {
    func setArtistData(artist: Artist?)
}

protocol NextPlaySongCategoryProtocol {
    func setNextPlaySongCategory(playedFrom:String?)
}

class SongsDetailViewController: BaseViewController
{
    
    @IBOutlet weak var backBTn: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var audioVideoSegmentControl: UISegmentedControl!
    @IBOutlet weak var playingFromTitleLabel: UILabel!
    @IBOutlet weak var playingFromSubHeadingLabel: UILabel!
    @IBOutlet weak var songImageView: UIImageView!
    {
        didSet{
            self.songImageView.roundCorner(radius: 10)
        }
    }
    @IBOutlet weak var audioVideoContainerView: UIView!
    @IBOutlet weak var audioVideoViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var songImageActtivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var songTitleLabel: UILabel!
    @IBOutlet weak var artistNameLabel: UILabel!
    @IBOutlet weak var favouriteBtn: UIButton!
    @IBOutlet weak var songRangeSeekSlider: RangeSeekSlider!
    @IBOutlet weak var playBtn: UIButton!{
        didSet{
            playBtn.roundCorner(radius: 25)
        }
    }
    @IBOutlet weak var forwardBtn: UIButton!
    @IBOutlet weak var nextSongBtn: UIButton!
    @IBOutlet weak var rewindBtn: UIButton!
    @IBOutlet weak var previousSongBtn: UIButton!
    @IBOutlet weak var playerContainerView: UIView!
    @IBOutlet weak var songInfoContainerView: UIView!
    
    @IBOutlet weak var durationsLabel: UILabel!
    @IBOutlet weak var endingAudioTimeLabel: UILabel!
    
    @IBOutlet weak var lyricsContainerView: UIView!{
        didSet{
            lyricsContainerView.layer.cornerRadius = 8
            lyricsContainerView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]//maxmaxy is right bottom corner//maxXminY is top right corner//minXmaxY is bottom left corner
        }
    }
    
    @IBOutlet weak var segmentControleContainerHeightConstarint: NSLayoutConstraint!
    @IBOutlet weak var segmentControlContainerView: UIView!
    
    internal var mViewModel = SongsDetailViewModel()
    private var avplayer :AVPlayer?
    private let videoController = AVPlayerViewController()
    private var setupVideoPlayer = true
    private var delegateMiniPlayer :SongDeatilDelegateForMiniPlayer?
    private var isfromMiniPlayer = false
    private var mProfileViewModel = ProfileViewModel()
    private var songsAndArtistViewModel = SongAndArtistViewModel()
    private var mDataManager = DataManager()
    private var SongAndAlbumUnlockingScreenView :SongAndAlbumUnlockingScreen?
    private var productIdentifiers: Set<String> = []
    private var successView: PaymentSuccessView!
    private var failedView: PaymentFailedView!
    private var favouriteIcon = Bool()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpView()
        self.hittingFavouriteSongApi()
        setFvrtBtnImage()
        productIdentifiers.removeAll()
        self.mViewModel.insertDataIntoRecentlyPlayed()
        self.bindingServerResponse()
        self.bindingServerErrorResponse()
        self.bindingServerCompletionResponse()
        self.bindingServerResponseForSubscribeToPlan()
        self.bindingServerErrorResponseForSubscribeToPlan()
        self.bindingServerResponseForGetFavouriteSongs()
        self.bindingServerErrorResponseForGetFavouriteSongs()
        self.selectedSongInMediaPlayer()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.hittingFavouriteSongApi()
        self.setFvrtBtnImage()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        containerView.addGradientBackground()
      }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        SKPaymentQueue.default().remove(self)
    }
    
    @IBAction func playPreviousSongBtnPressed(_ sender: Any) {
        AudioPlayer.shared.playPreviousSong()
        previousSongBtn.isEnabled = false
        //creating Animationmfor smooth user experience
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.8) {
            self.previousSongBtn.isEnabled = true
        }
        
    }
    
    @IBAction func playNextSongBtnPressed(_ sender: Any) {
        AudioPlayer.shared.isPlayingFrom = self.mViewModel.isplayingFromString
        AudioPlayer.shared.playNextSong(playNext: false)
        nextSongBtn.isEnabled = false
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.8) {
            self.nextSongBtn.isEnabled = true
        }
    }
    
    
    @IBAction func backBtnPressed(_ sender: Any) {
        self.addTransitionOnNavigationControler()
        self.popBackControllerWithOutAnimation()
       
        if let index = self.mViewModel.indexPathRow, self.mViewModel.songsArray?[index].songStatus == SongStatus.PREMIUM.rawValue{
            AudioPlayer.shared.stopPlaying()
        }
        
    }
    
    @IBAction func screenOverLayBtnPressed(_ sender: Any) {
        
        
        let vc = UIStoryboard.init(name: StoryBoardConstant.storyBoardMain, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.ScreenOverLayVC) as? ScreenOverlayViewController
        vc?.setDelegate(delegate: self)
        vc?.setSongObj(song: self.mViewModel.song ?? Song())
        self.navigationController?.pushViewController(vc!, animated: true)
        
        
    }
    
    // MARK: - Audio Video Segment Control
    @IBAction func segmentControlChanged(_ sender: Any) {
        
        switch audioVideoSegmentControl.selectedSegmentIndex{
        case 0:
            
            audioVideoSegmentControl.isEnabled = false
            
            //Video stoping
            if self.avplayer != nil {
                self.videoController.player?.rate = 0.0
                self.avplayer = nil
                self.videoController.removeFromParent()
                
                
            }
            
            
            UIView.animate(withDuration: 0.5, delay: 0, options: .beginFromCurrentState, animations: {
                
               
                self.songInfoContainerView.alpha = 0
                self.songImageView.alpha = 0
                self.videoView.alpha = 0
                self.playerContainerView.alpha = 0
                
            }) { _ in
                self.songImageView.isHidden = false
                self.videoView.isHidden = true
                self.playerContainerView.isHidden = false
                
                self.audioVideoSegmentControl.isEnabled = true
                
                self.songInfoContainerView.alpha = 1
                self.songImageView.alpha = 1
                self.videoView.alpha = 1
                self.playerContainerView.alpha = 1
                self.audioVideoContainerView.translatesAutoresizingMaskIntoConstraints = false
                self.audioVideoContainerView.heightAnchor.constraint(equalTo: self.containerView.heightAnchor, multiplier: 0.4).isActive = true
                
            
            }
        case 1:
            
            audioVideoSegmentControl.isEnabled = false
            //Audio pausing
            AudioPlayer.shared.pausePlaying()
            
            
            
            
            UIView.animate(withDuration: 0.5, delay: 0, options: .beginFromCurrentState, animations: {
                
               
                self.songInfoContainerView.alpha = 0
                self.songImageView.alpha = 0
                self.videoView.alpha = 0
                self.playerContainerView.alpha = 0
            }) { _ in
                self.songImageView.isHidden = true
                self.videoView.isHidden = false
                self.playerContainerView.isHidden = true
                
                self.audioVideoSegmentControl.isEnabled = true
                
                self.songInfoContainerView.alpha = 1
                self.songImageView.alpha = 1
                self.videoView.alpha = 1
                self.playerContainerView.alpha = 1
                self.audioVideoContainerView.translatesAutoresizingMaskIntoConstraints = false
                self.audioVideoContainerView.heightAnchor.constraint(equalTo: self.containerView.heightAnchor, multiplier: 0.26).isActive = true
                
                
                let videoURL = URL(string: "\(self.mViewModel.song?.songVideoURL ?? "")")
                if let vidURL = videoURL {
                    self.avplayer =  AVPlayer(url: vidURL)
                }
                do {
                    try AVAudioSession.sharedInstance().setCategory(.playback)
                } catch(let error) {
                    print(error.localizedDescription)
                }
                
                self.videoController.player = self.avplayer
                self.videoController.view.frame = self.videoView.frame
                self.videoController.videoGravity = .resizeAspectFill
                
                self.addChild(self.videoController)
                self.videoView.addSubview(self.videoController.view)
                
                
            }
            
            
        default:
            break;
            
        }
        
    }
    
    
    @IBAction func lyricsBtnPressed(_ sender: Any) {
        
        self.addTransitionOnNavigationControler()
        let vc = UIStoryboard.init(name: StoryBoardConstant.storyBoardMain, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.LyricsVC) as? LyricsViewController
        if let index = self.mViewModel.indexPathRow{
            vc?.setLyricsText(text: self.mViewModel.songsArray?[index].lyrics)
            self.navigationController?.pushViewController(vc!, animated: false)
        }
        
    }
    
    // MARK: -Favourite Btn Pressed
    @IBAction func favouriteBtnPressed(_ sender: UIButton) {
        let builder = SongAndArtistBuilder()
        builder.setSongId(self.mViewModel.song?.songID)
        if self.mViewModel.song?.isFavourit == true
        {
            if self.mViewModel.song?.songStatus == SongStatus.PREMIUM.rawValue
            {
                Utils.showErrorAlert(vc: self, message: StringConstant.favouriteSongs)
            }
             if UserDefaultUtils.getBool(key: CommonKeys.Favourite_Icon) == false
            {
                builder.setIsFavourite(true)
                 UserDefaultUtils.setBool(value: true, key: CommonKeys.Favourite_Icon)
            }
            else if self.mViewModel.song?.songStatus == StringConstant.purchased && UserDefaultUtils.getBool(key: CommonKeys.Favourite_Icon) == false
            {
                builder.setIsFavourite(true)
                 UserDefaultUtils.setBool(value: true, key: CommonKeys.Favourite_Icon)
            }
            else
            {
                 builder.setIsFavourite(false)
            }
        }
        
        else
        {
        
                builder.setIsFavourite(true)
        }
        self.showActivityIndicator()
        self.mProfileViewModel.setIsFavourite(data: builder.build())
    }
    
    // MARK: - playPauseBtn
    @IBAction func playPauseBtnPressed(_ sender: Any) {
        //If resuming it via mini player
        self.selectedSongInMediaPlayer()
    }
    
    @IBAction func rewindBtnPressed(_ sender: Any) {
        
        if AudioPlayer.shared.aVPlayer != nil {
            let time = (AudioPlayer.shared.aVPlayer?.currentTime ?? 0) - 10
            if time > 0 {
                AudioPlayer.shared.aVPlayer?.currentTime = time
            }else{
                AudioPlayer.shared.aVPlayer?.currentTime = 0
            }
            
            self.songRangeSeekSlider.selectedMaxValue = AudioPlayer.shared.aVPlayer?.currentTime ?? 0
            self.updateDurationLabel(CurrentTime: "\(Utils.secondsToHoursMinutesSecondsString(seconds: Int(AudioPlayer.shared.aVPlayer?.currentTime ?? 0)))", EndTime: "\(Utils.secondsToHoursMinutesSecondsString(seconds: (Int(self.mViewModel.song?.songDuration ?? "0")) ?? 0 / 1000 ))")
            self.songRangeSeekSlider.layoutSubviews()
            
        }
    }
    @IBAction func fastfarwordBtnPressed(_ sender: Any) {
        if AudioPlayer.shared.aVPlayer != nil {
            let time = ( AudioPlayer.shared.aVPlayer?.currentTime ?? 0) + 10
            if time < AudioPlayer.shared.aVPlayer?.duration ?? 0 {
                AudioPlayer.shared.aVPlayer?.currentTime = time
                self.songRangeSeekSlider.maxValue = CGFloat(Float(UserDefaultUtils.getInteger(key: StringConstant.audioFullTimeKey)))
                self.songRangeSeekSlider.selectedMaxValue = AudioPlayer.shared.aVPlayer?.currentTime ?? 0
                print(self.songRangeSeekSlider.selectedMaxValue)
                print(AudioPlayer.shared.aVPlayer?.currentTime)
                self.updateDurationLabel(CurrentTime: "\(Utils.secondsToHoursMinutesSecondsString(seconds: Int(time)))", EndTime: "\(Utils.secondsToHoursMinutesSecondsString(seconds: (Int(self.mViewModel.song?.songDuration ?? "0")) ?? 0 / 1000 ))")
                self.songRangeSeekSlider.layoutSubviews()
                self.songRangeSeekSlider.layoutIfNeeded()
            }
        }
        
    }
    private func fetchProducts()
    {
        let productIdentifier: String = self.mViewModel.song?.sku ?? ""
        print(productIdentifier)
        productIdentifiers.insert(productIdentifier)
            let productRequest = SKProductsRequest(productIdentifiers: productIdentifiers)
            productRequest.delegate = self
            productRequest.start()
    }
    private func hittingFavouriteSongApi()
    {
        mViewModel.songbuilder.settype(SongType.FAVORITES.rawValue)
        showActivityIndicator()
        songsAndArtistViewModel.songs(data: mViewModel.songbuilder)
    }
    private func selectedSongInMediaPlayer() {
        if mViewModel.purchasedSong?.songAudioURL == mViewModel.purchasedSongUrl
        {
            if AudioPlayer.shared.aVPlayer != nil {
                DispatchQueue.main.async {
                    let url = NSURL(string: self.mViewModel.purchasedSong?.songAudioURL ?? "")
                    AudioPlayer.shared.setDelegate(delegate: self)
                    if let URLForSong = url
                    {
                        self.playBtn.isEnabled = false
                        AudioPlayer.shared.downloadFileFromURL(url: URLForSong,false)
                    }
                }
                
            
                return
            }
            if self.mViewModel.isPlaying ?? false{
                self.mViewModel.isPlaying = false
                AudioPlayer.shared.displayLink?.invalidate()
                AudioPlayer.shared.aVPlayer?.pause()
                playBtn.setImage(UIImage(named: StringConstant.playImage), for: .normal)
                self.syncMediaCenterRemoteControlAudioRateTitle(title: StringConstant.paused, Rate: 0)
                AudioPlayer.shared.setPlaybackFalse()
                self.songRangeSeekSlider.maxValue = CGFloat(Float(UserDefaultUtils.getInteger(key: StringConstant.audioFullTimeKey)))
                self.songRangeSeekSlider.selectedMaxValue = (AudioPlayer.shared.aVPlayer?.currentTime ?? 0) + 10
                AudioPlayer.shared.syncSeekBarTimeInRemoteControlCenter()
            }else{
                self.mViewModel.isPlaying = true
                AudioPlayer.shared.aVPlayer?.prepareToPlay()
                AudioPlayer.shared.aVPlayer?.play()
                
                playBtn.setImage(UIImage(named: StringConstant.pauseImage), for: .normal)
                AudioPlayer.shared.instentiateCDisplayLink()
                AudioPlayer.shared.syncSeekBarTimeInRemoteControlCenter()
                AudioPlayer.shared.setPlaybacktrue()
                
                if UserDefaultUtils.isValueNil(key: StringConstant.titleOfAudioKey) != nil{
                    let title = UserDefaultUtils.getString(key: StringConstant.titleOfAudioKey)
                    self.syncMediaCenterRemoteControlAudioRateTitle(title: title, Rate: 1)
                }
                self.songRangeSeekSlider.maxValue = CGFloat(Float(UserDefaultUtils.getInteger(key: StringConstant.audioFullTimeKey)))
                self.songRangeSeekSlider.selectedMaxValue = (AudioPlayer.shared.aVPlayer?.currentTime ?? 0) + 10
                AudioPlayer.shared.syncSeekBarTimeInRemoteControlCenter()
            }
            
            delegateMiniPlayer?.setIsPlaying(isPlaying: self.mViewModel.isPlaying ?? false)
        }
       else  if UserDefaultUtils.getBool(key: CommonKeys.IsPlayingFromNotification) == false
        {
        if AudioPlayer.shared.aVPlayer == nil {
            DispatchQueue.main.async {
                let url = NSURL(string: self.mViewModel.song?.songAudioURL ?? "")
              //  let url = NSURL(string:  (UserDefaultUtils.getSavedSongData(key: StringConstant.saveSongKey) ?? Song()).songAudioURL ?? ""  )
                AudioPlayer.shared.setDelegate(delegate: self)
                if let URLForSong = url
                {
                    self.playBtn.isEnabled = false
                    AudioPlayer.shared.downloadFileFromURL(url: URLForSong,false)
                }
            }
            
        
            return
        }
        if self.mViewModel.isPlaying ?? false{
            self.mViewModel.isPlaying = false
            AudioPlayer.shared.displayLink?.invalidate()
            AudioPlayer.shared.aVPlayer?.pause()
            playBtn.setImage(UIImage(named: StringConstant.playImage), for: .normal)
            self.syncMediaCenterRemoteControlAudioRateTitle(title: StringConstant.paused, Rate: 0)
            AudioPlayer.shared.setPlaybackFalse()
            self.songRangeSeekSlider.maxValue = CGFloat(Float(UserDefaultUtils.getInteger(key: StringConstant.audioFullTimeKey)))
            self.songRangeSeekSlider.selectedMaxValue = (AudioPlayer.shared.aVPlayer?.currentTime ?? 0) + 10
            AudioPlayer.shared.syncSeekBarTimeInRemoteControlCenter()
        }else{
            self.mViewModel.isPlaying = true
            AudioPlayer.shared.aVPlayer?.prepareToPlay()
            AudioPlayer.shared.aVPlayer?.play()
            
            playBtn.setImage(UIImage(named: StringConstant.pauseImage), for: .normal)
            AudioPlayer.shared.instentiateCDisplayLink()
            AudioPlayer.shared.syncSeekBarTimeInRemoteControlCenter()
            AudioPlayer.shared.setPlaybacktrue()
            
            if UserDefaultUtils.isValueNil(key: StringConstant.titleOfAudioKey) != nil{
                let title = UserDefaultUtils.getString(key: StringConstant.titleOfAudioKey)
                self.syncMediaCenterRemoteControlAudioRateTitle(title: title, Rate: 1)
            }
            self.songRangeSeekSlider.maxValue = CGFloat(Float(UserDefaultUtils.getInteger(key: StringConstant.audioFullTimeKey)))
            self.songRangeSeekSlider.selectedMaxValue = (AudioPlayer.shared.aVPlayer?.currentTime ?? 0) + 10
            AudioPlayer.shared.syncSeekBarTimeInRemoteControlCenter()
        }
        
        delegateMiniPlayer?.setIsPlaying(isPlaying: self.mViewModel.isPlaying ?? false)
    }
       
        else
        {
            if AudioPlayer.shared.aVPlayer == nil {
                DispatchQueue.main.async {
                    let url = NSURL(string:  UserDefaultUtils.getString(key: CommonKeys.AudioUrl))
                    AudioPlayer.shared.setDelegate(delegate: self)
                    if let URLForSong = url
                    {
                        self.playBtn.isEnabled = false
                        AudioPlayer.shared.downloadFileFromURL(url: URLForSong,false)
                    }
                }
                
            
                return
            }
            if self.mViewModel.isPlaying ?? false{
                self.mViewModel.isPlaying = false
                AudioPlayer.shared.displayLink?.invalidate()
                AudioPlayer.shared.aVPlayer?.pause()
                
                playBtn.setImage(UIImage(named: StringConstant.playImage), for: .normal)
                self.syncMediaCenterRemoteControlAudioRateTitle(title: StringConstant.paused, Rate: 0)
                AudioPlayer.shared.setPlaybackFalse()
                AudioPlayer.shared.syncSeekBarTimeInRemoteControlCenter()
            }else{
                self.mViewModel.isPlaying = true
                AudioPlayer.shared.aVPlayer?.prepareToPlay()
                AudioPlayer.shared.aVPlayer?.play()
                
                playBtn.setImage(UIImage(named: StringConstant.pauseImage), for: .normal)
                AudioPlayer.shared.instentiateCDisplayLink()
                AudioPlayer.shared.syncSeekBarTimeInRemoteControlCenter()
                AudioPlayer.shared.setPlaybacktrue()
                
                if UserDefaultUtils.isValueNil(key: StringConstant.titleOfAudioKey) != nil{
                    let title = UserDefaultUtils.getString(key: StringConstant.titleOfAudioKey)
                    self.syncMediaCenterRemoteControlAudioRateTitle(title: title, Rate: 1)
                }
                AudioPlayer.shared.syncSeekBarTimeInRemoteControlCenter()
            }
            
            delegateMiniPlayer?.setIsPlaying(isPlaying: self.mViewModel.isPlaying ?? false)

        }
    }
    // MARK: - Set Up View
    fileprivate func disableNextPreviousBtn() {
        if let index = self.mViewModel.indexPathRow , self.mViewModel.songsArray?[index].songStatus == SongStatus.PREMIUM.rawValue{
        nextSongBtn.isEnabled  = false
            previousSongBtn.isEnabled = false}
    }
    fileprivate func setFvrtBtnImage() {
        
        if self.mViewModel.song?.isFavourit == true {
            
            if self.mViewModel.song?.songStatus == StringConstant.premium
            {
                self.favouriteBtn.setImage( UIImage(named: ImageConstant.isNotFvrt) , for: .normal)
                UserDefaultUtils.setBool(value: false, key: CommonKeys.Favourite_Icon)
            }
            if UserDefaultUtils.getBool(key: CommonKeys.Favourite_Icon) == false
            {
                self.favouriteBtn.setImage( UIImage(named: ImageConstant.isNotFvrt) , for: .normal)
            }
            else
            {
               self.favouriteBtn.setImage( UIImage(named: ImageConstant.isFvrt) , for: .normal)
            }
            
        }
        else if self.mViewModel.song?.isFavourit == false
        {
            self.favouriteBtn.setImage( UIImage(named: ImageConstant.isNotFvrt) , for: .normal)
        }
    }
    private func setUpView(){
        registerNib()
        self.songRangeSeekSlider.delegate = self
        self.songRangeSeekSlider.selectedMaxValue = 0
        if let isPlayingFromString = self.mViewModel.isplayingFromString{
            self.playingFromTitleLabel.text = isPlayingFromString
        }
        
        if let index = self.mViewModel.indexPathRow{
    
            if self.mViewModel.songsArray?[index].songVideoURL != nil{
                self.segmentControlContainerView.isHidden = false
                self.segmentControlContainerView.translatesAutoresizingMaskIntoConstraints = false
                self.segmentControlContainerView.heightAnchor.constraint(equalTo: self.containerView.heightAnchor, multiplier: 0.0639687).isActive = true
            }else{
                self.segmentControlContainerView.isHidden = true
                self.segmentControlContainerView.translatesAutoresizingMaskIntoConstraints = false
                self.segmentControlContainerView.heightAnchor.constraint(equalTo: self.containerView.heightAnchor, multiplier: 0).isActive = true
            }
        }
        
        self.audioVideoSegmentControl.setTitleColor(UIColor.white)
        self.playingFromSubHeadingLabel.text = "\(self.mViewModel.song?.albumName ?? "")"
        self.songTitleLabel.text = "\(self.mViewModel.song?.songTitle ?? "")"
        self.artistNameLabel.text = "\(self.mViewModel.song?.artistName ?? "")"
        self.disableNextPreviousBtn()
        
        self.songImageView.loadImage(imageUrl: self.mViewModel.song?.coverImageURL ?? "", placeHolderName: ImageConstant.noImagePlaceHolder, activiyIndicator: self.songImageActtivityIndicator)
        if let index = self.mViewModel.indexPathRow , self.mViewModel.songsArray?[index].songStatus == SongStatus.PREMIUM.rawValue || isfromMiniPlayer == false{
            self.backBTn.isEnabled = false
            //If its previously playing any audio then stop
            AudioPlayer.shared.stopPlaying()
            if UserDefaultUtils.getBool(key: CommonKeys.IsPlayingFromNotification) == true
            {
                songImageView.isHidden = false
                songImageView.alpha = 1
                playingFromTitleLabel.text = "Songs"
                songTitleLabel.text = UserDefaultUtils.getString(key: CommonKeys.SongTitle)
                self.songImageView.loadImage(imageUrl: UserDefaultUtils.getString(key: CommonKeys.NotImage), placeHolderName: ImageConstant.noImagePlaceHolder, activiyIndicator: self.songImageActtivityIndicator)
                
            }
            DispatchQueue.main.async {
                let url = NSURL(string:  self.mViewModel.song?.songAudioURL ?? "")
                AudioPlayer.shared.setDelegate(delegate: self)
                if let URLSong = url,let imageURL = URL(string: "\(self.mViewModel.song?.albumCoverImageURL ?? "")"){
                    self.playBtn.isEnabled = false
                    
                    AudioPlayer.shared.downloadFileFromURL(url: URLSong , true)
                    AudioPlayer.shared.playingFileName = "\(self.mViewModel.song?.songTitle ?? "" )"
                    AudioPlayer.shared.playingAlbumName = "\(self.mViewModel.song?.albumName ?? "")"
                    
                    self.getData(from: imageURL ) { data, response, error in
                            guard let data = data, error == nil else { return }
                            DispatchQueue.main.async() { [weak self] in
                                AudioPlayer.shared.PlayingFileImage = UIImage(data: data)
                            }
                    }
                    
                }
        }
        }else{
            
            nextSongBtn.isEnabled  = true
            previousSongBtn.isEnabled = true
            
            // MARK: -loading audiio player values
            self.mViewModel.indexPathRow = AudioPlayer.shared.indexPath
            print(self.mViewModel.indexPathRow)
            self.mViewModel.song = AudioPlayer.shared.songsArray?[AudioPlayer.shared.indexPath ?? 0]
            self.mViewModel.songsArray = AudioPlayer.shared.songsArray
            
            self.backBTn.isEnabled = true
            let tabBarController = tabBarController as? CustomTabbarController
            tabBarController?.mViewModel.isPlaying = self.mViewModel.isPlaying
            
            AudioPlayer.shared.setDelegate(delegate: self)

            
            self.updateDurationLabel(CurrentTime: Utils.secondsToHoursMinutesSecondsString(seconds: Int(UserDefaultUtils.getInteger(key: StringConstant.audioCurrentStatusBarStatusKey))), EndTime: self.mViewModel.songsArray?[self.mViewModel.indexPathRow ?? 0].songDuration ?? "")
            
            
            self.songRangeSeekSlider.selectedMaxValue = CGFloat(UserDefaultUtils.getInteger(key: StringConstant.audioCurrentStatusBarStatusKey))
            self.songRangeSeekSlider.maxValue = CGFloat(Float(UserDefaultUtils.getInteger(key: StringConstant.audioFullTimeKey)))
            if self.mViewModel.isPlaying ?? false{
                playBtn.setImage(UIImage(named: StringConstant.pauseImage), for: .normal)
            }else{
                playBtn.setImage(UIImage(named: StringConstant.playImage), for: .normal)
            }
            self.playingFromSubHeadingLabel.text = "\(self.mViewModel.song?.albumName ?? "")"
            self.songTitleLabel.text = "\(self.mViewModel.song?.songTitle ?? "")"
            self.artistNameLabel.text = "\(self.mViewModel.song?.artistName ?? "")"
            
            self.songImageView.loadImage(imageUrl: self.mViewModel.song?.coverImageURL ?? "", placeHolderName: ImageConstant.noImagePlaceHolder, activiyIndicator: self.songImageActtivityIndicator)
        }
        
    }
    
    
    // MARK: -registerNib
    private func registerNib(){
        // Songs Unlockin Nib
        self.SongAndAlbumUnlockingScreenView = UINib(nibName:    UINibConstant.SongAndAlbumUnlockingScreenNib, bundle: .main).instantiate(withOwner: nil, options: nil).first as? SongAndAlbumUnlockingScreen
        self.SongAndAlbumUnlockingScreenView?.setDelegate(delegate: self)
        self.SongAndAlbumUnlockingScreenView?.setSongObj(song: self.mViewModel.song ?? Song())
        self.SongAndAlbumUnlockingScreenView?.frame = self.view.bounds
        // Failed View Nib
        self.failedView = UINib(nibName:    UINibConstant.paymentFailedNib, bundle: .main).instantiate(withOwner: nil, options: nil).first as? PaymentFailedView
        self.failedView.frame = self.view.bounds
        self.failedView.delegate = self
        // Success View Nib
        self.successView = UINib(nibName:    UINibConstant.paymentSuccessNib, bundle: .main).instantiate(withOwner: nil, options: nil).first as? PaymentSuccessView
        self.successView.frame = self.view.bounds
        self.successView.delegate = self
        
    }
    private func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    private func syncMediaCenterRemoteControlAudioRateTitle(title:String,Rate:Int){
        AudioPlayer.shared.nowPlayingInfo?[MPMediaItemPropertyTitle] = title
        AudioPlayer.shared.nowPlayingInfo?[MPNowPlayingInfoPropertyPlaybackRate] = Rate
        MPNowPlayingInfoCenter.default().nowPlayingInfo =  AudioPlayer.shared.nowPlayingInfo
        
    }
    //MARK: - updateDurationLabel
    private func updateDurationLabel(CurrentTime:String,EndTime:String){
        durationsLabel.text =  CurrentTime
        endingAudioTimeLabel.text = EndTime
    }
    fileprivate func saveDataToSharedPrefrences() {
        UserDefaultUtils.setString(value: self.mViewModel.categoryType ?? "", key: StringConstant.catTypeKey)
        UserDefaultUtils.saveSongData(value: self.mViewModel.song ?? Song(), key: StringConstant.saveSongKey)
        UserDefaultUtils.setInteger(value: AudioPlayer.shared.indexPath ?? -1, key: StringConstant.saveIndexKey)
        UserDefaultUtils.saveSongArray(value:AudioPlayer.shared.songsArray ?? [Song](), key: StringConstant.saveSongArrayKey)
        UserDefaultUtils.setString(value: "\(self.mViewModel.isplayingFromString ?? "")", key: StringConstant.isPlayingFromKey)
        
    }
}
extension SongsDetailViewController:SongsDetailProtocol, ArtistLabelDetailViewControllerProtocol{
    func labelArtist(lblArtist: Artist?) {
        if let Data = lblArtist {
            
            self.mViewModel.artist = Data
        }
    }
    
    func setIsFromMiniPlayer(isFromMiniPlayer: Bool) {
        self.isfromMiniPlayer = isFromMiniPlayer
    }
    
    func setCompleteSongsArray(songsArray: [Song]?) {
        if let songsArray = songsArray {
            self.mViewModel.songsArray = songsArray
        }
    }
    
    func setIndexPath(row: Int?) {
        if let Row = row{
            self.mViewModel.indexPathRow = Row
        }
    }
    
    func setIsPlayingFromString(isplayingFromString:String?) {
        if let string = isplayingFromString{
            self.mViewModel.isplayingFromString = string
        }
    }
    
    func setPlayingCategoryType(categoryType: String?) {
        if let Data = categoryType{
            self.mViewModel.categoryType = Data
        }
    }
    
    func setSongData(song: Song?) {
        if let Data = song{
            print("My Selected Song Data: \(Data)")
            self.mViewModel.song = Data
        }
    }
    
    func setAlbumData(album: Album?) {
        if let Data = album{
            self.mViewModel.album = Data
        }
    }
}


extension SongsDetailViewController:RangeSeekSliderDelegate{
    
    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
        if slider == songRangeSeekSlider{
            
            self.mViewModel.maxValueOfSeekBar = maxValue
            print(maxValue)
            print(self.mViewModel.maxValueOfSeekBar)
            if AudioPlayer.shared.aVPlayer?.currentTime == AudioPlayer.shared.aVPlayer?.duration{
                if  self.mViewModel.indexPathRow != -1 {

                    self.updateDurationLabel(CurrentTime: Utils.secondsToHoursMinutesSecondsString(seconds: Int(self.mViewModel.songsArray?[ self.mViewModel.indexPathRow ?? 0].songDuration ?? "") ?? 0 / 1000), EndTime:self.mViewModel.songsArray?[ self.mViewModel.indexPathRow ?? 0].songDuration ?? "" )
                    self.songRangeSeekSlider.selectedMaxValue = AudioPlayer.shared.aVPlayer?.currentTime ?? 0
                }
            }else{
                if  self.mViewModel.indexPathRow != -1 {
                    self.updateDurationLabel(CurrentTime: Utils.secondsToHoursMinutesSecondsString(seconds: Int((AudioPlayer.shared.aVPlayer?.currentTime ?? 0 ))), EndTime: self.mViewModel.songsArray?[ self.mViewModel.indexPathRow ?? 0].songDuration ?? "")
                    
                }
            }
            
            AudioPlayer.shared.aVPlayer?.currentTime = TimeInterval(Int(maxValue))
            songRangeSeekSlider.selectedMaxValue = maxValue
            songRangeSeekSlider.layoutSubviews()
            //To pause at current time and then play this is to handle the case when user rigorously seek the bar back and forth
            if AudioPlayer.shared.aVPlayer != nil {
                if AudioPlayer.shared.aVPlayer?.isPlaying ?? false{
                    AudioPlayer.shared.aVPlayer?.pause()
                    AudioPlayer.shared.aVPlayer?.play()
                    AudioPlayer.shared.nowPlayingInfo?[MPNowPlayingInfoPropertyPlaybackRate] = 1
                }else{
                    AudioPlayer.shared.nowPlayingInfo?[MPNowPlayingInfoPropertyPlaybackRate] = 0
                }
                
                
                AudioPlayer.shared.nowPlayingInfo?[MPMediaItemPropertyPlaybackDuration] =  AudioPlayer.shared.aVPlayer?.duration
                AudioPlayer.shared.nowPlayingInfo?[MPNowPlayingInfoPropertyElapsedPlaybackTime] =  AudioPlayer.shared.aVPlayer?.currentTime
                
                MPNowPlayingInfoCenter.default().nowPlayingInfo = AudioPlayer.shared.nowPlayingInfo
            }
        }
    }
    
}
extension SongsDetailViewController : AudioPlayerPlayDelegate{
    func onNextOrPreviousSong(song: Song) {
        self.mViewModel.song = song
        self.setUpView()
        self.saveDataToSharedPrefrences()
        
        self.songRangeSeekSlider.selectedMaxValue = 0
        print("\nDelegate Activated\n")
    }
    
    
    
    func playerIsPlaying() {
        
        
       
        DispatchQueue.main.async {
            self.backBTn.isEnabled = true
            self.removeActivityIndicator()
            self.mViewModel.isPlaying = true
            self.playBtn.isEnabled = true
            self.audioVideoSegmentControl.isEnabled = true
            self.playBtn.setImage(UIImage(named: StringConstant.pauseImage), for: .normal)
            
            if !UserDefaultUtils.getBool(key: StringConstant.resumeplayeratCurrentPositionKey){
                self.songRangeSeekSlider.selectedMaxValue = 0
                
            }
            
            let float = CGFloat(((self.mViewModel.songsArray?[ self.mViewModel.indexPathRow ?? 0].songDuration ?? "") as NSString).floatValue)
            
            self.songRangeSeekSlider.selectedMaxValue =  float / 1000
            self.songRangeSeekSlider.layoutSubviews()
            
            
            
            
            
        }
    }
    
    func playerCatchedException() {
        
        self.backBTn.isEnabled = true
        self.playBtn.setImage(UIImage(named: StringConstant.playImage), for: .normal)

        self.audioVideoSegmentControl.isEnabled = true
        Utils.showToast(view: self.view, message: StringConstant.couldntPlayAudioFile)
        self.durationsLabel.text =  "\(StringConstant.zeroTime)"
        self.endingAudioTimeLabel.text = "\(StringConstant.zeroTime)"
        playBtn.isEnabled = false
        
    }
    
    func isUpdatingSeekBarView(currentTime: TimeInterval) {
     
        if AudioPlayer.shared.songsArray?[AudioPlayer.shared.indexPath ?? 0].songStatus == SongStatus.PREMIUM.rawValue{
            
        }else{
        if UserDefaultUtils.getBool(key: StringConstant.resumeplayeratCurrentPositionKey){
            UserDefaultUtils.setBool(value: false, key: StringConstant.resumeplayeratCurrentPositionKey)
            
            AudioPlayer.shared.aVPlayer?.currentTime = TimeInterval(UserDefaultUtils.getInteger(key: StringConstant.audioCurrentStatusBarStatusKey))
            AudioPlayer.shared.aVPlayer?.play()
        }
        }
        
        
        self.songRangeSeekSlider.selectedMinValue = 0
        self.songRangeSeekSlider.maxValue = CGFloat(Float(UserDefaultUtils.getInteger(key: StringConstant.audioFullTimeKey)))
        print(songRangeSeekSlider.selectedMaxValue)
        self.songRangeSeekSlider.layoutSubviews()
        self.mViewModel.maxValueOfSeekBar  = CGFloat(currentTime)
        self.mViewModel.isPlaying = true
        if  self.mViewModel.indexPathRow != -1 {//handling case:If audio file is not selected and play buton is pressed

            let intEndTime = self.mViewModel.songsArray?[ self.mViewModel.indexPathRow ?? 0].songDuration ?? ""
            
            self.updateDurationLabel(CurrentTime: Utils.secondsToHoursMinutesSecondsString(seconds: Int(currentTime)), EndTime: intEndTime)
            
            let float = CGFloat(((self.mViewModel.songsArray?[ self.mViewModel.indexPathRow ?? 0].songDuration ?? "") as NSString).floatValue)

            self.songRangeSeekSlider.maxValue = CGFloat(Float(UserDefaultUtils.getInteger(key: StringConstant.audioFullTimeKey)))
            print(songRangeSeekSlider.maxValue)
        }
        
        self.songRangeSeekSlider.layoutSubviews()
        self.songRangeSeekSlider.layoutIfNeeded()
    }
    
    func stopUpdatingSeekBarView() {
        print("\n\n\nStop Updating Song seekBar\n\n\n")
        self.mViewModel.isPlaying = false
        self.playBtn.setImage(UIImage(named: StringConstant.playImage), for: .normal)
        if  self.mViewModel.indexPathRow != -1{
          
            self.updateDurationLabel(CurrentTime: Utils.self.secondsToHoursMinutesSecondsString(seconds:  Int(self.mViewModel.songsArray?[self.mViewModel.indexPathRow ?? 0].songDuration ?? "") ?? 0), EndTime: self.mViewModel.songsArray?[ self.mViewModel.indexPathRow ?? 0].songDuration ?? "")
            let float = CGFloat(((self.mViewModel.songsArray?[ self.mViewModel.indexPathRow ?? 0].songDuration ?? "") as NSString).floatValue)
            
            self.songRangeSeekSlider.selectedMaxValue = float / 1000
            
            self.songRangeSeekSlider.layoutSubviews()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                
                self.durationsLabel.text =  "\(StringConstant.zeroTime)"
                
                self.songRangeSeekSlider.selectedMaxValue = 0
                self.songRangeSeekSlider.layoutSubviews()
            })
            
            self.songRangeSeekSlider.layoutSubviews()
        }
        self.songRangeSeekSlider.layoutSubviews()
        
        
        
        if self.mViewModel.song?.songStatus == SongStatus.PREMIUM.rawValue{
            if self.mViewModel.categoryType == RecentlyPlayedCatEnum.Albums.rawValue{
                self.SongAndAlbumUnlockingScreenView?.premiumTitle
                = StringConstant.thisIsPremiumAlbum
                self.SongAndAlbumUnlockingScreenView?.premiumDetailText  = StringConstant.unlockPremiumAlbum
                self.view.addSubview(SongAndAlbumUnlockingScreenView!)
            }else{
                self.SongAndAlbumUnlockingScreenView?.premiumTitle
                = StringConstant.thisIsPremiumSong
                self.SongAndAlbumUnlockingScreenView?.premiumDetailText = StringConstant.unlockPremiumSong
                
                self.view.addSubview(SongAndAlbumUnlockingScreenView!)
            }
            
        }else{
            AudioPlayer.shared.playNextSong(playNext: true)
        }
        
        
    }
    
    
    func applicationWillEnterBackgroundObs() {
        print("Did Become Background  Notification OBS")
    }
    
    func applicationWillBecomeActiveObs() {
        print("Did Become Active State Notification OBS")
    }
    
    func onStateChange(isPlaying: Bool, imageName: String) {
        self.mViewModel.isPlaying = isPlaying
        self.playBtn.setImage(UIImage(named: imageName), for: .normal)
        self.delegateMiniPlayer?.setIsPlaying(isPlaying: isPlaying)
       
    }
    
    
    
}
extension SongsDetailViewController:SongDetailProtocolForMiniPlayer{
    func setDelegateForMiniPlayer(delegate: SongDeatilDelegateForMiniPlayer) {
        self.delegateMiniPlayer = delegate
    }
    
    
}

// MARK: -Bindings

extension SongsDetailViewController{
   
    private func bindingServerResponseForGetFavouriteSongs() {
        self.songsAndArtistViewModel.serverResponseForSong.bind {
            if ($0 != nil) {
                
                if $0?.response?.status ?? false {
                    self.removeActivityIndicator()
                    if let favouriteSong = $0?.response?.data?.songs
                    {
                        for i in favouriteSong.indices
                        {
                            if self.mViewModel.song?.songID == favouriteSong[i].songID
                            {
                                self.mViewModel.song = favouriteSong[i]
                                print(self.mViewModel.song)
                                print(self.mViewModel.song?.isFavourit)
                            }
                            else if favouriteSong.count == 0
                            {
                                UserDefaultUtils.setBool(value: false, key: CommonKeys.Favourite_Icon)
                            }
                        }
                    }
                    self.setFvrtBtnImage()
    
                }
                else if $0?.response?.status == false {
                    
                    Utils.showErrorAlert(vc: self, message: $0?.response?.message)
                    self.removeActivityIndicator()
                    
                }
            }
        }
    }
    
    //MARK: - Binding Error Response
    private func bindingServerErrorResponseForGetFavouriteSongs() {
        self.songsAndArtistViewModel.errorResponseForSong.bind {
            if ($0 != nil) {
                Utils.showErrorAlert(vc: self, message: $0?.response?.message)
            }
            
            self.removeActivityIndicator()
        }
        
        
    }
    private func bindingServerResponse() {
        self.mProfileViewModel.serverResponseForFavouriteSong.bind {
            if ($0 != nil) {
                
                if $0?.response?.status ?? false {
                    self.removeActivityIndicator()
                    
                    self.mViewModel.song = self.mViewModel.setIsFavourite(songObj:  self.mViewModel.song ?? Song())
                    self.mViewModel.songbuilder.settype(SongType.FAVORITES.rawValue)
                    self.songsAndArtistViewModel.songs(data: self.mViewModel.songbuilder)
                }
                else if $0?.response?.status == false {
                   
                    Utils.showErrorAlert(vc: self, message: $0?.response?.message)
                    self.removeActivityIndicator()
                    
                }
            }
        }
    }
    
    //MARK: - Binding Error Response
    private func bindingServerErrorResponse() {
        self.mProfileViewModel.errorResponseForFavouriteSong.bind {
            if ($0 != nil) {
                Utils.showErrorAlert(vc: self, message: $0?.response?.message)
            }
            
            self.removeActivityIndicator()
        }
        
        
    }
    
    //MARK: - Binding Complete Response
    private func bindingServerCompletionResponse(){
        self.mProfileViewModel.completion.bind {
            if ($0 != nil) {
                
                
            }
        }
        
    }
    private func bindingServerResponseForSubscribeToPlan() {
        self.songsAndArtistViewModel.serverResponseForSubscribeToPlan.bind {
            if ($0 != nil) {
                
                if $0?.response?.status ?? false {
                    print($0?.response?.data?.purchasedSong)
                    self.mViewModel.purchasedSong = $0?.response?.data?.purchasedSong
                    self.mViewModel.purchasedSongUrl = $0?.response?.data?.purchasedSong?.songAudioURL
                    self.removeActivityIndicator()
                    self.successView
                        .alpha = 0.0
                    self.view.addSubview(self.successView)
                    UIView.animate(withDuration: 0.25) { () -> Void in
                        self.successView.alpha = 1.0
                        self.SongAndAlbumUnlockingScreenView?.isHidden = true
                        self.selectedSongInMediaPlayer()
                    }
                    
                }
                else if $0?.response?.status == false {
                   
                    self.removeActivityIndicator()
                    
                }
            }
        }
    }
    private func bindingServerErrorResponseForSubscribeToPlan() {
        self.songsAndArtistViewModel.errorResponseForForSubscribeToPlan.bind {
            if ($0 != nil) {
                self.failedView.setFailedMsg(errorLbl: StringConstant.Payment_Failed)
                Utils.showErrorAlert(vc: self, message: $0?.response?.message)
                self.failedView
                    .alpha = 0.0
                self.view.addSubview(self.failedView)
                UIView.animate(withDuration: 0.25) { () -> Void in
                    self.failedView.alpha = 1.0
                }
            }
            
            self.removeActivityIndicator()
        }
    }
}
extension SongsDetailViewController:SongAndAlbumUnlockingScreenDelegate{
    func buyNowBtnPressed() {
        let vc = UIStoryboard.init(name: StoryBoardConstant.storyBoardMain, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.subscriptionPlanVC) as? SubscriptionPlanViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    func payNowBtnPressed() {
       fetchProducts()
       self.showActivityIndicator()
    }
    
    func closeBtnPressed() {
        SongAndAlbumUnlockingScreenView?.removeFromSuperview()
    }
    
    
}
extension SongsDetailViewController:ScreenOverlayViewControllerDelegate{
    func setBackSongObjForFavorite(song: Song) {
        self.mViewModel.song = song
    }
    
    
}


extension SongsDetailViewController: ArtistDetailProtocol {
    func setArtistData(artist: Artist?) {
        if let Data = artist {
            self.mViewModel.artist = Data
        }
    }
}

extension SongsDetailViewController: SKProductsRequestDelegate, SKPaymentTransactionObserver {
    func productsRequest (_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        print(response.products.count)
        if let oProducts = response.products.first
        {
            print("product is available")
            print(productIdentifiers)
            self.purchase(aproduct: oProducts)
        }
        else
        {
            Utils.showErrorAlert(vc: self, message: StringConstant.productisNotAvailabe)
    
        }
        
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions {
            switch transaction.transactionState
            {
            case .purchasing:
                print("customer is in process of purchase")
            case .purchased:
                print("Purchased")
                SKPaymentQueue.default().finishTransaction(transaction)
                productIdentifiers.removeAll()
                getPurchaseToken()
                self.songsAndArtistViewModel.subscribeToPlan(data: self.mViewModel.updateParams())
            case .failed:
                     if let error = transaction.error as? SKError {
                         switch error.code {
                         case .unknown:
                             self.failedView.setFailedMsg(errorLbl: StringConstant.Unknown_Case)
                         case .clientInvalid:
                             self.failedView.setFailedMsg(errorLbl: StringConstant.Invalid_Client)
                         case .paymentCancelled:
                             self.failedView.setFailedMsg(errorLbl: StringConstant.Payment_Cancelled)
                         case .paymentInvalid:
                             self.failedView.setFailedMsg(errorLbl: StringConstant.Invalid_Payment)
                         case .paymentNotAllowed:
                             self.failedView.setFailedMsg(errorLbl: StringConstant.Payment_Not_Allowed)
                         case .storeProductNotAvailable:
                             self.failedView.setFailedMsg(errorLbl: StringConstant.Product_Not_Available)
                         case .cloudServicePermissionDenied:
                             self.failedView.setFailedMsg(errorLbl: StringConstant.Cloud_Service_Permission_Denied)
                         case .cloudServiceNetworkConnectionFailed:
                             self.failedView.setFailedMsg(errorLbl: StringConstant.Cloud_Service_Network_Connection_Failed)
                         default:
                             self.failedView.setFailedMsg(errorLbl: StringConstant.Something_Went_Wrong)
                         }
                         SKPaymentQueue.default().finishTransaction(transaction)
                         productIdentifiers.removeAll()
                         self.failedView.alpha = 0.0
                         self.view.addSubview(failedView)
                         UIView.animate(withDuration: 0.25) { () -> Void in
                             self.failedView.alpha = 1.0
                         }
                         self.removeActivityIndicator()
                     }
                
            case .restored:
                self.failedView.setFailedMsg(errorLbl: StringConstant.Restored)
            case .deferred:
                self.failedView.setFailedMsg(errorLbl: StringConstant.Suspended)
            default : break
            }
        }
    }
    
    
    func purchase(aproduct: SKProduct)
    {
        if SKPaymentQueue.canMakePayments()
        {
        let payment = SKPayment(product: aproduct)
        SKPaymentQueue.default().add(self)
        SKPaymentQueue.default().add(payment)
        }
    }
    private func getPurchaseToken()
     {
     if let url = Bundle.main.appStoreReceiptURL,
        let data = try? Data(contentsOf: url) {
           var receiptBase64 = data.base64EncodedString()
         self.mViewModel.purchaseToken = receiptBase64
         
     }
     }
}

extension SongsDetailViewController: FailedViewDelegate {
    func cancel() {
        self.failedView.removeFromSuperview()
    }

}
extension SongsDetailViewController: SuccessViewDelegate {

    func cancelView() {
        self.successView.removeFromSuperview()
    }
}

