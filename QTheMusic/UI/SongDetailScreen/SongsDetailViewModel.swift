//
//  SongsDetailViewModel.swift
//  QtheMusic
//
//  Created by Assad Khan on 28/04/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import Foundation
import UIKit
class SongsDetailViewModel:BaseViewModel{
     
    var categoryType :String?
    var album : Album?
    var song:Song?
    var purchasedSong:SongData?
    var purchasedSongUrl : String? = "url"
    var artist: Artist?
    var songsArray:[Song]?
    var favouriteSong:[Song]?
    var isplayingFromString:String?
    var isPlaying:Bool? = false
    var titleOfAudio = ""
    var maxValueOfSeekBar : CGFloat = 0
    var indexPathRow : Int? = -1
    var purchaseToken = String()
    let builder = SongAndArtistBuilder()
    var songbuilder : SongAndArtistBuilder = SongAndArtistBuilder()
    func insertDataIntoRecentlyPlayed() {
        if let CategoryType = categoryType {
            if  CategoryType == RecentlyPlayedCatEnum.Albums.rawValue {
                mDataManager.insertSongIntoDb(song: song)
                mDataManager.insertAlbumIntoDb(album: album)
            }else if CategoryType == RecentlyPlayedCatEnum.Songs.rawValue {
                mDataManager.insertSongIntoDb(song: song)
            } else if CategoryType == RecentlyPlayedCatEnum.Artists.rawValue {
                mDataManager.insertArtistIntoDb(artist: artist)
                mDataManager.insertSongIntoDb(song: song)
                mDataManager.insertAlbumIntoDb(album: album)
            }
        }
    }
    
    func setIsFavourite(songObj:Song)->Song?{
       return mDataManager.setIsFavourite(songObj: songObj)
    }
    func updateParams() -> Song {
        builder.setpurchaseType("APPLE_PAY")
        builder.setpurchaseToken(purchaseToken)
        builder.setItemType("SONG")
        builder.setSongId(song?.songID)
        builder.setPaidAmount(song?.price)
        song = builder.build()
        return song ?? Song()
    }
}
