//
//  BuyingHistoryViewController.swift
//  QtheMusic
//
//  Created by Muhammad Tanzeel Ur Rehman on 13/01/2023.
//  Copyright © 2023 TechSwivel. All rights reserved.
//

import Foundation
import UIKit

class BuyingHistoryViewController : BaseViewController {
    
    @IBOutlet weak var emptyResponseLbl: UILabel!
    
    @IBOutlet weak var buyingHistorylbl : UILabel!
    
    @IBOutlet weak var amountlbl: UILabel!
    
    @IBOutlet weak var totalSpent: UILabel!
    
    @IBOutlet weak var paymentType: UILabel!{
        didSet{
            paymentType.roundCorner(radius: 8)
            paymentType.layer.borderColor = UIColor.white.cgColor
            paymentType.layer.borderWidth = 1
        }
    }
    
    @IBOutlet weak var buyingHistoryTableView : UITableView!{
        didSet{
            buyingHistoryTableView.delegate = self
            buyingHistoryTableView.dataSource = self
        }
    }
    
    private var mViewModel = BuyingHistoryViewModel()
    private var mProfileViewModel = ProfileViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addGradientBackground()
        setBindigServerResponse()
        registerNib()
       let profile = mViewModel.setParam()
        mProfileViewModel.getBuyingHistory(data: profile)
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.view.addGradientBackground()
        
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        self.popBackNavigationWithTransition()
    }
    
    func registerNib () {
        
        buyingHistoryTableView.register(UINib(nibName: UINibConstant.BuyingSongCellnib, bundle: nil), forCellReuseIdentifier: UINibConstant.BuyingSongCellidentifier)
        
        buyingHistoryTableView.register(UINib(nibName: UINibConstant.BuyingAlbumCellnib, bundle: nil), forCellReuseIdentifier: UINibConstant.BuyingAlbumCellidentifier)
        

    }
    
    private func setBindigServerResponse() {
        
        self.bindingServerResponse()
        self.bindingServerErrorResponse()
        self.bindingServerCompletionResponse()
        
    }
    
}

extension BuyingHistoryViewController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        // update amount 
        var sum = 0.0

        for item in mViewModel.buyinghistory {
                sum += Double(item.price) + Double(item.albumPrice)
            }

        amountlbl.text = "$\(sum)"
        
        if section == 0 {
            return mViewModel.buyinghistory.count
        }else {
            return mViewModel.buyingalbumHistory.count
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            var height:CGFloat = CGFloat()
            if indexPath.section == 0 {
                height = 90
            }
        else if indexPath.section == 1 {
                height = 150
            }
            return height
        }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
           let headerView = UIView()
           headerView.backgroundColor = view.backgroundColor
           return headerView
       }
    
    // Set the spacing between sections
       func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
           return 1
       }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell: BuyingSongCell = tableView.dequeueReusableCell(withIdentifier: UINibConstant.BuyingSongCellidentifier, for: indexPath) as! BuyingSongCell
            cell.setUpCell(data: mViewModel.buyinghistory[indexPath.row])
            return cell
                        
            }
        
        if indexPath.section == 1 {
            
            let cell: BuyingAlbumCell = tableView.dequeueReusableCell(withIdentifier: UINibConstant.BuyingAlbumCellidentifier, for: indexPath) as! BuyingAlbumCell
           
            cell.setUpCell(data: mViewModel.buyingalbumHistory[indexPath.row])
            return cell
            
        }
        
        return UITableViewCell()
        
    }
    
   
    
}
// MARK:  Binding Server Response
extension BuyingHistoryViewController {
    
    private func bindingServerResponse() {
        self.mProfileViewModel.serverResponseForBuyingHistory.bind {
            if ($0 != nil) {
                
                if $0?.response?.status ?? false {
                    self.removeActivityIndicator()
                    
                    self.mViewModel.buyinghistory = $0?.response?.data?.buyinghistory ?? self.mViewModel.buyinghistory
                    self.mViewModel.buyingalbumHistory = $0?.response?.data?.buyinghistory ?? self.mViewModel.buyingalbumHistory
                    self.buyingHistoryTableView.reloadData()
    
                }
                else if $0?.response?.status == false {
                    
                    Utils.showErrorAlert(vc: self, message: $0?.response?.message)
                    self.removeActivityIndicator()
                    
                }
            }
        }
    }
    //MARK: - Binding Error Response
    private func bindingServerErrorResponse() {
        self.mProfileViewModel.errorResponseForBuyingHistory.bind {
            if ($0 != nil) {
                Utils.showErrorAlert(vc: self, message: $0?.response?.message)
            }
            
            self.removeActivityIndicator()
        }
        
        
    }
    
    //MARK: - Binding Complete Response
    private func bindingServerCompletionResponse(){
        self.mProfileViewModel.completion.bind {
            if ($0 != nil) {
                
                
            }
        }
        
    }
}
