//
//  SearchTabViewController.swift
//  QtheMusic
//
//  Created by Assad Khan on 05/04/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import UIKit

class SearchTabViewController: BaseViewController {
    
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var searchView: UIView!{
        didSet{
            searchView.roundCorner(radius: 12)
        }
    }
    
    @IBOutlet weak var recentlyPlayedContainerView: UIView!
    
    @IBOutlet weak var bottomSafeAreaContraint: NSLayoutConstraint!
    
    
    private var recentlyPlayedView:RecentlyPlayedView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadNib()
        self.bottomSafeAreaConstraintForVC = self.bottomSafeAreaContraint
        guard let View = self.recentlyPlayedView else{return}
        View.reloadDataBase()
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        guard let View = self.recentlyPlayedView else{return}
        View.reloadDataBase()
        
        
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        backgroundView.addGradientBackground()
    }
    
    @IBAction func searchBtnPressed(_ sender: Any) {
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.fade
        self.navigationController?.view.layer.add(transition, forKey: nil)
        
        let vc = UIStoryboard.init(name: StoryBoardConstant.storyBoardMain, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.searchingScreenVC) as? SearchingScreenViewController
        self.navigationController?.pushViewController(vc!, animated: false)
    }
    
    private func loadNib(){
        //RecentlyPlayed View
        self.recentlyPlayedView = UINib(nibName: UINibConstant.RecentlyPlayedView, bundle: .main).instantiate(withOwner: nil, options: nil).first as? RecentlyPlayedView
        self.recentlyPlayedView.frame = self.recentlyPlayedContainerView.bounds
        self.recentlyPlayedView.navigationController = self.navigationController
        self.recentlyPlayedView.tabbarViewController = self.tabBarController as? CustomTabbarController
        self.recentlyPlayedView.vc = self
        self.recentlyPlayedContainerView.addSubview(recentlyPlayedView)
        
    }
}
