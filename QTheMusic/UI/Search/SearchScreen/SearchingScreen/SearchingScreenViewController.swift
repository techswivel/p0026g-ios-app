//
//  SearchingScreenViewController.swift
//  QtheMusic
//
//  Created by Assad Khan on 05/04/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import UIKit
import SkeletonView

class SearchingScreenViewController: BaseViewController {
    @IBOutlet weak var searchTextField: UITextField!
    
    
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var searchBackgroundView: UIView!{
        didSet{
            searchBackgroundView.roundCorner(radius: 12)
        }
    }
    
    @IBOutlet weak var searchCloseBtn: UIButton!
    @IBOutlet weak var languageCollectionView: UICollectionView!{
        didSet{
            languageCollectionView.dataSource = self
            languageCollectionView.delegate = self
        }
    }
    @IBOutlet weak var searchedSongsTableView: UITableView!
    private var timer : Timer!
     
    
    
    
    private var mSongAndArtistViewModel = SongAndArtistViewModel()
    private var mViewModel = SearchingScreenViewModel()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUpView()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        searchedSongsTableView.isSkeletonable = true
        
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        backgroundView.addGradientBackground()
        view.layoutSkeletonIfNeeded()
    }
    @IBAction func backBtnPresssed(_ sender: Any) {
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.fade
        self.navigationController?.view.layer.add(transition, forKey: nil)
        if #available(iOS 13.0, *) {
            self.navigationController?.popViewController(animated: false)
        }else{
            self.navigationController?.popViewController(animated: false)
        }
        
        
        self.view.endEditing(true)
    }
    
    @IBAction func closeBtnPressed(_ sender: Any) {
        
        
        searchTextField.text = ""
        searchCloseBtn.isHidden = true
    }
    
    
    
    
    
    
    private func setUpView(){
        self.registerNib()
        
        self.bindingServerResponse()
        self.bindingServerErrorResponse()
        self.bindingServerCompletionResponse()
        
        
        searchTextField.addTarget(self, action: #selector(self.textFieldDidChange), for: UIControl.Event.editingChanged)
        
        searchedSongsTableView.dataSource = self
        searchedSongsTableView.delegate = self
        searchTextField.delegate  = self
        
        
        searchTextField.becomeFirstResponder()
        
        
        
        
    }
    
    
    private func registerNib(){
        
        
        languageCollectionView.register(UINib(nibName: UINibConstant.InterestCollectionViewCellNib, bundle: nil), forCellWithReuseIdentifier: UINibConstant.InterestCollectionViewCellIdentifier)
        
        searchedSongsTableView.register(UINib(nibName: UINibConstant.SearchingSongTableViewCellNib, bundle: nil),forCellReuseIdentifier: UINibConstant.SearchSongsCellIdentifier)
        
    }
}
// MARK: -CollectionView Delegate
extension SearchingScreenViewController : UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.mViewModel.languagesArray?.count ?? 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: UINibConstant.InterestCollectionViewCellIdentifier, for: indexPath) as! InterestCollectionViewCell
        
        
        if self.mViewModel.isAnimatableLanguages {
            if !(self.mViewModel.languagesArray?.isEmpty ?? true){
                cell.hideAnimation()
                cell.setUpCellForLanguages(language: self.mViewModel.languagesArray?[indexPath.row])
                
            }else{
                cell.showAnimation()
            }
            
        }else{
            
        }
        
        
        return cell
        
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let string = NSString(string: self.mViewModel.languagesArray?[indexPath.row].languageTitle ?? "" )
        let font = FontConstants.getManropeRegular(size: 12)
        let size = string.size(withAttributes: [NSAttributedString.Key.font : font])
        
        return CGSize(width: (size.width + 25), height: 30)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if self.mViewModel.languagesArray?[indexPath.row].isSelected == true {
            
        }else{
            if let data = self.mViewModel.languagesArray {
                for items in data{
                    items.isSelected = false
                }
            }
            
            self.mViewModel.languagesArray?[indexPath.row].isSelected = true
            self.mViewModel.languageId = self.mViewModel.languagesArray?[indexPath.row].languageID
            
            
            let builder = SongAndArtistBuilder()
            builder.setQuery(self.searchTextField.text)
            builder.setLanguageId(self.mViewModel.languageId)
            self.mSongAndArtistViewModel.searchSong(data: builder)
            
            self.mViewModel.isAnimatable = true
            self.mViewModel.isAnimatableLanguages = true
            
            self.mViewModel.songArray = nil
            self.searchedSongsTableView.reloadData()
        }
        self.languageCollectionView.reloadData()
    }
}
// MARK: - Table View Delegate
extension SearchingScreenViewController:UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.mViewModel.songArray?.count ?? 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: UINibConstant.SearchSongsCellIdentifier,for: indexPath) as! SearchingSongTableViewCell
        cell.selectionStyle = .none
        if self.mViewModel.isAnimatableLanguages {
            if !(self.mViewModel.songArray?.isEmpty ?? true){
                cell.hideAnimation()
                cell.setUpCell(data: self.mViewModel.songArray?[indexPath.row])
                
            }else{
                cell.showAnimation()
            }
            
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.mViewModel.songArray?[indexPath.row].type == SearchSongType.ALBUM.rawValue{
            let vc = UIStoryboard.init(name: StoryBoardConstant.storyBoardMain, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.AlbumDetailVc) as? AlbumDetailViewController
            let builder = SongAndArtistBuilder()
            builder.setalbumId(self.mViewModel.songArray?[indexPath.row].albumID)
            builder.setalbumTitle(self.mViewModel.songArray?[indexPath.row].albumTitle)
            builder.setalbumCoverImageURL(self.mViewModel.songArray?[indexPath.row].albumCoverImageURL)
            builder.setalbumStatus(self.mViewModel.songArray?[indexPath.row].albumStatus)
            builder.setNumberOfSongs(self.mViewModel.songArray?[indexPath.row].numberOfSongs)
            builder.setTimeStamp(Date().timeIntervalSince1970)
            vc?.setSongData(data: builder)
            vc?.setAlbumData(data: builder.build())
            self.addTransitionOnNavigationControler()
            self.navigationController?.pushViewController(vc!, animated: false)
        }else if self.mViewModel.songArray?[indexPath.row].type == SearchSongType.ARTIST.rawValue{
            let vc = UIStoryboard.init(name: StoryBoardConstant.storyBoardMain, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.ArtistDetailViewController) as? ArtistDetailViewController
            vc?.setArtistId(artistId: self.mViewModel.songArray?[indexPath.row].artistID)
            self.addTransitionOnNavigationControler()
            self.navigationController?.pushViewController(vc!, animated: false)
        }else{
            let vc = UIStoryboard.init(name: StoryBoardConstant.storyBoardMain, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.SongsDetailViewController) as? SongsDetailViewController
            vc?.setPlayingCategoryType(categoryType: RecentlyPlayedCatEnum.Songs.rawValue)
            vc?.setSongData(song: self.mViewModel.songArray?[indexPath.row])
            vc?.setIndexPath(row: indexPath.row)
            vc?.setCompleteSongsArray(songsArray: self.mViewModel.songArray)
            vc?.setIsPlayingFromString(isplayingFromString: "\(StringConstant.playingFrom)SEARCHED")
            self.addTransitionOnNavigationControler()
            self.navigationController?.pushViewController(vc!, animated: false)
        }
       
    }
    
    
}
extension SearchingScreenViewController{
    
    //MARK: - Binding Server Response
    private func bindingServerResponse() {
        self.mSongAndArtistViewModel.serverResponseForSearchSong.bind {
            if ($0 != nil) {
                
                if $0?.response?.status ?? false {
                    
                    
                    defer{
                        self.languageCollectionView.reloadData()
                        self.searchedSongsTableView.reloadData()
                    }
                    
                    
                    if !($0?.response?.data?.Languages?.isEmpty ?? true){
                        
                        self.mViewModel.languagesArray = $0?.response?.data?.Languages ?? [Language]()
                        self.mViewModel.languagesArray? .insert(Language(languageID: nil, languageTitle: StringConstant.all, isSelected: true), at: 0)
                    }
                    
                    self.mViewModel.songArray =  $0?.response?.data?.searchResults ?? [Song]()
                }
                else if $0?.response?.status == false {
                    print("Response for searching song is false")
                    Utils.showErrorAlert(vc: self, message: $0?.response?.message)
                    self.removeActivityIndicator()
                    
                }
            }
        }
    }
    
    //MARK: - Binding Error Response
    private func bindingServerErrorResponse() {
        self.mSongAndArtistViewModel.errorResponseForSearchSong.bind {
            if ($0 != nil) {
                Utils.showErrorAlert(vc: self, message: $0?.response?.message)
            }
            
            self.removeActivityIndicator()
        }
        
        
    }
    
    //MARK: - Binding Complete Response
    private func bindingServerCompletionResponse(){
        self.mSongAndArtistViewModel.completion.bind {
            if ($0 != nil) {
                
                
            }
        }
        
    }
}
// MARK: -UITextFieldDelegate
extension SearchingScreenViewController:UITextFieldDelegate{
    
    @objc func textFieldDidChange(textField:UITextField)
    {
        self.searchQuery()
        if  textField.text?.trimmingCharacters(in: .whitespacesAndNewlines) != ""
        {
            searchCloseBtn.isHidden = false
        }else{
            searchCloseBtn.isHidden = true
        }
        
    }
    
    
    func searchQuery(){
        if searchTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
            var count = 0
            if timer != nil {
                timer.invalidate()
            }
            
            self.timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { [weak self] (timer) in
                
                if self == nil {
                    return
                }
                count = count + 1
                
                if count == 2{
                    timer.invalidate()
                    
                    self?.mViewModel.isAnimatable = true
                    
                    if self?.mViewModel.languageId == nil{
                        
                        self?.mViewModel.isAnimatableLanguages = true
                        self?.mViewModel.languagesArray = nil
                        self?.languageCollectionView.reloadData()
                    }
                    
                    self?.mViewModel.songArray = nil
                    self?.searchedSongsTableView.reloadData()
                    
                    let builder = SongAndArtistBuilder()
                    builder.setQuery(self?.searchTextField.text)
                    builder.setLanguageId(self?.mViewModel.languageId)
                    self?.mSongAndArtistViewModel.searchSong(data: builder)
                    
                    print("Language id is\(self?.mViewModel.languageId)")
                    
                }
            }
            
        }
    }
}

