//
//  SearchingScreenViewModel.swift
//  QtheMusic
//
//  Created by Assad Khan on 06/04/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import Foundation

class SearchingScreenViewModel:BaseViewModel{
    
    var languagesArray : [Language]?
    var songArray :[Song]?
    var languageId :Int? = nil
    var isAnimatable = false
    var isAnimatableLanguages = false
    
    
}
