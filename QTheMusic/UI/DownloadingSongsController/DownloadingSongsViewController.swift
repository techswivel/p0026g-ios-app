//
//  DownloadingSongsViewController.swift
//  QtheMusic
//
//  Created by Muhammad Tanzeel Ur Rehman on 13/01/2023.
//  Copyright © 2023 TechSwivel. All rights reserved.
//

import Foundation
import UIKit

class DownloadingSongsViewController : BaseViewController {
   

    @IBOutlet weak var downloadingLbl : UILabel!
    
    @IBOutlet weak var downloadingTableView : UITableView!{
        didSet{
            downloadingTableView.delegate = self
            downloadingTableView.dataSource = self
        }
    }
       

    private var selectedCat:String?
    private var mViewModel = DownloadingSongsViewModel()
    private var mSongAndArtistViewModel = SongAndArtistViewModel()
    private var mdownloadManagerViewModel = DownloadManagerViewModel()
    internal var tabbarViewController: CustomTabbarController?
    private var downloadScreenNib : DownloadScreen?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addGradientBackground()
        setBindigServerResponse()
        
        downloadingTableView.register(UINib(nibName: UINibConstant.DownloadingSongsCellnib, bundle: nil), forCellReuseIdentifier: UINibConstant.DownloadingSongsCellidentifier)
        
        mViewModel.songBuilder.settype(SongType.DOWNLOADED.rawValue)
        showActivityIndicator()
        mSongAndArtistViewModel.songs(data: mViewModel.songBuilder)
        setUpDownloadNib()
    
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.view.addGradientBackground()
        
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        self.popBackNavigationWithTransition()
    }
    
    private func setBindigServerResponse() {
        
        self.bindingServerResponse()
        self.bindingServerErrorResponse()
        self.bindingServerCompletionResponse()
        
    }
    
    private func setUpDownloadNib(){
        self.downloadScreenNib = UINib(nibName:    UINibConstant.DownloadScreenNib, bundle: .main).instantiate(withOwner: nil, options: nil).first as? DownloadScreen
        self.downloadScreenNib?.setDelegate(delegate: self)
        self.downloadScreenNib?.frame = self.view.bounds
    }
    
}


extension DownloadingSongsViewController: DownloadScreenDelegate {
    
    func audioBtnPressed() {
        let currentCell = tableView(self.downloadingTableView, cellForRowAt: mViewModel.selectedIndex ?? IndexPath()) as! DownloadingSongsCell
        currentCell.startAudioDownloading()
        self.downloadScreenNib?.removeFromSuperview()
    }
    
    func videoBtnPressed() {
        let currentCell = tableView(self.downloadingTableView, cellForRowAt: mViewModel.selectedIndex ?? IndexPath()) as! DownloadingSongsCell
        currentCell.startVideodownloading()
        self.downloadScreenNib?.removeFromSuperview()
    }
    
    func removeDownloadScreen() {
        
    }
}

extension DownloadingSongsViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mViewModel.songsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = downloadingTableView.dequeueReusableCell(withIdentifier: UINibConstant.DownloadingSongsCellidentifier, for: indexPath) as! DownloadingSongsCell
        if self.mViewModel.songsArray.count != 0 {
            cell.showAnimation()
            DispatchQueue.main.asyncAfter(deadline: .now() + 4.0) {
                cell.hideAnimation()
                cell.setUpCell(data: self.mViewModel.songsArray[indexPath.row])
             }
        }
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = mViewModel.songsArray[indexPath.row]
        
        if item.localPath != nil {
           
            self.tabbarViewController?.setPlayingCategoryType(categoryType: self.selectedCat)
            self.tabbarViewController?.setSongData(song: self.mViewModel.songsArray[indexPath.row])
            self.tabbarViewController?.setIndexPath(row: indexPath.row)
            self.tabbarViewController?.setCompleteSongsArray(songsArray: self.mViewModel.songsArray)
            self.tabbarViewController?.setIsPlayingFromString(isplayingFromString: "\(StringConstant.playingFrom)DOWNLOADED")
            
            //Setting up Next and previous Audio Linkages
            AudioPlayer.shared.songsArray = self.mViewModel.songsArray
            AudioPlayer.shared.indexPath = indexPath.row
            
        } else {
            self.mViewModel.selectedIndex = indexPath
            self.mViewModel.song = item
            self.downloadScreenNib?.setSongObj(song: item)
            self.view.addSubview(self.downloadScreenNib!)
            
        }
    }
    
}
 // MARK: - Bindingserver response
extension DownloadingSongsViewController{
    
    private func bindingServerResponse() {
        self.mSongAndArtistViewModel.serverResponseForSong.bind {
            if ($0 != nil) {
                
                if $0?.response?.status ?? false {
                    self.removeActivityIndicator()
                    
                    self.mViewModel.songsArray = $0?.response?.data?.songs ?? self.mViewModel.songsArray
                    self.downloadingTableView.reloadData()
    
                }
                else if $0?.response?.status == false {
                    
                    Utils.showErrorAlert(vc: self, message: $0?.response?.message)
                    self.removeActivityIndicator()
                    
                }
            }
        }
    }
    //MARK: - Binding Error Response
    private func bindingServerErrorResponse() {
        self.mSongAndArtistViewModel.errorResponseForSong.bind {
            if ($0 != nil) {
                Utils.showErrorAlert(vc: self, message: $0?.response?.message)
            }
            
            self.removeActivityIndicator()
        }
        
        
    }
    
    //MARK: - Binding Complete Response
    private func bindingServerCompletionResponse(){
        self.mSongAndArtistViewModel.completion.bind {
            if ($0 != nil) {
                
                
            }
        }
        
    }
    
}
