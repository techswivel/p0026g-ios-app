//
//  DownloadingSongsViewModel.swift
//  QtheMusic
//
//  Created by Muhammad Tanzeel Ur Rehman on 13/01/2023.
//  Copyright © 2023 TechSwivel. All rights reserved.
//

import Foundation
import UIKit

class DownloadingSongsViewModel: BaseViewModel {
    
    var song:Song?
    
    var albumsArray  = [Album]()
    var songsArray = [Song]()
    var songBuilder : SongAndArtistBuilder = SongAndArtistBuilder()
    
    var selectedIndex: IndexPath?
    
    var localAudioPath : String?
    var localVideoPath : String?
    
    var navController:UINavigationController?
    
    func getSongsDataFromDb(){
        if let Data = mDataManager.getSongsDataFromDb(){
            self.songsArray = Data
        }
    }
}
