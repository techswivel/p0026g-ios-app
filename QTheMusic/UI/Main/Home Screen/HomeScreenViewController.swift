//
//  HomeScreenViewController.swift
//  QtheMusic
//
//  Created by Hammad Hassan on 05/04/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import UIKit
import SkeletonView
class HomeScreenViewController: BaseViewController {

   
    @IBOutlet weak var recommendedContainerView: UIView!
    @IBOutlet weak var yourMoodContainerView: UIView!
    @IBOutlet weak var trendingSongsContainerView: UIView!
    @IBOutlet weak var bottomConstraintFromSafeArea: NSLayoutConstraint!
    
    private var mAppStoreViewmodel: AppStoreViewModel = AppStoreViewModel()
    
    private var recommendedForYouView : RecommendedForYouView!
    private var yourMoodView : YourMoodView!
    private var trendingSongsView : TrendingSongsView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadNibViews()
        self.bottomSafeAreaConstraintForVC = self.bottomConstraintFromSafeArea
        self.bindAppStoreSuccessResponse()
        self.bindAppStoreErrorResponse()
        self.bindAppStoreCompleteResponse()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.view.addGradientBackground()
      
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        recommendedForYouView.recommendedCallBack()
        trendingSongsView.trendingCallBack()
        var receiptData: String? {
            guard
                let url = Bundle.main.appStoreReceiptURL,
                let data = try? Data(contentsOf: url)
            else { return nil }
            return data.base64EncodedString()
        }
        
        if receiptData != nil{
            let userData = UserDefaultUtils.getProfile(key: CommonKeys.KEY_PROFILE)
            if userData?.subscription != nil{
                mAppStoreViewmodel.getSubscriptionStatus(purchaseToken: receiptData ?? "")
            }
        }else{
            debugPrint("Receipt data is nill")
        }
        
    }
    
    private func loadNibViews(){
        //Recommended For You
        self.recommendedForYouView = UINib(nibName: UINibConstant.RecommendedForYouView, bundle: .main).instantiate(withOwner: nil, options: nil).first as? RecommendedForYouView
        self.recommendedForYouView.frame = self.recommendedContainerView.bounds
        self.recommendedForYouView.setDelegate(delegate: self)
        
        self.recommendedForYouView.vc = self
        self.recommendedForYouView.navigationController = self.navigationController
        self.recommendedForYouView.tabbarViewController = self.tabBarController as? CustomTabbarController
        
        self.recommendedContainerView.addSubview(recommendedForYouView)
        
        
        
        //Your Mood View
        self.yourMoodView = UINib(nibName: UINibConstant.YourMoodView, bundle: .main).instantiate(withOwner: nil, options: nil).first as? YourMoodView
        self.yourMoodView.frame = self.yourMoodContainerView.bounds
        self.yourMoodView.setDelegate(delegate: self)
        self.yourMoodContainerView.addSubview(yourMoodView)
        
        
        //Trending Songs View
        self.trendingSongsView = UINib(nibName: UINibConstant.TrendingSongsView, bundle: .main).instantiate(withOwner: nil, options: nil).first as? TrendingSongsView
        self.trendingSongsView.frame = self.trendingSongsContainerView.bounds
        self.trendingSongsView.setDelegate(delegate: self)
        self.trendingSongsView.setNavigationController(navCon: self.navigationController)
        
        self.trendingSongsView.navigationController = self.navigationController
        self.trendingSongsView.tabbarViewController = self.tabBarController as? CustomTabbarController
        self.trendingSongsView.vc = self
        
        self.trendingSongsContainerView.addSubview(trendingSongsView)
        
        
    }
    
    private func performCategoryDetailScreen(category: Category?) {
        let vc = UIStoryboard.init(name: StoryBoardConstant.storyBoardMain, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.CategoryDetailVC) as? CategoryDetailViewController
        vc?.setCategoryData(category: category)
        self.navigationController?.pushViewController(vc!, animated: false)
    }
    
    

}

//MARK: - Recommended For You View Delegate
extension HomeScreenViewController : RecommendedForYouDelegate {
    func recommendedSongsErrorObserver(error: String?) {
        Utils.showErrorAlert(vc: self, message: error)
    }
    
    func recommendedSongsExpiryObserver() {
        self.performExpirationInCaseOfBoxBinding()
    }
    
    
}

//MARK: -  YourMood View Delegates
extension HomeScreenViewController : YourMoodViewDelegate {
    func selectedCategory(category: Category?) {
        self.performCategoryDetailScreen(category: category)
    }
    
    func yourMoodErrorResponse(error: String?) {
        Utils.showErrorAlert(vc: self, message: error)
    }
    
    func youMoodExpiryResponse() {
        self.performExpirationInCaseOfBoxBinding()
    }
    
    
}

//MARK: - Trending Songs View Delegates
extension HomeScreenViewController : TrendingSongsDelegate {
    func trendingSongsErrorResponse(error: String?) {
        Utils.showErrorAlert(vc: self, message: error)
    }
    
    func trendingSongsExpiryReponse() {
        self.performExpirationInCaseOfBoxBinding()
    }
    
    
}

//MARK: - Subscription Status checking
extension HomeScreenViewController {
    private func bindAppStoreSuccessResponse(){
        self.mAppStoreViewmodel.successSubscriptionStatus.bind{
            if $0 != nil{
                switch $0?.status{
                case 0:
                    // success
                    self.handleAppStoreResponse(response: $0)
                    break
                case 21000:
                    // other then post request
                    debugPrint("Response fail for store Api code 2100: \($0?.status)")
                    break
                case 21001:
                    // invalid token
                    debugPrint("Response fail for store Api code 21001: \($0?.status)")
                    break
                default:
                    debugPrint("Response fail for store Api code default: \($0?.status)")
                    break
                }
            }
        }
    }
    
    
    private func bindAppStoreErrorResponse(){
        self.mAppStoreViewmodel.errorSubscriptionStatus.bind{
            if $0 != nil{
                Utils.showErrorAlert(vc: self, message: StringConstant.ErrorAlert)
            }
        }
    }
    
    private func bindAppStoreCompleteResponse(){
        self.mAppStoreViewmodel.completion.bind{
            if $0 != nil{
                debugPrint("complete Api request ")
            }
        }
    }
    
    
    private func handleAppStoreResponse(response:AppStoreResponse?){
        let now = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss VV"
        let expire = formatter.date(from: response?.latestReceiptInfo?.first?.expiresDate ?? "") as NSDate?
        if response?.pendingRenewalInfo?.first?.autoRenewStatus == SubscriptionStatus.ACTIVE.value {
           
            print("User has active subscription")
        }else if response?.pendingRenewalInfo?.first?.isInBillingRetryPeriod == SubscriptionGracePeriod.ACTIVE.value {
           
            print("User has active subscription but in grace period ")
        }
      //  else if now <= expire! as Date {
           
           // print("User has deActive subscription but remain his time ")
       // }
        else {
            let data = UserDefaultUtils.getProfile(key: CommonKeys.KEY_PROFILE)
            data?.subscription = nil
            UserDefaultUtils.saveProfile(value: data ?? AuthorizationBuilder().build(), key: CommonKeys.KEY_PROFILE)
            print("User subscription has been deActive ")
        }
    }
    
    
}
