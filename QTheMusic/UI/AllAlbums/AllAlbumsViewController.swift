//
//  AllAlbumsViewController.swift
//  QtheMusic
//
//  Created by Assad Khan on 13/05/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import UIKit



class AllAlbumsViewController: BaseViewController {

    
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var albumsCollectionView: UICollectionView!{
        
        didSet{
        albumsCollectionView.dataSource = self
        albumsCollectionView.delegate = self
        }
        
    }
    @IBOutlet weak var artistNameLabel: UILabel!
    
    internal var albumsArray :[Album]?{
        didSet{
            self.mViewModel.albumsArray = albumsArray
            
        }
    }
    internal var artistName:String?{
        didSet{
            self.mViewModel.artistName = artistName
        }
    }
    private var collectionViewFlowLayout : UICollectionViewFlowLayout!
    
   private var mViewModel = AllAlbumsViewModel()
  
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerNibs()
        self.albumsCollectionView.reloadData()
        
        self.artistNameLabel.text = "\(self.mViewModel.artistName ?? "")'s Album"
        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        backgroundView.frame = self.view.frame
        self.backgroundView.addGradientBackground()
        setupFlowLayout()
        view.layoutSkeletonIfNeeded()
    }
    
    
    
    
    @IBAction func backBtnPressed(_ sender: Any) {
        self.popBackNavigationWithTransition()
    }
    // MARK: - RegisterNibs
    private func registerNibs(){
        
        albumsCollectionView.register(UINib(nibName: UINibConstant.AlbumCollectionViewCellNib, bundle: nil), forCellWithReuseIdentifier: UINibConstant.AlbumCollectionViewCellIdentifier)
        
    }
    private func setupFlowLayout(){
        
        if collectionViewFlowLayout == nil{
            let width = ((self.view.frame.width - (16 + 50)) / 2)
            let height = albumsCollectionView.frame.width * 0.62
            //using two as two column are to be presented
            collectionViewFlowLayout = UICollectionViewFlowLayout()
            collectionViewFlowLayout.itemSize = CGSize(width: width, height: height)
            
            collectionViewFlowLayout.scrollDirection = .vertical
            collectionViewFlowLayout.minimumLineSpacing = 8
            collectionViewFlowLayout.minimumInteritemSpacing = 2
            albumsCollectionView.setCollectionViewLayout(collectionViewFlowLayout, animated: true)
        }
    }
}
extension AllAlbumsViewController:UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.mViewModel.albumsArray?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: UINibConstant.AlbumCollectionViewCellIdentifier, for: indexPath) as! AlbumCollectionViewCell
        cell.setUpCell(data: self.mViewModel.albumsArray?[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.addTransitionOnNavigationControler()
        let vc = UIStoryboard.init(name: StoryBoardConstant.storyBoardMain, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.AlbumDetailVc) as? AlbumDetailViewController
        let builder = SongAndArtistBuilder()
        builder.setalbumId(self.mViewModel.albumsArray?[indexPath.row].albumID)
        builder.setalbumTitle(self.mViewModel.albumsArray?[indexPath.row].albumTitle)
        builder.setalbumCoverImageURL(self.mViewModel.albumsArray?[indexPath.row].albumCoverImageURL)
        builder.setalbumStatus(self.mViewModel.albumsArray?[indexPath.row].albumStatus)
        builder.setNumberOfSongs(self.mViewModel.albumsArray?[indexPath.row].numberOfSongs)
        builder.setTimeStamp(Date().timeIntervalSince1970)
        vc?.setSongData(data: builder)
        vc?.setAlbumData(data: builder.build())
        vc?.setAlbum(data: self.mViewModel.albumsArray?[indexPath.row])
        self.navigationController?.pushViewController(vc!, animated: false)
    }
}
