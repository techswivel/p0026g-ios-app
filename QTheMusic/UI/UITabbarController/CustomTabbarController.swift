//
//  CustomTabbarController.swift
//  QtheMusic
//
//  Created by Assad Khan on 27/07/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import UIKit

class CustomTabbarController: UITabBarController {
    
    
    // 1
       internal var miniPlayer: MiniPlayerViewController = {
            var viewControllerStoryboardId = StoryBoardConstant.miniPlayerVC
            var storyboard = StoryBoardConstant.mainStoryBoard
            let viewController = storyboard.instantiateViewController(withIdentifier: viewControllerStoryboardId) as UIViewController?
            
            viewController?.view.translatesAutoresizingMaskIntoConstraints = false
            return viewController as! MiniPlayerViewController
        }()
        
       internal var containerView: UIView = {
            let uiView = UIView()
            uiView.translatesAutoresizingMaskIntoConstraints = false
            uiView.backgroundColor = .blue
            return uiView
        }()
    internal var isMiniPlayerHidden = false
    internal var mViewModel = CustomTabbarViewModel()
    private var isFromMiniPlayer = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        restoreMiniPlayerFromSharedPrefrences()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
       print("Tabbar view did appear")
        miniPlayer.setIsPlayingFromMiniPlayer(isFromMiniPlayerVC: true)
        miniPlayer.setDelegateToVCAndSetUpViewOnAppearing()
        
    }
    
    
    internal func addMiniPlayer(){
        //making it false when song other than resumed media player song is played
        UserDefaultUtils.setBool(value: false, key: StringConstant.resumeplayeratCurrentPositionKey)
        self.addChildView()
        self.setConstraintsAboveTabBar()
        self.isMiniPlayerHidden = false
       
        
    }
    
    
    internal func removeMiniPlayer(){
        self.containerView.removeFromSuperview()
        self.miniPlayer.removeFromParent()
        self.isMiniPlayerHidden =  true
        playerResumeSetting()
        
        
    }
    internal func playerResumeSetting(){
        UserDefaultUtils.setBool(value: false, key: StringConstant.isPlayingKey)
        UserDefaultUtils.setBool(value: false, key: StringConstant.ResumingMiniPlayerKey)
    }
    internal func addMiniPlayerBelowTabBar(){
        self.isMiniPlayerHidden = false
        self.containerView.removeFromSuperview()
        self.miniPlayer.removeFromParent()
        self.addChildView()
        self.setConstraintsBelowTabBar()
    }
    fileprivate func restoreMiniPlayerFromSharedPrefrences() {
        if UserDefaultUtils.isValueNil(key: StringConstant.isPlayingKey) != nil{
            if UserDefaultUtils.getBool(key: StringConstant.isPlayingKey){
                
                UserDefaultUtils.setBool(value: true, key: StringConstant.ResumingMiniPlayerKey)
                
            
                miniPlayer.setPlayingCategoryType(categoryType:UserDefaultUtils.getString(key: StringConstant.catTypeKey))
                miniPlayer.setSongData(song: UserDefaultUtils.getSavedSongData(key: StringConstant.saveSongKey))
                miniPlayer.setIndexPath(row: UserDefaultUtils.getInteger(key: StringConstant.saveIndexKey))
                miniPlayer.setCompleteSongsArray(songsArray: UserDefaultUtils.getSavedSongArray(key: StringConstant.saveSongArrayKey))
                miniPlayer.setIsPlayingFromString(isplayingFromString: UserDefaultUtils.getString(key: StringConstant.isPlayingFromKey))
                miniPlayer.setTabBarVC(tabBarVC: self)
                miniPlayer.setDelegate(delegate: self)
                miniPlayer.resumingMiniPlayer()
                self.mViewModel.categoryType = self.mViewModel.categoryType
                self.mViewModel.song = UserDefaultUtils.getSavedSongData(key: StringConstant.saveSongKey)
                self.mViewModel.indexPathRow = UserDefaultUtils.getInteger(key: StringConstant.saveIndexKey)
                self.mViewModel.songsArray = UserDefaultUtils.getSavedSongArray(key: StringConstant.saveSongArrayKey)
                self.mViewModel.isplayingFromString = UserDefaultUtils.getString(key: StringConstant.isPlayingFromKey)
                
                AudioPlayer.shared.playingFileName = self.mViewModel.song?.songTitle
                AudioPlayer.shared.playingAlbumName = self.mViewModel.song?.albumName
               
                AudioPlayer.shared.songsArray =  self.mViewModel.songsArray
                AudioPlayer.shared.indexPath = self.mViewModel.indexPathRow
                AudioPlayer.shared.isPlayingFrom = self.mViewModel.isplayingFromString
                if let imageUrl = URL(string: self.mViewModel.song?.coverImageURL ?? ""){
                self.getData(from: imageUrl ) { data, response, error in
                        guard let data = data, error == nil else { return }
                        DispatchQueue.main.async() { [weak self] in
                            AudioPlayer.shared.PlayingFileImage = UIImage(data: data)
                        }
                }
                }
                view.addSubview(containerView)
                
                addChild(miniPlayer)
               
                containerView.addSubview(miniPlayer.view)
                miniPlayer.didMove(toParent: self)
                
                
                self.setConstraintsAboveTabBar()
                self.isMiniPlayerHidden = false
                
                //find index of miniPlayer in TabBar's viewControllers array
                //and remove it.
                if let childIndex = viewControllers?.firstIndex(of: miniPlayer) {
                    viewControllers?.remove(at: childIndex)
                }
            }
            
        }
    }
    fileprivate func saveDataToSharedPrefrences() {
        UserDefaultUtils.setString(value: self.mViewModel.categoryType ?? "", key: StringConstant.catTypeKey)
        UserDefaultUtils.saveSongData(value: self.mViewModel.songsArray?[self.mViewModel.indexPathRow ?? 0] ?? Song(), key: StringConstant.saveSongKey)
        UserDefaultUtils.setInteger(value: self.mViewModel.indexPathRow ?? -1, key: StringConstant.saveIndexKey)
        UserDefaultUtils.saveSongArray(value:self.mViewModel.songsArray ?? [Song](), key: StringConstant.saveSongArrayKey)
        UserDefaultUtils.setString(value: "\(self.mViewModel.isplayingFromString ?? "")", key: StringConstant.isPlayingFromKey)
        UserDefaultUtils.setBool(value: true, key: StringConstant.isPlayingKey)
    }
    
    internal func addChildView() {
            

        saveDataToSharedPrefrences()
       
        miniPlayer.setPlayingCategoryType(categoryType: self.mViewModel.categoryType)
        miniPlayer.setSongData(song: self.mViewModel.songsArray?[self.mViewModel.indexPathRow ?? 0])
        miniPlayer.setIndexPath(row: self.mViewModel.indexPathRow)
        miniPlayer.setCompleteSongsArray(songsArray: self.mViewModel.songsArray)
        miniPlayer.setIsPlayingFromString(isplayingFromString: "\(StringConstant.playingFrom)SEARCHED")
        miniPlayer.setTabBarVC(tabBarVC: self)
        miniPlayer.setDelegate(delegate: self)
        
        AudioPlayer.shared.playingFileName = self.mViewModel.song?.songTitle
        AudioPlayer.shared.playingAlbumName = self.mViewModel.song?.albumName
       
        AudioPlayer.shared.songsArray =  self.mViewModel.songsArray
        AudioPlayer.shared.indexPath = self.mViewModel.indexPathRow
        AudioPlayer.shared.isPlayingFrom = self.mViewModel.isplayingFromString
        
        
        if let imageUrl = URL(string: self.mViewModel.song?.coverImageURL ?? ""){
        self.getData(from: imageUrl ) { data, response, error in
                guard let data = data, error == nil else { return }
                DispatchQueue.main.async() { [weak self] in
                    AudioPlayer.shared.PlayingFileImage = UIImage(data: data)
                }
        }
        }
        view.addSubview(containerView)
        
        addChild(miniPlayer)
        miniPlayer.setupView()
        containerView.addSubview(miniPlayer.view)
        miniPlayer.didMove(toParent: self)
        
       
        
            
             //find index of miniPlayer in TabBar's viewControllers array
             //and remove it.
            if let childIndex = viewControllers?.firstIndex(of: miniPlayer) {
                viewControllers?.remove(at: childIndex)
            }
        }
    private func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    internal  func setConstraintsAboveTabBar() {
            let g = view.safeAreaLayoutGuide
            NSLayoutConstraint.activate([
                containerView.leadingAnchor.constraint(equalTo: g.leadingAnchor),
                containerView.trailingAnchor.constraint(equalTo: g.trailingAnchor),
                containerView.bottomAnchor.constraint(equalTo: tabBar.topAnchor),
                containerView.heightAnchor.constraint(equalToConstant: 64.0),
                
                miniPlayer.view.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
                miniPlayer.view.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
                miniPlayer.view.topAnchor.constraint(equalTo: containerView.topAnchor),
                miniPlayer.view.bottomAnchor.constraint(equalTo: containerView.bottomAnchor),
            ])
        }

    internal func setConstraintsBelowTabBar() {
        let g = view.safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            containerView.leadingAnchor.constraint(equalTo: g.leadingAnchor),
            containerView.trailingAnchor.constraint(equalTo: g.trailingAnchor),
            containerView.bottomAnchor.constraint(equalTo: tabBar.bottomAnchor),
            
            containerView.heightAnchor.constraint(equalTo: self.tabBar.heightAnchor),
            
            miniPlayer.view.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            miniPlayer.view.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            miniPlayer.view.topAnchor.constraint(equalTo: containerView.topAnchor),
            miniPlayer.view.bottomAnchor.constraint(equalTo: containerView.bottomAnchor),
        ])
    }
    
    
    
   
}
extension CustomTabbarController:SongsDetailProtocol{
    func setIsFromMiniPlayer(isFromMiniPlayer: Bool) {
        
    }
    
    func setCompleteSongsArray(songsArray: [Song]?) {
        if let songsArray = songsArray {
            self.mViewModel.songsArray = songsArray
        }
    }
    
    func setIndexPath(row: Int?) {
        if let Row = row{
            self.mViewModel.indexPathRow = Row
        }
    }
    
    func setIsPlayingFromString(isplayingFromString:String?) {
        if let string = isplayingFromString{
            self.mViewModel.isplayingFromString = string
        }
    }
    
    func setPlayingCategoryType(categoryType: String?) {
        if let Data = categoryType{
            self.mViewModel.categoryType = Data
        }
    }
    
    func setSongData(song: Song?) {
        if let Data = song{
            self.mViewModel.song = Data
        }
    }
    
    func setAlbumData(album: Album?) {
        if let Data = album{
            self.mViewModel.album = Data
        }
    }
}

extension CustomTabbarController:MiniPlayerViewControllerDelegate{
    
   
    
    func navigateToMainAudioPlayer(isplaying:Bool,category: String) {
        
        let transition = CATransition()
          transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.push
        transition.subtype = .fromTop
          //instead "kCAGravityLeft" try with different transition subtypes

          self.navigationController?.view.layer.add(transition, forKey: kCATransition)
          
        
        let vc = UIStoryboard.init(name: StoryBoardConstant.storyBoardMain, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.SongsDetailViewController) as? SongsDetailViewController
        vc?.setPlayingCategoryType(categoryType: category)
        vc?.setSongData(song: self.mViewModel.songsArray?[self.mViewModel.indexPathRow ?? 0])
        vc?.setIndexPath(row: self.mViewModel.indexPathRow ?? 0)
        vc?.setCompleteSongsArray(songsArray: self.mViewModel.songsArray)
        vc?.setIsPlayingFromString(isplayingFromString: "\(self.mViewModel.isplayingFromString ?? "")")
        vc?.mViewModel.isPlaying = isplaying
        vc?.setDelegateForMiniPlayer(delegate:self)
        vc?.setIsFromMiniPlayer(isFromMiniPlayer: true)
        self.mViewModel.isPlaying = isplaying
        UserDefaultUtils.setBool(value : false, key: CommonKeys.IsPlayingFromNotification)
        self.navigationController?.pushViewController(vc!, animated: false)
    }
    
    
}
extension CustomTabbarController:SongDeatilDelegateForMiniPlayer{
    
    
    func setIsPlaying(isPlaying: Bool) {
        print("NewDelegate Isplaying \(isPlaying)")
        self.mViewModel.isPlaying = isPlaying
    }
    
    
}

