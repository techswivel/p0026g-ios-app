//
//  CustomTabbarViewModel.swift
//  QtheMusic
//
//  Created by Assad Khan on 02/08/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import Foundation
import UIKit
class CustomTabbarViewModel:BaseViewModel{
    
    var categoryType :String?
    var album : Album?
    var song:Song?
    var songsArray:[Song]?
    var isplayingFromString:String?
    var isPlaying:Bool? = false
    var titleOfAudio = ""
    var maxValueOfSeekBar : CGFloat = 0
    var indexPathRow : Int? = -1
}
