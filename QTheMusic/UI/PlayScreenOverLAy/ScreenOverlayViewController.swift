//
//  ScreenOverlayViewController.swift
//  QtheMusic
//
//  Created by Assad Khan on 06/09/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import UIKit



protocol ScreenOverlayViewControllerProtcol{
    func setSongObj(song:Song)
    func setDelegate(delegate:ScreenOverlayViewControllerDelegate)
    
}
protocol ScreenOverlayViewControllerDelegate{
    func setBackSongObjForFavorite(song:Song)
}
class ScreenOverlayViewController: BaseViewController {
    
    @IBOutlet weak var favouriteImage: UIImageView!
    
    @IBOutlet weak var playlistImage: UIImageView!
    
    @IBOutlet weak var addToPaylist: UIButton!
    @IBOutlet weak var songCoverImageView: UIImageView!{
        didSet{
            songCoverImageView.roundCornerImageView()
        }
    }
    @IBOutlet weak var albumNameLabel: UILabel!
    
    @IBOutlet weak var artistNameLabel: UILabel!
    
    @IBOutlet weak var favouriteBtn: UIButton!
    
    @IBOutlet weak var songCoverACtivityIndicator: UIActivityIndicatorView!
    fileprivate var downloadManager = DownloadManager()
    private var delegate:ScreenOverlayViewControllerDelegate?
    private var mViewModel = ScreenOverlayViewModel()
    private var mProfileViewModel = ProfileViewModel()
    private var AllPlayListScreen : AllPlaylistView?
    private var downloadScreenNib : DownloadScreen?
    private var setPlaylistValue =  Bool()
    private var mSongNetworkViewModel = SongAndArtistViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        
    }
    @IBAction func offlineDownloadBtnPressed(_ sender: Any) {
        
      if  UserDefaultUtils.getProfile(key: CommonKeys.KEY_PROFILE)?.subscription != nil {
          if self.mViewModel.song?.songVideoURL != nil{
              
              self.view.addSubview(self.downloadScreenNib!)
              
          }else{
              //Download songAudio
              if let url = URL(string: self.mViewModel.song?.songAudioURL ?? ""){
                          DownloadManager.shared.addDownload(url)
                      }
          }
      }else{
          Utils.showAlert(vc: self, message: StringConstant.thisFeaturesAvailable)
      }
    }
    
    @IBAction func closeBtnPressed(_ sender: Any) {
        self.popBackController()
    }
    
    @IBAction func addToPlaylistBtnPressed(_ sender: Any) {
      
        self.mProfileViewModel.getPlaylist()
        
    
    }
    @IBAction func addToFvrtBtnPressed(_ sender: UIButton) {
        let builder = SongAndArtistBuilder()
        builder.setSongId(self.mViewModel.song?.songID)
        if self.mViewModel.song?.isFavourit == true
        {
            if self.mViewModel.song?.songStatus == SongStatus.PREMIUM.rawValue
            {
                Utils.showErrorAlert(vc: self, message: StringConstant.favouriteSongs)
            }
            if UserDefaultUtils.getBool(key: CommonKeys.Favourite_Icon) == false
            {
                builder.setIsFavourite(true)
                 UserDefaultUtils.setBool(value: true, key: CommonKeys.Favourite_Icon)
            }
            else
            {
                 builder.setIsFavourite(false)
            }
        }
        
        else
        {
        
                builder.setIsFavourite(true)
        }
        self.showActivityIndicator()
        self.mProfileViewModel.setIsFavourite(data: builder.build())
    }
    
    // MARK: - Setup View
    
    private func setUpView(){
        DownloadManager.shared.processDelegate = self
        DownloadManager.shared.song = self.mViewModel.song
        self.setPlaylistValueType()
        self.bindingServerResponse()
        self.bindingServerErrorResponse()
        self.bindingServerCompletionResponse()
        self.bindingServerResponseForPlaylist()
        self.bindingServerErrorResponseForPlaylist()
        self.bindingServerResponseForUpdatePlaylist()
        self.bindingServerErrorResponseForUpdatePlaylist()
        
        
        self.setFvrtBtnImage()
        
        self.AllPlayListScreen = UINib(nibName:    UINibConstant.AllPlaylistViewNib, bundle: .main).instantiate(withOwner: nil, options: nil).first as? AllPlaylistView
        self.AllPlayListScreen?.setDelegate(delegate: self)
        self.AllPlayListScreen?.vc = self
        self.AllPlayListScreen?.frame = self.view.bounds
        
        
        self.downloadScreenNib = UINib(nibName:    UINibConstant.DownloadScreenNib, bundle: .main).instantiate(withOwner: nil, options: nil).first as? DownloadScreen
        self.downloadScreenNib?.setDelegate(delegate: self)
        self.downloadScreenNib?.setSongObj(song: self.mViewModel.song ?? Song())
        self.downloadScreenNib?.frame = self.view.bounds
        
        
        
        
        guard let Song = self.mViewModel.song else {return}
        if UserDefaultUtils.getBool(key: CommonKeys.IsPlayingFromNotification) == true
        {
            self.songCoverImageView.loadImage(imageUrl: UserDefaultUtils.getString(key: CommonKeys.NotImage), placeHolderName: ImageConstant.noImagePlaceHolder, activiyIndicator: self.songCoverACtivityIndicator)
            self.albumNameLabel.text = "\(UserDefaultUtils.getString(key: CommonKeys.SongTitle))"
            self.artistNameLabel.text = "\(UserDefaultUtils.getString(key: CommonKeys.ArtistName))"
        }
        else
        {
        self.songCoverImageView.loadImage(imageUrl: Song.coverImageURL ?? "", placeHolderName: ImageConstant.noImagePlaceHolder, activiyIndicator: self.songCoverACtivityIndicator)
        
        self.artistNameLabel.text = "\(Song.artistName ?? "")"
        self.albumNameLabel.text = "\(Song.songTitle ?? "")"
    }
    }
    
    fileprivate func setFvrtBtnImage() {
        
        if self.mViewModel.song?.isFavourit == true {
            
            if self.mViewModel.song?.songStatus == StringConstant.premium
            {
                favouriteImage.image = UIImage(named: ImageConstant.isNotFvrt)
                self.favouriteBtn.setTitle(StringConstant.addToFavorite, for: .normal)
                UserDefaultUtils.setBool(value: false, key: CommonKeys.Favourite_Icon)
            }
            if UserDefaultUtils.getBool(key: CommonKeys.Favourite_Icon) == false
            {
                favouriteImage.image = UIImage(named: ImageConstant.isNotFvrt)
                self.favouriteBtn.setTitle(StringConstant.addToFavorite, for: .normal)
            }
            else
            {
                favouriteImage.image = UIImage(named: ImageConstant.isFvrt)
                self.favouriteBtn.setTitle(StringConstant.removeFromFavorite, for: .normal)
            }
            
        }
        else if self.mViewModel.song?.isFavourit == false
        {
            favouriteImage.image = UIImage(named: ImageConstant.isNotFvrt)
            self.favouriteBtn.setTitle(StringConstant.addToFavorite, for: .normal)
        }
    }
    fileprivate func setPlaylistValueType() {
        print(mViewModel.song?.isAddedToPlayList)
        if self.mViewModel.song?.isAddedToPlayList == true {
            self.addToPaylist.setTitle(StringConstant.removeFromPlaylist, for: .normal)
            
        }
        else
        {
            self.addToPaylist.setTitle(StringConstant.addToPlaylist, for: .normal)
            
           
        }
    }
}

// MARK: - Binding Extension
extension ScreenOverlayViewController{
    private func bindingServerResponse() {
        self.mProfileViewModel.serverResponseForFavouriteSong.bind {
            if ($0 != nil) {
                
                if $0?.response?.status ?? false {
                    self.removeActivityIndicator()
                    
                    self.mViewModel.song = self.mViewModel.setIsFavourite(songObj:  self.mViewModel.song ?? Song())
                    self.delegate?.setBackSongObjForFavorite(song: self.mViewModel.song ?? Song())
                    self.setFvrtBtnImage()
                   
                    
                }
                else if $0?.response?.status == false {
                   
                    Utils.showErrorAlert(vc: self, message: $0?.response?.message)
                    self.removeActivityIndicator()
                    
                }
            }
        }
    }
    
    //MARK: - Binding Error Response
    private func bindingServerErrorResponse() {
        self.mProfileViewModel.errorResponseForFavouriteSong.bind {
            if ($0 != nil) {
                Utils.showErrorAlert(vc: self, message: $0?.response?.message)
            }
            
            self.removeActivityIndicator()
        }
        
        
    }
    
    
    
    
    private func bindingServerResponseForPlaylist() {
        self.mProfileViewModel.serverResponseForPlaylist.bind {
            if ($0 != nil) {
                
                if $0?.response?.status ?? false {
                    self.removeActivityIndicator()
                    
                    self.AllPlayListScreen?.setPlayList(playListArray: $0?.response?.data?.playlists ?? [Playlist]())
                    self.view.addSubview(self.AllPlayListScreen!)
                }
                else if $0?.response?.status == false {
                   
                    Utils.showErrorAlert(vc: self, message: $0?.response?.message)
                    self.removeActivityIndicator()
                    
                }
            }
        }
    }
    
    //MARK: - Binding Error Response
    private func bindingServerErrorResponseForPlaylist() {
        self.mProfileViewModel.errorResponseForPlaylist.bind {
            if ($0 != nil) {
                Utils.showErrorAlert(vc: self, message: $0?.response?.message)
            }
            
            self.removeActivityIndicator()
        }
        
        
    }
    
    
    
    
    
    private func bindingServerResponseForUpdatePlaylist() {
        self.mProfileViewModel.serverResponseForUpdatePlaylist.bind {
            if ($0 != nil) {
                if $0?.response?.status ?? false {
                    print($0?.response?.data)
                    self.removeActivityIndicator()
                    print("Succcefuly removing playlist")
                    self.AllPlayListScreen?.removeFromSuperview()
                }
                else if $0?.response?.status == false {
                   
                    Utils.showErrorAlert(vc: self, message: $0?.response?.message)
                    self.removeActivityIndicator()
                    
                }
            }
        }
    }
    
    //MARK: - Binding Error Response
    private func bindingServerErrorResponseForUpdatePlaylist() {
        self.mProfileViewModel.errorResponseForUpdatePlaylist.bind {
            if ($0 != nil) {
                Utils.showErrorAlert(vc: self, message: $0?.response?.message)
            }
            
            self.removeActivityIndicator()
        }
        
        
    }
    
    //MARK: - Binding Complete Response
    private func bindingServerCompletionResponse(){
        self.mProfileViewModel.completion.bind {
            if ($0 != nil) {
                
                
            }
        }
        
    }
}

extension ScreenOverlayViewController:ScreenOverlayViewControllerProtcol{
    func setDelegate(delegate: ScreenOverlayViewControllerDelegate) {
        self.delegate = delegate
    }
    
    func setSongObj(song: Song) {
        self.mViewModel.song = song
    }
    
    
}
extension ScreenOverlayViewController: DownloadProcessProtocol {


    func downloadingProgress(_ percent: Float, fileName: String) {
       
        
    }

    func downloadSucceeded(_ fileName: String) {

        print("success")
    }

    func downloadWithError(_ error: Error?, fileName: String) {
        print(error)
    }
}
extension ScreenOverlayViewController:AllPlaylistViewDelegate{
    func doneBtnPressed(playListId: Int) {
        let songbuilder = SongAndArtistBuilder()
        songbuilder.setSongId(self.mViewModel.song?.songID ?? -1)
        if self.addToPaylist.titleLabel?.text == StringConstant.addToPlaylist
        {
            if self.mViewModel.song?.songStatus == StringConstant.premium
            {
                Utils.showErrorAlert(vc: self, message: StringConstant.premiumSongs)
                self.addToPaylist.setTitle(StringConstant.addToPlaylist, for: .normal)
            }
            else
            {
            songbuilder.setplayListId(playListId)
            songbuilder.settype(PlaylistEnum.ADD.rawValue)
            self.addToPaylist.setTitle(StringConstant.removeFromPlaylist, for: .normal)
            }
        }
        else if self.addToPaylist.titleLabel?.text == StringConstant.removeFromPlaylist
        {
            songbuilder.setplayListId(playListId)
            songbuilder.settype(PlaylistEnum.REMOVE.rawValue)
            self.addToPaylist.setTitle(StringConstant.addToPlaylist, for: .normal)
        }
        self.showActivityIndicator()
        self.mProfileViewModel.updatePlaylist(builder: songbuilder)
        
    }
    
    func closeBtnPressed() {
        AllPlayListScreen?.removeFromSuperview()
    }
    
   
    
    
}
extension ScreenOverlayViewController:DownloadScreenDelegate{
    func audioBtnPressed() {
        //Download Audio Url
        
       
        if let url = URL(string: self.mViewModel.song?.songAudioURL ?? ""){
                    DownloadManager.shared.addDownload(url)
                }
        self.downloadScreenNib?.removeFromSuperview()
    }
    
    func videoBtnPressed() {
        //Download Video Url
        
        if let url = URL(string: self.mViewModel.song?.songVideoURL ?? ""){
                    DownloadManager.shared.addDownload(url)
                }
        self.downloadScreenNib?.removeFromSuperview()
    }
    
    func removeDownloadScreen() {
        self.downloadScreenNib?.removeFromSuperview()
    }
    
    
}
