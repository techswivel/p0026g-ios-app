//
//  ScreenOverlayViewModel.swift
//  QtheMusic
//
//  Created by Assad Khan on 06/09/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import Foundation


class ScreenOverlayViewModel:BaseViewModel{
    
    var song:Song?
    func setIsFavourite(songObj:Song)->Song?{
       return mDataManager.setIsFavourite(songObj: songObj)
    }
}
