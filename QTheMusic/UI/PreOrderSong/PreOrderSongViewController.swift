//
//  PreOrderSongViewController.swift
//  QtheMusic
//
//  Created by Assad Khan on 13/05/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import UIKit
import StoreKit
protocol PreOrderSongViewControllerProtocol{
    func setArtistId(artistId:Int?)
}
class PreOrderSongViewController: BaseViewController {
    
    @IBOutlet weak var preOrderSongTableView: UITableView!{
        didSet{
            preOrderSongTableView.dataSource = self
            preOrderSongTableView.delegate = self
        }
    }
    
    private var mViewModel = PreOrderSongViewModel()
    private var mSongAndArtistViewModel = SongAndArtistViewModel()
    private var productIdentifiers: Set<String> = []
    private var successView: PaymentSuccessView!
    private var failedView: PaymentFailedView!
    private var mTranesctrionItem: SKPaymentTransaction?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        productIdentifiers.removeAll()
        self.setUpView()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        SKPaymentQueue.default().remove(self)
    }
    @objc func handleOrder(_ sender: UIButton){
        self.showActivityIndicator()
        let productIdentifier: String = self.mViewModel.preOrderArray?[sender.tag].sku ?? ""
        productIdentifiers.insert(productIdentifier)
            let productRequest = SKProductsRequest(productIdentifiers: productIdentifiers)
        mViewModel.index = sender.tag
        let data = UserDefaultUtils.getProfile(key: CommonKeys.KEY_PROFILE)
        if data?.addressModObj?.completeAddress == nil
            
        {
            Utils.showErrorAlert(vc: self, message: StringConstant.completeAddress)
            self.removeActivityIndicator()
        }
        else
        {
            productRequest.delegate = self
            productRequest.start()
        }
    }
    @IBAction func backBtnPressed(_ sender: Any) {
        self.popBackNavigationWithTransition()
    }
    
    private func setUpView(){
        self.registerNib()
        self.bindingServerResponse()
        self.bindingServerErrorResponse()
        self.bindingServerCompletionResponse()
        self.bindingServerResponseForSubscribeToPlan()
        self.bindingServerErrorResponseForSubscribeToPlan()
        self.mSongAndArtistViewModel.getPreOrderDetail(artistId: self.mViewModel.artistId)
        
    }
    private func registerNib(){
        preOrderSongTableView.register(UINib(nibName: UINibConstant.PreOrderSongsTableViewCellNib, bundle: nil),forCellReuseIdentifier: UINibConstant.PreOrderSongsTableViewCellIdentifier)
        self.failedView = UINib(nibName:    UINibConstant.paymentFailedNib, bundle: .main).instantiate(withOwner: nil, options: nil).first as? PaymentFailedView
        self.failedView.frame = self.view.bounds
        self.failedView.delegate = self
        // Success View Nib
        self.successView = UINib(nibName:    UINibConstant.paymentSuccessNib, bundle: .main).instantiate(withOwner: nil, options: nil).first as? PaymentSuccessView
        self.successView.frame = self.view.bounds
        self.successView.delegate = self
        
    }
    
    
}

extension PreOrderSongViewController{
    
    //MARK: - Binding Server Response
    private func bindingServerResponse() {
        self.mSongAndArtistViewModel.serverResponseForPreOrderDetail.bind {
            if ($0 != nil) {
                
                if $0?.response?.status ?? false {
                    
                    defer{
                        self.preOrderSongTableView.reloadData()
                    }
                    
                    self.mViewModel.preOrderArray = $0?.response?.data?.preOrderData
                   
                }
                else if $0?.response?.status == false {
                    print("Response for ArtistDetailViewController is false")
                    Utils.showErrorAlert(vc: self, message: $0?.response?.message)
                    self.removeActivityIndicator()
                    
                }
            }
        }
    }
    
    //MARK: - Binding Error Response
    private func bindingServerErrorResponse() {
        self.mSongAndArtistViewModel.errorResponseForPreOrderDetail.bind {
            if ($0 != nil) {
                Utils.showErrorAlert(vc: self, message: $0?.response?.message)
            }
            
            self.removeActivityIndicator()
        }
        
        
    }
    
    //MARK: - Binding Complete Response
    private func bindingServerCompletionResponse(){
        self.mSongAndArtistViewModel.completion.bind {
            if ($0 != nil) {
                
                
            }
        }
        
    }
}

extension PreOrderSongViewController:UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.mViewModel.preOrderArray?.count ?? 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: UINibConstant.PreOrderSongsTableViewCellIdentifier, for: indexPath) as! PreOrderSongsTableViewCell
        
        if !(self.mViewModel.preOrderArray?.isEmpty ??  true){
            cell.hideShimmerEffect()
            cell.setUpCell(data: self.mViewModel.preOrderArray?[indexPath.row])
            cell.setDelegate(delegate: self)
        }else{
            cell.showShimmerEffect()
        }
        cell.selectionStyle = .none
        cell.orderNowBtn.tag = indexPath.row
        cell.orderNowBtn.addTarget(self, action: #selector(self.handleOrder(_:)), for: .touchUpInside)
       
        return cell
    }
    
    
    
}
extension PreOrderSongViewController:PreOrderSongViewControllerProtocol{
    func setArtistId(artistId: Int?) {
        self.mViewModel.artistId = artistId
    }
    
    
}
extension PreOrderSongViewController:PreOrderSongsTableViewCellDelegate{
    
    func orderNowbtnPressed() {
    }
    
    
    
    
}

extension PreOrderSongViewController: SKProductsRequestDelegate, SKPaymentTransactionObserver {
    func productsRequest (_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        print(response.products.count)
        if let oProducts = response.products.first
        {
            print("product is available")
            print(productIdentifiers)
            self.purchase(aproduct: oProducts)
        }
        else
        {
            Utils.showErrorAlert(vc: self, message: StringConstant.productisNotAvailabe)
    
        }
        
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        DispatchQueue.main.async{
        for transaction in transactions {
            switch transaction.transactionState
            {
            case .purchasing:
                print("customer is in process of purchase")
            case .purchased:
                print("Purchased")
                SKPaymentQueue.default().finishTransaction(transaction)
                self.productIdentifiers.removeAll()
                self.getPurchaseToken()
                self.mSongAndArtistViewModel.subscribeToPlan(data: self.mViewModel.setParams())
            case .failed:
                     if let error = transaction.error as? SKError {
                         switch error.code {
                         case .unknown:
                             self.failedView.setFailedMsg(errorLbl: StringConstant.Unknown_Case)
                         case .clientInvalid:
                             self.failedView.setFailedMsg(errorLbl: StringConstant.Invalid_Client)
                         case .paymentCancelled:
                             self.failedView.setFailedMsg(errorLbl: StringConstant.Payment_Cancelled)
                         case .paymentInvalid:
                             self.failedView.setFailedMsg(errorLbl: StringConstant.Invalid_Payment)
                         case .paymentNotAllowed:
                             self.failedView.setFailedMsg(errorLbl: StringConstant.Payment_Not_Allowed)
                         case .storeProductNotAvailable:
                             self.failedView.setFailedMsg(errorLbl: StringConstant.Product_Not_Available)
                         case .cloudServicePermissionDenied:
                             self.failedView.setFailedMsg(errorLbl: StringConstant.Cloud_Service_Permission_Denied)
                         case .cloudServiceNetworkConnectionFailed:
                             self.failedView.setFailedMsg(errorLbl: StringConstant.Cloud_Service_Network_Connection_Failed)
                         default:
                             self.failedView.setFailedMsg(errorLbl: StringConstant.Something_Went_Wrong)
                         }
                         SKPaymentQueue.default().finishTransaction(transaction)
                         self.productIdentifiers.removeAll()
                         self.failedView.alpha = 0.0
                         self.view.addSubview(self.failedView)
                         UIView.animate(withDuration: 0.25) { () -> Void in
                             self.failedView.alpha = 1.0
                         }
                         self.removeActivityIndicator()
                     }
                
            case .restored:
                self.failedView.setFailedMsg(errorLbl: StringConstant.Restored)
            case .deferred:
                self.failedView.setFailedMsg(errorLbl: StringConstant.Suspended)
            default : break
            }
        }
    }
    }

    func purchase(aproduct: SKProduct)
    {
        if SKPaymentQueue.canMakePayments()
        {
        let payment = SKPayment(product: aproduct)
        SKPaymentQueue.default().add(self)
        SKPaymentQueue.default().add(payment)
        }
    }
    private func getPurchaseToken()
     {
     if let url = Bundle.main.appStoreReceiptURL,
        let data = try? Data(contentsOf: url) {
       //  let str = String(decoding: data, as: UTF8.self)
       //  mViewModel.purchaseToken = str
         let receiptBase64 = data.base64EncodedString()
        mViewModel.purchaseToken = receiptBase64
     }
     }
     
}
extension PreOrderSongViewController: FailedViewDelegate {
    func cancel() {
        self.failedView.removeFromSuperview()
    }

}
extension PreOrderSongViewController: SuccessViewDelegate {

    func cancelView() {
        self.successView.removeFromSuperview()
    }
}

extension PreOrderSongViewController{
private func bindingServerResponseForSubscribeToPlan() {
    self.mSongAndArtistViewModel.serverResponseForSubscribeToPlan.bind {
        if ($0 != nil) {
            
            if $0?.response?.status ?? false {
                let item = $0?.response?.data?.purchasedPreOrder
                self.mViewModel.preOrderArray?.filter {$0.sku == item?.sku}.first?.isPurchesed = item?.isPurchesed
                self.preOrderSongTableView.reloadData()
                self.removeActivityIndicator()
                DispatchQueue.main.async{
                self.successView
                    .alpha = 0.0
                self.view.addSubview(self.successView)
                UIView.animate(withDuration: 0.25) { () -> Void in
                    self.successView.alpha = 1.0
                }
                }
                
            }
            else if $0?.response?.status == false {
               
                self.removeActivityIndicator()
                
            }
        }
    }
}
private func bindingServerErrorResponseForSubscribeToPlan() {
    self.mSongAndArtistViewModel.errorResponseForForSubscribeToPlan.bind {
        if ($0 != nil) {
            self.failedView.setFailedMsg(errorLbl: StringConstant.Payment_Failed)
            Utils.showErrorAlert(vc: self, message: $0?.response?.message)
            self.removeActivityIndicator()
            self.failedView.alpha = 0.0
            self.view.addSubview(self.failedView)
            UIView.animate(withDuration: 0.25) { () -> Void in
                self.failedView.alpha = 1.0
            }
        }
    }
}
}
