//
//  PreOrderSongViewModel.swift
//  QtheMusic
//
//  Created by Assad Khan on 13/05/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import Foundation

class PreOrderSongViewModel:BaseViewModel{
    
    var preOrderArray:[PreOrderData]?
    var artistId:Int?
    var song: Song?
    var index: Int?
    var purchaseToken: String?
    let builder = SongAndArtistBuilder()
    func setParams() -> Song {
        if preOrderArray?[index ?? 0].preOrderType == "Song"
        {
        builder.setpurchaseType("APPLE_PAY")
            builder.setpurchaseToken(purchaseToken)
        builder.setItemType("PRE_ORDER_SONG")
        builder.setPreOrderSongId(preOrderArray?[index ?? 0].preOrderID)
        builder.setPaidAmount(preOrderArray?[index ?? 0].price)
        }
        else
        {
            builder.setpurchaseType("APPLE_PAY")
            builder.setpurchaseToken(purchaseToken)
            builder.setItemType("PRE_ORDER_ALBUM")
            builder.setPreOrderSongId(preOrderArray?[index ?? 0].preOrderID)
            builder.setPaidAmount(preOrderArray?[index ?? 0].price)
            
        }
        song = builder.build()
        print(song?.purchaseToken)
        return song ?? Song()
    }
    
}
