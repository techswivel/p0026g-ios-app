//
//  FollowingArtistViewModel.swift
//  QtheMusic
//
//  Created by Muhammad Tanzeel Ur Rehman on 08/12/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import Foundation
import UIKit

class FollowingArtistViewModel: BaseViewModel {
    
    var lablesArtist : Artist?
    var labelsAlbum : Album?
    
    var artistId:Int?
    var isFollowing:Bool?
    var albumsArray  = [Album]()
    var songsArray = [Song]()
    var artistArray = [Artist]()
    
    var songBuilder: SongAndArtistBuilder = SongAndArtistBuilder()
    
}
