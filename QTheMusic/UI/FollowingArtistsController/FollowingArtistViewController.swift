//
//  FollowingArtistViewController.swift
//  QtheMusic
//
//  Created by Muhammad Tanzeel Ur Rehman on 08/12/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import Foundation
import UIKit

class FollowingArtistViewController: BaseViewController{
    
    @IBOutlet weak var emptyResponseLbl: UILabel!
    
    @IBOutlet weak var followingArtistLbl: UILabel!
    
    @IBOutlet weak var followingArtistTable: UITableView!{
        didSet{
            followingArtistTable.delegate = self
            followingArtistTable.dataSource = self
        }
    }
    
    private var mViewModel = FollowingArtistViewModel()
    private var mSongAndArtistViewModel = SongAndArtistViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        emptyResponseLbl.isHidden = true
        self.view.addGradientBackground()
        setBindigServerResponse()
        
        followingArtistTable.register(UINib(nibName: UINibConstant.FollowingArtistTableCellnib, bundle: nil), forCellReuseIdentifier: UINibConstant.FollowingArtistTableCellIdentifier)
        
        mViewModel.songBuilder.settype(SongType.ARTIST.rawValue)
        showActivityIndicator()
        mSongAndArtistViewModel.getFollowArtist()
        
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        self.popBackNavigationWithTransition()
    }
    
    // remove row
    @objc func handleDelete(_ sender: UIButton){
        mSongAndArtistViewModel.followArtist(artistId: mViewModel.artistArray[sender.tag].artistID, isFollowed: false)
        mViewModel.artistArray.remove(at: sender.tag)
        followingArtistTable.deleteRows(at:[IndexPath(row:sender.tag,section:0)],with:.none)
        followingArtistTable.reloadData()
    }
    
    private func setBindigServerResponse() {
        
        self.bindingServerResponseForFollowArtist()
        self.bindingServerErrorResponseForFollowArtist()
        self.bindingServerCompletionResponse()
        
    }
    
}

extension FollowingArtistViewController: UITableViewDelegate,UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let artistDetailVC = UIStoryboard.init(name: StoryBoardConstant.storyBoardMain, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.ArtistDetailViewController) as! ArtistDetailViewController
        
        artistDetailVC.artistName = mViewModel.artistArray[indexPath.row].artistName ?? "Atif Aslam"
        
        self.navigationController?.pushViewController(artistDetailVC, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mViewModel.artistArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = followingArtistTable.dequeueReusableCell(withIdentifier: UINibConstant.FollowingArtistTableCellIdentifier, for: indexPath) as! FollowingArtistTableCell
        if self.mViewModel.artistArray.count != 0 {
            cell.showAnimation()
            DispatchQueue.main.asyncAfter(deadline: .now() + 4.0) {
                cell.hideAnimation()
                cell.setupCell(data: self.mViewModel.artistArray[indexPath.row])
             }
        }
        cell.selectionStyle = .none
        cell.unfollowBtn.tag = indexPath.row
        mViewModel.artistId = indexPath.row
        cell.unfollowBtn.addTarget(self, action: #selector(self.handleDelete(_:)), for: .touchUpInside)
        return cell
    }
    
}

//MARK: - Binding Server Response For Follow Artist
extension FollowingArtistViewController {
    
    private func bindingServerResponseForFollowArtist() {
        self.mSongAndArtistViewModel.serverResponseForGetFollowArtist.bind {
            if ($0 != nil) {
                
                if $0?.response?.status ?? false {
                    
                    self.removeActivityIndicator()
                    self.mViewModel.artistArray = $0?.response?.data?.artist ?? self.mViewModel.artistArray
                    if self.mViewModel.artistArray.count == 0
                    {
                        self.emptyResponseLbl.isHidden = false
                    }
                    else
                    {
                        self.emptyResponseLbl.isHidden = true
                    }
                    self.followingArtistTable.reloadData()
                    
                    print("following artists data shown")
                }
                else if $0?.response?.status == false {
                    
                    print("Response for Following Artist is false")
                    Utils.showErrorAlert(vc: self, message: $0?.response?.message)
                    self.removeActivityIndicator()
                    
                }
            }
        }
    }
    
    //MARK: - Binding Error Response For Following Artist
    private func bindingServerErrorResponseForFollowArtist() {
        self.mSongAndArtistViewModel.errorResponseForGetFollowArtist.bind {
            if ($0 != nil) {
                Utils.showErrorAlert(vc: self, message: $0?.response?.message)
            }
            
            self.removeActivityIndicator()
        }
        
        
    }
    
    
    //MARK: - Binding Complete Response
    private func bindingServerCompletionResponse(){
        self.mSongAndArtistViewModel.completion.bind {
            if ($0 != nil) {
                self.removeActivityIndicator()
            }
        }
        
    }
}

