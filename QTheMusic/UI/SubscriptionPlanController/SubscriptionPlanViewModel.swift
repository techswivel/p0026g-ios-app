//
//  SubscriptionPlanViewModel.swift
//  QtheMusic
//
//  Created by zeeshan on 12/04/2023.
//  Copyright © 2023 TechSwivel. All rights reserved.
//

import Foundation
class SubscriptionPlanViewModel:BaseViewModel
{
    var subscriptionArray = [Subscription]()
    var song:Song?
    var index: Int?
    let builder = SongAndArtistBuilder()
    var purchaseToken = String()
    func subscriptionParams() -> Song {
        builder.setpurchaseType("APPLE_PAY")
        builder.setpurchaseToken(purchaseToken)
        builder.setItemType("PLAN_SUBSCRIPTION")
        builder.setPlanId(subscriptionArray[index ?? 0].planID)
        builder.setPrice(subscriptionArray[index ?? 0].planPrice ?? 0)
        song = builder.build()
        return song ?? Song()
    }
    
}
