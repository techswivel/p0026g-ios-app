//
//  SubscriptionPlanViewController.swift
//  QtheMusic
//
//  Created by zeeshan on 12/04/2023.
//  Copyright © 2023 TechSwivel. All rights reserved.
//

import UIKit
import StoreKit
class SubscriptionPlanViewController: BaseViewController {
    @IBOutlet weak var subscriptionPlansTableView: UITableView!
    {
        didSet
        {
            subscriptionPlansTableView.register(UINib(nibName: UINibConstant.subscriptionPlanCellnib, bundle: nil), forCellReuseIdentifier: UINibConstant.subscriptionPlanCellnib)
        }
    }
    private var mViewModel = SubscriptionPlanViewModel()
    private var mSongAndArtistViewModel =
    SongAndArtistViewModel()
    private var productIdentifiers: Set<String> = []
    private var productIdentifier: String?
    private var successView: PaymentSuccessView!
    private var failedView: PaymentFailedView!
    override func viewDidLoad() {
        super.viewDidLoad()
        registerNibs()
        productIdentifiers.removeAll()
        self.view.addGradientBackground()
        setbindingServerResponse()
        mSongAndArtistViewModel.getPackagesPlan()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        SKPaymentQueue.default().remove(self)
    }
    @IBAction func onClickCrossBtn(_ sender: Any) {
        self.popBackNavigationWithTransition()
    }
    @objc func handleSubscribe(_ sender: UIButton){
        self.showActivityIndicator()
         productIdentifier = self.mViewModel.subscriptionArray[sender.tag].sku ?? ""
        mViewModel.index = sender.tag
        productIdentifiers.insert(productIdentifier ?? "")
            let productRequest = SKProductsRequest(productIdentifiers: productIdentifiers)
            productRequest.delegate = self
            productRequest.start()
    }
    private func setbindingServerResponse()
    {
        bindingServerResponseForSubscriptionPlan()
        bindingServerErrorResponseForSubscriptionPlan()
        bindingServerResponseForSubscribeToPlan()
        bindingServerErrorResponseForSubscribeToPlan()
        bindingServerCompletionResponse()
    }
    private func registerNibs()
    {
        // Failed View Nib
        self.failedView = UINib(nibName:    UINibConstant.paymentFailedNib, bundle: .main).instantiate(withOwner: nil, options: nil).first as? PaymentFailedView
        self.failedView.frame = self.view.bounds
        self.failedView.delegate = self
        // Success View Nib
        self.successView = UINib(nibName:    UINibConstant.paymentSuccessNib, bundle: .main).instantiate(withOwner: nil, options: nil).first as? PaymentSuccessView
        self.successView.frame = self.view.bounds
        self.successView.delegate = self
    }
}
extension SubscriptionPlanViewController: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mViewModel.subscriptionArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = subscriptionPlansTableView.dequeueReusableCell(withIdentifier: UINibConstant.subscriptionPlanCellnib, for: indexPath) as! SubsciptionPlanCell
        if self.mViewModel.subscriptionArray.count != 0 {
            cell.showAnimation()
            DispatchQueue.main.asyncAfter(deadline: .now() + 4.0) {
                cell.hideAnimation()
                cell.setPlanNumber(plan: self.self.mViewModel.subscriptionArray[indexPath.row].planID ?? 0)
                cell.setPlanTitle(Title: self.mViewModel.subscriptionArray[indexPath.row].planTitle ?? "")
                cell.setDuration(Duration: self.mViewModel.subscriptionArray[indexPath.row].planDuration ?? "")
                cell.planPrice.text = String(self.mViewModel.subscriptionArray[indexPath.row].planPrice ?? 0)
                
             }
        }
        cell.selectionStyle = .none
        cell.subscriptionBtn.tag = indexPath.row
        cell.subscriptionBtn.addTarget(self, action: #selector(self.handleSubscribe(_:)), for: .touchUpInside)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 173
    }
}
extension SubscriptionPlanViewController {
    
    private func bindingServerResponseForSubscriptionPlan() {
        self.mSongAndArtistViewModel.serverResponseForGetPackagesPlan.bind {
            if ($0 != nil) {
                
                if $0?.response?.status ?? false {

                    guard let PlansArray = $0?.response?.data?.plans else {return}
                    self.mViewModel.subscriptionArray = PlansArray
                    self.removeActivityIndicator()
                    self.subscriptionPlansTableView.reloadData()
                    
                    
                }
                else if $0?.response?.status == false {
                    
                    print("Response for Subsription Plan  is false")
                    Utils.showErrorAlert(vc: self, message: $0?.response?.message)
                    self.removeActivityIndicator()
                    
                }
                
                
                
            }
        }
    }
    
    //MARK: - Binding Error Response For Following Artist
    private func bindingServerErrorResponseForSubscriptionPlan() {
        self.mSongAndArtistViewModel.errorResponseForForSubscribeToPlan.bind {
            if ($0 != nil) {
               
                Utils.showErrorAlert(vc: self, message: $0?.response?.message)
            }
            
            self.removeActivityIndicator()
        }
        
        
    }
    
    
    //MARK: - Binding Complete Response
    private func bindingServerCompletionResponse(){
        self.mSongAndArtistViewModel.completion.bind {
            if ($0 != nil) {
                self.removeActivityIndicator()
            }
        }
        
    }

    private func bindingServerResponseForSubscribeToPlan() {
        self.mSongAndArtistViewModel.serverResponseForSubscribeToPlan.bind {
            if ($0 != nil) {
                
                if $0?.response?.status ?? false {
                    self.removeActivityIndicator()
                    let data = UserDefaultUtils.getProfile(key: CommonKeys.KEY_PROFILE)
                    data?.subscription = $0?.response?.data?.purchasedSubscription?.toSubscription()
                    UserDefaultUtils.saveProfile(value: data ?? AuthorizationBuilder().build(), key: CommonKeys.KEY_PROFILE)
                    self.successView
                        .alpha = 0.0
                    self.view.addSubview(self.successView)
                    UIView.animate(withDuration: 0.25) { () -> Void in
                        self.successView.alpha = 1.0
                    }
                    
                }
                else if $0?.response?.status == false {
                   
                    self.removeActivityIndicator()
                    
                }
            }
        }
    }

    private func bindingServerErrorResponseForSubscribeToPlan() {
        self.mSongAndArtistViewModel.errorResponseForForSubscribeToPlan.bind {
            if ($0 != nil) {
                self.failedView.setFailedMsg(errorLbl: StringConstant.Payment_Failed)
                self.removeActivityIndicator()
                Utils.showErrorAlert(vc: self, message: $0?.response?.message)
                self.failedView.alpha = 0.0
                self.view.addSubview(self.failedView)
                UIView.animate(withDuration: 0.25) { () -> Void in
                    self.failedView.alpha = 1.0
                }
            }
            
            self.removeActivityIndicator()
        }
    }
}

extension SubscriptionPlanViewController: SKProductsRequestDelegate, SKPaymentTransactionObserver {
    func productsRequest (_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        print(response.products.count)
        if let oProducts = response.products.first
        {
            print("product is available")
            print(productIdentifiers)
            self.purchase(aproduct: oProducts)
        }
        else
        {
            Utils.showErrorAlert(vc: self, message: StringConstant.productisNotAvailabe)
        
        }
        
    }
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions {
            switch transaction.transactionState
            {
            case .purchasing:
                print("customer is in process of purchase")
            case .purchased:
                print("Purchased")
                SKPaymentQueue.default().finishTransaction(transaction)
                productIdentifiers.removeAll()
                getPurchaseToken()
                self.mSongAndArtistViewModel.subscribeToPlan(data: self.mViewModel.subscriptionParams())
            case .failed:
                     if let error = transaction.error as? SKError {
                         switch error.code {
                         case .unknown:
                             self.failedView.setFailedMsg(errorLbl: StringConstant.Unknown_Case)
                         case .clientInvalid:
                             self.failedView.setFailedMsg(errorLbl: StringConstant.Invalid_Client)
                         case .paymentCancelled:
                             self.failedView.setFailedMsg(errorLbl: StringConstant.Payment_Cancelled)
                         case .paymentInvalid:
                             self.failedView.setFailedMsg(errorLbl: StringConstant.Invalid_Payment)
                         case .paymentNotAllowed:
                             self.failedView.setFailedMsg(errorLbl: StringConstant.Payment_Not_Allowed)
                         case .storeProductNotAvailable:
                             self.failedView.setFailedMsg(errorLbl: StringConstant.Product_Not_Available)
                         case .cloudServicePermissionDenied:
                             self.failedView.setFailedMsg(errorLbl: StringConstant.Cloud_Service_Permission_Denied)
                         case .cloudServiceNetworkConnectionFailed:
                             self.failedView.setFailedMsg(errorLbl: StringConstant.Cloud_Service_Network_Connection_Failed)
                         default:
                             self.failedView.setFailedMsg(errorLbl: StringConstant.Something_Went_Wrong)
                         }
                         SKPaymentQueue.default().finishTransaction(transaction)
                         productIdentifiers.removeAll()
                         self.failedView.alpha = 0.0
                         self.view.addSubview(failedView)
                         UIView.animate(withDuration: 0.25) { () -> Void in
                             self.failedView.alpha = 1.0
                         }
                         self.removeActivityIndicator()
                     }
                
            case .restored:
                self.failedView.setFailedMsg(errorLbl: StringConstant.Restored)
            case .deferred:
                self.failedView.setFailedMsg(errorLbl: StringConstant.Suspended)
            default : break
            }
        }
    }
    
    
   private func purchase(aproduct: SKProduct)
    {
        if SKPaymentQueue.canMakePayments()
        {
        let payment = SKPayment(product: aproduct)
        SKPaymentQueue.default().add(self)
        SKPaymentQueue.default().add(payment)
        }
    }
   private func getPurchaseToken()
    {
    if let url = Bundle.main.appStoreReceiptURL,
       let data = try? Data(contentsOf: url) {
          var receiptBase64 = data.base64EncodedString()
        self.mViewModel.purchaseToken = receiptBase64
        
    }
    }
}

extension SubscriptionPlanViewController: SuccessViewDelegate
{
    func cancelView() {
        self.removeActivityIndicator()
        self.successView.removeFromSuperview()
    }
    
    
}
extension SubscriptionPlanViewController: FailedViewDelegate
{
    func cancel() {
        self.removeActivityIndicator()
        self.failedView.removeFromSuperview()
    }
    
}
