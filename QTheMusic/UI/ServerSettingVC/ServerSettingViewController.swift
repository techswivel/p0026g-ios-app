//
//  ViewController.swift
//  Base iOS Project
//
//  Created by Hammad Hassan on 21/11/2020.
//  Copyright © 2020 TechSwivel. All rights reserved.
//

import UIKit
import FirebaseCrashlytics
import StoreKit
import RxRelay
import RxSwift
import Alamofire

class ServerSettingViewController: BaseViewController {
    @IBOutlet weak var schemeName: UILabel!
    @IBOutlet weak var schemeURL: UILabel!
    
    @IBOutlet weak var bundleID: UILabel!
    @IBOutlet weak var versionNumber: UILabel!
    @IBOutlet weak var buildNumber: UILabel!
    @IBOutlet weak var minIos: UILabel!
    @IBOutlet weak var currentLocation : UILabel!
    @IBOutlet weak var syncTimeLabel: UILabel!
    
    let bag = DisposeBag()
    var products = BehaviorRelay<[SKProduct]>(value: [])
 
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        DispatchQueue.main.async {
            self.fetchProducts()
        }

        self.schemeName.text = ConfigurationManager.currentEnvironment?.rawValue
        self.schemeURL.text = ProjectConstants.current.getBaseURL()
        self.bundleID.text = Bundle.main.bundleIdentifier
        self.versionNumber.text = (Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String)
        self.buildNumber.text = (Bundle.main.infoDictionary!["CFBundleVersion"] as! String)
          
        if let infoPlist = Bundle.main.infoDictionary {
          let minimumVersion = infoPlist["MinimumOSVersion"]
          if let minimumVersion = minimumVersion {
            self.minIos.text = "iOS \(minimumVersion)"
          }
        }

        guard let fetchTime = UserDefaultUtils.getSyncTime(key: CommonKeys.LAST_SYNC_TIME) else {return}
        print("Date Here: \(fetchTime + ProjectConstants.current.getTimerValue().1)")
        let formatter = DateFormatter()
        formatter.dateFormat = "E, d MMM yyyy HH:mm:ss"
        self.syncTimeLabel.text = formatter.string(from: fetchTime)
        
       
        
        
        
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.view.addGradientBackground()
       
    }
    
    @IBAction func crashBtnPressed(_ sender: Any) {
        fatalError()
        
    }
    @IBAction func backBtnPressed(_ sender: Any) {
        self.popBackController()
        
    }
    
    
    @IBAction func mSubscribe(_ sender: Any) {
        purchase(product: .Subscription) // comment here to for song purchase
    }
    
    @IBAction func purchaseSong(_ sender: Any) {
        purchase(product: .Song)
    }
    
}

// MARK: In app purchase methods
extension ServerSettingViewController {
    private func fetchProducts() {
        self.showActivityIndicator()
        InAppPurchaseHandler.shared.fetchAvailableProducts { (products,message)  in
            
            if message != nil {
                DispatchQueue.main.async {
                    Utils.showErrorAlert(vc: self, message: message ?? "Something Went Wrnong")
                }
                self.removeActivityIndicator()
            }else {
                self.removeActivityIndicator()
                self.products.accept(products!)
            }
            
        }
    }
    
    private func purchase(product: PurchaseProduct) {
        guard let _product = products.value.first(where: {$0.productIdentifier == product.rawValue}) else {
            DispatchQueue.main.async {
                Utils.showErrorAlert(vc: self, message: "Product: \(product.rawValue) not found in Products Set")
            }
            return
        }
        
        self.showActivityIndicator()
        
        InAppPurchaseHandler.shared.purchase(product: _product) { (message, product, transaction) in
            
            if let transaction = transaction, let product = product {
                print("Transaction : \(transaction)")
                print("Product: \(product.productIdentifier)")
                var receiptData: String? {
                    guard
                        let url = Bundle.main.appStoreReceiptURL,
                        let data = try? Data(contentsOf: url)
                    else { return nil }
                    return data.base64EncodedString()
                }
                print("token: \(receiptData)")
                if transaction.error == nil {
                    if transaction.transactionState == .purchased {
                        self.removeActivityIndicator()
                        Utils.showErrorAlert(vc: self, message: message)
                        
                    } else if transaction.transactionState == .failed {
                        self.removeActivityIndicator()
                        Utils.showErrorAlert(vc:self, message: message)
                    }
                } else {
                    self.removeActivityIndicator()
                    Utils.showErrorAlert(vc:self, message: message)
                }
            }else if transaction?.error != nil {
                self.removeActivityIndicator()
                Utils.showErrorAlert(vc:self, message: message)
            }else if message != nil {
                self.removeActivityIndicator()
                Utils.showErrorAlert(vc:self, message: message)
            }else {
                self.removeActivityIndicator()
            }
        }
    }
}


