//
//  FavoriteSongsViewController.swift
//  QtheMusic
//
//  Created by Muhammad Tanzeel Ur Rehman on 08/12/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import Foundation
import UIKit

class FavoriteSongsViewController: BaseViewController{
    @IBOutlet weak var emptyResponseLbl: UILabel!
    @IBOutlet weak var favoriteSongLbl: UILabel!
    
    @IBOutlet weak var favoriteSongTable: UITableView!{
        didSet{
            favoriteSongTable.delegate = self
            favoriteSongTable.dataSource = self
        }
    }
    
    private var mViewModel = FavoriteSongViewModel()
    private var mProfileViewModel = ProfileViewModel()
    private var mSongAndArtistViewModel = SongAndArtistViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addGradientBackground()
        setBindigServerResponse()
        emptyResponseLbl.isHidden = true
        favoriteSongTable.register(UINib(nibName: UINibConstant.SongsTableViewCellNib, bundle: nil), forCellReuseIdentifier: UINibConstant.SongsTableViewCellIdentifier)
        
        mViewModel.songbuilder.settype(SongType.FAVORITES.rawValue)
        showActivityIndicator()
        mSongAndArtistViewModel.songs(data: mViewModel.songbuilder)
    
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        self.popBackNavigationWithTransition()
    }
    
    private func setBindigServerResponse() {
        
        self.bindingServerResponse()
        self.bindingServerErrorResponse()
        self.bindingServerCompletionResponse()
        
    }
    
}

extension FavoriteSongsViewController: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mViewModel.songsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = favoriteSongTable.dequeueReusableCell(withIdentifier: UINibConstant.SongsTableViewCellIdentifier, for: indexPath) as! SongsTableViewCell
        if self.mViewModel.songsArray.count != 0 {
            cell.showAnimation()
            DispatchQueue.main.asyncAfter(deadline: .now() + 4.0) {
                cell.hideAnimation()
                cell.setUpCell(data: self.mViewModel.songsArray[indexPath.row])
             }
        }
        cell.selectionStyle = .none
        return cell 
    }
    
    
    // delete Slider row
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let delete = UIContextualAction(style: .destructive, title: StringConstant.tDelete ) { [unowned self] action, view, completionHandler in
            let builder = SongAndArtistBuilder()
            builder.setSongId(self.mViewModel.songsArray[indexPath.row].songID)
            builder.setIsFavourite(false)
            self.mProfileViewModel.setIsFavourite(data: builder.build())
            mViewModel.songsArray.remove(at: indexPath.row)
            favoriteSongTable.reloadData()
            completionHandler(true)
        }
        return UISwipeActionsConfiguration(actions: [delete])
    }
    
    
}
// MARK: Binding

extension FavoriteSongsViewController{
    
    
    private func bindingServerResponse() {
        self.mSongAndArtistViewModel.serverResponseForSong.bind {
            if ($0 != nil) {
                
                if $0?.response?.status ?? false {
                    self.removeActivityIndicator()
                    
                    self.mViewModel.songsArray = $0?.response?.data?.songs ?? self.mViewModel.songsArray
                    if self.mViewModel.songsArray.count == 0
                    {
                        self.emptyResponseLbl.isHidden = false
                    }
                    else
                    {
                        self.emptyResponseLbl.isHidden = true
                    }
                    self.favoriteSongTable.reloadData()
    
                }
                else if $0?.response?.status == false {
                    
                    Utils.showErrorAlert(vc: self, message: $0?.response?.message)
                    self.removeActivityIndicator()
                    
                }
            }
        }
    }
    
    //MARK: - Binding Error Response
    private func bindingServerErrorResponse() {
        self.mSongAndArtistViewModel.errorResponseForSong.bind {
            if ($0 != nil) {
                Utils.showErrorAlert(vc: self, message: $0?.response?.message)
            }
            
            self.removeActivityIndicator()
        }
        
        
    }
    
    //MARK: - Binding Complete Response
    private func bindingServerCompletionResponse(){
        self.mSongAndArtistViewModel.completion.bind {
            if ($0 != nil) {
                
                
            }
        }
        
    }
}
