//
//  FavoriteSongViewModel.swift
//  QtheMusic
//
//  Created by Muhammad Tanzeel Ur Rehman on 08/12/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import Foundation
import UIKit

class FavoriteSongViewModel: BaseViewModel {
    
    var categoryType :String?
    var album : Album?
    var song:Song?
    var songArray : Song?
    var songsArray = [Song]()
    var isplayingFromString:String?
    var isPlaying:Bool? = false
    var titleOfAudio = ""
    var maxValueOfSeekBar : CGFloat = 0
    var indexPathRow : Int? = -1
    
    var songbuilder : SongAndArtistBuilder = SongAndArtistBuilder()
    
    func insertDataIntoRecentlyPlayed(){
        if let CategoryType = categoryType {
            if   CategoryType == RecentlyPlayedCatEnum.Albums.rawValue{
                
                mDataManager.insertSongIntoDb(song: song)
                
                mDataManager.insertAlbumIntoDb(album: album)
                
            }else if CategoryType == RecentlyPlayedCatEnum.Songs.rawValue{
                
                
                mDataManager.insertSongIntoDb(song: song)
            }
            
        }
    }
}
