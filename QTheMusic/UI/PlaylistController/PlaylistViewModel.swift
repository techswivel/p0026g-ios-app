//
//  PlaylistViewModel.swift
//  QtheMusic
//
//  Created by Muhammad Tanzeel Ur Rehman on 07/12/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

class PlaylistViewModel: BaseViewModel {
    
  
    
    var categoryType :String?
    var album : Album?
    var song:Song?
    var songsArray = [Playlist]()
    var isplayingFromString:String?
    var isPlaying:Bool? = false
    var titleOfAudio = ""
    var maxValueOfSeekBar : CGFloat = 0
    var indexPathRow : Int? = -1
    
    var playlist: Playlist?
    var songBuilder: SongAndArtistBuilder = SongAndArtistBuilder()
    
    func insertDataIntoRecentlyPlayed(){
        if let CategoryType = categoryType {
            if   CategoryType == RecentlyPlayedCatEnum.Albums.rawValue{
                
                mDataManager.insertSongIntoDb(song: song)
                mDataManager.insertAlbumIntoDb(album: album)
                
            }else if CategoryType == RecentlyPlayedCatEnum.Songs.rawValue{
                
                mDataManager.insertSongIntoDb(song: song)
            }
        }
    }
}
