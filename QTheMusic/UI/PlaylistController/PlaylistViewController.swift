//
//  PlaylistViewController.swift
//  QtheMusic
//
//  Created by Muhammad Tanzeel Ur Rehman on 08/12/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import Foundation
import UIKit

protocol PlaylistViewControllerDelegate {
    func addNew(todo: String)
}

class PlaylistViewController : BaseViewController {
    
    
    @IBOutlet weak var emptyResponseLbl: UILabel!
    @IBOutlet weak var playlistlabel : UILabel!
    
    @IBOutlet weak var playlistTableView : UITableView! {
        didSet {
            playlistTableView.delegate = self
            playlistTableView.dataSource = self
        }
    }
    
    // variables
    
    private var delegate : PlaylistViewControllerDelegate?
    private var mViewModel = PlaylistViewModel()
    private var listTitle : UITextField!
    private var mProfileViewModel = ProfileViewModel()
    private var mSongAndArtistViewModel = SongAndArtistViewModel()
    private var playlistView : AddPlaylistView!
    
    private var getTitle : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addGradientBackground()
        setBindigServerResponse()
        emptyResponseLbl.isHidden = true
        playlistTableView.register(UINib(nibName: UINibConstant.PlaylistCellnib, bundle: nil), forCellReuseIdentifier: UINibConstant.PlaylistCellIdentifier)
        nibAddplaylist()
        mProfileViewModel.getPlaylist()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.view.addGradientBackground()
        
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        self.popBackNavigationWithTransition()
    }
    
    @IBAction func addBtnPressed (_ sender : Any) {
        self.view.addSubview(self.playlistView)
    }
    
    // register nib for addlist view
    private func nibAddplaylist () {
        
        self.playlistView = UINib(nibName: UINibConstant.Addplaylist, bundle: .main).instantiate(withOwner: nil, options: nil).first as? AddPlaylistView
        self.playlistView.frame = self.view.bounds
        playlistView.setDelegate(delegate: self)
    }
    private func setBindigServerResponse(){
        
        self.bindingServerResponse()
        self.bindingServerErrorResponse()
        self.bindingServerCompletionResponse()
        
        self.bindingServerResponsePlaylist()
        self.bindingServerErrorResponsePlaylist()
        self.bindingServerCompletionResponsePlaylist()
        
    }
 
}

// MARK: Tableview Delegates

extension PlaylistViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        // songs screen
        let playlistSongVC = UIStoryboard.init(name: StoryBoardConstant.storyBoardMain, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.songslistScreen) as! GeneralSongViewController
        // data passing to next screen
        playlistSongVC.playListId = mViewModel.songsArray[indexPath.row].playListID ?? 0
        playlistSongVC.titlelabel = mViewModel.songsArray[indexPath.row].playListTitle ?? ""
        self.navigationController?.pushViewController(playlistSongVC, animated: true)
        
    }
    
    private func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.backgroundColor = UIColor.clear
    }
    
    // table data
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mViewModel.songsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = playlistTableView.dequeueReusableCell(withIdentifier: UINibConstant.PlaylistCellIdentifier, for: indexPath) as! PlaylistTableCell
        
        
        if self.mViewModel.songsArray.count != 0 {
            cell.showAnimation()
            DispatchQueue.main.asyncAfter(deadline: .now() + 4.0) {
                cell.hideAnimation()
                cell.setupCell(playlist: self.mViewModel.songsArray[indexPath.row])
                
             }
        }
        cell.selectionStyle = .none
        return cell
    }
    
    // delete Slider row
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let delete = UIContextualAction(style: .destructive, title: StringConstant.tDelete ) { [unowned self] action, view, completionHandler in
            mProfileViewModel.deletePlaylist(playlistID: mViewModel.songsArray[indexPath.row].playListID ?? 0)
            mViewModel.songsArray.remove(at: indexPath.row)
            playlistTableView.reloadData()
            completionHandler(true)
        }
        return UISwipeActionsConfiguration(actions: [delete])
    }
    
}

//MARK: - Update User Name Delegate
extension PlaylistViewController: AddPlaylistDelegate{
    
    internal func updateTitle(name: String) {
        self.getTitle = name
        mProfileViewModel.savePlaylist(title: getTitle)
        playlistTableView.reloadData()
        playlistView.removeFromSuperview()
    }
    
    internal func closeAddTitleView() {
        self.playlistView.removeFromSuperview()
    }
}

// MARK: Binding server response for AddPlaylist
extension PlaylistViewController{
    
    private func bindingServerResponsePlaylist(){
        self.mProfileViewModel.serverResponseForSavePlaylist.bind {
            if ($0 != nil) {
                
                if $0?.response?.status ?? false {
                    self.removeActivityIndicator()
                    self.mViewModel.songsArray.append(($0?.response?.data?.playlist ?? self.mViewModel.playlist)! )
                    if self.mViewModel.songsArray.count == 0
                    {
                        self.emptyResponseLbl.isHidden = false
                    }
                    else
                    {
                        self.emptyResponseLbl.isHidden = true
                    }
                   
                    self.playlistTableView.reloadData()
                    
                }
                else if $0?.response?.status == false {
                    Utils.showErrorAlert(vc: self, message: $0?.response?.message)
                    self.removeActivityIndicator()
                }
            }
        }
    }
    //MARK: - Binding Error Response
    private func bindingServerErrorResponsePlaylist() {
        self.mProfileViewModel.errorResponseForSavePlaylist.bind {
            if ($0 != nil) {
                Utils.showErrorAlert(vc: self, message: $0?.response?.message)
            }
            self.removeActivityIndicator()
        }
    }
    //MARK: - Binding Complete Response
    private func bindingServerCompletionResponsePlaylist(){
        self.mProfileViewModel.completion.bind {
            if ($0 != nil) {
            }
        }
    }
    
// MARK: Binding server response for Playlist
    private func bindingServerResponse() {
        self.mProfileViewModel.serverResponseForPlaylist.bind {
            if ($0 != nil) {
                
                if $0?.response?.status ?? false {
                    self.removeActivityIndicator()
                    
                    self.mViewModel.songsArray = $0?.response?.data?.playlists ?? self.mViewModel.songsArray
                    if self.mViewModel.songsArray.count == 0
                    {
                        self.emptyResponseLbl.isHidden = false
                    }
                    else
                    {
                        self.emptyResponseLbl.isHidden = true
                    }
                    self.playlistTableView.reloadData()
                }
                else if $0?.response?.status == false {
                    
                    Utils.showErrorAlert(vc: self, message: $0?.response?.message)
                    self.removeActivityIndicator()
                }
            }
        }
    }
    
    //MARK: - Binding Error Response
    private func bindingServerErrorResponse() {
        self.mProfileViewModel.errorResponseForPlaylist.bind {
            if ($0 != nil) {
                Utils.showErrorAlert(vc: self, message: $0?.response?.message)
            }
            
            self.removeActivityIndicator()
        }
        
        
    }
    
    //MARK: - Binding Complete Response
    private func bindingServerCompletionResponse(){
        self.mProfileViewModel.completion.bind {
            if ($0 != nil) {
                
                
            }
        }
        
    }
}
