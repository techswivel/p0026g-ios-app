//
//  PlaylistSongController.swift
//  QtheMusic
//
//  Created by Muhammad Tanzeel Ur Rehman on 08/12/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import Foundation
import UIKit

class GeneralSongViewController : BaseViewController {
    
    @IBOutlet weak var emptyResponseLbl : UILabel!
    
    
    
    @IBOutlet weak var listlabel : UILabel!
    
    @IBOutlet weak var playlistSongsTableV: UITableView! {
        didSet {
            playlistSongsTableV.delegate = self
            playlistSongsTableV.dataSource = self
        }
        
    }
    
    var titlelabel = ""
    var playListId = Int()
    private var mViewModel = GeneralSongViewModel()
    private var mProfileViewModel = ProfileViewModel()
    private var mSongAndArtistViewModel = SongAndArtistViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        emptyResponseLbl.isHidden = true
        self.view.addGradientBackground()
        setBindigServerResponse()
        listlabel.text = titlelabel
        
        playlistSongsTableV.register(UINib(nibName: UINibConstant.SongsTableViewCellNib, bundle: nil), forCellReuseIdentifier: UINibConstant.SongsTableViewCellIdentifier)
        
        mViewModel.songBuilder.settype(SongType.PLAY_LIST.rawValue)
        mViewModel.songBuilder.setPlayListId(playListId)
        
        showActivityIndicator()
        mSongAndArtistViewModel.songs(data: mViewModel.songBuilder)
    
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        self.popBackNavigationWithTransition()
    }
    
    private func setBindigServerResponse() {
        self.bindingServerResponse()
        self.bindingServerErrorResponse()
        self.bindingServerCompletionResponse()
    }
}

extension GeneralSongViewController : UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mViewModel.songsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = playlistSongsTableV.dequeueReusableCell(withIdentifier: UINibConstant.SongsTableViewCellIdentifier, for: indexPath) as! SongsTableViewCell
        if self.mViewModel.songsArray.count != 0 {
            cell.showAnimation()
            DispatchQueue.main.asyncAfter(deadline: .now() + 4.0) {
                cell.hideAnimation()
                cell.setUpCell(data: self.mViewModel.songsArray[indexPath.row])
             }
        }
        cell.selectionStyle = .none
        return cell
    }
    
    // delete Slider row
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let delete = UIContextualAction(style: .destructive, title: StringConstant.tDelete ) { [unowned self] action, view, completionHandler in
            let songbuilder = SongAndArtistBuilder()
            songbuilder.setSongId(self.mViewModel.songsArray[indexPath.row].songID)
            songbuilder.setplayListId(self.mViewModel.songsArray[indexPath.row].playListID)
            songbuilder.settype(PlaylistEnum.REMOVE.rawValue)
            self.mProfileViewModel.updatePlaylist(builder: songbuilder)
            mViewModel.songsArray.remove(at: indexPath.row)
            playlistSongsTableV.deleteRows(at: [indexPath], with: .fade)
            completionHandler(true)
        }
        return UISwipeActionsConfiguration(actions: [delete])
    }
    
}
// MARK: Binding

extension GeneralSongViewController{
    
    
    private func bindingServerResponse() {
        self.mSongAndArtistViewModel.serverResponseForSong.bind {
            if ($0 != nil) {
                
                if $0?.response?.status ?? false {
                    self.removeActivityIndicator()
                    
                    self.mViewModel.songsArray = $0?.response?.data?.songs ?? self.mViewModel.songsArray
                    if self.mViewModel.songsArray.count == 0
                    {
                        self.emptyResponseLbl.isHidden = false
                    }
                    else
                    {
                        self.emptyResponseLbl.isHidden = true
                    }
                    self.playlistSongsTableV.reloadData()
                    
                }
                else if $0?.response?.status == false {
                    
                    Utils.showErrorAlert(vc: self, message: $0?.response?.message)
                    self.removeActivityIndicator()
                    
                }
            }
        }
    }
    
    //MARK: - Binding Error Response
    private func bindingServerErrorResponse() {
        self.mSongAndArtistViewModel.errorResponseForSong.bind {
            if ($0 != nil) {
                Utils.showErrorAlert(vc: self, message: $0?.response?.message)
            }
            
            self.removeActivityIndicator()
        }
        
        
    }
    
    //MARK: - Binding Complete Response
    private func bindingServerCompletionResponse(){
        self.mSongAndArtistViewModel.completion.bind {
            if ($0 != nil) {
                
                
            }
        }
        
    }
}

