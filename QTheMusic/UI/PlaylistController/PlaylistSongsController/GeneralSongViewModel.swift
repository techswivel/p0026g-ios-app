//
//  PlaylistSongViewModel.swift
//  QtheMusic
//
//  Created by Muhammad Tanzeel Ur Rehman on 08/12/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import Foundation
import UIKit

class GeneralSongViewModel: BaseViewModel {
    
    var song:Song?
    
    var albumsArray  = [Album]()
    var songsArray = [Song]()
    var songBuilder : SongAndArtistBuilder = SongAndArtistBuilder()

}
