//
//  CommonKeys.swift
//  Base iOS Project
//
//  Created by Mac on 13/01/2021.
//  Copyright © 2021 TechSwivel. All rights reserved.
//

import Foundation

class CommonKeys{
    
    public static let KEY_PROFILE = "_user_profile_"
    public static let LAST_SYNC_TIME = "_sync_time_"
    public static let SYNC_STATUS = "_sync_status_"
    public static let IsPlayingFromNotification = "isPlayingFromNotification"
    public static let AudioUrl = "audioUrl"
    public static let SongTitle = "songTitle"
    public static let NotImage = "image"
    public static let ArtistName = "artist"
    public static let SONG_PRICE = "price"
    public static let Payment_Failed = "PaymentFailed"
    public static let Favourite_SONG = "favourite"
    public static let Favourite_Icon = "FavIcon"
}
