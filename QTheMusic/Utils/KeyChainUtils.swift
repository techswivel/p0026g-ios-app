//
//  KeyChainUtils.swift
//  Base iOS Project
//
//  Created by Mac on 13/01/2021.
//  Copyright © 2021 TechSwivel. All rights reserved.
//

import Foundation
import SwiftKeychainWrapper


struct AppleUserProfile: Codable,Equatable {
    let firstName : String?
    let lastName : String?
    let email : String?
    let userIdentifier : String?
    
    
}


class KeyChainWrapperUtils {
    
    static let shared = KeyChainWrapperUtils()
    
 
    
    public func getAppleUserProfiles() -> [AppleUserProfile]?{
        
        if let savedProfile = KeychainWrapper.standard.data(forKey: .appleUserKey) {
            let decoder = JSONDecoder()
            print(AppleUserProfile.self,"Profile data")
            if let loadedProfile = try? decoder.decode([AppleUserProfile].self, from: savedProfile) {
                print("Loaded")
                return loadedProfile

            }else{"HERE is it "}
        }else{print("In else")}
        print("Nil")
        return nil
    }
    
    
    private  func saveAppleUserIdList(userProfile:[AppleUserProfile]){
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(userProfile) {
            KeychainWrapper.standard[.appleUserKey] = encoded
             
        }
    }
    
    public  func addAppleUserProfileToList(userProfile:AppleUserProfile){
        if let savedProfiles = KeychainWrapper.standard.data(forKey: .appleUserKey) {
            let decoder = JSONDecoder()
            if var loadedProfiles = try? decoder.decode([AppleUserProfile].self, from: savedProfiles) {
                
                var userAlreadyExist = false
                for i in 0..<loadedProfiles.count {
                    if userProfile.userIdentifier == loadedProfiles[i].userIdentifier {
                        userAlreadyExist = true
                        
                        
                        
                        /// iF existing user in Keychain  updates its firstName or lastName then it must be update in Keychain
                        if userProfile.firstName != nil && userProfile.lastName != nil && userProfile.email != nil {
                            loadedProfiles[i] = userProfile
                            self.saveAppleUserIdList(userProfile: loadedProfiles)
                        }
                        
                        
                        
                    }
                }
                
                if userAlreadyExist == false {
                    print("user not exist 1")
                    loadedProfiles.append(userProfile)
                    self.saveAppleUserIdList(userProfile: loadedProfiles)
                    
                }
                
                
            }
            else {
                print("user not exist 2")
                var newList = [AppleUserProfile]()
                newList.append(userProfile)
                self.saveAppleUserIdList(userProfile: newList)
            }
            
            
        } else {
            print("user not exist 3")
            var newList = [AppleUserProfile]()
            newList.append(userProfile)
            self.saveAppleUserIdList(userProfile: newList)
        }
        

    }
    
    public  func isAppleIdAlreadySave(userProfile:AppleUserProfile) -> Bool{
        
        if let savedProfiles = KeychainWrapper.standard.data(forKey: .appleUserKey) {
            let decoder = JSONDecoder()
            if let loadedProfiles = try? decoder.decode([AppleUserProfile].self, from: savedProfiles) {
                
                if loadedProfiles.contains(userProfile) {
                    return true
                }else {
                    return false
                }
        
            }
        }
        
        return false
    }
    
    
    
}

extension KeychainWrapper.Key {
    static let appleUserKey: KeychainWrapper.Key = "appleUserKey"
}


class KeyChainUtils {
    
    private let keychainAccessGroupName = ProjectConstants.current.KEY_CHAIN_SHARED_GROUP
    //MARK: - Add Data in Keychain:
    func addKeychainData(itemKey: String, itemValue: String) -> Bool {
        

        guard let valueData = itemValue.data(using: String.Encoding.utf8) else {
            print("False in add");return false
        }
        
        // Delete the item before adding, otherwise there will be multiple items with the same name
        deleteKeychainData(itemKey:itemKey)
        
        let queryAdd: [String: AnyObject] = [
          kSecClass as String: kSecClassGenericPassword,
          kSecAttrAccount as String: itemKey as AnyObject,
          kSecValueData as String: valueData as AnyObject,
          kSecAttrAccessible as String: kSecAttrAccessibleWhenUnlocked,
          kSecAttrAccessGroup as String : keychainAccessGroupName as AnyObject
        ]
        
       
         
        
        
        let resultCode = SecItemAdd(queryAdd as CFDictionary, nil)
        print("Result code = \(resultCode)")
        if resultCode != noErr { print("Not added ");return false }
        print("Added")
        return true
    }
      
    //MARK: - Delete KeyChain Data:
      func deleteKeychainData(itemKey: String) ->Bool {
        
          let queryDelete: [String: AnyObject] = [
            kSecClass as String: kSecClassGenericPassword,
            kSecAttrAccount as String: itemKey as AnyObject,
            kSecAttrAccessGroup as String:keychainAccessGroupName as AnyObject
          ]
          
         
           
          
          
          let resultCodeDelete = SecItemDelete(queryDelete as CFDictionary)
          
          if resultCodeDelete != noErr { print("Not deleted"); return false }
          print("Deleted")
          return true
        
      }
      
    
    
    //MARK: - Query KeyChain Data:
    
      func queryKeychainData (itemKey: String) throws -> String? {
        
          let queryLoad: [String: AnyObject] = [
            kSecClass as String: kSecClassGenericPassword,
            kSecAttrAccount as String: itemKey as AnyObject,
            kSecReturnData as String: kCFBooleanTrue,
            kSecMatchLimit as String: kSecMatchLimitOne,
            kSecAttrAccessGroup as String:keychainAccessGroupName as AnyObject
          ]
          
          
            
          
          
          var result: AnyObject?
          
          let resultCodeLoad = withUnsafeMutablePointer(to: &result) {
            SecItemCopyMatching(queryLoad as CFDictionary, UnsafeMutablePointer($0))
          }
          
          if resultCodeLoad == noErr {
            if let result = result as? Data,
              let keyValue = NSString(data: result,
                                  encoding: String.Encoding.utf8.rawValue) as? String {
              print("Found")
              return keyValue
            }
          }
          print("Not found")
          return nil
      }
      
      
      /// Creates a new unique user identifier or retrieves the last one created
     static func getUUID() -> String? {

          /// create a keychain helper instance
          let keychain = KeyChainUtils()

         
          /// this is the key we'll use to store the uuid in the keychain
        /// let uuidKey = "com.myorg.myappid.unique_uuid"
        let uuidKey = "_Eloumo.unique_uuid_"

          /// check if we already have a uuid stored, if so return it
           if let uuid = try? keychain.queryKeychainData(itemKey: uuidKey) {
              /// Appending Uuid with scheme name
               ///
               print("UUID FOUND IN KECHAAIN GROUP \(uuid)")
              if ConfigurationManager.currentEnvironment == .Staging {
                  return uuid + "-staging"
              }else if ConfigurationManager.currentEnvironment == .Development {
                  return uuid + "-development"
              }else if ConfigurationManager.currentEnvironment == .Acceptance {
                  return uuid + "-acceptance"
              }else {
                  return uuid
              }
              
              
              
           }else{
               print("No uuid found in keychain")
           }

          /// generate a new id
         guard let newId = UIDevice.current.identifierForVendor?.uuidString else {
            
              return nil
          }

         print("New uuid \(newId)")
          /// store new identifier in keychain
          keychain.addKeychainData(itemKey: uuidKey, itemValue: newId)

         /// Appending Uuid with scheme name
         if ConfigurationManager.currentEnvironment == .Staging {
             return newId + "-staging"
         }else if ConfigurationManager.currentEnvironment == .Development {
             return newId + "-development"
         }
         else if ConfigurationManager.currentEnvironment == .Acceptance {
             return newId + "-acceptance"
         }else {
             return newId
         }
         
      }
    
}
