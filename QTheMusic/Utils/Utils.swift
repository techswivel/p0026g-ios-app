//
//  Utils.swift
//  Base iOS Project
//
//  Created by Mac on 13/01/2021.
//  Copyright © 2021 TechSwivel. All rights reserved.
//

import Foundation
import UIKit

class Utils {
  
    
    public static func dateConverstion(timeStamp:Double, format: DateFormatEnum) -> String {
        let date = Date(timeIntervalSince1970: timeStamp)
        let dateFormatter = DateFormatter()
        
        switch format {
        case .FORMAT_HOMESCREEN_TOOLBAR :
            dateFormatter.dateFormat =  "d MMMM YYYY"
            break
        case .CALANDAR_DATE_FORMAT:
            dateFormatter.dateFormat = "MMM, d E"
        case .CALANDAR_YEAR_FORMAT :
            dateFormatter.dateFormat = "YYYY"
        case .CALANDAR_MONTHYEAR_FORMAT:
            dateFormatter.dateFormat = "MMM yyyy"
        case .CALANDAR_DAY_FORMAT:
            dateFormatter.dateFormat = "dd"
        case .RELEASING_DATE:
            
            dateFormatter.timeZone = .current
            dateFormatter.dateFormat  = "d MMM yyyy"
           
        }
        
        

        return dateFormatter.string(from: date)
    }
    
    
    static public func showToast(view:UIView,message : String) {
        
        let toastLabel = UILabel(frame: CGRect(x: view.frame.size.width/2 - 150, y: view.frame.size.height-100, width: 300, height: 45))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.font = FontConstants.getManropeRegular(size: 15)
        toastLabel.textAlignment = .center;
        toastLabel.text = message
        toastLabel.numberOfLines = 10
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        view.addSubview(toastLabel)
        UIView.animate(withDuration: 5.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
    
    static public func showToast(view:UIView,message : String,complete onComplete: @escaping () -> Void) {
        
        let toastLabel = UILabel(frame: CGRect(x: view.frame.size.width/2 - 150, y: view.frame.size.height-100, width: 300, height: 45))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.font = FontConstants.getManropeRegular(size: 15)
        toastLabel.textAlignment = .center;
        toastLabel.text = message
        toastLabel.numberOfLines = 10
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        view.addSubview(toastLabel)
        UIView.animate(withDuration: 5.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            onComplete()
            toastLabel.removeFromSuperview()
        })
    }
    
    static public func showAlert(vc : UIViewController,title:String,message:String,titleYesButton:String,onYesPress yes: @escaping () -> Void){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let yesAction = UIAlertAction(title: titleYesButton, style: .default) { (action) in
            yes()
        }

        alert.addAction(yesAction)

        vc.present(alert, animated: true)
    }
    
    static public func showAlert(vc:UIViewController,message:String){
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(okAction)
        vc.present(alert, animated: true)
    }
    static public func showErrorAlert(vc:UIViewController,message:String?){
        let alert = UIAlertController(title: "Alert", message: message ?? "\(StringConstant.ErrorAlert)", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(okAction)
        vc.present(alert, animated: true)
    }
    
    static public func showAlert(vc:UIViewController,title:String,message:String,titleYesButton:String,titleNoButton:String,onYesPress yes: @escaping () -> Void,onNoPress no: @escaping () -> Void){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let yesAction = UIAlertAction(title: titleYesButton, style: .default) { (action) in
            yes()
        }
        let noAction = UIAlertAction(title: titleNoButton, style: .default){(action)in
            no()
        }
        alert.addAction(yesAction)
        alert.addAction(noAction)
     
        vc.present(alert, animated: true)
    }
    
    static public func showExpireAlert(vc:UIViewController,onOkPress ok: @escaping () -> Void){
        let alert = UIAlertController(title: StringConstant.sessionExpiryAlerTitle, message: StringConstant.sessionExpiryAlerMsssage, preferredStyle: .alert)
        let yesAction = UIAlertAction(title: StringConstant.AlertOkAction, style: .default) { (action) in
            ok()
        }
        alert.addAction(yesAction)
        vc.present(alert, animated: true)
    }

    static public func showActionSheet(vc:UIViewController,title:String?,message:String?,noBtnTile:String,onNoPress no : @escaping() ->Void){
      
        let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)

            let noBtnAction = UIAlertAction(title: noBtnTile, style: .cancel) { _ in
                no()
            }
            actionSheetControllerIOS8.addAction(noBtnAction)

            

            vc.present(actionSheetControllerIOS8, animated: true, completion: nil)
    }
    //MARK: - converting Seconds to String
   static func secondsToHoursMinutesSecondsString (seconds : Int) -> String {
        
        
        let h = seconds / 3600
        let m = (seconds % 3600) / 60
        let s = ((seconds % 3600) % 60)
        var duration = ""
        if h > 0 && h < 10{
            duration.append("0\(h):")
        }else if(h > 0 && h > 9){
            duration.append("\(h):")
        }
        if m > 0 && m < 10{
            duration.append("0\(m):")
        }else if(m > 0 && m > 9){
            duration.append("\(m):")
        }else{
            duration.append("00:")
        }
        if s > 0 && s < 10{
            duration.append("0\(s)")
        }else if(s > 0 && s > 9){
            duration.append("\(s)")
        }else{
            duration.append("00")
        }
        
        return duration
        
        
    }
    
    public static func convertTimeStampToDate(timeStamp:Double)-> Date{
        let epocTime = TimeInterval(timeStamp)
        let mydate = Date.init(timeIntervalSince1970: epocTime)
        return mydate
    }
}

