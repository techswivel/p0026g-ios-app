//
//  UserDefaultUtils.swift
//  Base iOS Project
//
//  Created by Mac on 13/01/2021.
//  Copyright © 2021 TechSwivel. All rights reserved.
//

import Foundation

class UserDefaultUtils {
    private static func getUserDefault()->UserDefaults{
        return UserDefaults.standard
        
    }
    
    public static func isValueNil(key:String) ->Any?{
       return getUserDefault().object(forKey: key)
    }
    public static func setInteger(value:Int,key:String){
        getUserDefault().set(value, forKey: key)
        getUserDefault().synchronize()
    }
    
    public static func getInteger(key:String) -> Int{
        return getUserDefault().integer(forKey: key)
    }
    
    public static func setDouble(value:Double,key:String){
        getUserDefault().set(value, forKey: key)
        getUserDefault().synchronize()
    }
    
    public static func getDouble(key:String) -> Double{
        return getUserDefault().double(forKey: key)
    }
    
    public static func setString(value:String,key:String){
        getUserDefault().set(value, forKey: key)
        getUserDefault().synchronize()
    }
    
    public static func getString(key:String) -> String{
        return getUserDefault().string(forKey: key) ?? ""
        
    }
    
    public static func setBool(value:Bool,key:String){
        getUserDefault().set(value, forKey: key)
        getUserDefault().synchronize()
    }
    
    public static func getBool(key:String) -> Bool{
        return getUserDefault().bool(forKey: key)
    }
    
    public static func saveSongData(value:Song,key  :String){
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(value) {
             getUserDefault().set(encoded, forKey: key)
             getUserDefault().synchronize()
        }
    }
    
    public static func saveSyncTime(value: Date, key:String) {
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(value) {
             getUserDefault().set(encoded, forKey: key)
             getUserDefault().synchronize()
        }
    }
    
    public static func getSyncTime(key:String) -> Date? {
        if let savedSyncDate = getUserDefault().object(forKey: key) as? Data {
             let decoder = JSONDecoder()
             if let loadedSyncDate = try? decoder.decode(Date.self, from: savedSyncDate) {
                 return loadedSyncDate
             }
         }
         getUserDefault().synchronize()
         return nil
    }
    
    public static func getSavedSongData(key:String)->Song?{
        if let savedSong = getUserDefault().object(forKey: key) as? Data {
             let decoder = JSONDecoder()
             if let loadedSong = try? decoder.decode(Song.self, from: savedSong) {
                 return loadedSong
             }
         }
         getUserDefault().synchronize()
         return nil
    }
    public static  func saveSongArray(value:[Song],key :String){
       
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(value) {
             getUserDefault().set(encoded, forKey: key)
             getUserDefault().synchronize()
        }
    }
    
    public static func getSavedSongArray(key:String)->[Song]?{
        if let savedSongs = getUserDefault().object(forKey: key) as? Data {
          let decoder = JSONDecoder()
          if let loadedSongs = try? decoder.decode([Song].self, from: savedSongs) {
             
              return loadedSongs
              
          }
        }
        getUserDefault().synchronize()
        
        return nil
    }
    
    public static func saveProfile(value:Authorization,key:String){
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(value) {
             getUserDefault().set(encoded, forKey: key)
             getUserDefault().synchronize()
        }
        
       
    }
    
    public static func getProfile(key:String) -> Authorization?{
       if let savedProfile = getUserDefault().object(forKey: key) as? Data {
            let decoder = JSONDecoder()
            if let loadedProfile = try? decoder.decode(Authorization.self, from: savedProfile) {
                return loadedProfile
                // print("jWTLoaded: \(loadedProfile.jwt)")
            }
        }
        getUserDefault().synchronize()
        return nil
    }
    public static func saveCategory(value:[Category],key:String){
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(value) {
             getUserDefault().set(encoded, forKey: key)
             getUserDefault().synchronize()
        }
    }
    public static func getCategory(key:String) -> [Category]?{
       if let savedProfile = getUserDefault().object(forKey: key) as? Data {
            let decoder = JSONDecoder()
            if let loadedProfile = try? decoder.decode([Category].self, from: savedProfile) {
                return loadedProfile
            }
        }
        getUserDefault().synchronize()
        return nil
    }
    public static func removeValueForKey(key:String){

        getUserDefault().removeObject(forKey: key)
        getUserDefault().synchronize()
   }
    
    
    
    //MARK: - Get Object from UserDefault
     public static func object<T: Codable>(_ type: T.Type, with key: String, usingDecoder decoder: JSONDecoder = JSONDecoder()) -> T? {
             guard let data = getUserDefault().value(forKey: key) as? Data else { return nil }
             return try? decoder.decode(type.self, from: data)
         }
     
    //MARK: - Set Object in UserDefault
      public static func set<T: Codable>(object: T, forKey key: String, usingEncoder encoder: JSONEncoder = JSONEncoder()) {
             let data = try? encoder.encode(object)
             getUserDefault().set(data, forKey: key)
         }
    
}
