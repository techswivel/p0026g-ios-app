//
//  PasswordTextField.swift
//  Toothdolist
//
//  Created by macbook on 08/09/2021.
//  Copyright © 2021 TechSwivel. All rights reserved.
//

import Foundation
import UIKit
class PasswordTextField: UITextField {

    override var isSecureTextEntry: Bool {
        didSet {
            if isFirstResponder {
                _ = becomeFirstResponder()
                //MARK:- Do something what you want
            }
        }
    }

    override func becomeFirstResponder() -> Bool {

        let success = super.becomeFirstResponder()
        if isSecureTextEntry, let text = self.text {
           
            self.text?.removeAll()
            insertText(text)
        }
         return success
    }
}
