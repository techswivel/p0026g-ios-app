//
//  AllPlaylistView.swift
//  QtheMusic
//
//  Created by Assad Khan on 06/09/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import UIKit

protocol AllPlaylistViewProtocol{
    func setDelegate(delegate:AllPlaylistViewDelegate)
    func setPlayList(playListArray:[Playlist])
}
protocol AllPlaylistViewDelegate{
    func closeBtnPressed()
    func doneBtnPressed(playListId:Int)
}

class AllPlaylistView: UIView, UITableViewDelegate, UITableViewDataSource {
  
    

    @IBOutlet weak var outerView: UIImageView!{
        didSet{
            
        }
    }
    @IBOutlet weak var doneBtn: UIButton!{
        didSet{
            doneBtn.addBtnCornerRadius()
        }
    }
    
    
    @IBOutlet weak var containerView: UIView!{
        didSet{
            containerView.roundCorner(radius: 12)
        }
    }
    
    @IBOutlet weak var playListTableView: UITableView?{
        didSet{
            playListTableView?.delegate = self
            playListTableView?.dataSource = self
        }
    }
    
    private var delegate:AllPlaylistViewDelegate?
    private var playListArray = [Playlist]()
    private var playListid:Int?
    internal var vc:ScreenOverlayViewController?
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setUpView()
    }
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        self.setUpView()
    }
    
    
    @IBAction func closeBtnPressed(_ sender: Any) {
        delegate?.closeBtnPressed()
        
    }
    @IBAction func doneBtnPressed(_ sender: Any) {
       
        guard let id = self.playListid else{
            Utils.showAlert(vc: vc!, message: StringConstant.selectPlayList) ;return}
        
        
        delegate?.doneBtnPressed(playListId: id)
    }
   
    
    private func setUpView(){
        guard let tableView = playListTableView else{return}
        tableView.register(UINib(nibName: UINibConstant.PlayListTableViewCell, bundle: nil),  forCellReuseIdentifier: UINibConstant.PlayListCellIdentifier)
        for items in self.playListArray{
            items.isSelected = false
            
        }
        self.playListid = nil
        tableView.reloadData()
    }
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.playListArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: UINibConstant.PlayListCellIdentifier, for: indexPath) as! PlayListTableViewCell
        cell.setupCell(playList: self.playListArray[indexPath.row])
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        for items in self.playListArray{
            items.isSelected = false
        }
        
        self.playListArray[indexPath.row].isSelected = true
        self.playListid =  self.playListArray[indexPath.row].playListID
        self.playListTableView?.reloadData()
    }
    
}
extension AllPlaylistView:AllPlaylistViewProtocol{
    func setPlayList(playListArray: [Playlist]) {
        self.playListArray = playListArray
    }
    
    func setDelegate(delegate: AllPlaylistViewDelegate) {
        self.delegate = delegate
    }
    
    
}
