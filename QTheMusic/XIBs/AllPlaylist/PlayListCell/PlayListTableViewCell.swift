//
//  PlayListTableViewCell.swift
//  QtheMusic
//
//  Created by Assad Khan on 09/09/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import UIKit

class PlayListTableViewCell: UITableViewCell {
    @IBOutlet weak var playListName: UILabel!
    
    @IBOutlet weak var checkBox: CheckBox!{
        didSet{
            checkBox.style = .tick
            checkBox.checkmarkColor = ColorConstrants.RED_THEME
            checkBox.checkedBorderColor = UIColor.black
            checkBox.uncheckedBorderColor  = UIColor.black
            checkBox.checkboxBackgroundColor = UIColor.black
            checkBox.borderStyle = .square
            checkBox.borderWidth = 0
            checkBox.isUserInteractionEnabled = false
        }
    }
    @IBOutlet weak var numOfSongs: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func setupCell(playList:Playlist)
    {
        if playList.isSelected{
            checkBox.isChecked = true
        }else{
            checkBox.isChecked = false
        }
        playListName.text = playList.playListTitle
        numOfSongs.text = "\(playList.totalSongs) songs"
        
    }
}
