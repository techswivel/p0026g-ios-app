//
//  SearchingSongTableViewCellTableViewCell.swift
//  QtheMusic
//
//  Created by Assad Khan on 06/04/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import UIKit
import SkeletonView
class SearchingSongTableViewCell: UITableViewCell {
    
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var activityLoader: UIActivityIndicatorView!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        photoImageView.isHidden = true
        titleLabel.isHidden = true
        descriptionLabel.isHidden = true
        activityLoader.isHidden = true
        
    }
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    internal func showAnimation(){
        self.isUserInteractionEnabled = false
        photoImageView.isHidden = false
        titleLabel.isHidden = false
        descriptionLabel.isHidden = false
        activityLoader.isHidden = false
        
        photoImageView.roundCorner(radius: 0)
        photoImageView.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        titleLabel.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        descriptionLabel.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        activityLoader.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        
    }
    
    internal func hideAnimation(){
        self.isUserInteractionEnabled = true
        photoImageView.isHidden = false
        titleLabel.isHidden = false
        descriptionLabel.isHidden = false
        activityLoader.isHidden = false
        
        photoImageView.hideSkeleton()
        titleLabel.hideSkeleton()
        descriptionLabel.hideSkeleton()
        activityLoader.hideSkeleton()
    }
    internal  func setUpCell(data:Song?){
        guard  data != nil else{return}
        self.isUserInteractionEnabled = true
        if data?.type == SearchSongType.ALBUM.rawValue{
            photoImageView.roundCornerImageView()
            photoImageView.loadImage(imageUrl: data?.albumCoverImageURL ?? "", placeHolderName: ImageConstant.noImagePlaceHolder, activiyIndicator: activityLoader)
            titleLabel.text = data?.albumTitle
            descriptionLabel.text = "Album by \(data?.albumArtist ?? "")"
            
        }else if  data?.type == SearchSongType.ARTIST.rawValue{
            
            photoImageView.roundCorner(radius: (Float(photoImageView.frame.height) / 2))
            titleLabel.text = data?.albumName
            descriptionLabel.text = data?.artistName
            photoImageView.loadImage(imageUrl: data?.artistCoverImageURL ?? "", placeHolderName: ImageConstant.noImagePlaceHolder, activiyIndicator: activityLoader)
            
        }else{
            photoImageView.roundCornerImageView()
            
            titleLabel.text = data?.songTitle
            descriptionLabel.text = data?.songDuration ?? ""
            photoImageView.loadImage(imageUrl: data?.coverImageURL ?? "", placeHolderName: ImageConstant.noImagePlaceHolder, activiyIndicator: activityLoader)
        }
        
        
        
        
    }
    
}
