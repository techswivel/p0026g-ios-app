//
//  PhoneNumberView.swift
//  QtheMusic
//
//  Created by Hammad Hassan on 13/05/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import UIKit
import SKCountryPicker

protocol PhoneNumberViewDelegate {
    func closeView()
    func updatePhoneNumber(number:String)
}

protocol PhoneNumberViewDelegeteImp{
    func setDelegateAndVC(delegate: PhoneNumberViewDelegate?,vc:UIViewController)
}

class PhoneNumberView: UIView {

    @IBOutlet weak var blurView: UIImageView!{
        didSet{
            self.blurView.blurImageViewWithTransparentBlackColor()
            
        }
    }
    @IBOutlet weak var containerView: UIView!{
        didSet{
            self.containerView.layer.cornerRadius = 12
        }
    }
    @IBOutlet weak var phoneNoWarningImg: UIImageView!
    @IBOutlet weak var phoneNumberView: UIView!{
        didSet{
            self.phoneNumberView.layer.cornerRadius = 10
            self.phoneNumberView.layer.borderWidth = 1.5
            self.phoneNumberView.layer.borderColor = ColorConstrants.RED_THEME.cgColor
        }
    }
    
    
    @IBOutlet weak var sendCodeButton: UIButton!{
        didSet{
            self.sendCodeButton.layer.cornerRadius = 10
        }
    }
    
    
    @IBOutlet weak var countryChangeButton: UIButton!
    
    @IBOutlet weak var countryCodeLabel: UILabel!
    
    @IBOutlet weak var phoneNumberTextField: UITextField!
    
    
    private var delegate : PhoneNumberViewDelegate?
    private var ViewController : UIViewController!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.phoneNumberTextField.delegate = self
        
        self.setUpCountryPicker()
        
        /// Removing +1 prefix from phone number textfield if user insert phone number from predictive
        self.phoneNumberTextField.addTarget(self, action: #selector(textFiedDidChange(_:)), for: .editingChanged)
        
    }
    
    private func setUpCountryPicker(){
        guard let country = CountryManager.shared.currentCountry else {
            self.countryChangeButton.setTitle(StringConstant.pickCountry, for: .normal)
            self.countryCodeLabel.isHidden = true
            return
        }
        
        self.countryCodeLabel.text = country.dialingCode
        self.countryChangeButton.setImage(country.flag!, for: .normal)
      
        
    }
        
    
    
    @IBAction func closeButtonPressed(_ sender: UIButton) {
        self.delegate?.closeView()
    }
    
    @IBAction func phoneNumberChangeButtonPressed(_ sender: UIButton) {
        CountryPickerWithSectionViewController.presentController(on: self.ViewController) { [weak self] (country: Country) in
            
            guard let self = self else { return }
            
            self.countryChangeButton.setImage(country.flag!, for: .normal)
            self.countryCodeLabel.text = country.dialingCode
            
        }
    }
    
    
    
    
    @IBAction func sendCodeButtonPressed(_ sender: UIButton) {
        if self.phoneNumberTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).isPhoneNumber == false {
            self.phoneNoWarningImg.isHidden = false
            return
        }
        
        self.phoneNoWarningImg.isHidden = true
        let phoneNumber = (self.countryCodeLabel.text ?? "") + (self.phoneNumberTextField.text ?? "")
        self.delegate?.updatePhoneNumber(number:phoneNumber)
        
    }
    
    
   
    
}

//MARK: - UitextField Delegate Methods
extension PhoneNumberView : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let text = textField.text
        let textRange = Range(range, in: text!)
        let updatedText = text!.replacingCharacters(in: textRange!, with: string)
        
        if textField == self.phoneNumberTextField {
            let phoneNumber = updatedText.trimmingCharacters(in: .whitespacesAndNewlines)
            if phoneNumber.isPhoneNumber {
                self.phoneNoWarningImg.isHidden = true
                
            }
            else {
                self.phoneNoWarningImg.isHidden = false
                
            }
            
        }
        
        return true
    }
    
    /// Removing +1 prefix from phone number textfield if user insert phone number from predictive
    @objc func textFiedDidChange(_ sender: Any) {
           let prefix = "+1" // What ever you want may be an array and step thru it
           guard self.phoneNumberTextField.text!.hasPrefix(prefix) else { return }
        self.phoneNumberTextField.text  = String(self.phoneNumberTextField.text!.dropFirst(prefix.count).trimmingCharacters(in: .whitespacesAndNewlines))
       }
    

}
//MARK: - Phone Number View Delegate Imp
extension PhoneNumberView : PhoneNumberViewDelegeteImp {
    func setDelegateAndVC(delegate: PhoneNumberViewDelegate?, vc: UIViewController) {
        self.delegate = delegate
        self.ViewController = vc
    }
    
    
}
