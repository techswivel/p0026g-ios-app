//
//  CalendarView.swift
//  Toothdolist
//
//  Created by Hammad Hassan on 19/01/2021.
//  Copyright © 2021 TechSwivel. All rights reserved.
//

import UIKit
import FSCalendar
//MARK: -  Calendar View Delegate
protocol CalendarViewDelegate {
    func Ok(date: Date)
    func cancel()
}

class CalendarView: UIView {

    @IBOutlet weak var blurView: UIImageView!{
        didSet{
        blurView.blurImageViewWithTransparentBlackColor()
        }}
  
    @IBOutlet weak var topContainerYear: UILabel!
    
    @IBOutlet weak var topContainerFormatedDate: UILabel!
    
    @IBOutlet weak var datePicker: UIDatePicker!
    
    var selectedDate = Date()

    var delegate : CalendarViewDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
       
        
        self.topContainerFormatedDate.text = Utils.dateConverstion(timeStamp: selectedDate.timeIntervalSince1970, format: .CALANDAR_DATE_FORMAT)
        self.topContainerYear.text = Utils.dateConverstion(timeStamp: selectedDate.timeIntervalSince1970, format: .CALANDAR_YEAR_FORMAT)
        let currentDate = Date()
        let oneDayBeforeCurrentDay = Calendar.current.date(byAdding: .day,value: -1, to: currentDate)
        datePicker.maximumDate = oneDayBeforeCurrentDay
        
    }
    
    
    @IBAction func datePicker(_ sender: Any) {
        self.selectedDate = datePicker.date
        
        /// Updating Labels Of Calenders with selected Date
        self.topContainerFormatedDate.text = Utils.dateConverstion(timeStamp: datePicker.date.timeIntervalSince1970, format: .CALANDAR_DATE_FORMAT)
        self.topContainerYear.text = Utils.dateConverstion(timeStamp: datePicker.date.timeIntervalSince1970, format: .CALANDAR_YEAR_FORMAT)
        
        
    }
    
    func setDateOnDatePicker(dobTimeStamp:Double){
        
        self.topContainerFormatedDate.text = Utils.dateConverstion(timeStamp:dobTimeStamp, format: .CALANDAR_DATE_FORMAT)
        self.topContainerYear.text = Utils.dateConverstion(timeStamp: dobTimeStamp, format: .CALANDAR_YEAR_FORMAT)
        let date = Utils.convertTimeStampToDate(timeStamp: dobTimeStamp)
        self.datePicker.setDate(date, animated: true)
    }
    
    
    
    @IBAction func okButtonPressed(_ sender: UIButton) {
        print("Selected: \(self.selectedDate)")
        self.delegate?.Ok(date: self.selectedDate)
    }
    
    @IBAction func cancelButtonPressed(_ sender: UIButton) {
        self.delegate?.cancel()
    }
    
    func getPreviousMonth(date:Date)->Date {
        do {
            return try Calendar.current.date(byAdding: .month, value: -1, to:date)!
        } catch { print(error)}
        
    }

    func getNextMonth(date:Date)->Date {
        do {
            return try  Calendar.current.date(byAdding: .month, value: 1, to:date)!
        } catch { print(error)}
        
        
    }
    
}


