//
//  SongAndAlbumUnlockingScreen.swift
//  QtheMusic
//
//  Created by Assad Khan on 07/09/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import UIKit


protocol SongAndAlbumUnlockingScreenProtocol{
    func setDelegate(delegate:SongAndAlbumUnlockingScreenDelegate)
    func setSongObj(song:Song)
}
protocol SongAndAlbumUnlockingScreenDelegate{
    func closeBtnPressed()
    func buyNowBtnPressed()
    func payNowBtnPressed()
}

class SongAndAlbumUnlockingScreen: UIView {

    @IBOutlet weak var outerView: UIImageView!{
        didSet{
            
        }
    }
    @IBOutlet weak var songImageActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var songCoverImageView: UIImageView!{
        didSet{
            self.songCoverImageView.roundCornerImageView()
        }
    }
    
    @IBOutlet weak var containerView: UIView!{
        didSet{
            containerView.roundCorner(radius: 12)
        }
    }
    
    @IBOutlet weak var songName: UILabel!
    @IBOutlet weak var isPremiumLabel: UILabel!
    
    @IBOutlet weak var premiumDetailLabel: UILabel!
    
    @IBOutlet weak var payNowBtn: UIButton!{
        didSet{
            payNowBtn.addBtnCornerRadius()
        }
    }
    
    
    @IBOutlet weak var buyAPlanBtn: UIButton!{
        didSet{
            buyAPlanBtn.addBtnCornerRadius()
            buyAPlanBtn.layer.borderWidth = 1
            buyAPlanBtn.layer.borderColor = ColorConstrants.RED_THEME.cgColor
    }
}
    private var delegate:SongAndAlbumUnlockingScreenDelegate?
    
    private var song:Song?
    internal var premiumTitle:String?
    internal var songAlbumName:String?
    internal var premiumDetailText:String?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       
        self.setUpView()
        
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        setUpView()
        setBuyAPlanButton()
    }
    
    
    @IBAction func buyAPlanBtnPressed(_ sender: Any) {
        self.delegate?.buyNowBtnPressed()
    }
    
    @IBAction func payNowBtnPressed(_ sender: Any) {
        self.delegate?.payNowBtnPressed()
    }
    
    @IBAction func closeBtnPRessed(_ sender: Any) {
        self.delegate?.closeBtnPressed()
    }
    
    
    private func setUpView(){
        
        songCoverImageView.loadImage(imageUrl: self.song?.coverImageURL ?? "", placeHolderName: ImageConstant.noImagePlaceHolder, activiyIndicator: self.songImageActivityIndicator)
        
        songName.text = self.song?.songTitle
        isPremiumLabel.text = self.premiumTitle
        premiumDetailLabel.text = self.premiumDetailText
        
    }
    private func setBuyAPlanButton()
    {
        let data = UserDefaultUtils.getProfile(key: CommonKeys.KEY_PROFILE)
        print(data?.subscription?.toString())
        if data?.subscription == nil
        {
            buyAPlanBtn.isHidden = false
        }
        else
        {
            buyAPlanBtn.isHidden = true
        }
    }
    
}
extension  SongAndAlbumUnlockingScreen:SongAndAlbumUnlockingScreenProtocol{
    func setDelegate(delegate: SongAndAlbumUnlockingScreenDelegate) {
        self.delegate = delegate
    }
    
    func setSongObj(song: Song) {
        self.song = song
        payNowBtn.setTitle("Pay $\(String(describing: self.song?.price ?? 0)) Now", for: .normal)
    }
    
    
    
}
