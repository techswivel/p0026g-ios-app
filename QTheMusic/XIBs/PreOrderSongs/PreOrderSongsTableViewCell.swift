//
//  PreOrderSongsTableViewCell.swift
//  QtheMusic
//
//  Created by Assad Khan on 13/05/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import UIKit

protocol PreOrderSongsTableViewCellProtocol{
    func setDelegate(delegate:PreOrderSongsTableViewCellDelegate)
}
protocol PreOrderSongsTableViewCellDelegate{
    func orderNowbtnPressed()
}

class PreOrderSongsTableViewCell: UITableViewCell {

    @IBOutlet weak var songNameLabel: UILabel!
    @IBOutlet weak var releasingDateLabel: UILabel!
    @IBOutlet weak var purchasedLabel: UILabel!
    @IBOutlet weak var orderNowBtn: UIButton!{
        didSet{
            orderNowBtn.roundCorner(radius: 15)
        }
    }
    @IBOutlet weak var numberOfPreOrdersLabel: UILabel!
    
    @IBOutlet weak var containerView: UIView!{
        didSet{
            containerView.roundCorner(radius: 13)
        }
    }
    
    @IBOutlet weak var songImageView: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    private var delegate:PreOrderSongsTableViewCellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func orderNowBtnPressed(_ sender: Any) {
        
        self.delegate?.orderNowbtnPressed()
    }
    
    internal func setUpCell(){
        isUserInteractionEnabled = true
    }
    internal func showShimmerEffect(){
        self.isUserInteractionEnabled = false
        songNameLabel.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        releasingDateLabel.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        purchasedLabel.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        numberOfPreOrdersLabel.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        songImageView.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        activityIndicator.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        
    }
    internal func hideShimmerEffect(){
        isUserInteractionEnabled = true
        songNameLabel.hideSkeleton()
        releasingDateLabel.hideSkeleton()
        purchasedLabel.hideSkeleton()
        numberOfPreOrdersLabel.hideSkeleton()
        songImageView.hideSkeleton()
        activityIndicator.hideSkeleton()
        
    }
    internal func setUpCell(data:PreOrderData?){
        guard let Data = data else{return}
        songNameLabel.text = "\(Data.preOrderTitle ?? "")"
        
        if Data.totalnumberOfSongs ?? 0 > 0 {
            songNameLabel.text = "\(Data.preOrderTitle ?? "") (\(Data.totalnumberOfSongs ?? -1) songs)"
            
        }
        numberOfPreOrdersLabel.text = "Preorders: \(Data.preOrderedCount ?? -1)/\(Data.totalPreOrders ?? -1)"
        songImageView.loadImage(imageUrl: Data.preOrderCoverImageURL ?? "", placeHolderName: ImageConstant.noImagePlaceHolder, activiyIndicator: activityIndicator)
        if Data.isPurchesed ?? false{
            purchasedLabel.isHidden = false
            orderNowBtn.isHidden = true
        }else{
            purchasedLabel.isHidden = true
            orderNowBtn.isHidden = false
            let myNormalAttributedTitle = NSAttributedString(string: "Order Now $\(Data.price ?? -1)",
            attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
            orderNowBtn.setAttributedTitle(myNormalAttributedTitle, for: .normal)
            
        }
        
        releasingDateLabel.text = Utils.dateConverstion(timeStamp: Double(Data.releaseDate ?? 0), format: .RELEASING_DATE)
    }
}
extension  PreOrderSongsTableViewCell :PreOrderSongsTableViewCellProtocol{
    func setDelegate(delegate: PreOrderSongsTableViewCellDelegate) {
        self.delegate = delegate
    }
    
    
}
