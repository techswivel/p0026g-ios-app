//
//  InterestCollectionViewCell.swift
//  QtheMusic
//
//  Created by Assad Khan on 29/03/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import UIKit

class InterestCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var backgroundContainer: UIView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        titleLabel.isHidden = true
        backgroundContainer.isHidden = true
        // Initialization code
        
    }
    
    internal func setUpCell(category:Category?){
        guard category != nil else{ return}
        titleLabel.isHidden = false
        backgroundContainer.isHidden = false
        
        
        self.isUserInteractionEnabled = true
        
        backgroundContainer.roundCorner(radius: 9)
        
        titleLabel.text = category?.categoryTitle
        titleLabel.font = FontConstants.getManropeRegular(size: 12)
        if category?.isSelected ?? false{
            backgroundContainer.backgroundColor = ColorConstrants.RED_THEME
        }else{
            backgroundContainer.backgroundColor = ColorConstrants.UNSELECTEDCELL_INTERESTCOLOR
        }
        
    }
    
    internal func setUpCellForMenu(data:RecentlyPlayedMenu?){
        guard data != nil else{ return}
        titleLabel.isHidden = false
        backgroundContainer.isHidden = false
        backgroundContainer.roundCorner(radius: 9)
        
        self.isUserInteractionEnabled = true
        
        titleLabel.text = data?.menuItem
        if data?.isSelected ?? false{
            backgroundContainer.backgroundColor = ColorConstrants.RED_THEME
        }else{
            backgroundContainer.backgroundColor = ColorConstrants.UNSELECTEDCELL_INTERESTCOLOR
        }
        
    }
    
    internal func setUpCellForLanguages(language:Language?){
        guard language != nil else{ return}
        titleLabel.isHidden = false
        backgroundContainer.isHidden = false
        backgroundContainer.roundCorner(radius: 9)
        titleLabel.text = language?.languageTitle
        titleLabel.font = FontConstants.getManropeRegular(size: 12)
        
        self.isUserInteractionEnabled = true
        
        if language?.isSelected ?? false{
            backgroundContainer.backgroundColor = ColorConstrants.RED_THEME
        }else{
            backgroundContainer.backgroundColor = ColorConstrants.UNSELECTEDCELL_INTERESTCOLOR
        }
        
    }
    
    internal func showAnimation(){
        titleLabel.isHidden = false
        backgroundContainer.isHidden = false
        self.isUserInteractionEnabled = false
        titleLabel.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        backgroundContainer.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        
    }
    
    internal func hideAnimation(){
        titleLabel.isHidden = false
        backgroundContainer.isHidden = false
        self.isUserInteractionEnabled = true
        
        titleLabel.hideSkeleton()
        backgroundContainer.hideSkeleton()
      
    }
}

