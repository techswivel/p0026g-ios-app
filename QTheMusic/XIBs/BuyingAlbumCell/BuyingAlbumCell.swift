//
//  BuyingAlbumCell.swift
//  QtheMusic
//
//  Created by Muhammad Tanzeel Ur Rehman on 14/01/2023.
//  Copyright © 2023 TechSwivel. All rights reserved.
//

import Foundation
import UIKit

class BuyingAlbumCell: UITableViewCell {
    
    @IBOutlet weak var photoImage: UIImageView!{
        didSet{
            photoImage.roundCorner(radius: 5)
        }
    }
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var descriptionlbl: UILabel!
    
    @IBOutlet weak var price: UILabel!
    
    @IBOutlet weak var arrowIamge: UIImageView!
    
    @IBOutlet weak var backview: UIView!
    
    @IBOutlet weak var datetimelbl : UILabel!
    
    @IBOutlet weak var cardname: UILabel!
    
    @IBOutlet weak var activityIndicator : UIActivityIndicatorView!
    
    @IBOutlet weak var albumSongTableView : UITableView! {
        didSet{
            albumSongTableView.delegate = self
            albumSongTableView.dataSource = self
        }
    }
    
    @IBOutlet weak var sepratorView1 : UIView!
    
    @IBOutlet weak var sepratorView2 : UIView!
    
    private var mViewModel = BuyingAlbumCellViewModel()
    private var mProfileViewModel = ProfileViewModel()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setBindigServerResponse()
        registerNib()
    }
    
    private func registerNib() {
            
        albumSongTableView.register(UINib(nibName: UINibConstant.AlbumSongCellnib, bundle: nil), forCellReuseIdentifier: UINibConstant.AlbumSongCellidentifier)
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        
    }
  
    
    
    
    internal func showAnimation(){
        
        activityIndicator.isHidden = true
        
        
        self.isUserInteractionEnabled = false
        
        titleLabel.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        descriptionlbl.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        photoImage.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        activityIndicator.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        price.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        arrowIamge.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        backview.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        datetimelbl.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        cardname.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        
    }
    
    internal func hideAnimation(){
        
        activityIndicator.isHidden = false
        self.isUserInteractionEnabled = true
        
        titleLabel.hideSkeleton()
        descriptionlbl.hideSkeleton()
        photoImage.hideSkeleton()
        activityIndicator.hideSkeleton()
        price.hideSkeleton()
        arrowIamge.hideSkeleton()
        backview.hideSkeleton()
        datetimelbl.hideSkeleton()
        cardname.hideSkeleton()
    }
    
    internal func setUpCell(data:BuyingHistory?){
        guard let Data = data else {
            return
        }
        
        self.isUserInteractionEnabled = true
        photoImage.loadImage(imageUrl: Data.albumCoverImageUrl ?? "", placeHolderName: ImageConstant.noImagePlaceHolder, activiyIndicator: activityIndicator)
        descriptionlbl.text = "\(Data.numberOfSongs)"
        
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.clear
        selectedBackgroundView = bgColorView
        
        layer.cornerRadius = 8
        layer.masksToBounds = true
        
        titleLabel.text = "\(Data.albumTitle ?? "")"
        price.text = "$\(Data.albumPrice )"
    
        datetimelbl.text = "\(Data.timeOfPurchased ?? "")"
        if  (Data.albumTypeOfTransection != nil)  {
            cardname.text = Data.albumTypeOfTransection

        }else{
            cardname.text = "\(Data.albumCardPrefix )"
        }
    }
    
    private func setBindigServerResponse() {
        
        self.bindingServerResponse()
        self.bindingServerErrorResponse()
        self.bindingServerCompletionResponse()
        
    }
    
}

extension BuyingAlbumCell : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return mViewModel.albumsong.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: AlbumSongCell = tableView.dequeueReusableCell(withIdentifier: UINibConstant.AlbumSongCellidentifier, for: indexPath) as! AlbumSongCell
        cell.setUpCell(data: mViewModel.albumsong[indexPath.row])
        return cell
    }
    
    
}

extension BuyingAlbumCell {
    
    private func bindingServerResponse() {
        self.mProfileViewModel.serverResponseForBuyingHistory.bind {
            if ($0 != nil) {
                
                if $0?.response?.status ?? false {
                    
                    self.mViewModel.albumsong = $0?.response?.data?.buyinghistory ?? self.mViewModel.albumsong
                    self.albumSongTableView.reloadData()
    
                }
                else if $0?.response?.status == false {
                    
                }
            }
        }
    }
   
    private func bindingServerErrorResponse() {
        self.mProfileViewModel.errorResponseForBuyingHistory.bind {
            if ($0 != nil) {
              
            }
        
        }
        
        
    }
    
   
    private func bindingServerCompletionResponse(){
        self.mProfileViewModel.completion.bind {
            if ($0 != nil) {
                
                
            }
        }
        
    }
}
