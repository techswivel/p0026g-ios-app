//
//  BuyingAlbumCellViewModel.swift
//  QtheMusic
//
//  Created by Muhammad Tanzeel Ur Rehman on 24/01/2023.
//  Copyright © 2023 TechSwivel. All rights reserved.
//

import Foundation
import UIKit

class BuyingAlbumCellViewModel : BaseViewModel {
    
    var albumsong = [BuyingHistory]()
    var albumsArray  = [Album]()
    var songsArray = [Song]()
    var songBuilder : SongAndArtistBuilder = SongAndArtistBuilder()
}
