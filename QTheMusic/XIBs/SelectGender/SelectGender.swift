//
//  SelectGender.swift
//  QtheMusic
//
//  Created by Assad Khan on 21/03/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import UIKit
protocol SelectGenderProtocol{
    func setDelegate(delegate:SelectGenderDelegate?)
}
protocol SelectGenderDelegate{
    func updateBtnPressed(selectedOption:String?)
    func closeBtnPressed()
}

class SelectGender: UIView {

    @IBOutlet weak var backgroundView: UIImageView!{
        didSet{
            backgroundView.blurImageViewWithTransparentBlackColor()
        }
    }
    @IBOutlet weak var innerBackgroundView: UIView!
    {
        didSet{
            innerBackgroundView.roundCorner(radius: 12)
        }
        
    }
    @IBOutlet weak var updateBtn: UIButton!{
        didSet{
            updateBtn.roundCorner(radius: 12)
        }
    }
    
    
    @IBOutlet weak var closeBtn: UIButton!
    
    @IBOutlet weak var maleChecmMark: CheckBox!
    {
        didSet{
            maleChecmMark.style = .circle
            maleChecmMark.borderStyle = .rounded
        }
    }
    @IBOutlet weak var femaleChecmMark: CheckBox!
    {
        didSet{
            femaleChecmMark.style = .circle
            femaleChecmMark.borderStyle = .rounded
        }
    }
    @IBOutlet weak var nonbinaryChecmMark: CheckBox!
    {
        didSet{
            nonbinaryChecmMark.style = .circle
            nonbinaryChecmMark.borderStyle = .rounded
        }
    }
    @IBOutlet weak var noanswerChecmMark: CheckBox!
    {
        didSet{
            noanswerChecmMark.style = .circle
            noanswerChecmMark.borderStyle = .rounded
        }
    }
    @IBOutlet weak var maleBackgroundView: UIView!
    {
        didSet{
            maleBackgroundView.roundCorner(radius: 12)
            maleBackgroundView.layer.borderColor = UIColor.white.cgColor
            maleBackgroundView.layer.borderWidth = 1
        }
    }
    
    @IBOutlet weak var noanswerBackgroundView: UIView!
    {
        didSet{
            noanswerBackgroundView.roundCorner(radius: 12)
            noanswerBackgroundView.layer.borderColor = UIColor.white.cgColor
            noanswerBackgroundView.layer.borderWidth = 1
        }
    }
    @IBOutlet weak var femaleBackgroundView: UIView!
    {
        didSet{
            femaleBackgroundView.roundCorner(radius: 12)
            femaleBackgroundView.layer.borderColor = UIColor.white.cgColor
            femaleBackgroundView.layer.borderWidth = 1
        }
    }
    
    @IBOutlet weak var nonbinaryBackgroundView: UIView!
    {
        didSet{
            nonbinaryBackgroundView.roundCorner(radius: 12)
            nonbinaryBackgroundView.layer.borderColor = UIColor.white.cgColor
            nonbinaryBackgroundView.layer.borderWidth = 1
        }
    }
    
    private var delegate :SelectGenderDelegate?
    private var selectedGender:String?
    
    
    
    @IBAction func maleCheckMarkPressed(_ sender: Any) {
        self.setMaleCheckMark()
    }
    
    @IBAction func femaleCheckMarkPressed(_ sender: Any) {
        self.setFeMaleCheckMark()
    }
    @IBAction func nonbinaryCheckMarkPressed(_ sender: Any) {
        self.setNonBinaryCheckMark()
    }
    
    @IBAction func noanswerCheckMarkPressed(_ sender: Any) {
        self.setNoAnswerCheckMark()
    }
    
    
    @IBAction func updateBtnPressed(_ sender: Any) {
        print("Selected Option Is \(self.selectedGender)")
        
        self.delegate?.updateBtnPressed(selectedOption: self.selectedGender)
        
    }
    @IBAction func closeBtnPressed(_ sender: Any) {
        self.delegate?.closeBtnPressed()
    }
    
    internal func setMaleCheckMark(){
        maleChecmMark.isChecked = true
        femaleChecmMark.isChecked = false
        nonbinaryChecmMark.isChecked = false
        noanswerChecmMark.isChecked = false
        
        self.selectedGender = SelectGenderEnum.MALE.rawValue
    }
    
    internal func setFeMaleCheckMark(){
        maleChecmMark.isChecked = false
        femaleChecmMark.isChecked = true
        nonbinaryChecmMark.isChecked = false
        noanswerChecmMark.isChecked = false
        
        self.selectedGender = SelectGenderEnum.FEMALE.rawValue
        
    }
    internal func setNonBinaryCheckMark(){
        
        maleChecmMark.isChecked = false
        femaleChecmMark.isChecked = false
        nonbinaryChecmMark.isChecked = true
        noanswerChecmMark.isChecked = false
        
        self.selectedGender = SelectGenderEnum.NON_BINARY.rawValue
    }
    internal func setNoAnswerCheckMark(){
        maleChecmMark.isChecked = false
        femaleChecmMark.isChecked = false
        nonbinaryChecmMark.isChecked = false
        noanswerChecmMark.isChecked = true
        
        self.selectedGender = SelectGenderEnum.NOT_ANSWERED.rawValue
        
    }
    
}
extension SelectGender:SelectGenderProtocol{
    func setDelegate(delegate: SelectGenderDelegate?) {
        if let delegate = delegate {
            self.delegate = delegate
        }
    }
    
    
}
