//
//  SubsciptionPlanCell.swift
//  QtheMusic
//
//  Created by zeeshan on 12/04/2023.
//  Copyright © 2023 TechSwivel. All rights reserved.
//

import UIKit

class SubsciptionPlanCell: UITableViewCell {

    @IBOutlet weak var subscriptionBtn: UIButton!
    
    @IBOutlet weak var planNumber: UILabel!
    
    @IBOutlet weak var planTitle: UILabel!
    
    @IBOutlet weak var planPrice: UILabel!
    
    @IBOutlet weak var planDuration: UILabel!
    
    @IBOutlet weak var planNumberView: UIView!
    
    @IBOutlet weak var planLabelView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    internal func showAnimation(){

        self.isUserInteractionEnabled = false
        
        subscriptionBtn.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
       // planNumber.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
     //   planTitle.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
     //   planPrice.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
     //   planDuration.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        planNumberView.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        planLabelView.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
    
        
    }
    
    internal func hideAnimation(){
       
        self.isUserInteractionEnabled = true
        subscriptionBtn.hideSkeleton()
        planNumber.hideSkeleton()
        planTitle.hideSkeleton()
        planPrice.hideSkeleton()
        planDuration.hideSkeleton()
        planNumberView.hideSkeleton()
        planLabelView.hideSkeleton()
    }
    
    override func layoutSubviews() {
       super.layoutSubviews()
        contentView.frame = contentView.frame.inset(by:  UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0))
        }
    func setPlanNumber(plan: Int)
    {
        planNumber.text = String(describing: (plan))
    }
    func setPlanTitle(Title: String)
    {
        planTitle.text = Title
    }
    func setDuration(Duration: String)
    {
        planDuration.text = Duration
    }
    func setPrice(Price: String)
    {
        planPrice.text = Price
    }
    @IBAction func subscriptionBtn(_ sender: Any) {
    }
}
