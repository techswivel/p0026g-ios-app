//
//  ListeningHistoryView.swift
//  QtheMusic
//
//  Created by Muhammad Tanzeel Ur Rehman on 13/01/2023.
//  Copyright © 2023 TechSwivel. All rights reserved.
//

import Foundation
import UIKit

protocol ListeningHistorySongsDelegate{
    func listeningSongsErrorResponse(error:String?)
    func listeningSongsExpiryReponse()
}

protocol ListeningHistorySongsDelegateImp {
    func setDelegate(delegate:ListeningHistorySongsDelegate?)
    func setNavigationController(navCon:UINavigationController?)
}

class ListeningHistoryView : UIView {
    
    
    @IBOutlet weak var songsTableView:UITableView!{
        didSet{
            songsTableView.delegate = self
            songsTableView.dataSource = self
        }
    }
    @IBOutlet weak var albumsArtistCollectionView:UICollectionView!{
        didSet{
            albumsArtistCollectionView.delegate = self
            albumsArtistCollectionView.dataSource = self
        }
    }
    
    @IBOutlet weak var recentlyPlayedCategoryCollectionView:UICollectionView!{
        didSet{
            recentlyPlayedCategoryCollectionView.delegate = self
            recentlyPlayedCategoryCollectionView.dataSource  = self
        }
    }
    
    private var viewModel = ListeningHistoryviewModel()
    private var selectedCat:String?
    private var collectionViewFlowLayout : UICollectionViewFlowLayout!
    private var collectionViewFlowLayoutForArtist : UICollectionViewFlowLayout!
    private var collectionViewFlowLayoutForCat : UICollectionViewFlowLayout!
    internal var navigationController :UINavigationController?
    internal var tabbarViewController: CustomTabbarController?
    internal var vc : ListeningHistoryViewController?
    private var delegate: ListeningHistorySongsDelegate?

    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setUpView()
        
        
        
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
       
        
    }
    
    private func setUpView(){
        self.registerNib()
    }
    private func registerNib(){
        
        albumsArtistCollectionView.translatesAutoresizingMaskIntoConstraints = false
        albumsArtistCollectionView.bottomAnchor.constraint(equalTo: self.songsTableView.bottomAnchor).isActive = true
        albumsArtistCollectionView.trailingAnchor.constraint(equalTo: self.songsTableView.trailingAnchor).isActive = true
        recentlyPlayedCategoryCollectionView.register(UINib(nibName: UINibConstant.InterestCollectionViewCellNib, bundle: nil), forCellWithReuseIdentifier: UINibConstant.InterestCollectionViewCellIdentifier)
        
        albumsArtistCollectionView.register(UINib(nibName: UINibConstant.AlbumCollectionViewCellNib, bundle: nil), forCellWithReuseIdentifier: UINibConstant.AlbumCollectionViewCellIdentifier)
        
        albumsArtistCollectionView.register(UINib(nibName: UINibConstant.ArtistCollectionViewCellNib, bundle: nil), forCellWithReuseIdentifier: UINibConstant.ArtistCollectionViewCellIdentifier)
        
        songsTableView.register(UINib(nibName: UINibConstant.SongsTableViewCellNib, bundle: nil),forCellReuseIdentifier: UINibConstant.SongsTableViewCellIdentifier)
        
        
        self.selectedCat = RecentlyPlayedCatEnum.Songs.rawValue
        self.viewModel.recentlyPlayedCategories[0].isSelected = true
        self.viewModel.getSongsDataFromDb()
        
        self.songsTableView.reloadData()
        setupFlowLayout()
        self.songsTableView.isHidden = false
        self.albumsArtistCollectionView.isHidden = true
    }
    
    // MARK: - Collection View Flow Layout
    private func setupFlowLayout(){
        
        if selectedCat == RecentlyPlayedCatEnum.Albums.rawValue{
            
            let width = (albumsArtistCollectionView.frame.width - 16) / 2
            let height = albumsArtistCollectionView.frame.width * 0.62
            //using two as two column are to be presented
            collectionViewFlowLayoutForArtist = UICollectionViewFlowLayout()
            
            collectionViewFlowLayoutForArtist.itemSize = CGSize(width: width, height: height)
            
            collectionViewFlowLayoutForArtist.scrollDirection = .vertical
            collectionViewFlowLayoutForArtist.minimumLineSpacing = 8
            collectionViewFlowLayoutForArtist.minimumInteritemSpacing = self.viewModel.interItemSpacing
            albumsArtistCollectionView.setCollectionViewLayout(collectionViewFlowLayoutForArtist, animated: true)
        }
        else if self.selectedCat == RecentlyPlayedCatEnum.Artists.rawValue {
            
            let width = (albumsArtistCollectionView.frame.width - (5 * self.viewModel.interItemSpacing)) / 3
            let height = albumsArtistCollectionView.frame.width * 0.4
            //using two as two column are to be presented
            collectionViewFlowLayout = UICollectionViewFlowLayout()
            
            collectionViewFlowLayout.itemSize = CGSize(width: width, height: height)
            
            collectionViewFlowLayout.scrollDirection = .vertical
            collectionViewFlowLayout.minimumLineSpacing = 8
            collectionViewFlowLayout.minimumInteritemSpacing = self.viewModel.interItemSpacing
            albumsArtistCollectionView.setCollectionViewLayout(collectionViewFlowLayout, animated: true)
        }
        if collectionViewFlowLayoutForCat == nil{
            
            
            
            collectionViewFlowLayoutForCat = UICollectionViewFlowLayout()
            
            collectionViewFlowLayoutForCat.itemSize = CGSize(width: 100, height: 30)
            
            collectionViewFlowLayoutForCat.scrollDirection = .horizontal
            collectionViewFlowLayoutForCat.minimumLineSpacing = 10
            collectionViewFlowLayoutForCat.minimumInteritemSpacing = self.viewModel.interItemSpacing
            recentlyPlayedCategoryCollectionView.setCollectionViewLayout(collectionViewFlowLayoutForCat, animated: true)
        }
        
    }
    internal func reloadDataBase(){
        if self.selectedCat != nil{
           
            if self.selectedCat == RecentlyPlayedCatEnum.Songs.rawValue{
                
                songsTableView.isHidden = false
                albumsArtistCollectionView.isHidden = true
                
                self.viewModel.getSongsDataFromDb()
                self.songsTableView.reloadData()
                
                
            }else if self.selectedCat == RecentlyPlayedCatEnum.Albums.rawValue{
                songsTableView.isHidden = true
                albumsArtistCollectionView.isHidden = false
                
                self.viewModel.getAlbumsDataFromDb()
                
                self.albumsArtistCollectionView.reloadData()
                setupFlowLayout()
                
            }else{
                songsTableView.isHidden = true
                albumsArtistCollectionView.isHidden = false
                self.viewModel.getArtistsDataFromDb()
                
                self.albumsArtistCollectionView.reloadData()
                setupFlowLayout()
                
            }
            
            
        }
    }
}

extension ListeningHistoryView : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView{
        case recentlyPlayedCategoryCollectionView:
            return self.viewModel.recentlyPlayedCategories.count
        case albumsArtistCollectionView:
            if self.selectedCat == RecentlyPlayedCatEnum.Songs.rawValue{
                return   self.viewModel.songsArray.count
            }else if self.selectedCat == RecentlyPlayedCatEnum.Albums.rawValue{
                return  self.viewModel.albumsArray.count
            }else{
                return  self.viewModel.artistArray.count
            }
        default :
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView{
        case recentlyPlayedCategoryCollectionView:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: UINibConstant.InterestCollectionViewCellIdentifier, for: indexPath) as! InterestCollectionViewCell
            
            cell.setUpCellForMenu(data: self.viewModel.recentlyPlayedCategories[indexPath.row])
            
            return cell
        case albumsArtistCollectionView:
            
            if self.selectedCat == RecentlyPlayedCatEnum.Artists.rawValue{
                
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: UINibConstant.ArtistCollectionViewCellIdentifier, for: indexPath) as! ArtistCollectionViewCell
                
                cell.setUpCell(data: self.viewModel.artistArray[indexPath.row])
                return cell
                
                
            }else if self.selectedCat == RecentlyPlayedCatEnum.Albums.rawValue{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: UINibConstant.AlbumCollectionViewCellIdentifier, for: indexPath) as! AlbumCollectionViewCell
                cell.setUpCell(data: self.viewModel.albumsArray[indexPath.row])
                
                return cell
            }
            else{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: UINibConstant.ArtistCollectionViewCellIdentifier, for: indexPath) as! ArtistCollectionViewCell
                
                
                return cell
            }
            
        default:
            
            
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: UINibConstant.ArtistCollectionViewCellIdentifier, for: indexPath) as! ArtistCollectionViewCell
            
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        switch collectionView{
        case recentlyPlayedCategoryCollectionView:
            
            
            self.selectedCat = self.viewModel.recentlyPlayedCategories[indexPath.row].menuItem ?? ""
            
            if self.viewModel.recentlyPlayedCategories[indexPath.row].isSelected == true {
                
                
            }else{
                
                
                for items in self.viewModel.recentlyPlayedCategories{
                    items.isSelected = false
                }
                
                self.viewModel.recentlyPlayedCategories[indexPath.row].isSelected = true
                
                
                
                if self.selectedCat == RecentlyPlayedCatEnum.Songs.rawValue{
                    
                    songsTableView.isHidden = false
                    albumsArtistCollectionView.isHidden = true
                    
                    self.viewModel.getSongsDataFromDb()
                    self.songsTableView.reloadData()
                    
                    
                }else if self.selectedCat == RecentlyPlayedCatEnum.Albums.rawValue{
                    songsTableView.isHidden = true
                    albumsArtistCollectionView.isHidden = false
                    
                    self.viewModel.getAlbumsDataFromDb()
                    
                    self.albumsArtistCollectionView.reloadData()
                    setupFlowLayout()
                    
                }else{
                    songsTableView.isHidden = true
                    albumsArtistCollectionView.isHidden = false
                    self.viewModel.getArtistsDataFromDb()
                    
                    self.albumsArtistCollectionView.reloadData()
                    setupFlowLayout()
                    
                }
                
                
            }
            self.recentlyPlayedCategoryCollectionView.reloadData()
            
        case albumsArtistCollectionView:
            if self.selectedCat == RecentlyPlayedCatEnum.Albums.rawValue{
                
                let vc = UIStoryboard.init(name: StoryBoardConstant.storyBoardMain, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.AlbumDetailVc) as? AlbumDetailViewController
                let builder = SongAndArtistBuilder()
                builder.setalbumId(self.viewModel.albumsArray[indexPath.row].albumID)
                builder.setalbumTitle(self.viewModel.albumsArray[indexPath.row].albumTitle)
                builder.setalbumCoverImageURL(self.viewModel.albumsArray[indexPath.row].albumCoverImageURL)
                builder.setalbumStatus(self.viewModel.albumsArray[indexPath.row].albumStatus)
                builder.setNumberOfSongs(self.viewModel.albumsArray[indexPath.row].numberOfSongs)
                builder.setTimeStamp(Date().timeIntervalSince1970)
                vc?.setSongData(data: builder)
                vc?.setAlbumData(data: builder.build())
                vc?.setTabBarVC(tabBarVC: self.tabbarViewController ?? UITabBarController())
                let transition = CATransition()
                transition.duration = 0.5
                transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
                transition.type = CATransitionType.fade
                self.navigationController?.view.layer.add(transition, forKey: nil)
                self.navigationController?.pushViewController(vc!, animated: false)
            }else if self.selectedCat == RecentlyPlayedCatEnum.Artists.rawValue{
                
                let vc = UIStoryboard.init(name: StoryBoardConstant.storyBoardMain, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.ArtistDetailViewController) as? ArtistDetailViewController
                vc?.setTabBarVC(tabBarVC: self.tabbarViewController ?? UITabBarController())
                vc?.setArtistId(artistId: self.viewModel.artistArray[indexPath.row].artistID)
                let transition = CATransition()
                transition.duration = 0.5
                transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
                transition.type = CATransitionType.fade
                self.navigationController?.view.layer.add(transition, forKey: nil)
                self.navigationController?.pushViewController(vc!, animated: false)
                
            }
        default:
            print("Default")
        }
        
        
    }
    
}

extension ListeningHistoryView : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.viewModel.songsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
            let cell = tableView.dequeueReusableCell(withIdentifier: UINibConstant.SongsTableViewCellIdentifier, for: indexPath) as! SongsTableViewCell
            cell.setUpCell(data: self.viewModel.songsArray[indexPath.row])
            cell.selectionStyle = .none
            return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if self.viewModel.songsArray[indexPath.row].songStatus == SongStatus.PREMIUM.rawValue{
            let vc = UIStoryboard.init(name: StoryBoardConstant.storyBoardMain, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.SongsDetailViewController) as? SongsDetailViewController
            
            vc?.setPlayingCategoryType(categoryType: self.selectedCat)
            vc?.setSongData(song: self.viewModel.songsArray[indexPath.row])
            vc?.setIndexPath(row: indexPath.row)
            vc?.setCompleteSongsArray(songsArray: self.viewModel.songsArray)
            vc?.setIsPlayingFromString(isplayingFromString: "\(StringConstant.playingFrom)HISTORY")
            vc?.setIsFromMiniPlayer(isFromMiniPlayer: false)
            let transition = CATransition()
            transition.duration = 0.5
            transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
            transition.type = CATransitionType.fade
            self.viewModel.navController?.view.layer.add(transition, forKey: nil)
            self.viewModel.navController?.pushViewController(vc!, animated: false)
            self.tabbarViewController?.removeMiniPlayer()
        }else{
            
            self.tabbarViewController?.setPlayingCategoryType(categoryType: self.selectedCat)
            self.tabbarViewController?.setSongData(song: self.viewModel.songsArray[indexPath.row])
            self.tabbarViewController?.setIndexPath(row: indexPath.row)
            self.tabbarViewController?.setCompleteSongsArray(songsArray: self.viewModel.songsArray)
            self.tabbarViewController?.setIsPlayingFromString(isplayingFromString: "\(StringConstant.playingFrom)HISTORY")

           
            self.tabbarViewController?.addMiniPlayer()
       

            
            
        }
        
    
    }
    
}
extension ListeningHistoryView: ListeningHistorySongsDelegateImp {
    func setNavigationController(navCon:UINavigationController?) {
        if let NavCon = navCon{
            self.viewModel.navController = NavCon
        }
    }

    func setDelegate(delegate: ListeningHistorySongsDelegate?) {
        self.delegate = delegate
    }



}
