//
//  ListeningHistoryViewModel.swift
//  QtheMusic
//
//  Created by Muhammad Tanzeel Ur Rehman on 13/01/2023.
//  Copyright © 2023 TechSwivel. All rights reserved.
//

import Foundation
import UIKit

class ListeningHistoryviewModel: BaseViewModel {
    
    var songsArray = [Song]()
    var artistArray = [Artist]()
    var albumsArray  = [Album]()
    
    let nunmberOfItemPerRow : CGFloat = 3
    let lineSpacing : CGFloat = 1
    let interItemSpacing : CGFloat = 2
    
    var navController:UINavigationController?
    
    
    var recentlyPlayedCategories = [
        RecentlyPlayedMenu(isSelected: false, menuItem: RecentlyPlayedCatEnum.Songs.rawValue),
        RecentlyPlayedMenu(isSelected: false, menuItem: RecentlyPlayedCatEnum.Albums.rawValue),
        RecentlyPlayedMenu(isSelected: false, menuItem: RecentlyPlayedCatEnum.Artists.rawValue)
    ]


    
    func getSongsDataFromDb(){
        if let Data = mDataManager.getSongsDataFromDb(){
            self.songsArray = Data
        }
    }
    func getAlbumsDataFromDb(){
        if let Data = mDataManager.getAlbumsDataFromDb(){
            self.albumsArray = Data
           // self.albumsArray.remove(at: 0)
        }
    }
    func getArtistsDataFromDb(){
        if let Data = mDataManager.getArtistsDataFromDb(){
            self.artistArray = Data
            self.artistArray.remove(at: 0)
        }
    }
    
}
