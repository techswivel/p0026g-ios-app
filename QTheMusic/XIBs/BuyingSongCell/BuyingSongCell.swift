//
//  BuyingSongCell.swift
//  QtheMusic
//
//  Created by Muhammad Tanzeel Ur Rehman on 14/01/2023.
//  Copyright © 2023 TechSwivel. All rights reserved.
//

import Foundation
import UIKit

class BuyingSongCell : UITableViewCell{
    
    @IBOutlet weak var photoImage: UIImageView!{
        didSet{
            photoImage.roundCorner(radius: 5)
        }
    }
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var descriptionlbl: UILabel!
    
    @IBOutlet weak var price: UILabel!
    
    @IBOutlet weak var arrowIamge: UIImageView!
    
    @IBOutlet weak var backview: UIView!
    
    @IBOutlet weak var datetimelbl : UILabel!
    
    @IBOutlet weak var cardname: UILabel!
    
    @IBOutlet weak var activityIndicator : UIActivityIndicatorView!
    
    @IBOutlet weak var sepratorView: UIView!
    
    
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    
    internal func showAnimation(){
        
        activityIndicator.isHidden = true
        
        
        self.isUserInteractionEnabled = false
        
        titleLabel.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        descriptionlbl.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        photoImage.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        activityIndicator.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        price.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        arrowIamge.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        backview.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        datetimelbl.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        cardname.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        
    }
    
    internal func hideAnimation(){
        
        activityIndicator.isHidden = false
        self.isUserInteractionEnabled = true
        
        titleLabel.hideSkeleton()
        descriptionlbl.hideSkeleton()
        photoImage.hideSkeleton()
        activityIndicator.hideSkeleton()
        price.hideSkeleton()
        arrowIamge.hideSkeleton()
        backview.hideSkeleton()
        datetimelbl.hideSkeleton()
        cardname.hideSkeleton()
    }
    
    internal func setUpCell(data:BuyingHistory?){
        guard let Data = data else {
            return
        }
        
        self.isUserInteractionEnabled = true
        photoImage.loadImage(imageUrl: Data.coverImageUrl ?? "", placeHolderName: ImageConstant.noImagePlaceHolder, activiyIndicator: activityIndicator)
        descriptionlbl.text = "\(Utils.secondsToHoursMinutesSecondsString(seconds: Data.songDuration))"
        
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.clear
        selectedBackgroundView = bgColorView
        
        layer.cornerRadius = 8
        layer.masksToBounds = true
    
        titleLabel.text = "\(Data.songTitle ?? "")"
        price.text = "$\(Data.price )"
        datetimelbl.text = "\(Data.timeOfPurchased ?? "")"
        if  (Data.typeOfTransection != nil)  {
            cardname.text = Data.typeOfTransection

        }else{
            cardname.text = "\(Data.cardPrefix )"
        }

    }
}
