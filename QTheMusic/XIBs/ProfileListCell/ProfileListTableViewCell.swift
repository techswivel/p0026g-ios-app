//
//  ProfileListTableViewCell.swift
//  QtheMusic
//
//  Created by Hammad Hassan on 29/04/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import UIKit

class ProfileListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var profileListIcon: UIImageView!
    
    @IBOutlet weak var profileListName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func setProfileData(data: ProfileList){
        self.profileListIcon.image = UIImage(named: "\(data.ProfileListIcon)")
        self.profileListName.text = data.ProfileListName
    }
    
}
