//
//  ChangeUserNameView.swift
//  QtheMusic
//
//  Created by Hammad Hassan on 16/05/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialTextControls_OutlinedTextFields

protocol UpdateUserNameDelegate{
    func updateName(name:String)
    func closeUpdateNameView()
}
protocol UpdateUserNameDelegateImp{
    func setDelegate(delegate:UpdateUserNameDelegate)
}

class ChangeUserNameView: UIView {

    @IBOutlet weak var userNameTextField: MDCOutlinedTextField!{
        didSet{
            userNameTextField.placeholder = StringConstant.name
            
            userNameTextField.setFloatingLabelColor(UIColor.white, for: .normal)
            userNameTextField.setFloatingLabelColor(ColorConstrants.PRIMARY_COLOR, for: .editing)
            userNameTextField.setFloatingLabelColor(UIColor.white, for: .disabled)
            
            userNameTextField.setNormalLabelColor(UIColor.white, for: .normal)
            userNameTextField.setNormalLabelColor(UIColor.white, for: .editing)
            userNameTextField.setNormalLabelColor(UIColor.white, for: .disabled)
            
            userNameTextField.label.text = StringConstant.name
            
            userNameTextField.setTextColor(UIColor.white, for: .editing)
            userNameTextField.setTextColor(UIColor.white, for: .normal)
            userNameTextField.setTextColor(UIColor.white, for: .disabled)
            
            
            userNameTextField.setLeadingAssistiveLabelColor(UIColor.red, for: .normal)
            userNameTextField.setLeadingAssistiveLabelColor(UIColor.red, for: .disabled)
            userNameTextField.setLeadingAssistiveLabelColor(UIColor.red, for: .editing)
            
            userNameTextField.setOutlineColor(UIColor.white, for: .normal)
            userNameTextField.setOutlineColor(ColorConstrants.PRIMARY_COLOR, for: .editing)
        }
    }
    
    @IBOutlet weak var updateButton: UIButton!{
        didSet{
            self.updateButton.layer.cornerRadius = 12
        }
    }
    
    @IBOutlet weak var containerView: UIView!{
        didSet{
            self.containerView.layer.cornerRadius = 12
        }
    }
    
    @IBOutlet weak var blurViewImg: UIImageView!{
        didSet{
            self.blurViewImg.blurImageViewWithTransparentBlackColor()
        }
    }
    
    @IBOutlet weak var warningImage: UIImageView!
    
    
    private var delegate : UpdateUserNameDelegate?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.userNameTextField.delegate = self
        
    }
    
    internal func setData(data:Authorization?){
        
        self.userNameTextField.text = data?.name
    }
    
    @IBAction func closeButtonPressed(_ sender: UIButton) {
        self.delegate?.closeUpdateNameView()
    }
    
    @IBAction func updateButtonPressed(_ sender: UIButton) {
        
        if self.userNameTextField.text?.isEmpty ?? true{
            self.warningImage.isHidden = false
            return
        }else{
            self.warningImage.isHidden = true
            self.delegate?.updateName(name: self.userNameTextField.text ?? "")
        }
        
        
       
    }
    
    
}

//MARK: - Update UserName Delegate Imp
extension ChangeUserNameView : UpdateUserNameDelegateImp{
    func setDelegate(delegate: UpdateUserNameDelegate) {
        self.delegate = delegate
    }
    
    
}

extension ChangeUserNameView : UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.clearsOnInsertion = false
        textField.clearsOnBeginEditing = false
        switch textField {
        case userNameTextField:
            userNameTextField.leadingAssistiveLabel.text = ""
            warningImage.isHidden = true
        default:
            print("no field found")
        }
        return true
    }
}
