//
//  RecentlyPlayViewModel.swift
//  QtheMusic
//
//  Created by Assad Khan on 19/04/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import Foundation
import UIKit
class RecentlyPlayViewModel:BaseViewModel{
    
    var songsArray = [Song]()
    var artistArray = [Artist]()
    var albumsArray  = [Album]()
    
    let nunmberOfItemPerRow : CGFloat = 3
    let lineSpacing : CGFloat = 1
    let interItemSpacing : CGFloat = 2
    
    var recentlyPlayedCategories = [
        RecentlyPlayedMenu(isSelected: false, menuItem: RecentlyPlayedCatEnum.Songs.rawValue),
        RecentlyPlayedMenu(isSelected: false, menuItem: RecentlyPlayedCatEnum.Albums.rawValue),
        RecentlyPlayedMenu(isSelected: false, menuItem: RecentlyPlayedCatEnum.Artists.rawValue)
    ]
    
    
    
    
    func getSongsDataFromDb(){
        var nomralSongArray = [Song]()
        var count = 0
        if let Data = mDataManager.getSongsDataFromDb(){
            for song in Data where song.songStatus != SongStatus.PREMIUM.rawValue {
                nomralSongArray.append(song)
                count += 1
                if count == 5 {break}
            }
            self.songsArray = Array(nomralSongArray.prefix(upTo: count))
            }
        }
    
    func getAlbumsDataFromDb(){
        var nomralAlbumArray = [Album]()
        var count = 0
        if let Data = mDataManager.getAlbumsDataFromDb(){
            for album in Data where album.albumStatus != SongStatus.PREMIUM.rawValue {
                nomralAlbumArray.append(album)
                count += 1
                if count == 5 {break}
            }
            self.albumsArray = Array(nomralAlbumArray.prefix(upTo: count))
            //self.albumsArray.remove(at: 0)
        }
    }
    func getArtistsDataFromDb(){
        var nomralArtistArray = [Artist]()
        var count = 0
        if let Data = mDataManager.getArtistsDataFromDb(){
            for artist in Data {
                nomralArtistArray.append(artist)
                count += 1
                if count == 5 {break}
            }
            self.artistArray = Array(nomralArtistArray.prefix(upTo: count))
            self.artistArray.remove(at: 0)
        }
    }
}
