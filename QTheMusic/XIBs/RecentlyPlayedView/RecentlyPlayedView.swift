//
//  RecentlyPlayedView.swift
//  QtheMusic
//
//  Created by Assad Khan on 19/04/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import UIKit

class RecentlyPlayedView: UIView {
    
    @IBOutlet weak var songsTableView:UITableView!{
        didSet{
            songsTableView.delegate = self
            songsTableView.dataSource = self
        }
    }
    @IBOutlet weak var albumsArtistCollectionView:UICollectionView!{
        didSet{
            albumsArtistCollectionView.delegate = self
            albumsArtistCollectionView.dataSource = self
        }
    }
    
    @IBOutlet weak var recentlyPlayedCategoryCollectionView:UICollectionView!{
        didSet{
            recentlyPlayedCategoryCollectionView.delegate = self
            recentlyPlayedCategoryCollectionView.dataSource  = self
        }
    }
    
    
    private var mViewModel = RecentlyPlayViewModel()
    private var selectedCat:String?
    private var collectionViewFlowLayout : UICollectionViewFlowLayout!
    private var collectionViewFlowLayoutForArtist : UICollectionViewFlowLayout!
    private var collectionViewFlowLayoutForCat : UICollectionViewFlowLayout!
    internal var navigationController :UINavigationController?
    internal var tabbarViewController: CustomTabbarController?
    internal var vc : SearchTabViewController?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setUpView()
        
        
        
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
       
        
    }
    
    private func setUpView(){
        self.registerNib()
    }
    private func registerNib(){
        
        albumsArtistCollectionView.translatesAutoresizingMaskIntoConstraints = false
        albumsArtistCollectionView.bottomAnchor.constraint(equalTo: self.songsTableView.bottomAnchor).isActive = true
        albumsArtistCollectionView.trailingAnchor.constraint(equalTo: self.songsTableView.trailingAnchor).isActive = true
        recentlyPlayedCategoryCollectionView.register(UINib(nibName: UINibConstant.InterestCollectionViewCellNib, bundle: nil), forCellWithReuseIdentifier: UINibConstant.InterestCollectionViewCellIdentifier)
        
        albumsArtistCollectionView.register(UINib(nibName: UINibConstant.AlbumCollectionViewCellNib, bundle: nil), forCellWithReuseIdentifier: UINibConstant.AlbumCollectionViewCellIdentifier)
        
        albumsArtistCollectionView.register(UINib(nibName: UINibConstant.ArtistCollectionViewCellNib, bundle: nil), forCellWithReuseIdentifier: UINibConstant.ArtistCollectionViewCellIdentifier)
        
        songsTableView.register(UINib(nibName: UINibConstant.SongsTableViewCellNib, bundle: nil),forCellReuseIdentifier: UINibConstant.SongsTableViewCellIdentifier)
        
        
        self.selectedCat = RecentlyPlayedCatEnum.Songs.rawValue
        self.mViewModel.recentlyPlayedCategories[0].isSelected = true
        self.mViewModel.getSongsDataFromDb()
        
        self.songsTableView.reloadData()
        setupFlowLayout()
        self.songsTableView.isHidden = false
        self.albumsArtistCollectionView.isHidden = true
    }
    
    // MARK: - Collection View Flow Layout
    private func setupFlowLayout(){
        
        if selectedCat == RecentlyPlayedCatEnum.Albums.rawValue{
            
            let width = (albumsArtistCollectionView.frame.width - 16) / 2
            let height = albumsArtistCollectionView.frame.width * 0.62
            //using two as two column are to be presented
            collectionViewFlowLayoutForArtist = UICollectionViewFlowLayout()
            
            collectionViewFlowLayoutForArtist.itemSize = CGSize(width: width, height: height)
            
            collectionViewFlowLayoutForArtist.scrollDirection = .vertical
            collectionViewFlowLayoutForArtist.minimumLineSpacing = 8
            collectionViewFlowLayoutForArtist.minimumInteritemSpacing = self.mViewModel.interItemSpacing
            albumsArtistCollectionView.setCollectionViewLayout(collectionViewFlowLayoutForArtist, animated: true)
        }
        else if self.selectedCat == RecentlyPlayedCatEnum.Artists.rawValue {
            
            let width = (albumsArtistCollectionView.frame.width - (5 * self.mViewModel.interItemSpacing)) / 3
            let height = albumsArtistCollectionView.frame.width * 0.4
            //using two as two column are to be presented
            collectionViewFlowLayout = UICollectionViewFlowLayout()
            
            collectionViewFlowLayout.itemSize = CGSize(width: width, height: height)
            
            collectionViewFlowLayout.scrollDirection = .vertical
            collectionViewFlowLayout.minimumLineSpacing = 8
            collectionViewFlowLayout.minimumInteritemSpacing = self.mViewModel.interItemSpacing
            albumsArtistCollectionView.setCollectionViewLayout(collectionViewFlowLayout, animated: true)
        }
        if collectionViewFlowLayoutForCat == nil{
            
            
            
            collectionViewFlowLayoutForCat = UICollectionViewFlowLayout()
            
            collectionViewFlowLayoutForCat.itemSize = CGSize(width: 100, height: 30)
            
            collectionViewFlowLayoutForCat.scrollDirection = .horizontal
            collectionViewFlowLayoutForCat.minimumLineSpacing = 10
            collectionViewFlowLayoutForCat.minimumInteritemSpacing = self.mViewModel.interItemSpacing
            recentlyPlayedCategoryCollectionView.setCollectionViewLayout(collectionViewFlowLayoutForCat, animated: true)
        }
        
    }
    internal func reloadDataBase(){
        if self.selectedCat != nil{
           
            if self.selectedCat == RecentlyPlayedCatEnum.Songs.rawValue{
                
                songsTableView.isHidden = false
                albumsArtistCollectionView.isHidden = true
                
                self.mViewModel.getSongsDataFromDb()
                self.songsTableView.reloadData()
                
                
            }else if self.selectedCat == RecentlyPlayedCatEnum.Albums.rawValue{
                songsTableView.isHidden = true
                albumsArtistCollectionView.isHidden = false
                
                self.mViewModel.getAlbumsDataFromDb()
                
                self.albumsArtistCollectionView.reloadData()
                setupFlowLayout()
                
            }else{
                songsTableView.isHidden = true
                albumsArtistCollectionView.isHidden = false
                self.mViewModel.getArtistsDataFromDb()
                
                self.albumsArtistCollectionView.reloadData()
                setupFlowLayout()
                
            }
            
            
        }
    }
}

// MARK: -CollectionView Delegate
extension RecentlyPlayedView : UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView{
        case recentlyPlayedCategoryCollectionView:
            return self.mViewModel.recentlyPlayedCategories.count
        case albumsArtistCollectionView:
            if self.selectedCat == RecentlyPlayedCatEnum.Songs.rawValue{
                return   self.mViewModel.songsArray.count
            }else if self.selectedCat == RecentlyPlayedCatEnum.Albums.rawValue{
                return  self.mViewModel.albumsArray.count
            }else{
                return  self.mViewModel.artistArray.count
            }
        default :
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView{
        case recentlyPlayedCategoryCollectionView:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: UINibConstant.InterestCollectionViewCellIdentifier, for: indexPath) as! InterestCollectionViewCell
            
            cell.setUpCellForMenu(data: self.mViewModel.recentlyPlayedCategories[indexPath.row])
            
            return cell
        case albumsArtistCollectionView:
            
            if self.selectedCat == RecentlyPlayedCatEnum.Artists.rawValue{
                
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: UINibConstant.ArtistCollectionViewCellIdentifier, for: indexPath) as! ArtistCollectionViewCell
                
                cell.setUpCell(data: self.mViewModel.artistArray[indexPath.row])
                return cell
                
                
            }else if self.selectedCat == RecentlyPlayedCatEnum.Albums.rawValue{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: UINibConstant.AlbumCollectionViewCellIdentifier, for: indexPath) as! AlbumCollectionViewCell
                cell.setUpCell(data: self.mViewModel.albumsArray[indexPath.row])
                
                return cell
            }
            else{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: UINibConstant.ArtistCollectionViewCellIdentifier, for: indexPath) as! ArtistCollectionViewCell
                
                
                return cell
            }
            
        default:
            
            
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: UINibConstant.ArtistCollectionViewCellIdentifier, for: indexPath) as! ArtistCollectionViewCell
            
            
            return cell
            
        }
        
    }
    
    
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        switch collectionView{
        case recentlyPlayedCategoryCollectionView:
            
            
            self.selectedCat = self.mViewModel.recentlyPlayedCategories[indexPath.row].menuItem ?? ""
            
            if self.mViewModel.recentlyPlayedCategories[indexPath.row].isSelected == true {
                
                
            }else{
                
                
                for items in self.mViewModel.recentlyPlayedCategories{
                    items.isSelected = false
                }
                
                self.mViewModel.recentlyPlayedCategories[indexPath.row].isSelected = true
                
                
                
                if self.selectedCat == RecentlyPlayedCatEnum.Songs.rawValue{
                    
                    songsTableView.isHidden = false
                    albumsArtistCollectionView.isHidden = true
                    
                    self.mViewModel.getSongsDataFromDb()
                    self.songsTableView.reloadData()
                    
                    
                }else if self.selectedCat == RecentlyPlayedCatEnum.Albums.rawValue{
                    songsTableView.isHidden = true
                    albumsArtistCollectionView.isHidden = false
                    
                    self.mViewModel.getAlbumsDataFromDb()
                    
                    self.albumsArtistCollectionView.reloadData()
                    setupFlowLayout()
                    
                }else{
                    songsTableView.isHidden = true
                    albumsArtistCollectionView.isHidden = false
                    self.mViewModel.getArtistsDataFromDb()
                    
                    self.albumsArtistCollectionView.reloadData()
                    setupFlowLayout()
                    
                }
                
                
            }
            self.recentlyPlayedCategoryCollectionView.reloadData()
            
        case albumsArtistCollectionView:
            if self.selectedCat == RecentlyPlayedCatEnum.Albums.rawValue{
                
                let vc = UIStoryboard.init(name: StoryBoardConstant.storyBoardMain, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.AlbumDetailVc) as? AlbumDetailViewController
                let builder = SongAndArtistBuilder()
                builder.setalbumId(self.mViewModel.albumsArray[indexPath.row].albumID)
                builder.setalbumTitle(self.mViewModel.albumsArray[indexPath.row].albumTitle)
                builder.setalbumCoverImageURL(self.mViewModel.albumsArray[indexPath.row].albumCoverImageURL)
                builder.setalbumStatus(self.mViewModel.albumsArray[indexPath.row].albumStatus)
                builder.setNumberOfSongs(self.mViewModel.albumsArray[indexPath.row].numberOfSongs)
                builder.setTimeStamp(Date().timeIntervalSince1970)
                vc?.setSongData(data: builder)
                vc?.setAlbumData(data: builder.build())
                vc?.setTabBarVC(tabBarVC: self.tabbarViewController ?? UITabBarController())
                let transition = CATransition()
                transition.duration = 0.5
                transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
                transition.type = CATransitionType.fade
                self.navigationController?.view.layer.add(transition, forKey: nil)
                self.navigationController?.pushViewController(vc!, animated: false)
            }else if self.selectedCat == RecentlyPlayedCatEnum.Artists.rawValue{
                
                let vc = UIStoryboard.init(name: StoryBoardConstant.storyBoardMain, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.ArtistDetailViewController) as? ArtistDetailViewController
                vc?.setTabBarVC(tabBarVC: self.tabbarViewController ?? UITabBarController())
                vc?.setArtistId(artistId: self.mViewModel.artistArray[indexPath.row].artistID)
                let transition = CATransition()
                transition.duration = 0.5
                transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
                transition.type = CATransitionType.fade
                self.navigationController?.view.layer.add(transition, forKey: nil)
                self.navigationController?.pushViewController(vc!, animated: false)
                
            }
        default:
            print("Default")
        }
        
        
    }
    
}
extension RecentlyPlayedView : UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.mViewModel.songsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: UINibConstant.SongsTableViewCellIdentifier, for: indexPath) as! SongsTableViewCell
        cell.setUpCell(data: self.mViewModel.songsArray[indexPath.row])
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if self.mViewModel.songsArray[indexPath.row].songStatus == SongStatus.PREMIUM.rawValue{
            let vc = UIStoryboard.init(name: StoryBoardConstant.storyBoardMain, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.SongsDetailViewController) as? SongsDetailViewController
            vc?.setPlayingCategoryType(categoryType: self.selectedCat)
            vc?.setSongData(song: self.mViewModel.songsArray[indexPath.row])
            vc?.setIndexPath(row: indexPath.row)
            vc?.setCompleteSongsArray(songsArray: self.mViewModel.songsArray)
            vc?.setIsPlayingFromString(isplayingFromString: "\(StringConstant.playingFrom)SEARCHED")
            vc?.setIsFromMiniPlayer(isFromMiniPlayer: false)
            self.navigationController?.pushViewController(vc!, animated: false)
            self.tabbarViewController?.removeMiniPlayer()
        }else{
            
            self.tabbarViewController?.setPlayingCategoryType(categoryType: self.selectedCat)
            self.tabbarViewController?.setSongData(song: self.mViewModel.songsArray[indexPath.row])
            self.tabbarViewController?.setIndexPath(row: indexPath.row)
            self.tabbarViewController?.setCompleteSongsArray(songsArray: self.mViewModel.songsArray)
            self.tabbarViewController?.setIsPlayingFromString(isplayingFromString: "\(StringConstant.playingFrom)SEARCHED")
            guard let vc = vc else {return
                
            }
            
            //Setting up Next and previous Audio Linkages
            AudioPlayer.shared.songsArray = self.mViewModel.songsArray
            AudioPlayer.shared.indexPath = indexPath.row
            
            self.tabbarViewController?.addMiniPlayer()
            vc.setBottomSafeAreConstraintForMiniPlayer()
            
            
        }
        
    
    }
}
