//
//  ChangeAddressView.swift
//  QtheMusic
//
//  Created by Hammad Hassan on 17/05/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialTextControls_OutlinedTextFields

protocol ChangeAddressViewDelegate{
    func closePressed()
    func updatePressed(address:String,city:String,state:String,country:String,zipCode:String)
}

protocol ChangeAddressViewDelegateImp{
    func setDelegate(delegate:ChangeAddressViewDelegate)
}


class ChangeAddressView: UIView {

    @IBOutlet weak var blurView: UIImageView!{
        didSet{
            self.blurView.blurImageViewWithTransparentBlackColor()
        }
    }
    @IBOutlet weak var containerView: UIView!{
        didSet{
            self.containerView.layer.cornerRadius = 12
        }
    }
    @IBOutlet weak var updateButton: UIButton!{
        didSet{
            self.updateButton.layer.cornerRadius = 12
        }
    }
    
    
    @IBOutlet weak var addressTextField: MDCOutlinedTextField!{
        didSet{
            addressTextField.placeholder = StringConstant.address
            
            addressTextField.setFloatingLabelColor(UIColor.white, for: .normal)
            addressTextField.setFloatingLabelColor(ColorConstrants.PRIMARY_COLOR, for: .editing)
            addressTextField.setFloatingLabelColor(UIColor.white, for: .disabled)
            
            addressTextField.setNormalLabelColor(UIColor.white, for: .normal)
            addressTextField.setNormalLabelColor(UIColor.white, for: .editing)
            addressTextField.setNormalLabelColor(UIColor.white, for: .disabled)
            
            addressTextField.label.text = StringConstant.address
            
            addressTextField.setTextColor(UIColor.white, for: .editing)
            addressTextField.setTextColor(UIColor.white, for: .normal)
            addressTextField.setTextColor(UIColor.white, for: .disabled)
            
            
            addressTextField.setLeadingAssistiveLabelColor(UIColor.red, for: .normal)
            addressTextField.setLeadingAssistiveLabelColor(UIColor.red, for: .disabled)
            addressTextField.setLeadingAssistiveLabelColor(UIColor.red, for: .editing)
            
            addressTextField.setOutlineColor(UIColor.white, for: .normal)
            addressTextField.setOutlineColor(ColorConstrants.PRIMARY_COLOR, for: .editing)
        }
    }
    
    
    @IBOutlet weak var cityTextField: MDCOutlinedTextField!{
        didSet{
            cityTextField.placeholder = StringConstant.city
            
            cityTextField.setFloatingLabelColor(UIColor.white, for: .normal)
            cityTextField.setFloatingLabelColor(ColorConstrants.PRIMARY_COLOR, for: .editing)
            cityTextField.setFloatingLabelColor(UIColor.white, for: .disabled)
            
            cityTextField.setNormalLabelColor(UIColor.white, for: .normal)
            cityTextField.setNormalLabelColor(UIColor.white, for: .editing)
            cityTextField.setNormalLabelColor(UIColor.white, for: .disabled)
            
            cityTextField.label.text = StringConstant.city
            
            cityTextField.setTextColor(UIColor.white, for: .editing)
            cityTextField.setTextColor(UIColor.white, for: .normal)
            cityTextField.setTextColor(UIColor.white, for: .disabled)
            
            
            cityTextField.setLeadingAssistiveLabelColor(UIColor.red, for: .normal)
            cityTextField.setLeadingAssistiveLabelColor(UIColor.red, for: .disabled)
            cityTextField.setLeadingAssistiveLabelColor(UIColor.red, for: .editing)
            
            cityTextField.setOutlineColor(UIColor.white, for: .normal)
            cityTextField.setOutlineColor(ColorConstrants.PRIMARY_COLOR, for: .editing)
        }
    }
    
    @IBOutlet weak var stateTextField: MDCOutlinedTextField!{
        didSet{
            stateTextField.placeholder = StringConstant.state
            
            stateTextField.setFloatingLabelColor(UIColor.white, for: .normal)
            stateTextField.setFloatingLabelColor(ColorConstrants.PRIMARY_COLOR, for: .editing)
            stateTextField.setFloatingLabelColor(UIColor.white, for: .disabled)
            
            stateTextField.setNormalLabelColor(UIColor.white, for: .normal)
            stateTextField.setNormalLabelColor(UIColor.white, for: .editing)
            stateTextField.setNormalLabelColor(UIColor.white, for: .disabled)
            
            stateTextField.label.text = StringConstant.state
            
            stateTextField.setTextColor(UIColor.white, for: .editing)
            stateTextField.setTextColor(UIColor.white, for: .normal)
            stateTextField.setTextColor(UIColor.white, for: .disabled)
            
            
            stateTextField.setLeadingAssistiveLabelColor(UIColor.red, for: .normal)
            stateTextField.setLeadingAssistiveLabelColor(UIColor.red, for: .disabled)
            stateTextField.setLeadingAssistiveLabelColor(UIColor.red, for: .editing)
            
            stateTextField.setOutlineColor(UIColor.white, for: .normal)
            stateTextField.setOutlineColor(ColorConstrants.PRIMARY_COLOR, for: .editing)
        }
    }
    
    @IBOutlet weak var countryTextField: MDCOutlinedTextField!{
        didSet{
            countryTextField.placeholder = StringConstant.country
            
            countryTextField.setFloatingLabelColor(UIColor.white, for: .normal)
            countryTextField.setFloatingLabelColor(ColorConstrants.PRIMARY_COLOR, for: .editing)
            countryTextField.setFloatingLabelColor(UIColor.white, for: .disabled)
            
            countryTextField.setNormalLabelColor(UIColor.white, for: .normal)
            countryTextField.setNormalLabelColor(UIColor.white, for: .editing)
            countryTextField.setNormalLabelColor(UIColor.white, for: .disabled)
            
            countryTextField.label.text = StringConstant.country
            
            countryTextField.setTextColor(UIColor.white, for: .editing)
            countryTextField.setTextColor(UIColor.white, for: .normal)
            countryTextField.setTextColor(UIColor.white, for: .disabled)
            
            
            countryTextField.setLeadingAssistiveLabelColor(UIColor.red, for: .normal)
            countryTextField.setLeadingAssistiveLabelColor(UIColor.red, for: .disabled)
            countryTextField.setLeadingAssistiveLabelColor(UIColor.red, for: .editing)
            
            countryTextField.setOutlineColor(UIColor.white, for: .normal)
            countryTextField.setOutlineColor(ColorConstrants.PRIMARY_COLOR, for: .editing)
        }
    }
    @IBOutlet weak var zipCodeTextField: MDCOutlinedTextField!{
        didSet{
            zipCodeTextField.placeholder = StringConstant.zipCode
        
            zipCodeTextField.setFloatingLabelColor(UIColor.white, for: .normal)
            zipCodeTextField.setFloatingLabelColor(ColorConstrants.PRIMARY_COLOR, for: .editing)
            zipCodeTextField.setFloatingLabelColor(UIColor.white, for: .disabled)
            
            zipCodeTextField.setNormalLabelColor(UIColor.white, for: .normal)
            zipCodeTextField.setNormalLabelColor(UIColor.white, for: .editing)
            zipCodeTextField.setNormalLabelColor(UIColor.white, for: .disabled)
            
            zipCodeTextField.label.text = StringConstant.zipCode
            
            zipCodeTextField.setTextColor(UIColor.white, for: .editing)
            zipCodeTextField.setTextColor(UIColor.white, for: .normal)
            zipCodeTextField.setTextColor(UIColor.white, for: .disabled)
            
            
            zipCodeTextField.setLeadingAssistiveLabelColor(UIColor.red, for: .normal)
            zipCodeTextField.setLeadingAssistiveLabelColor(UIColor.red, for: .disabled)
            zipCodeTextField.setLeadingAssistiveLabelColor(UIColor.red, for: .editing)
            
            zipCodeTextField.setOutlineColor(UIColor.white, for: .normal)
            zipCodeTextField.setOutlineColor(ColorConstrants.PRIMARY_COLOR, for: .editing)
        }
    }

    @IBOutlet weak var cityWarningImg: UIImageView!
    @IBOutlet weak var stateWarningImg: UIImageView!
    @IBOutlet weak var countryWarningImg: UIImageView!
    @IBOutlet weak var zipWarningImg: UIImageView!
    
    
    private var delegate : ChangeAddressViewDelegate?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.cityTextField.delegate = self
        self.stateTextField.delegate = self
        self.countryTextField.delegate = self
        self.zipCodeTextField.delegate = self
        
    }
    
    func setAddressData(data:Authorization?){
        
        if let address = data?.addressModObj?.completeAddress {
            self.addressTextField.text = address
        }
        if let city = data?.addressModObj?.city {
            self.cityTextField.text = city
        }
        
        if let state = data?.addressModObj?.state {
            self.stateTextField.text = state
        }
        
        if let country = data?.addressModObj?.country{
            self.countryTextField.text = country
        }
        
        if let zipCode = data?.addressModObj?.zipCode {
            self.zipCodeTextField.text = zipCode
        }
        
        
        
    }
    
    
    @IBAction func closeButtonPressed(_ sender: UIButton) {
        self.delegate?.closePressed()
    }
    
    @IBAction func updateButtonPressed(_ sender: UIButton) {
        
        if self.addressTextField.text?.isEmpty == false {
            
            if self.cityTextField.text?.isEmpty ?? true {
                self.cityWarningImg.isHidden = false
                return
            }else {
                self.cityWarningImg.isHidden = true
            }
            
            if self.stateTextField.text?.isEmpty ?? true {
                self.stateWarningImg.isHidden = false
                return
            }else {
                self.stateWarningImg.isHidden = true
            }
            
            if self.countryTextField.text?.isEmpty ?? true {
                self.countryWarningImg.isHidden = false
                return
            }else {
                self.countryWarningImg.isHidden = true
            }
            
            if self.zipCodeTextField.text?.isEmpty ?? true {
                self.zipWarningImg.isHidden = false
                return
            }else {
                self.countryWarningImg.isHidden = true
            }
            
            self.delegate?.updatePressed(address: self.addressTextField.text ?? "", city: self.cityTextField.text ?? "", state: self.stateTextField.text ?? "", country: self.countryTextField.text ?? "", zipCode: self.zipCodeTextField.text ?? "")
            
        }else {
            Utils.showToast(view: self, message: StringConstant.nothingToUpdate)
        }
        
        
        
        
       
    }
    
}

extension ChangeAddressView : ChangeAddressViewDelegateImp {
    func setDelegate(delegate: ChangeAddressViewDelegate) {
        self.delegate = delegate
    }
    
    
}


extension ChangeAddressView : UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.clearsOnInsertion = false
        textField.clearsOnBeginEditing = false
        switch textField {
        case cityTextField:
            cityTextField.leadingAssistiveLabel.text = ""
            cityWarningImg.isHidden = true
        case stateTextField:
            stateTextField.leadingAssistiveLabel.text = ""
            stateWarningImg.isHidden = true
        case countryTextField:
            countryTextField.leadingAssistiveLabel.text = ""
            countryWarningImg.isHidden = true
        case zipCodeTextField:
            zipCodeTextField.leadingAssistiveLabel.text = ""
            zipWarningImg.isHidden = true
            
        default:
            print("no field found")
        }
        return true
    }
}

