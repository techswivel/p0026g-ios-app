//
//  FollowingArtistTableCell.swift
//  QtheMusic
//
//  Created by Muhammad Tanzeel Ur Rehman on 13/12/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import Foundation
import UIKit

class FollowingArtistTableCell : UITableViewCell {
    
    @IBOutlet weak var  artistPicture : UIImageView!{
        didSet{
            artistPicture.roundCorner(radius: Float(artistPicture.frame.height) / 2)
        }
    }
    
    @IBOutlet weak var activityIndicator : UIActivityIndicatorView!
    
    @IBOutlet weak var artistName: UILabel!
    
    @IBOutlet weak var unfollowBtn: UIButton!{
        didSet{
            unfollowBtn.roundCorner(radius: 17)
            unfollowBtn.layer.borderColor = ColorConstrants.FOLLOWINGBTN_COLOR.cgColor
            unfollowBtn.layer.borderWidth = 1
        }
    }
    
    
    override class func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    @IBAction func unfollowBtn(_sender: UIButton) {
        
        
    }
    
    
    internal func showAnimation(){
        artistPicture.isHidden = false
        activityIndicator.isHidden = false
        artistName.isHidden  = false
        self.isUserInteractionEnabled = false
        
        artistPicture.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        activityIndicator.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        artistName.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        unfollowBtn.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        unfollowBtn.layer.borderWidth = 0
        
    }
    
    
    internal func hideAnimation(){
        artistPicture.hideSkeleton()
        activityIndicator.hideSkeleton()
        artistName.hideSkeleton()
        unfollowBtn.hideSkeleton()
        unfollowBtn.layer.borderWidth = 1
        self.isUserInteractionEnabled = true
    }
    
    internal func setupCell (data: Artist?) {
        guard let Data = data else {
            return
        }
        artistPicture.isHidden = false
        activityIndicator.isHidden = false
        artistName.isHidden  = false
        self.isUserInteractionEnabled = true
        
        artistPicture.loadImage(imageUrl: Data.artistCoverImageURL ?? "" , placeHolderName: ImageConstant.noImagePlaceHolder, activiyIndicator: activityIndicator)
        artistName.text = "\(Data.artistName ?? "" )"
        unfollowBtn(_sender: UIButton())
        
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.clear
        selectedBackgroundView = bgColorView
    }
}


