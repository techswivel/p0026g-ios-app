//
//  ListeningSongTableCell.swift
//  QtheMusic
//
//  Created by Muhammad Tanzeel Ur Rehman on 12/01/2023.
//  Copyright © 2023 TechSwivel. All rights reserved.
//

import Foundation
import SkeletonView

class ListeningSongTableCell : UITableViewCell {
    
    @IBOutlet weak var photoImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionlbl: UILabel!
    @IBOutlet weak var activityLoader: UIActivityIndicatorView!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        photoImage.isHidden = true
        titleLabel.isHidden = true
        descriptionlbl.isHidden = true
        activityLoader.isHidden = true
        
    }
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    internal func showAnimation(){
        self.isUserInteractionEnabled = false
        photoImage.isHidden = false
        titleLabel.isHidden = false
        descriptionlbl.isHidden = false
        activityLoader.isHidden = false
        
        photoImage.roundCorner(radius: 0)
        photoImage.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        titleLabel.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        descriptionlbl.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        activityLoader.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        
    }
    
    internal func hideAnimation(){
        self.isUserInteractionEnabled = true
        photoImage.isHidden = false
        titleLabel.isHidden = false
        descriptionlbl.isHidden = false
        activityLoader.isHidden = false
        
        photoImage.hideSkeleton()
        titleLabel.hideSkeleton()
        descriptionlbl.hideSkeleton()
        activityLoader.hideSkeleton()
    }
    internal  func setUpCell(data:Song?){
        guard  data != nil else{return}
        self.isUserInteractionEnabled = true
        if data?.type == SearchSongType.ALBUM.rawValue{
            photoImage.roundCornerImageView()
            photoImage.loadImage(imageUrl: data?.albumCoverImageURL ?? "", placeHolderName: ImageConstant.noImagePlaceHolder, activiyIndicator: activityLoader)
            titleLabel.text = data?.albumTitle
            descriptionlbl.text = "Album by \(data?.albumArtist ?? "")"
            
        }else if  data?.type == SearchSongType.ARTIST.rawValue{
            
            photoImage.roundCorner(radius: (Float(photoImage.frame.height) / 2))
            titleLabel.text = data?.albumName
            descriptionlbl.text = data?.artistName
            photoImage.loadImage(imageUrl: data?.artistCoverImageURL ?? "", placeHolderName: ImageConstant.noImagePlaceHolder, activiyIndicator: activityLoader)
            
        }else{
            photoImage.roundCornerImageView()
            
            titleLabel.text = data?.songTitle
            descriptionlbl.text = data?.songDuration
            //descriptionlbl.text = "\( Utils.secondsToHoursMinutesSecondsString(seconds:data?.songDuration ?? "" ))"
            photoImage.loadImage(imageUrl: data?.coverImageURL ?? "", placeHolderName: ImageConstant.noImagePlaceHolder, activiyIndicator: activityLoader)
        }
        
        
        
        
    }
    
}
