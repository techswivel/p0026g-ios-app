//
//  PaymentFailedView.swift
//  QtheMusic
//
//  Created by zeeshan on 16/04/2023.
//  Copyright © 2023 TechSwivel. All rights reserved.
//
protocol FailedViewDelegate {
    func cancel()
}

import UIKit
import Lottie
class PaymentFailedView: UIView {

    @IBOutlet weak var paymentFailedLbl: UILabel!
    @IBOutlet weak var failedView: AnimationView!
    @IBOutlet weak var blurImage: UIImageView!{
        didSet{
            blurImage.blurImageViewWithTransparentBlackColor()
        }}
     var delegate : FailedViewDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        setFailedMsg(errorLbl: paymentFailedLbl.text ?? "")
        failedView.contentMode = .scaleAspectFit
        failedView.loopMode = .loop
        failedView.play()
    }
    @IBAction func onClickFailedBtn(_ sender: Any) {
        self.delegate?.cancel()
    }
     func setFailedMsg(errorLbl: String)
    {
        paymentFailedLbl.text = errorLbl
    }
}
