//
//  Addplaylist.swift
//  QtheMusic
//
//  Created by Muhammad Tanzeel Ur Rehman on 07/12/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import Foundation
import UIKit
import MaterialComponents.MaterialTextControls_OutlinedTextFields

protocol AddPlaylistDelegate{
    func updateTitle(name:String)
    func closeAddTitleView()
}
protocol AddPlaylistDelegateImp{
    func setDelegate(delegate:AddPlaylistDelegate)
}

class AddPlaylistView : UIView {
    
    @IBOutlet weak var addPlaylistTextfield: MDCOutlinedTextField!{
        didSet{
            addPlaylistTextfield.placeholder = StringConstant.aTitle
            addPlaylistTextfield.roundCorner(radius: 12)
            
            addPlaylistTextfield.setFloatingLabelColor(UIColor.white, for: .normal)
            addPlaylistTextfield.setFloatingLabelColor(ColorConstrants.PRIMARY_COLOR, for: .editing)
            addPlaylistTextfield.setFloatingLabelColor(UIColor.white, for: .disabled)
            
            addPlaylistTextfield.setNormalLabelColor(UIColor.white, for: .normal)
            addPlaylistTextfield.setNormalLabelColor(UIColor.white, for: .editing)
            addPlaylistTextfield.setNormalLabelColor(UIColor.white, for: .disabled)
            
            addPlaylistTextfield.label.text = StringConstant.aTitle
            
            addPlaylistTextfield.setTextColor(UIColor.white, for: .editing)
            addPlaylistTextfield.setTextColor(UIColor.white, for: .normal)
            addPlaylistTextfield.setTextColor(UIColor.white, for: .disabled)
            
            
            addPlaylistTextfield.setLeadingAssistiveLabelColor(UIColor.red, for: .normal)
            addPlaylistTextfield.setLeadingAssistiveLabelColor(UIColor.red, for: .disabled)
            addPlaylistTextfield.setLeadingAssistiveLabelColor(UIColor.red, for: .editing)
            
            addPlaylistTextfield.setOutlineColor(UIColor.white, for: .normal)
            addPlaylistTextfield.setOutlineColor(ColorConstrants.PRIMARY_COLOR, for: .editing)
        }
    }
    
    
    @IBOutlet weak var createButton: UIButton!{
        didSet{
            self.createButton.layer.cornerRadius = 13
        }
    }
    
    @IBOutlet weak var containerView: UIView!{
        didSet{
            self.containerView.layer.cornerRadius = 12
        }
    }
    
    @IBOutlet weak var blurViewImg: UIImageView!{
        didSet{
            self.blurViewImg.blurImageViewWithTransparentBlackColor()
        }
    }
    
    private var delegate : AddPlaylistDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.addPlaylistTextfield.delegate = self
        
    }
    
    internal func setData(data:Authorization?){
        
        self.addPlaylistTextfield.text = data?.name
    }
    
    @IBAction func closeButtonPressed(_ sender: UIButton) {
        self.delegate?.closeAddTitleView()
    }
    
    @IBAction func createButtonPressed(_ sender: UIButton) {
        
        if self.addPlaylistTextfield.text?.isEmpty ?? true{
            return
        }else{
            
            self.delegate?.updateTitle(name: self.addPlaylistTextfield.text ?? "")
            
        }
        
    }
    
}

//MARK: - Update UserName Delegate Imp

extension AddPlaylistView : AddPlaylistDelegateImp{
    func setDelegate(delegate: AddPlaylistDelegate) {
        self.delegate = delegate
    }
    
}

extension AddPlaylistView : UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.clearsOnInsertion = false
        textField.clearsOnBeginEditing = false
        switch textField {
        case addPlaylistTextfield:
            addPlaylistTextfield.leadingAssistiveLabel.text = ""
        default:
            print("no field found")
        }
        return true
    }
}

