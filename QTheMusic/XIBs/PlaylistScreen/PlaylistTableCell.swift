//
//  PlaylistTablevVewCell.swift
//  QtheMusic
//
//  Created by Muhammad Tanzeel Ur Rehman on 07/12/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import Foundation
import UIKit

class PlaylistTableCell : UITableViewCell {
    
    
    @IBOutlet weak var songsImage: UIImageView!
    @IBOutlet weak var playlistTitle : UILabel!
    @IBOutlet weak var noofSong: UILabel!
    
    override class func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func setupCell (playlist: Playlist) {
        
        playlistTitle.text = playlist.playListTitle
        noofSong.text = "\(playlist.totalSongs ?? 0) song"
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.clear
        selectedBackgroundView = bgColorView
    }
    internal func showAnimation(){

        
        self.isUserInteractionEnabled = false
        
        playlistTitle.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        noofSong.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        songsImage.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        
        
    }
    
    internal func hideAnimation(){
       
        self.isUserInteractionEnabled = true
        
        playlistTitle.hideSkeleton()
        noofSong.hideSkeleton()
        songsImage.hideSkeleton()
    }
}
