//
//  SongsCollectionViewCell.swift
//  QtheMusic
//
//  Created by Hammad Hassan on 28/04/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import UIKit

class SongsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var songImage: UIImageView!{
        didSet {
            songImage.layer.cornerRadius = 8
        }
    }
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    @IBOutlet weak var songTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.layer.cornerRadius = 8
    }
    
    
    internal func setupData(data:Song?){
        guard let Data = data else {
            return
        }
        
        songImage.loadImage(imageUrl: Data.coverImageURL ?? "", placeHolderName: ImageConstant.noImagePlaceHolder, activiyIndicator: activityIndicator)
        
    }

    
    
    internal func showAnimation(){
        
        
        songImage.isHidden = false
        songTitle.isHidden = false
        
        
        songImage.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        songTitle.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))

        
    }
    internal func hideAnimation(){
        songImage.isHidden = false
        songTitle.isHidden = false
        
        //activityIndicator.isHidden = false
        songImage.hideSkeleton()
        songTitle.hideSkeleton()
    }
    
    

}
