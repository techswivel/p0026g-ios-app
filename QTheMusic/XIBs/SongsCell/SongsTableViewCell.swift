//
//  SongsTableViewCell.swift
//  QtheMusic
//
//  Created by Assad Khan on 12/04/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import UIKit

    class SongsTableViewCell: UITableViewCell {
        
        @IBOutlet weak var heading: UILabel!
        @IBOutlet weak var descriptioon: UILabel!
        @IBOutlet weak var songImageView: UIImageView!{
            didSet{
                songImageView.roundCorner(radius: 5)
            }
        }
        @IBOutlet weak var playIconImageView: UIImageView!
        @IBOutlet weak var playIconImageBackgroundView: UIView!{
            didSet{
                playIconImageBackgroundView.roundCorner(radius: (Float(playIconImageBackgroundView.frame.height) / 2))
            }
        }
        
        @IBOutlet weak var premiumIconBackGroundView: UIView!{
            didSet{
                premiumIconBackGroundView.roundCorner(radius: (Float(playIconImageBackgroundView.frame.height) / 2))
            }
        }
        @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
        
        
        override func awakeFromNib() {
            super.awakeFromNib()
            // Initialization code
            
            
            
        }
        
        override func setSelected(_ selected: Bool, animated: Bool) {
            super.setSelected(selected, animated: animated)
            
            // Configure the view for the selected state
        }
        
        internal func showAnimation(){
 
            activityIndicator.isHidden = true

            
            self.isUserInteractionEnabled = false
            
            heading.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
            descriptioon.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
            songImageView.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
            activityIndicator.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
            premiumIconBackGroundView.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
            playIconImageBackgroundView.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
            
        }
        
        internal func hideAnimation(){
           
            activityIndicator.isHidden = false
            playIconImageBackgroundView.isHidden = false
            premiumIconBackGroundView.isHidden = false
            self.isUserInteractionEnabled = true
            
            heading.hideSkeleton()
            descriptioon.hideSkeleton()
            songImageView.hideSkeleton()
            activityIndicator.hideSkeleton()
            playIconImageBackgroundView.hideSkeleton()
            premiumIconBackGroundView.hideSkeleton()
        }
        
        internal func setUpCell(data:Song?){
            guard let Data = data else {
                return
            }
            
            self.isUserInteractionEnabled = true
            songImageView.loadImage(imageUrl: Data.coverImageURL ?? "", placeHolderName: ImageConstant.noImagePlaceHolder, activiyIndicator: activityIndicator)
            descriptioon.text = Data.songDuration
            
            print("songsStatus:\(data?.songStatus)")
            heading.text = "\(Data.songTitle ?? "")"
            if data?.songStatus == SongStatus.PREMIUM.rawValue{
                playIconImageBackgroundView.isHidden = true
                premiumIconBackGroundView.isHidden = false
            }else{
                playIconImageBackgroundView.isHidden = false
                premiumIconBackGroundView.isHidden = true
            }
    
        }
    }
