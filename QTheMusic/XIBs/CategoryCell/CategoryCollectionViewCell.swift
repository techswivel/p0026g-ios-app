//
//  CategoryCollectionViewCell.swift
//  QtheMusic
//
//  Created by Assad Khan on 26/04/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import UIKit

class CategoryCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var containerView: UIView!{
        didSet
        {
            containerView.roundCorner(radius: 8)
        }
    }
    @IBOutlet weak var catImageView: UIImageView!{
        didSet{
            catImageView.roundCorner(radius: 8)
        }
        
    }
    @IBOutlet weak var categoryTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    internal func setUpCell(data:Category?){
        guard let Data = data else {return}
        self.isUserInteractionEnabled = true
        catImageView.loadImage(imageUrl: Data.categoryImageURL ?? "", placeHolderName: ImageConstant.noImagePlaceHolder, activiyIndicator: activityIndicator)
        categoryTitle.text = "\(Data.categoryTitle ?? "")"
        containerView.isHidden = false
        
    }
    
    internal func showShimmerEffect(){
        self.isUserInteractionEnabled = false
        catImageView.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        categoryTitle.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        activityIndicator.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        containerView.isHidden = true
    }
    internal func stopAnimation(){
        self.isUserInteractionEnabled = true
        catImageView.hideSkeleton()
        categoryTitle.hideSkeleton()
        activityIndicator.hideSkeleton()
        containerView.isHidden = true
    }
}
