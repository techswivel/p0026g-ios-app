//
//  ListeningTitleCollectionCell.swift
//  QtheMusic
//
//  Created by Muhammad Tanzeel Ur Rehman on 12/01/2023.
//  Copyright © 2023 TechSwivel. All rights reserved.
//

import Foundation
import SkeletonView

class ListeningTitleCollectionCell : UICollectionViewCell {
    
    @IBOutlet weak var backgroundContainerView: UIView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        titleLabel.isHidden = true
        backgroundContainerView.isHidden = true
        // Initialization code
    }
    
    internal func setUpCell(category:Category?){
        guard category != nil else{ return}
        titleLabel.isHidden = false
        backgroundContainerView.isHidden = false
        
        
        self.isUserInteractionEnabled = true
        
        backgroundContainerView.roundCorner(radius: 9)
        
        titleLabel.text = category?.categoryTitle
        titleLabel.font = FontConstants.getManropeRegular(size: 12)
        if category?.isSelected ?? false{
            backgroundContainerView.backgroundColor = ColorConstrants.RED_THEME
        }else{
            backgroundContainerView.backgroundColor = ColorConstrants.UNSELECTEDCELL_INTERESTCOLOR
        }
        
    }
    
    internal func setUpCellForMenu(data:RecentlyPlayedMenu?){
        guard data != nil else{ return}
        titleLabel.isHidden = false
        backgroundContainerView.isHidden = false
        backgroundContainerView.roundCorner(radius: 9)
        
        self.isUserInteractionEnabled = true
        
        titleLabel.text = data?.menuItem
        if data?.isSelected ?? false{
            backgroundContainerView.backgroundColor = ColorConstrants.RED_THEME
        }else{
            backgroundContainerView.backgroundColor = ColorConstrants.UNSELECTEDCELL_INTERESTCOLOR
        }
        
    }
    
    internal func setUpCellForLanguages(language:Language?){
        guard language != nil else{ return}
        titleLabel.isHidden = false
        backgroundContainerView.isHidden = false
        backgroundContainerView.roundCorner(radius: 9)
        titleLabel.text = language?.languageTitle
        titleLabel.font = FontConstants.getManropeRegular(size: 12)
        
        self.isUserInteractionEnabled = true
        
        if language?.isSelected ?? false{
            backgroundContainerView.backgroundColor = ColorConstrants.RED_THEME
        }else{
            backgroundContainerView.backgroundColor = ColorConstrants.UNSELECTEDCELL_INTERESTCOLOR
        }
        
    }
    
    internal func showAnimation(){
        titleLabel.isHidden = false
        backgroundContainerView.isHidden = false
        self.isUserInteractionEnabled = false
        titleLabel.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        backgroundContainerView.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        
    }
    
    internal func hideAnimation(){
        titleLabel.isHidden = false
        backgroundContainerView.isHidden = false
        self.isUserInteractionEnabled = true
        
        titleLabel.hideSkeleton()
        backgroundContainerView.hideSkeleton()
      
    }
    
}
