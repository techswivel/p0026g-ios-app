//
//  AlbumSongCell.swift
//  QtheMusic
//
//  Created by Muhammad Tanzeel Ur Rehman on 20/01/2023.
//  Copyright © 2023 TechSwivel. All rights reserved.
//

import Foundation
import UIKit

class AlbumSongCell : UITableViewCell {
    
    @IBOutlet weak var photoImage: UIImageView!{
        didSet{
            photoImage.roundCorner(radius: 5)
        }
    }
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var descriptionlbl: UILabel!
    
    @IBOutlet weak var price: UILabel!
    
    @IBOutlet weak var activityIndicator : UIActivityIndicatorView!
    
    
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    
    internal func showAnimation(){
        
        activityIndicator.isHidden = true
        
        
        self.isUserInteractionEnabled = false
        
        titleLabel.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        descriptionlbl.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        photoImage.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        activityIndicator.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        price.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
      
        
    }
    
    internal func hideAnimation(){
        
        activityIndicator.isHidden = false
        self.isUserInteractionEnabled = true
        
        titleLabel.hideSkeleton()
        descriptionlbl.hideSkeleton()
        photoImage.hideSkeleton()
        activityIndicator.hideSkeleton()
        price.hideSkeleton()
        
    }
    
    internal func setUpCell(data:BuyingHistory?){
        guard let Data = data else {
            return
        }
        
        self.isUserInteractionEnabled = true
        photoImage.loadImage(imageUrl: Data.albumSongCoverImageUrl ?? "", placeHolderName: ImageConstant.noImagePlaceHolder, activiyIndicator: activityIndicator)
        descriptionlbl.text = "\(Data.albumSongDuration)"
        
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.clear
        selectedBackgroundView = bgColorView
        
        titleLabel.text = "\(Data.albumSongTitle ?? "")"
        price.text = "$\(Data.albumSongPrice )"
        
    }
    
}
