//
//  AlbumCollectionViewCell.swift
//  QtheMusic
//
//  Created by Assad Khan on 12/04/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import UIKit

class AlbumCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var albumPictureImageView: UIImageView!{
        didSet{
            self.albumPictureImageView.layer.cornerRadius = 8
        }
    }
    @IBOutlet weak var PlayImageView: UIImageView!
    @IBOutlet weak var isPremiumImageView: UIImageView!
    @IBOutlet weak var premiumIconBackGroundView: UIView!{
        didSet{
            premiumIconBackGroundView.roundCorner(radius: (Float(premiumIconBackGroundView.frame.height) / 2))
        }
    }
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var playIconBackgroundView: UIView!
    {
        didSet{
            playIconBackgroundView.roundCorner(radius: (Float(playIconBackgroundView.frame.height) / 2))
        }
    }
    @IBOutlet weak var subHeadingLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    var isFromRecommendedView = false
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.layer.cornerRadius = 8
    }
    internal func showAnimation(){
        
        
        albumPictureImageView.isHidden = false
        premiumIconBackGroundView.isHidden = false
        headingLabel.isHidden = false
        subHeadingLabel.isHidden = false
        activityIndicator.isHidden = false
        playIconBackgroundView.isHidden = false
        self.isUserInteractionEnabled = false
        
        albumPictureImageView.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        premiumIconBackGroundView.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        headingLabel.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        subHeadingLabel.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        activityIndicator.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        playIconBackgroundView.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        
    }
    internal func hideAnimation(){
        albumPictureImageView.isHidden = false
        premiumIconBackGroundView.isHidden = false
        headingLabel.isHidden = false
        subHeadingLabel.isHidden = false
        activityIndicator.isHidden = false
        playIconBackgroundView.isHidden = false
        self.isUserInteractionEnabled = true
        
        albumPictureImageView.hideSkeleton()
        premiumIconBackGroundView.hideSkeleton()
        headingLabel.hideSkeleton()
        subHeadingLabel.hideSkeleton()
        activityIndicator.hideSkeleton()
        playIconBackgroundView.hideSkeleton()
    }
    
    internal func setUpCell(data:Album?){
        guard let Data = data else {return }
        self.isUserInteractionEnabled = true
        albumPictureImageView.loadImage(imageUrl: Data.albumCoverImageURL ?? "", placeHolderName: ImageConstant.noImagePlaceHolder, activiyIndicator: activityIndicator)
        headingLabel.text = "\(Data.albumTitle ?? "")"
        subHeadingLabel.text = "\(Data.numberOfSongs) Songs"
        if Data.albumStatus == SongStatus.PREMIUM.rawValue{
            premiumIconBackGroundView.isHidden = false
            playIconBackgroundView.isHidden = true
            
        }else{
            
            premiumIconBackGroundView.isHidden = true
        
            if self.isFromRecommendedView {
                self.playIconBackgroundView.isHidden = true
            }else {
                self.playIconBackgroundView.isHidden = false
            }
            
        }
        
    }
    

    
}
