//
//  TrendingSongsView.swift
//  QtheMusic
//
//  Created by Hammad Hassan on 19/04/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import UIKit

protocol TrendingSongsDelegate{
    func trendingSongsErrorResponse(error:String?)
    func trendingSongsExpiryReponse()
}

protocol TrendingSongsDelegateImp {
    func setDelegate(delegate:TrendingSongsDelegate?)
    func trendingCallBack()
    func setNavigationController(navCon:UINavigationController?)
}
class TrendingSongsView: UIView {

    @IBOutlet weak var totalSongsLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            tableView.dataSource = self
            tableView.delegate = self
            tableView.separatorStyle = .none
        }
    }
    
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    
    internal var navigationController :UINavigationController?
    internal var tabbarViewController: CustomTabbarController?
    internal var vc : HomeScreenViewController?
    
    private var mViewModel = TrendingSongsViewModel()
    private var trendingSongNetworkViewModel = SongAndArtistViewModel()
    
    private var delegate: TrendingSongsDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.registerNib()
       
        self.bindingAllResponses()
        self.layoutSubviews()
        

        let builder = SongAndArtistBuilder()
        builder.settype(SongType.TRENDING.rawValue)
        self.trendingSongNetworkViewModel.songs(data: builder)
        
        
        
        
    }
    
    private func registerNib(){
        tableView.register(UINib(nibName: UINibConstant.SongsTableViewCellNib, bundle: nil),forCellReuseIdentifier: UINibConstant.SongsTableViewCellIdentifier)
        
    }
    
    
    override func layoutSubviews() {
        superview?.updateConstraints()
        self.tableViewHeightConstraint.constant = self.tableView.contentSize.height
        
    }
    
    private func bindingAllResponses(){
        self.bindingTrendingSongsServerResponse()
        self.bindingTrendingSongsErrorResponse()
        self.bindingTrendingSongsExpiryResponse()
    }
    
    private func bindingTrendingSongsServerResponse(){
        self.trendingSongNetworkViewModel.serverResponseForSong.bind {
            if($0 != nil){
              
                if let totalSongs = $0?.response?.data?.totalSongs{
                    self.totalSongsLabel.isHidden = false
                    self.totalSongsLabel.text = "\(totalSongs) \(StringConstant.songs)"
                }else {
                    self.totalSongsLabel.isHidden = true
                }
                
                
                if let trendingSongs = $0?.response?.data?.songs{
                    self.mViewModel.songsArray = trendingSongs
                   
                }
                
                self.tableView.reloadData()
                self.layoutSubviews()
            }
        }
    }
    
    
    private func bindingTrendingSongsErrorResponse(){
        self.trendingSongNetworkViewModel.errorResponseForSong.bind {
            if ($0 != nil) {
                self.delegate?.trendingSongsErrorResponse(error: $0?.response?.message)
            }
        }
    }
    
    private func bindingTrendingSongsExpiryResponse(){
        self.trendingSongNetworkViewModel.expireObserer.bind {
            if ($0 != nil){
                self.delegate?.trendingSongsExpiryReponse()
            }
        }
    }
    
    
}


extension TrendingSongsView : UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.mViewModel.songsArray?.count ?? 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: UINibConstant.SongsTableViewCellIdentifier, for: indexPath) as! SongsTableViewCell
        if self.mViewModel.songsArray == nil {
            cell.showAnimation()
        }else {
            cell.hideAnimation()
        cell.setUpCell(data: self.mViewModel.songsArray?[indexPath.row])
        }
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        UserDefaultUtils.setBool(value : false, key: CommonKeys.IsPlayingFromNotification)
        if self.mViewModel.songsArray?[indexPath.row].songStatus == SongStatus.PREMIUM.rawValue{
            let vc = UIStoryboard.init(name: StoryBoardConstant.storyBoardMain, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.SongsDetailViewController) as? SongsDetailViewController
            vc?.setPlayingCategoryType(categoryType: RecentlyPlayedCatEnum.Songs.rawValue)
            vc?.setSongData(song: self.mViewModel.songsArray?[indexPath.row])
            
            vc?.setIndexPath(row: indexPath.row)
            vc?.setCompleteSongsArray(songsArray: self.mViewModel.songsArray)
            vc?.setIsPlayingFromString(isplayingFromString: "\(StringConstant.playingFrom)TRENDING")
            vc?.setIsFromMiniPlayer(isFromMiniPlayer: false)
            let transition = CATransition()
            transition.duration = 0.5
            transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
            transition.type = CATransitionType.fade
            self.mViewModel.navController?.view.layer.add(transition, forKey: nil)
            self.mViewModel.navController?.pushViewController(vc!, animated: false)
            self.tabbarViewController?.removeMiniPlayer()
        }else{
            
            self.tabbarViewController?.setPlayingCategoryType(categoryType: RecentlyPlayedCatEnum.Songs.rawValue)
            self.tabbarViewController?.setSongData(song: self.mViewModel.songsArray?[indexPath.row])
            self.tabbarViewController?.setIndexPath(row: indexPath.row)
            self.tabbarViewController?.setCompleteSongsArray(songsArray: self.mViewModel.songsArray)
            self.tabbarViewController?.setIsPlayingFromString(isplayingFromString: "\(StringConstant.playingFrom)TRENDING")
           
            guard let vc = vc else {return
                
            }
            self.tabbarViewController?.addMiniPlayer()
            vc.setBottomSafeAreConstraintForMiniPlayer()
        }
        
    }
}

extension TrendingSongsView: TrendingSongsDelegateImp {
    func setNavigationController(navCon:UINavigationController?) {
        if let NavCon = navCon{
            self.mViewModel.navController = NavCon
        }
    }
    
    func setDelegate(delegate: TrendingSongsDelegate?) {
        self.delegate = delegate
    }
    func trendingCallBack() {
        if mViewModel.songsArray?.count != 0
        {
            let builder = SongAndArtistBuilder()
            builder.settype(SongType.TRENDING.rawValue)
            self.trendingSongNetworkViewModel.songs(data: builder)
            tableView.reloadData()
        }
    }
    
    
    
}
