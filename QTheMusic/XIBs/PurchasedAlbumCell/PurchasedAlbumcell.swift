//
//  PurchasedAlbumcell.swift
//  QtheMusic
//
//  Created by Muhammad Tanzeel Ur Rehman on 13/12/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import Foundation
import UIKit

class PurchasedAlbumcell : UICollectionViewCell {
    
    @IBOutlet weak var albumPicture: UIImageView!{
        didSet{
            albumPicture.layer.cornerRadius = 8
        }
    }
    
    @IBOutlet weak var PlayImage: UIImageView!
    @IBOutlet weak var isPremiumImage: UIImageView!
    @IBOutlet weak var premiumIconBackGround: UIView!{
        didSet{
            premiumIconBackGround.roundCorner(radius: (Float(premiumIconBackGround.frame.height) / 2))
        }
    }
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var playIconBackground: UIView!
    {
        didSet{
            playIconBackground.roundCorner(radius: (Float(playIconBackground.frame.height) / 2))
        }
    }
    @IBOutlet weak var subHeadingLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    var isFromRecommendedView = false
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.layer.cornerRadius = 8
    }
    internal func showAnimation(){
        
        
        albumPicture.isHidden = false
        premiumIconBackGround.isHidden = false
        headingLabel.isHidden = false
        subHeadingLabel.isHidden = false
        activityIndicator.isHidden = false
        playIconBackground.isHidden = false
        self.isUserInteractionEnabled = false
        
        albumPicture.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        premiumIconBackGround.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        headingLabel.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        subHeadingLabel.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        activityIndicator.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        playIconBackground.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        
    }
    internal func hideAnimation(){
        albumPicture.isHidden = false
        premiumIconBackGround.isHidden = false
        headingLabel.isHidden = false
        subHeadingLabel.isHidden = false
        activityIndicator.isHidden = false
        playIconBackground.isHidden = false
        self.isUserInteractionEnabled = true
        
        albumPicture.hideSkeleton()
        premiumIconBackGround.hideSkeleton()
        headingLabel.hideSkeleton()
        subHeadingLabel.hideSkeleton()
        activityIndicator.hideSkeleton()
        playIconBackground.hideSkeleton()
    }
    
    internal func setUpCell(data:Album?){
        guard let Data = data else {return }
        self.isUserInteractionEnabled = true
        albumPicture.loadImage(imageUrl: Data.albumCoverImageURL ?? "", placeHolderName: ImageConstant.noImagePlaceHolder, activiyIndicator: activityIndicator)
        headingLabel.text = "\(Data.albumTitle ?? "")"
        subHeadingLabel.text = "\(Data.numberOfSongs) Songs"
        if Data.albumStatus == SongStatus.PREMIUM.rawValue{
            premiumIconBackGround.isHidden = false
            playIconBackground.isHidden = true
            
        }else{
            
            premiumIconBackGround.isHidden = true
        
            if self.isFromRecommendedView {
                self.playIconBackground.isHidden = true
            }else {
                self.playIconBackground.isHidden = false
            }
            
        }
        
    }
    
    
   
    
}

