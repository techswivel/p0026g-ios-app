//
//  YourMoodView.swift
//  QtheMusic
//
//  Created by Hammad Hassan on 19/04/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import UIKit

protocol YourMoodViewDelegate{
    func yourMoodErrorResponse(error:String?)
    func youMoodExpiryResponse()
    func selectedCategory(category: Category?)
}

protocol YourMoodViewDelegateImp {
    func setDelegate(delegate:YourMoodViewDelegate?)
}

class YourMoodView: UIView {

    @IBOutlet weak var yourMoodCollectionView: UICollectionView!
    
    private var mViewModel = YourMoodViewModel()
    private let yourMoodNetworkViewModel = SongAndArtistViewModel()
    
    private var delegate: YourMoodViewDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.yourMoodCollectionView.dataSource = self
        self.yourMoodCollectionView.delegate = self
        self.registerNib()
        self.bindingAllReponses()
        
        
        self.yourMoodNetworkViewModel.getCtegories(categoryType: CategoryType.RECOMMENDED.rawValue)
        
    }

    private func registerNib(){
        yourMoodCollectionView.register(UINib(nibName: UINibConstant.CategoriesCollectionViewCellNib, bundle: nil), forCellWithReuseIdentifier: UINibConstant.CategoriesCollectionCellIdentifier)

        
  
}
    private func bindingAllReponses(){
        self.bindingCategoryServerResponse()
        self.bindingCategoryErrorResponse()
        self.bindingCategoryExpiryResponse()
    }
    
    private func bindingCategoryServerResponse(){
        self.yourMoodNetworkViewModel.serverResponseForGetCategories.bind {
            if ($0 != nil){
                if let yourMoodList = $0?.response?.data?.category {
                    self.mViewModel.interestArray = yourMoodList
                    self.yourMoodCollectionView.reloadData()
                }
                
            }
        }
    }
    
    private func bindingCategoryErrorResponse(){
        self.yourMoodNetworkViewModel.errorResponseForGetCategories.bind {
        
        if ($0 != nil){
            self.delegate?.yourMoodErrorResponse(error: $0?.response?.message)
        }
        }
    }
    private func bindingCategoryExpiryResponse(){
        self.yourMoodNetworkViewModel.expireObserer.bind {
            if ($0 != nil){
                self.delegate?.youMoodExpiryResponse()
            }
        }
    }
    
    
}

extension YourMoodView : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.mViewModel.interestArray?.count ?? 3
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
      
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: UINibConstant.CategoriesCollectionCellIdentifier, for: indexPath) as! CategoriesCollectionViewCell
        if self.mViewModel.interestArray == nil {
            cell.showAnimation()
        }else {
            cell.hideAnimation()
            cell.setCategoryData(data: self.mViewModel.interestArray?[indexPath.row])
        }
            return cell

        
        
    }
    
   
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.delegate?.selectedCategory(category: self.mViewModel.interestArray?[indexPath.row])
    }
    
    
    //MARK: - Uicollection FlowLayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let height = collectionView.bounds.height
        
        return CGSize(width: height * 1.8, height: height)
        
  
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
    }
    
}

extension YourMoodView : YourMoodViewDelegateImp {
    func setDelegate(delegate: YourMoodViewDelegate?) {
        self.delegate = delegate
    }
    
    
}
