//
//  CategoriesCollectionViewCell.swift
//  QtheMusic
//
//  Created by Hammad Hassan on 28/04/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import UIKit

class CategoriesCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var catBG: UIImageView!
    
    @IBOutlet weak var redShadowLayer: UIView!
    @IBOutlet weak var catTitle: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.layer.cornerRadius = 8
    }

    internal func setCategoryData(data:Category?){
        
        guard let Data = data else {return}
  
        catBG.loadImage(imageUrl: Data.categoryImageURL ?? "", placeHolderName: ImageConstant.noImagePlaceHolder, activiyIndicator: self.activityIndicator)
        
        catTitle.text = Data.categoryTitle
       
    }
        
    
    
    
    
    
    internal func showAnimation(){
        self.redShadowLayer.isHidden = true
        self.catBG.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        catTitle.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
    }
    
    internal func hideAnimation(){
        self.redShadowLayer.isHidden = false
        self.catBG.hideSkeleton()
        self.catTitle.hideSkeleton()
    }
    
}
