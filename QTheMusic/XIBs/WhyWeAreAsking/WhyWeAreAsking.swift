//
//  WhyWeAreAsking.swift
//  QtheMusic
//
//  Created by Assad Khan on 21/03/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import UIKit

protocol WhyWeAreAskingProtocol{
    func setDelegate(delegate:WhyWeAreAskingDelegate?)
}
protocol WhyWeAreAskingDelegate{
    func closeWhyWeAskingView()
}
class WhyWeAreAsking: UIView {

    @IBOutlet weak var backgroundView: UIImageView!{
        didSet{
            backgroundView.blurImageViewWithTransparentBlackColor()
        }
    }
    
    
    @IBOutlet weak var innerBackgroundView: UIView!{
        didSet{
            innerBackgroundView.roundCorner(radius: 12)
        }
    }
    
    private var delegate :WhyWeAreAskingDelegate?
    
    @IBAction func closeBtnPressed(_ sender: Any) {
        self.delegate?.closeWhyWeAskingView()
    }
}
extension WhyWeAreAsking:WhyWeAreAskingProtocol{
    func setDelegate(delegate: WhyWeAreAskingDelegate?) {
        if let delegate = delegate {
            self.delegate = delegate
        }
    }
    
    
}
