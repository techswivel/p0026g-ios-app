//
//  DownloadingSongsCell.swift
//  QtheMusic
//
//  Created by Muhammad Tanzeel Ur Rehman on 13/01/2023.
//  Copyright © 2023 TechSwivel. All rights reserved.
//

import Foundation
import UIKit

class DownloadingSongsCell : UITableViewCell {
   
    @IBOutlet weak var photoImage: UIImageView!{
        didSet{
            photoImage.roundCorner(radius: 8)
        }
    }
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionlbl: UILabel!
    
    @IBOutlet weak var progress : UIProgressView!{
        didSet{
            progress.trackTintColor = .white
            progress.tintColor = .red
            // Do any additional setup after loading the view.
            progress.setProgress(0.0, animated: false)
        }
        
    }
    
    @IBOutlet weak var playIconImageView: UIImageView!
    @IBOutlet weak var playIconImageBackgroundView: UIView!{
        didSet{
            playIconImageBackgroundView.roundCorner(radius: (Float(playIconImageBackgroundView.frame.height) / 2))
        }
    }
    
    @IBOutlet weak var activityIndicator : UIActivityIndicatorView!
    
    let totalValue:Float = 80.0
    var firstNumber:Int? = 0
    
    
    private var song: Song?
    
    override class func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    
    internal func showAnimation(){
        
        activityIndicator.isHidden = true
        
        
        self.isUserInteractionEnabled = false
        
        titleLabel.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        descriptionlbl.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        photoImage.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        activityIndicator.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        playIconImageBackgroundView.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        progress.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
    }
    
    internal func hideAnimation(){
        
        activityIndicator.isHidden = false
        playIconImageBackgroundView.isHidden = false
        progress.isHidden = false
        self.isUserInteractionEnabled = true
        
        titleLabel.hideSkeleton()
        descriptionlbl.hideSkeleton()
        photoImage.hideSkeleton()
        activityIndicator.hideSkeleton()
        progress.hideSkeleton()
        playIconImageBackgroundView.hideSkeleton()
    }
    
    func setProgress(firstValue:Float){
          let processValue = firstValue/totalValue
           progress.setProgress(processValue, animated: true)
       }
    
    func startAudioDownloading(){
        showProgressBarView()
    
        if let url = URL(string: self.song?.songAudioURL ?? ""){
                    DownloadManager.shared.addDownload(url)
                }
    }
    
    func startVideodownloading(){
        showProgressBarView()
        if let url = URL(string: self.song?.songAudioURL ?? ""){
                    DownloadManager.shared.addDownload(url)
                }
    }
    
    private func showProgressBarView(){
        playIconImageBackgroundView.isHidden = true
        progress.isHidden = false
        descriptionlbl.isHidden = true
    }
    
    internal func setUpCell(data:Song?){
        guard let Data = data else {
            return
        }
        self.song = Data
        self.isUserInteractionEnabled = true
        photoImage.loadImage(imageUrl: Data.coverImageURL ?? "", placeHolderName: ImageConstant.noImagePlaceHolder, activiyIndicator: activityIndicator)
        descriptionlbl.text = data?.songDuration
        
        
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.clear
        selectedBackgroundView = bgColorView
        
        print("songsStatus:\(String(describing: data?.songStatus))")
        titleLabel.text = "\(Data.songTitle ?? "")"
        
        playIconImageBackgroundView.isHidden = false
        progress.isHidden = true
        descriptionlbl.isHidden = false
        DownloadManager.shared.processDelegate = self
    }
    
}

extension DownloadingSongsCell: DownloadProcessProtocol{
    
    func downloadingProgress(_ percent: Float, fileName: String) {
        print("downloading \(percent) %")
        setProgress(firstValue: percent)
    }
    
    func downloadSucceeded(_ fileName: String) {
        playIconImageBackgroundView.isHidden = false
        progress.isHidden = true
        descriptionlbl.isHidden = false
    }
    
    func downloadWithError(_ error: Error?, fileName: String) {
        playIconImageBackgroundView.isHidden = false
        progress.isHidden = true
        descriptionlbl.isHidden = false
    }
    
    
}
