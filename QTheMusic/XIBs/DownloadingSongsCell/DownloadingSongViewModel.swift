//
//  DownloadingSongViewModel.swift
//  QtheMusic
//
//  Created by Muhammad Tanzeel Ur Rehman on 16/02/2023.
//  Copyright © 2023 TechSwivel. All rights reserved.
//

import Foundation
import UIKit

class DownloadingSongViewModel : BaseViewModel {
    
    var songsArray = [Song]()
    var songBuilder : SongAndArtistBuilder = SongAndArtistBuilder()
    
    var localAudioPath : String?
    var localVideoPath : String?
    

    func getSongsDataFromDb(){
        if let Data = mDataManager.getSongsDataFromDb(){
            self.songsArray = Data
        }
    }
    
    
}
