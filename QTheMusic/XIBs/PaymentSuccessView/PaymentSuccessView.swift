//
//  PaymentSuccessView.swift
//  QtheMusic
//
//  Created by zeeshan on 14/04/2023.
//  Copyright © 2023 TechSwivel. All rights reserved.
//
protocol SuccessViewDelegate {
    func cancelView()
}

import UIKit
import Lottie
class PaymentSuccessView: UIView {

    
    @IBOutlet weak var successView: AnimationView!
    @IBOutlet weak var blurImage: UIImageView!{
        didSet{
            blurImage.blurImageViewWithTransparentBlackColor()
        }}
    
    var delegate : SuccessViewDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        
        successView.contentMode = .scaleAspectFit
        successView.loopMode = .loop
        successView.play()
        
}
    
    @IBAction func onClickSuccessBtn(_ sender: Any) {
        self.delegate?.cancelView()
    }
}
