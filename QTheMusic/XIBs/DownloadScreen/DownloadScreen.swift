//
//  DownloadScreen.swift
//  QtheMusic
//
//  Created by Assad Khan on 12/09/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import UIKit


protocol DownloadScreenProtocol{
    func setDelegate(delegate:DownloadScreenDelegate)
    func setSongObj(song:Song)
    
}
protocol DownloadScreenDelegate{
    func audioBtnPressed()
    func videoBtnPressed()
    func removeDownloadScreen()
}
class DownloadScreen: UIView {

    @IBOutlet weak var containerView: UIView!{
        didSet{
            containerView.roundCorner(radius:   12)
        }
    }

    private var delegate:DownloadScreenDelegate?
    private var song:Song?
    
    @IBAction func audioBtnPressed(_ sender: Any) {
        self.delegate?.audioBtnPressed()
    }
    
    @IBAction func videoBtnPressed(_ sender: Any) {
        self.delegate?.videoBtnPressed()
    }
    
    @IBAction func removeDownloadScreenBtnPressed(_ sender: Any) {
    
        self.delegate?.removeDownloadScreen()
    }
    
    
}
extension DownloadScreen:DownloadScreenProtocol{
    func setDelegate(delegate: DownloadScreenDelegate) {
        self.delegate = delegate
    }
    
    func setSongObj(song: Song) {
        self.song = song
    }
    
    
}
