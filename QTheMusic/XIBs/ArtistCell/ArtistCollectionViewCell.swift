//
//  ArtistCollectionViewCell.swift
//  QtheMusic
//
//  Created by Assad Khan on 12/04/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import UIKit

class ArtistCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var artistPictureImageView: UIImageView!{
        didSet
        {
            artistPictureImageView.roundCorner(radius: 50)
        }
    }
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var artistNameLabel: UILabel!{
        didSet{
            artistNameLabel.adjustsFontSizeToFitWidth = true
            artistNameLabel.minimumScaleFactor = 0.4
            artistNameLabel.numberOfLines = 0
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    internal func showAnimation(){
        artistPictureImageView.isHidden = false
        activityIndicator.isHidden = false
        artistNameLabel.isHidden  = false
        self.isUserInteractionEnabled = false
        
        artistPictureImageView.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        activityIndicator.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        artistNameLabel.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        
    }
    
    internal func hideAnimation(){
        artistPictureImageView.hideSkeleton()
        activityIndicator.hideSkeleton()
        artistNameLabel.hideSkeleton() 
        self.isUserInteractionEnabled = true
    }
    internal func setUpCell(data:Artist?){
        guard let Data = data else {
            return
        }
        artistPictureImageView.isHidden = false
        activityIndicator.isHidden = false
        artistNameLabel.isHidden  = false
        self.isUserInteractionEnabled = true
        
        artistPictureImageView.loadImage(imageUrl: Data.artistCoverImageURL ?? "", placeHolderName: ImageConstant.noImagePlaceHolder, activiyIndicator: activityIndicator)
        artistNameLabel.text = "\(Data.artistName ?? "")"
    }
    

    
    
}
