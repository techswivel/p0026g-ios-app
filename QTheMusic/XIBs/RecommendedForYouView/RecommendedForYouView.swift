//
//  RecommendedForYouView.swift
//  QtheMusic
//
//  Created by Hammad Hassan on 12/04/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import UIKit

protocol RecommendedForYouDelegate{
     func recommendedSongsErrorObserver(error:String?)
     func recommendedSongsExpiryObserver()
}

protocol RecommendedForYouDelegateImp{
    func setDelegate(delegate:RecommendedForYouDelegate?)
    func recommendedCallBack()
}

class RecommendedForYouView: UIView {

    @IBOutlet weak var categoryCollectionView: UICollectionView!{
        didSet{
            self.categoryCollectionView.delegate = self
            self.categoryCollectionView.dataSource = self
        }
    }
    
    @IBOutlet weak var musicCollectionView: UICollectionView!{
        didSet{
            self.musicCollectionView.delegate = self
            self.musicCollectionView.dataSource = self
        }
    }
    
    
    private var selectedCat:String? = "Songs"
    private var collectionViewFlowLayoutForAlbum : UICollectionViewFlowLayout!
    private var collectionViewLayoutForSongs: UICollectionViewFlowLayout!
    private var collectionViewFlowLayoutForArtist : UICollectionViewFlowLayout!
    private var collectionViewFlowLayoutForCat : UICollectionViewFlowLayout!
    
    
    private let mViewModel = RecommendedForYouViewModel()
    private let recommendedSongsNetworkViewModel = SongAndArtistViewModel()
    internal var navigationController :UINavigationController?
    internal var tabbarViewController: CustomTabbarController?
    internal var vc : HomeScreenViewController?
    private var delegate: RecommendedForYouDelegate?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

        registerNib()
        self.bindingAllReponse()
        
    }
    

    private func registerNib(){

        categoryCollectionView.register(UINib(nibName: UINibConstant.InterestCollectionViewCellNib, bundle: nil), forCellWithReuseIdentifier: UINibConstant.InterestCollectionViewCellIdentifier)
        
        
        
        musicCollectionView.register(UINib(nibName: UINibConstant.SongsCollectionViewCellNib, bundle: nil), forCellWithReuseIdentifier: UINibConstant.SongsCollectionViewCellIdentifier)
        
        
        
        musicCollectionView.register(UINib(nibName: UINibConstant.AlbumCollectionViewCellNib, bundle: nil), forCellWithReuseIdentifier: UINibConstant.AlbumCollectionViewCellIdentifier)
        
        musicCollectionView.register(UINib(nibName: UINibConstant.ArtistCollectionViewCellNib, bundle: nil), forCellWithReuseIdentifier: UINibConstant.ArtistCollectionViewCellIdentifier)
        
       
        self.selectedCat = RecentlyPlayedCatEnum.Songs.rawValue
       
        self.mViewModel.recentlyPlayedCategories[0].isSelected = true
        

        let builder = SongAndArtistBuilder()
        builder.settype(SongRecommendType.SONGS.rawValue)
        builder.setRequestType(RequestType.RECOMMENDED.rawValue)
        self.recommendedSongsNetworkViewModel.getRecommendedSongs(data: builder)
        self.musicCollectionView.reloadData()
    }
    
    func addTransitionOnNavigationControler(){
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.fade
        self.navigationController?.view.layer.add(transition, forKey: nil)
    }

    private func bindingAllReponse(){
        self.bindingRecommendedResponse()
        self.bindingRecommendedErrorResponse()
        self.bindingRecommendedExpiryResponse()
    }
    
    private func bindingRecommendedResponse(){
        self.recommendedSongsNetworkViewModel.serverResponseForRecommendedSong.bind {
            if ($0 != nil) {
    
                if let songsData = $0?.response?.data?.songs{
                    self.mViewModel.songsArray = songsData
                    

                }
                
                if let albumData = $0?.response?.data?.albums{
                    self.mViewModel.albumsArray = albumData
                    
                }
                
                if let artistData = $0?.response?.data?.artist{
                    self.mViewModel.artistArray = artistData
                    
                }
                
                
                self.musicCollectionView.reloadData()
               
            }
                
        }
    }
    
    private func bindingRecommendedErrorResponse(){
        self.recommendedSongsNetworkViewModel.errorResponseForRecommendedSong.bind {
            if ($0 != nil){
                self.delegate?.recommendedSongsErrorObserver(error: $0?.response?.message)
            }
        }
    }
    
    private func bindingRecommendedExpiryResponse(){
        self.recommendedSongsNetworkViewModel.expireObserer.bind {
            if ($0 != nil){
                self.delegate?.recommendedSongsExpiryObserver()
                
            }
        }
    }

    
}


extension RecommendedForYouView : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case categoryCollectionView :
            return self.mViewModel.recentlyPlayedCategories.count
        case musicCollectionView :
            if self.selectedCat == RecentlyPlayedCatEnum.Songs.rawValue{
                
                return   self.mViewModel.songsArray?.count ?? 3
            }else if self.selectedCat == RecentlyPlayedCatEnum.Albums.rawValue{
                return  self.mViewModel.albumsArray?.count ?? 3
            }else{
                print("artistArrayCOunt")
                return  self.mViewModel.artistArray?.count ?? 3
            }
            
            
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView {
            
        case categoryCollectionView:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: UINibConstant.InterestCollectionViewCellIdentifier, for: indexPath) as! InterestCollectionViewCell
            cell.setUpCellForMenu(data: self.mViewModel.recentlyPlayedCategories[indexPath.row])
        
            return cell
            
            
        case musicCollectionView :
            print(self.selectedCat)
            if self.selectedCat == RecentlyPlayedCatEnum.Songs.rawValue {
             
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: UINibConstant.SongsCollectionViewCellIdentifier, for: indexPath) as! SongsCollectionViewCell
                if self.mViewModel.songsArray == nil {
                    print("enter here")
                    cell.showAnimation()
                }else {
                    cell.hideAnimation()
                    cell.setupData(data: self.mViewModel.songsArray?[indexPath.row])
                }
                
                return cell
                
            }
         else if self.selectedCat == RecentlyPlayedCatEnum.Albums.rawValue{
           
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: UINibConstant.AlbumCollectionViewCellIdentifier, for: indexPath) as! AlbumCollectionViewCell
             cell.isFromRecommendedView = true
             if self.mViewModel.albumsArray == nil{
                 cell.showAnimation()
             }else {
                 cell.hideAnimation()
                 cell.setUpCell(data: self.mViewModel.albumsArray?[indexPath.row])
             }

                return cell
            }
            else if self.selectedCat == RecentlyPlayedCatEnum.Artists.rawValue{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: UINibConstant.ArtistCollectionViewCellIdentifier, for: indexPath) as! ArtistCollectionViewCell
                if self.mViewModel.artistArray == nil {
                    cell.showAnimation()
                }else {
                    cell.hideAnimation()
                cell.setUpCell(data: self.mViewModel.artistArray?[indexPath.row])
                    
                }
                return cell
                
                
            }
    
        default:
            print("songs dequeue")
            
        }
        
        return UICollectionViewCell()
    }
    
   
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch collectionView{
        case categoryCollectionView:
              
            self.selectedCat = self.mViewModel.recentlyPlayedCategories[indexPath.row].menuItem ?? ""
            
            if self.mViewModel.recentlyPlayedCategories[indexPath.row].isSelected == true {
               
                
            }else{
               
                
                for items in self.mViewModel.recentlyPlayedCategories{
                    items.isSelected = false
                }
                
                self.mViewModel.recentlyPlayedCategories[indexPath.row].isSelected = true
      
                
                if self.selectedCat == RecentlyPlayedCatEnum.Songs.rawValue{
 
                    self.mViewModel.songsArray = nil
                    
                    self.musicCollectionView.reloadData()
                    
                    if mViewModel.songsArray == nil {
                        let builder = SongAndArtistBuilder()
                        builder.settype(SongRecommendType.SONGS.rawValue)
                        builder.setRequestType(RequestType.RECOMMENDED.rawValue)
                        self.recommendedSongsNetworkViewModel.getRecommendedSongs(data: builder)
                        self.musicCollectionView.reloadData()
                       

                    }else {
                        self.musicCollectionView.reloadData()
                        
                    }
                    
                   
                    
                }else if self.selectedCat == RecentlyPlayedCatEnum.Albums.rawValue{
                    self.mViewModel.albumsArray = nil
                    
                    self.musicCollectionView.reloadData()
                    
                    if mViewModel.albumsArray == nil {
                        let builder = SongAndArtistBuilder()
                        builder.settype(SongRecommendType.ALBUM.rawValue)
                        builder.setRequestType(RequestType.RECOMMENDED.rawValue)
                        self.recommendedSongsNetworkViewModel.getRecommendedSongs(data: builder)
                        self.musicCollectionView.reloadData()
                       
                    }else {
                        self.musicCollectionView.reloadData()
                       
                    }
                }else if self.selectedCat == RecentlyPlayedCatEnum.Artists.rawValue{
                    self.mViewModel.artistArray = nil
                    
                    self.musicCollectionView.reloadData()
                    
                    if mViewModel.artistArray == nil {
                        let builder = SongAndArtistBuilder()
                        builder.settype(SongRecommendType.ARTIST.rawValue)
                        builder.setRequestType(RequestType.RECOMMENDED.rawValue)
                        self.recommendedSongsNetworkViewModel.getRecommendedSongs(data: builder)
                        self.musicCollectionView.reloadData()
                       
                    }else {
                        self.musicCollectionView.reloadData()
                      
                    }
                }
                
                
            }
            self.categoryCollectionView.reloadData()
      
        case musicCollectionView:
            
            if self.selectedCat == RecentlyPlayedCatEnum.Songs.rawValue
            {
                if self.mViewModel.songsArray?[indexPath.row].songStatus == SongStatus.PREMIUM.rawValue{
                    let vc = UIStoryboard.init(name: StoryBoardConstant.storyBoardMain, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.SongsDetailViewController) as? SongsDetailViewController
                    vc?.setPlayingCategoryType(categoryType: RecentlyPlayedCatEnum.Songs.rawValue)
                    vc?.setSongData(song: self.mViewModel.songsArray?[indexPath.row])
                    vc?.setIndexPath(row: indexPath.row)
                    vc?.setCompleteSongsArray(songsArray: self.mViewModel.songsArray)
                    vc?.setIsPlayingFromString(isplayingFromString: "\(StringConstant.playingFrom)TRENDING")
                    vc?.setIsFromMiniPlayer(isFromMiniPlayer: false)
                    let transition = CATransition()
                    transition.duration = 0.5
                    transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
                    transition.type = CATransitionType.fade
                    self.navigationController?.view.layer.add(transition, forKey: nil)
                    UserDefaultUtils.setBool(value : false, key: CommonKeys.IsPlayingFromNotification)
                    self.navigationController?.pushViewController(vc!, animated: false)
                    self.tabbarViewController?.removeMiniPlayer()
                }else{
                    
                    self.tabbarViewController?.setPlayingCategoryType(categoryType: RecentlyPlayedCatEnum.Songs.rawValue)
                    self.tabbarViewController?.setSongData(song: self.mViewModel.songsArray?[indexPath.row])
                    self.tabbarViewController?.setIndexPath(row: indexPath.row)
                    self.tabbarViewController?.setCompleteSongsArray(songsArray: self.mViewModel.songsArray)
                    self.tabbarViewController?.setIsPlayingFromString(isplayingFromString: "\(StringConstant.playingFrom)TRENDING")
                    guard let vc = vc else {return
                        
                    }
                    self.tabbarViewController?.addMiniPlayer()
                    vc.setBottomSafeAreConstraintForMiniPlayer()
                }
                
            } else if self.selectedCat == RecentlyPlayedCatEnum.Albums.rawValue {
                print("Selected Album Id:",self.mViewModel.albumsArray![indexPath.row].albumID)
                let vc = UIStoryboard.init(name: StoryBoardConstant.storyBoardMain, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.AlbumDetailVc) as? AlbumDetailViewController
                let builder = SongAndArtistBuilder()
                builder.setalbumId(self.mViewModel.albumsArray![indexPath.row].albumID)
                vc?.setSongData(data: builder)
                vc?.setAlbum(data: self.mViewModel.albumsArray?[indexPath.row])
                UserDefaultUtils.setBool(value : false, key: CommonKeys.IsPlayingFromNotification)
                self.navigationController?.pushViewController(vc!, animated: true)
            }
            else if self.selectedCat == RecentlyPlayedCatEnum.Artists.rawValue{
                self.addTransitionOnNavigationControler()
                let vc = UIStoryboard.init(name: StoryBoardConstant.storyBoardMain, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.ArtistDetailViewController) as? ArtistDetailViewController
                
                vc?.setArtistId(artistId: self.mViewModel.artistArray?[indexPath.row].artistID)
                UserDefaultUtils.setBool(value : false, key: CommonKeys.IsPlayingFromNotification)
                self.navigationController?.pushViewController(vc!, animated: false)
                
            }
        default:
            print("Default")
        }
    
    
}
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        switch collectionView {
        case musicCollectionView:
            let height = (musicCollectionView.frame.height)
            let width = height
            return CGSize(width: width, height: height)
            

        case categoryCollectionView:
            return CGSize(width: 100, height: 30)

        default:
            print("default case")
        }
        return CGSize(width: 0, height: 0)
     }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        switch collectionView {
        case categoryCollectionView:
            return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 15)
        case musicCollectionView :
            return UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        default:
            print("set default edges")
        }
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }

}


extension RecommendedForYouView : RecommendedForYouDelegateImp {
    func setDelegate(delegate: RecommendedForYouDelegate?) {
        self.delegate = delegate
    }
    
    func recommendedCallBack() {
        if mViewModel.songsArray?.count != 0 
        {
            let builder = SongAndArtistBuilder()
            builder.settype(SongRecommendType.SONGS.rawValue)
            builder.setRequestType(RequestType.RECOMMENDED.rawValue)
            self.recommendedSongsNetworkViewModel.getRecommendedSongs(data: builder)
            self.musicCollectionView.reloadData()
    }
       else if mViewModel.albumsArray?.count != 0
        {
            let builder = SongAndArtistBuilder()
            builder.settype(SongRecommendType.ALBUM.rawValue)
            builder.setRequestType(RequestType.RECOMMENDED.rawValue)
            self.recommendedSongsNetworkViewModel.getRecommendedSongs(data: builder)
            self.musicCollectionView.reloadData()
    }
        else if mViewModel.artistArray?.count != 0
         {
             let builder = SongAndArtistBuilder()
             builder.settype(SongRecommendType.ARTIST.rawValue)
             builder.setRequestType(RequestType.RECOMMENDED.rawValue)
             self.recommendedSongsNetworkViewModel.getRecommendedSongs(data: builder)
             self.musicCollectionView.reloadData()
     }
    }
}
