//
//  RecommendedForYouViewModel.swift
//  QtheMusic
//
//  Created by Hammad Hassan on 28/04/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import Foundation

class RecommendedForYouViewModel : BaseViewModel {
   
    var songsArray : [Song]?
    var artistArray : [Artist]?
    var albumsArray : [Album]?
    
    
    
    var recentlyPlayedCategories = [
         RecentlyPlayedMenu(isSelected: true, menuItem: "Songs"),
         RecentlyPlayedMenu(isSelected: false, menuItem: "Albums"),
         RecentlyPlayedMenu(isSelected: false, menuItem: "Artists")
     ]
    
}
