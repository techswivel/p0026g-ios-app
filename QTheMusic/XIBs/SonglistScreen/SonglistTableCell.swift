//
//  SonglistTableCell.swift
//  QtheMusic
//
//  Created by Muhammad Tanzeel Ur Rehman on 08/12/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import Foundation
import UIKit

class SonglistTableCell : UITableViewCell {
    
    @IBOutlet weak var heading: UILabel!
    @IBOutlet weak var descriptioon: UILabel!
    @IBOutlet weak var songImage: UIImageView!{
        didSet{
            songImage.roundCorner(radius: 5)
        }
    }
    @IBOutlet weak var playIconImage: UIImageView!
    @IBOutlet weak var playIconImageBackground: UIView!{
        didSet{
            playIconImageBackground.roundCorner(radius: (Float(playIconImageBackground.frame.height) / 2))
        }
    }
    
    @IBOutlet weak var premiumIconBackGround: UIView!{
        didSet{
            premiumIconBackGround.roundCorner(radius: (Float(playIconImageBackground.frame.height) / 2))
        }
    }
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    internal func showAnimation(){

        activityIndicator.isHidden = true

        
        self.isUserInteractionEnabled = false
        
        heading.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        descriptioon.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        songImage.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        activityIndicator.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        premiumIconBackGround.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        playIconImageBackground.showAnimatedGradientSkeleton(usingGradient: .init(colors: [UIColor.lightGray,UIColor.darkGray]), animation: nil, transition: .crossDissolve(0.25))
        
    }
    
    internal func hideAnimation(){
       
        activityIndicator.isHidden = false
        playIconImageBackground.isHidden = false
        premiumIconBackGround.isHidden = false
        self.isUserInteractionEnabled = true
        
        heading.hideSkeleton()
        descriptioon.hideSkeleton()
        songImage.hideSkeleton()
        activityIndicator.hideSkeleton()
        playIconImageBackground.hideSkeleton()
        premiumIconBackGround.hideSkeleton()
    }
    
    internal func setUpCell(data:Song?){
        guard let Data = data else {
            return
        }
        
        self.isUserInteractionEnabled = true
        songImage.loadImage(imageUrl: Data.coverImageURL ?? "", placeHolderName: ImageConstant.noImagePlaceHolder, activiyIndicator: activityIndicator)
        descriptioon.text = Data.songDuration ?? ""//"\(Utils.secondsToHoursMinutesSecondsString(seconds: Data.songDuration))"
        
        print("songsStatus:\(data?.songStatus)")
        heading.text = "\(Data.songTitle ?? "")"
        if data?.songStatus == SongStatus.PREMIUM.rawValue{
            playIconImageBackground.isHidden = true
            premiumIconBackGround.isHidden = false
        }else{
            playIconImageBackground.isHidden = false
            premiumIconBackGround.isHidden = true
        }
    }
    
}
