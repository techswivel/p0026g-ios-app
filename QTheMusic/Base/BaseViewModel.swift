//
//  BaseViewModel.swift
//  Base iOS Project
//
//  Created by Hammad Hassan on 09/02/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//
import Foundation
import RxSwift

class BaseViewModel: NSObject {
    
    let mDataManager:DataManager = DataManager()
   
    let disposeBag = DisposeBag()
    
    var completion : Box<ResponseMain?> = Box(nil)
    var ErrorResponse : Box<ResponseMain?> = Box(nil)
    var expireObserer : Box<ResponseMain?> = Box(nil)
    var expiredObserer : Box<MainResponse?> = Box(nil)
    
    internal func handleError<T>(data:ApiError<T>?,errorObserver:Box<T?>, expireObserver:Box<T?>){
        switch data{
        case .forbidden(let error):
            errorObserver.value = error as T
            break
        case .badRequest(let error):
            errorObserver.value = error as T
            break
        case .unAuthorized(let error):
            expireObserver.value = error as T
            break
        case .paymentIssue(let error):
            errorObserver.value = error as T
            break
        case .notFound(let error):
            errorObserver.value = error as T
            break
        case .notAllowed(let error):
            errorObserver.value = error as T
            break
        case .unProcessEntity(let error):
            errorObserver.value = error as T
            break
        case .internalServerError(let error):
            errorObserver.value = error as T
            break
        case .onSuccess(let error):
            errorObserver.value = error as T
            break
        case .defaultError(let error):
            errorObserver.value = error as T
            break
        case .none:
            errorObserver.value = nil
        }
    }
}
