//
//  BaseViewController.swift
//  QtheMusic
//
//  Created by Assad Khan on 14/03/2022.
//  Copyright © 2022 TechSwivel. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import GoogleSignIn

class BaseViewController: UIViewController {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate

    private var mViewModel = BaseViewModel()
    private var activityIndicatorView : ActivityIndicatorView!
    internal var bottomSafeAreaConstraintForVC:NSLayoutConstraint?
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNibView()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setBottomSafeAreConstraintForMiniPlayer()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
      }
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    private func setupNibView(){
        /// Activity Indicator View
        self.activityIndicatorView = UINib(nibName: UINibConstant.ActivityIndicatorNib, bundle: .main).instantiate(withOwner: nil, options: nil).first as? ActivityIndicatorView
        self.activityIndicatorView.frame = self.view.frame
        
    }
    internal func showActivityIndicator(){
        self.view.addSubview(activityIndicatorView)
    }
    //MARK: - Switched To Home Storyboard
   internal func goToMainStoryboard(){
       self.navigationController?.setViewControllers([StoryBoardConstant.mainStoryBoard.instantiateViewController(withIdentifier: StoryBoardConstant.tabbarRootVC)], animated: true)
    }
    //MARK: - Switched To AUth StoryBoard
    internal  func goToAuthStoryBoard() {

        self.navigationController?.setViewControllers([StoryBoardConstant.AuthStoryBoard.instantiateViewController(withIdentifier: StoryBoardConstant.SignInVC)], animated: true)
    }
    internal func removeActivityIndicator(){
        DispatchQueue.main.async {
            self.activityIndicatorView.removeFromSuperview()
        }
    }
    func popBackController() {
         if #available(iOS 13.0, *) {
             self.navigationController?.popViewController(animated: true)
         }else{
            self.navigationController?.popViewController(animated: true)
         }
     }
    
    func popBackControllerWithOutAnimation() {
         
        if #available(iOS 13.0, *) {
             self.navigationController?.popViewController(animated: false)
         }else{
            self.navigationController?.popViewController(animated: false)
         }
     }
    
    

    //MARK: - Perform Expiry In Case of Box Binding
   internal func performExpirationInCaseOfBoxBinding(){
    
        ///Showing Session Expire Alert
        Utils.showExpireAlert(vc: self, onOkPress: {
            self.clearApplicationSession()
        })
        self.removeActivityIndicator()
       
   }
    internal func setBottomSafeAreConstraintForMiniPlayer() {
        guard let bottomSafeAreaConstraint = bottomSafeAreaConstraintForVC else {
            return
        }

        
        let tabbar = tabBarController as? CustomTabbarController
        if tabbar?.isMiniPlayerHidden ?? false {
            bottomSafeAreaConstraint.constant = 0
            self.view.layoutIfNeeded()
        }else{
            bottomSafeAreaConstraint.constant = 65
            self.view.layoutIfNeeded()
        }
    }
    func addTransitionOnNavigationControler(){
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.fade
        self.navigationController?.view.layer.add(transition, forKey: nil)
    }
    

    //MARK: - Clear Application Session
    func clearApplicationSession() {
         
         do {
             try AppDelegate.getFirebaseAuth().signOut()
             logoutSession()
             print("all logout has been perform")
             LoginManager().logOut()
             GIDSignIn.sharedInstance().signOut()
             UserDefaultUtils.removeValueForKey(key: CommonKeys.KEY_PROFILE)
             self.goToAuthStoryBoard()
         } catch let signOutError as NSError {
             print ("Error signing out: %@", signOutError)
         }
         
     }
    
    //MARK: - Logout Session
    internal func logoutSession(){
        UserDefaultUtils.removeValueForKey(key: CommonKeys.KEY_PROFILE)
       
    }
    

    func popBackNavigationWithTransition(){
        addTransitionOnNavigationControler()
        popBackControllerWithOutAnimation()
    }

    //MARK: - Go TO Under Development VC
     func goToInterestSelectionScreen(){
         let vc = UIStoryboard.init(name: StoryBoardConstant.storyBoardAuth, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.yourInterestScreen) as? InterestSelectionViewController
         vc!.selectedState = true
         self.navigationController?.pushViewController(vc!, animated: true)
    }

    func goToUnderDevelopementScreen(){
        let vc = UIStoryboard.init(name: StoryBoardConstant.storyBoardMain, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.UnderDevelopmentVC) as? UnderDevelopmentViewController
        self.navigationController?.pushViewController(vc!, animated: true)
   }
}
