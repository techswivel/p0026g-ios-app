//
//  SceneDelegate.swift
//  Base iOS Project
//
//  Created by Hammad Hassan on 21/11/2020.
//  Copyright © 2020 TechSwivel. All rights reserved.
//

import UIKit
import Siren
import AppTrackingTransparency
import FBSDKCoreKit
import FBSDKLoginKit

@available(iOS 13.0, *)
class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?
    var authNavController : UINavigationController = UINavigationController()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        
        
        
        ///Setting up Auth storyboard's Splash screen as first view
        guard let newScene = (scene as? UIWindowScene) else { return }
        let authStoryBoard = UIStoryboard(name: StoryBoardConstant.storyBoardAuth, bundle: nil)
        let splashVC = authStoryBoard.instantiateViewController(withIdentifier: StoryBoardConstant.splashVC)
        self.appDelegate.authNavController = UINavigationController(rootViewController: splashVC)
        self.appDelegate.authNavController.setViewControllers([splashVC], animated: true)
        self.appDelegate.authNavController.isNavigationBarHidden = true
        
        self.window = UIWindow(windowScene: newScene)
        self.window?.rootViewController = self.appDelegate.authNavController
        self.window?.makeKeyAndVisible()
    
        
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
        guard let _ = (scene as? UIWindowScene) else { return }
        /// Integration of Siren to Update App on user side.
        Siren.shared.rulesManager = RulesManager(majorUpdateRules: .critical,
                                                 minorUpdateRules: .relaxed,
                                                 patchUpdateRules: .critical,
                                                 revisionUpdateRules: Rules(promptFrequency: .immediately, forAlertType: .option))
        
        Siren.shared.wail(performCheck: .onForeground) { results in
            switch results {
                
            case .success(let updateResults):
                print("AlertAction ", updateResults.alertAction)
                print("Localization ", updateResults.localization)
                print("Model ", updateResults.model)
                print("UpdateType ", updateResults.updateType)
                
            case .failure(let error):
                print("Siren: ", error.localizedDescription)
            }
            
        }
    }
    func scene(_ scene: UIScene, openURLContexts URLContexts: Set<UIOpenURLContext>) {
        guard let url = URLContexts.first?.url else {
            return
        }

        ApplicationDelegate.shared.application(
            UIApplication.shared,
            open: url,
            sourceApplication: nil,
            annotation: [UIApplication.OpenURLOptionsKey.annotation]
        )
    }
    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not neccessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
            if  self.appDelegate.application != nil {
                
            if #available(iOS 15, *){
                ATTrackingManager.requestTrackingAuthorization { status in
                    if status == .authorized {
                        
                            
                        DispatchQueue.main.async {
                            
                        
                            FBSDKCoreKit.ApplicationDelegate.shared.application(
                            self.appDelegate.application!,
                            didFinishLaunchingWithOptions: self.appDelegate.launchOptions ?? nil
                        )
                    
                    }
                    
                    }}
            }
            }

        })
        
     
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }


}

