//
//  AppDelegate.swift
//  Base iOS Project
//
//  Created by Hammad Hassan on 21/11/2020.
//  Copyright © 2020 TechSwivel. All rights reserved.
//

import UIKit
import Siren
import Firebase
import IQKeyboardManagerSwift
import AppTrackingTransparency
import FBSDKCoreKit
import GoogleSignIn
import FirebaseAuth
import FirebaseMessaging
import UserNotifications
import Foundation
import RealmSwift
import Darwin

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, MessagingDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?
    var authNavController : UINavigationController = UINavigationController()
    var launchOptions: [UIApplication.LaunchOptionsKey: Any]?
    var application:UIApplication?
    let mDataManager = DataManager()
   
   
    //MARK: - Did Finish Launching with Option
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        self.launchOptions = launchOptions
        self.application = application
        
        
        
        //MARK: - Setting Facebook SDK
            /// Checking if App Tracking is Authorized then set Facebook AP ID.
            if #available(iOS 14, *){
              if  ATTrackingManager.trackingAuthorizationStatus == .authorized {
                DispatchQueue.main.async {
                    FBSDKCoreKit.ApplicationDelegate.shared.application(
                  application,
                  didFinishLaunchingWithOptions: launchOptions
                )
                }
              }
            } /// Setting Facebook SDK for below iOS 14
            else {
              DispatchQueue.main.async {
                  FBSDKCoreKit.ApplicationDelegate.shared.application(
                application,
                didFinishLaunchingWithOptions: launchOptions
              )
              }
            }
        
        // Override point for customization after application launch.
        IQKeyboardManager.shared.enable = true
        /// Integration of Siren to Update App on user side.
        Siren.shared.rulesManager = RulesManager(majorUpdateRules: .critical,
                                                 minorUpdateRules: .relaxed,
                                                 patchUpdateRules: .critical,
                                                 revisionUpdateRules: Rules(promptFrequency: .immediately, forAlertType: .option))
        
        Siren.shared.wail(performCheck: .onForeground) { results in
            switch results {
                
            case .success(let updateResults):
                print("AlertAction ", updateResults.alertAction)
                print("Localization ", updateResults.localization)
                print("Model ", updateResults.model)
                print("UpdateType ", updateResults.updateType)
                
            case .failure(let error):
                print("Siren: ", error.localizedDescription)
            }
            
        }
        
        /// Subscribing for FireBase Messages Topics.
       
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        UNUserNotificationCenter.current().delegate = self
        Messaging.messaging().subscribe(toTopic: ProjectConstants.current.FIRE_BASE_MESSAGES_TOPIC) { error in
            print("Subscribed to \(ProjectConstants.current.FIRE_BASE_MESSAGES_TOPIC) topic")
        }
        Messaging.messaging().subscribe(toTopic: ProjectConstants.current.FREE_PLAN_USER) { error in
            print("Subscribed to \(ProjectConstants.current.FREE_PLAN_USER) topic")
        }
     
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge ]) { success, _ in
            guard success else {
                return
            }
            print("success in APNS registry")
        }
        application.registerForRemoteNotifications()
    
        ///Setting Up Google SignIn Client_ID for different Enivronments
        //setting up Stripe for Live and Test Mode.
        GIDSignIn.sharedInstance().clientID = ConfigurationManager.getGoogleClientId()
        
        
        GIDSignIn.sharedInstance().delegate = self as? GIDSignInDelegate
        
        
        
        
        
        /// Setting Up Auth StoryBoard's splash screen as First ViewController
        let authStoryBoard = UIStoryboard(name: StoryBoardConstant.storyBoardAuth, bundle: nil)
        let splashVC = authStoryBoard.instantiateViewController(withIdentifier: StoryBoardConstant.splashVC)
        self.authNavController = UINavigationController(rootViewController: splashVC)
        authNavController.isNavigationBarHidden = true
        
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = authNavController
        self.window?.makeKeyAndVisible()
        
        self.setTimerToScheduleSyncData()
        
        return true
    }
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        messaging.token { token, _ in
          guard let token = token else{
            return
          }
          print("Token: \(token)")
        }
      }
     
      func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
          print("Failed to register for remote notifications: \(error)")
      }
    //MARK: - Facebook & Google Delegate methods
    func application(
        _ app: UIApplication,
        open url: URL,
        options: [UIApplication.OpenURLOptionsKey : Any] = [:]
    ) -> Bool {
        ApplicationDelegate.shared.application(
            app,
            open: url,
            sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
            annotation: options[UIApplication.OpenURLOptionsKey.annotation]
        )
        ///google signin return.
        return GIDSignIn.sharedInstance().handle(url)
    }
    public static func getFirebaseAuth() -> Auth{
        return Auth.auth()
    }
    // MARK: UISceneSession Lifecycle

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
   
}
extension AppDelegate
{
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .sound, .badge ])
        let userInfo = notification.request.content.userInfo
        let notiData = userInfo["aps"] as! [String:Any]
        let notiData1 = notiData["alert"] as! [String:Any]
        let title = notiData1["title"] as? String ?? ""
        let body = notiData1["body"] as? String ?? ""
        let preOrderType = userInfo["type"] as? String ?? ""
        print(userInfo)
        print(preOrderType)
        if preOrderType == SubscribeToPlan.PRE_ORDER_SONG.rawValue || preOrderType == SubscribeToPlan.PRE_ORDER_ALBUM.rawValue
        {
       let notificationData = userInfo["notification"] as? String ?? ""
        if let data = notificationData.data(using: .utf8) {
            print(data)
            do {
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                let model = try decoder.decode(PreOrderModel.self, from: data)
                UserDefaults.standard.set(true, forKey: CommonKeys.IsPlayingFromNotification)
                UserDefaultUtils.setString(value : model.songAudioUrl, key: CommonKeys.AudioUrl)
                UserDefaultUtils.setString(value : model.songTitle, key: CommonKeys.SongTitle)
                UserDefaultUtils.setString(value: model.coverImageUrl, key: CommonKeys.NotImage)
                UserDefaultUtils.setString(value : model.artist, key: CommonKeys.ArtistName)
            } catch {
                print("Error decoding JSON: \(error)")
            }
    }
        }
    }
      func userNotificationCenter(_ center: UNUserNotificationCenter,
                    didReceive response: UNNotificationResponse,
                    withCompletionHandler completionHandler: @escaping () -> Void) {
          
          let userInfo = response.notification.request.content.userInfo
          let notiData = userInfo["aps"] as! [String:Any]
             let notiData1 = notiData["alert"] as! [String:Any]
            let title = notiData1["title"] as? String ?? ""
            let body = notiData1["body"] as? String ?? ""
          let preOrderType = userInfo["type"] as? String ?? ""
          print(userInfo)
          print(preOrderType)
          if preOrderType == SubscribeToPlan.PRE_ORDER_SONG.rawValue || preOrderType == SubscribeToPlan.PRE_ORDER_ALBUM.rawValue
          {
         let notificationData = userInfo["notification"] as? String ?? ""
          if let data = notificationData.data(using: .utf8) {
              print(data)
              do {
                  let decoder = JSONDecoder()
                  decoder.keyDecodingStrategy = .convertFromSnakeCase
                  let model = try decoder.decode(PreOrderModel.self, from: data)
                  UserDefaults.standard.set(true, forKey: CommonKeys.IsPlayingFromNotification)
                  UserDefaultUtils.setString(value : model.songAudioUrl, key: CommonKeys.AudioUrl)
                  UserDefaultUtils.setString(value : model.songTitle, key: CommonKeys.SongTitle)
                  UserDefaultUtils.setString(value: model.coverImageUrl, key: CommonKeys.NotImage)
                  UserDefaultUtils.setString(value : model.artist, key: CommonKeys.ArtistName)
              } catch {
                  print("Error decoding JSON: \(error)")
              }
      }
          let conversationVC = (UIStoryboard.init(name: StoryBoardConstant.storyBoardMain, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardConstant.SongsDetailViewController) as? SongsDetailViewController) ?? UIViewController()
          self.authNavController.pushViewController(conversationVC, animated: true)

          
                 
      }
   

}
}
//MARK: -  Update Sync Data With Schedule Time
extension AppDelegate {
    
    private func setTimerToScheduleSyncData() {
        Timer.scheduledTimer(timeInterval: ProjectConstants.current.getTimerValue().0, target: self, selector: #selector(self.UpdateSyncData), userInfo: nil, repeats: true)
    }

    @objc func UpdateSyncData(){
      
        let auth = UserDefaultUtils.getProfile(key: CommonKeys.KEY_PROFILE)
        if auth != nil{
        let threadQueue = DispatchQueue(label: "work-queue", qos: .background)
        threadQueue.async {
            
        if LandscapeManager.shared.isFirstFetch {
            
            UserDefaultUtils.saveSyncTime(value: Date(), key: CommonKeys.LAST_SYNC_TIME)
            
            LandscapeManager.shared.isFirstFetch = true
        }
        
            guard let fetchTime = UserDefaultUtils.getSyncTime(key: CommonKeys.LAST_SYNC_TIME) else {return}
               
        let currentDateTime = Date()

            if fetchTime + ProjectConstants.current.getTimerValue().1  < currentDateTime {
            print("called sync api")
            self.mDataManager.syncRecentlyPlayedData()
            UserDefaultUtils.setString(value: StringConstant.running, key: CommonKeys.SYNC_STATUS)
            
            UserDefaultUtils.saveSyncTime(value: currentDateTime, key: CommonKeys.LAST_SYNC_TIME)
            
           
         
        } else {
            UserDefaultUtils.setString(value: StringConstant.enqueued, key: CommonKeys.SYNC_STATUS)
            print("stil time not came")
        }
            
        }
        
    }
    }
}
